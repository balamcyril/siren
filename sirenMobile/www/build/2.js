webpackJsonp([2],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SynchronisationPageModule", function() { return SynchronisationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__synchronisation__ = __webpack_require__(487);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SynchronisationPageModule = /** @class */ (function () {
    function SynchronisationPageModule() {
    }
    SynchronisationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__synchronisation__["a" /* SynchronisationPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__synchronisation__["a" /* SynchronisationPage */]),
            ],
        })
    ], SynchronisationPageModule);
    return SynchronisationPageModule;
}());

//# sourceMappingURL=synchronisation.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 487:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SynchronisationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the SynchronisationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SynchronisationPage = /** @class */ (function () {
    function SynchronisationPage(translate, geolocation, navCtrl, toastCtrl, mylocalstorage, loadingCtrl, navParams, services) {
        this.translate = translate;
        this.geolocation = geolocation;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.mylocalstorage = mylocalstorage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.userconnecte = {};
        this.total = 0;
        this.totaleffectue = 0;
    }
    SynchronisationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.total = 9;
        this.totaleffectue = 0;
        if (navigator.onLine) {
            var etat = 0;
            this.services.getAllObservationByEtat(etat).then(function (resultat) {
                _this.listedesobservations = resultat;
                var compteur = 0;
                if (_this.listedesobservations.length != 0) {
                    var _loop_1 = function (index) {
                        _this.services.saveObservation(_this.listedesobservations[index]).subscribe(function (result) {
                            _this.services.updateObservation(_this.listedesobservations[index].id).then(function (resultat) {
                                compteur++;
                                _this.services.findresultatByObservationId(_this.listedesobservations[index].id).then(function (resultat2) {
                                    ////////////////////// logiquement ici je vais itérer sur la liste des reponses
                                    if (resultat2 != 0) {
                                        var listeresultats;
                                        listeresultats = resultat2;
                                        var k = 0;
                                        listeresultats.forEach(function (element) {
                                            element.observation = result;
                                            _this.services.saveresulatat(element).subscribe(function (result) {
                                                k++;
                                                if (k == listeresultats.length) {
                                                    if (compteur == _this.listedesobservations.length) {
                                                        _this.Synchronisateur();
                                                    }
                                                }
                                            }, function (error) {
                                                alert("one error was done " + JSON.stringify(error));
                                            }, function () {
                                            });
                                        });
                                    }
                                    else {
                                        if (compteur == _this.listedesobservations.length) {
                                            _this.Synchronisateur();
                                        }
                                    }
                                });
                                ///////////////////////////////////////////////////////////////////
                            });
                        }, function (error) {
                            alert("erreur de sauvegarde de observation " + JSON.stringify(error));
                        }, function () {
                        });
                    };
                    for (var index = 0; index < _this.listedesobservations.length; index++) {
                        _loop_1(index);
                    }
                }
                else {
                    _this.Synchronisateur();
                }
            });
        }
        else {
            this.translate.get(['INFOSCONNEXION']).subscribe(function (langs) {
                _this.presentToast(langs['INFOSCONNEXION']);
                _this.navCtrl.setRoot('AcceuilPage');
            });
        }
    };
    SynchronisationPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 5000,
        });
        toast.present();
    };
    SynchronisationPage.prototype.Synchronisateur = function () {
        var _this = this;
        this.services.deletteAllPays().then(function (result1) {
            _this.services.getpays().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createPays(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllFonctions().then(function (result1) {
            _this.services.getfonctions().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createFonction(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllGroupes().then(function (result1) {
            _this.services.groupes().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createGroupe(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllSousGroupes().then(function (result1) {
            _this.services.allsousgroupes().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    var sous_groupe = {
                        "id": element.id,
                        "nom_fr": element.nom_fr,
                        "nom_en": element.nom_en,
                        "image": element.image,
                        "description_fr": element.description_fr,
                        "description_en": element.description_en,
                        "groupes": element.groupes.id
                    };
                    _this.services.createSousGroupe(sous_groupe).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert("uen erreur au rendez-vous c quoi le pb sousgroupe");
                alert(JSON.stringify(error));
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllReponses().then(function (result1) {
            _this.services.allreponses().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    if (undefined != element.questions_next) {
                        var reponse = {};
                        reponse = {
                            "id": element.id,
                            "titre_fr": element.titre_fr,
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "questions_next": element["questions_next"].id,
                            "image": element.image,
                            "taille": element.taille
                        };
                        _this.services.createReponse(reponse).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    }
                    else {
                        var reponse = {};
                        reponse = {
                            "id": element.id,
                            "titre_fr": element.titre_fr,
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "image": element.image,
                            "taille": element.taille
                        };
                        _this.services.createReponse(reponse).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    }
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAlltypeObservation().then(function (result1) {
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                //this.services.acceuil().subscribe((result) =>{ 
                _this.services.projet_typeObservation(_this.userconnecte["projet"].id).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        _this.services.createTypeObservation(element).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                    console.log(error);
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllProjets().then(function (result1) {
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.services.findProjetsByUserId(_this.userconnecte["id"]).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        var projet = {
                            "id": element.id,
                            "nom": element.nom,
                            "rowid": element.id,
                            "lieu": element.lieu,
                            "public": element.public,
                            "pays": element["pays"].id,
                            "utilisateurs": element['utilisateurs'].id,
                            "note": element.note,
                            "logo": element.logo,
                            "mention": element.mention
                        };
                        _this.services.createProjet(projet).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllProjetMenu().then(function (result1) {
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.projetId = _this.userconnecte["projet"].id;
                _this.services.getprojet_typeObservation(_this.projetId).subscribe(function (result) {
                    var i = 0;
                    //this.services.setCurrentMenuProjet(result);
                    result.forEach(function (element) {
                        var projet = {
                            "id": element.id,
                            "nomFr": element.nomFr,
                            "nomEn": element.nomEn,
                            "title": element.title
                        };
                        _this.services.createProjetMenu(projet).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllEspeces().then(function (result1) {
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.projetId = _this.userconnecte["projet"].id;
                _this.services.findEspecesByProjetId(_this.projetId).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        var espece = {
                            "id": element.id,
                            "nom_fr": element.nom_fr,
                            "nom_en": element.nom_en,
                            "image": element.image,
                            "description_fr": element.description_fr,
                            "description_en": element.description_en,
                            "sous_groupes": element["sous_groupes"].id,
                            "questions_animal": element["questions_animal"].id,
                            "questions_menaces": element["questions_menances"].id,
                            "questions_signe": element["questions_signe"].id,
                            "questions_alimentation": element["questions_alimentation"].id
                        };
                        _this.services.createEspece(espece).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                    alert("une erreur au rendez-vous c quoi le pb");
                    alert(JSON.stringify(error));
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllUtilisateur().then(function (result) {
            _this.mylocalstorage.getSession().then(function (result2) {
                _this.userconnecte = result2;
                _this.projetId = _this.userconnecte["projet"].id;
                var utilisateurs = {
                    "id": _this.userconnecte["id"],
                    "username": _this.userconnecte["email"],
                    "email": _this.userconnecte["email"],
                    "password": _this.userconnecte["password"],
                    "ville": _this.userconnecte["ville"],
                    "telephone": _this.userconnecte["telephone"],
                    "pays": _this.userconnecte["pays"].id,
                    "fonctions": _this.userconnecte["fonctions"].id,
                    "projet": _this.userconnecte["projet"].id,
                    "monuserid": _this.userconnecte["id"]
                };
                _this.services.createUtilisateur(utilisateurs).then(function (result1) {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllInscription().then(function (result) {
            _this.mylocalstorage.getSession().then(function (result2) {
                _this.userconnecte = result2;
                _this.projetId = _this.userconnecte["projet"].id;
                var utilisateurs = {
                    "id": _this.userconnecte["id"],
                    "username": _this.userconnecte["email"],
                    "email": _this.userconnecte["email"],
                    "password": _this.userconnecte["password"],
                    "ville": _this.userconnecte["ville"],
                    "telephone": _this.userconnecte["telephone"],
                    "pays": _this.userconnecte["pays"].id,
                    "fonctions": _this.userconnecte["fonctions"].id,
                    "projet": _this.userconnecte["projet"].id,
                    "monuserid": _this.userconnecte["id"]
                };
                _this.services.createInscription(utilisateurs).then(function (result1) {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllQuestions().then(function (result1) {
            _this.services.allquestions().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    var myquestionnext;
                    if (element["questions"] == undefined) {
                        myquestionnext = "null";
                    }
                    else {
                        myquestionnext = element["questions"].id;
                    }
                    var question = {
                        "id": element.id,
                        "titre_fr": element.titre_fr,
                        "titre_en": element.titre_en,
                        "type_reponse": element.type_reponse,
                        "interval": element.interval,
                        "type_debut": element.type_debut,
                        "image": element.image,
                        "isPicture": element.isPicture,
                        "questions": myquestionnext
                    };
                    _this.services.createQuestion(question).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
    };
    SynchronisationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-synchronisation',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/synchronisation/synchronisation.html"*/'\n\n<ion-header>\n    <navbar [title]= "\'Synchronisation\'" ></navbar>\n</ion-header>\n\n\n<ion-content class="backgroundcorps">\n\n    <p class="textesacceuil">{{ \'INFOSCRO\' | translate }}</p>\n    <ion-col>\n        <img class="moimage"  src="assets/imgs/loading.gif">\n    </ion-col>\n    <p class="pourcentage"> {{totaleffectue}} sur {{total}}</p>\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/synchronisation/synchronisation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_services_services__["a" /* ServicesProvider */]])
    ], SynchronisationPage);
    return SynchronisationPage;
}());

//# sourceMappingURL=synchronisation.js.map

/***/ })

});
//# sourceMappingURL=2.js.map