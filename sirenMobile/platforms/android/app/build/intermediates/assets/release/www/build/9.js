webpackJsonp([9],{

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MesprojetPageModule", function() { return MesprojetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mesprojet__ = __webpack_require__(478);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MesprojetPageModule = /** @class */ (function () {
    function MesprojetPageModule() {
    }
    MesprojetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__mesprojet__["a" /* MesprojetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mesprojet__["a" /* MesprojetPage */]),
            ],
        })
    ], MesprojetPageModule);
    return MesprojetPageModule;
}());

//# sourceMappingURL=mesprojet.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 478:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MesprojetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__configs_configs__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MesprojetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MesprojetPage = /** @class */ (function () {
    function MesprojetPage(navCtrl, geolocation, mylocalstorage, loadingCtrl, navParams, services) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.mylocalstorage = mylocalstorage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.userconnecte = {};
        this.observationchoisie = {};
        this.montypeObservations = "";
        this.monsousgroupe = "";
        this.mongroupe = "";
        this.monespece = "";
        this.testeur = 0;
        this.montesteur = 0;
        this.detail = 0;
        this.listequestions = [];
    }
    MesprojetPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.testeur = 0;
        this.detail = 0;
        this.mylocalstorage.getSession().then(function (result) {
            if (navigator.onLine) {
                _this.userconnecte = result;
                var loading_1 = _this.loadingCtrl.create();
                loading_1.present();
                _this.services.findProjetsByUserId(_this.userconnecte["id"]).subscribe(function (resultat) {
                    _this.mesprojets = resultat;
                }, function (error) {
                    console.log(error);
                    //votre opération a  echoué!!
                    loading_1.dismiss();
                }, function () {
                    loading_1.dismiss();
                });
            }
            else {
                //this.userconnecte = result;
                //this.services.getAllProjetsByUserId(this.userconnecte["id"]).then((resultat) =>{
                _this.services.getAllProjets().then(function (resultat) {
                    _this.mesprojets = resultat;
                });
            }
        });
    };
    MesprojetPage.prototype.findObservationByProjet = function (id) {
        var _this = this;
        this.mylocalstorage.getSession().then(function (result) {
            if (navigator.onLine) {
                _this.testeur = 1;
                _this.userconnecte = result;
                _this.services.getAllObservationByUserIdAndProjet(_this.userconnecte["id"], id).then(function (resultat) {
                    _this.mesobservations = resultat;
                });
            }
            else {
                _this.testeur = 2;
                _this.userconnecte = result;
                _this.services.getAllObservationByUserIdAndProjet(_this.userconnecte["id"], id).then(function (resultat) {
                    _this.mesobservations = resultat;
                });
            }
        });
    };
    MesprojetPage.prototype.retours = function () {
        this.testeur = 0;
        this.detail = 0;
        this.observationchoisie = {};
        this.montypeObservations = "";
        this.monsousgroupe = "";
        this.mongroupe = "";
        this.monespece = "";
        this.testeur = 0;
        this.detail = 0;
        this.listequestions = [];
    };
    MesprojetPage.prototype.details = function (element) {
        var _this = this;
        this.detail = 1;
        this.testeur = 4;
        this.observationchoisie = element;
        this.services.getAllTypeObsevationsById(element.typeObservations).then(function (resultat) {
            _this.montypeObservations = resultat[0].nom_fr;
        });
        this.services.getAllSousGroupesById(element.sousgroupe).then(function (resultat) {
            if (resultat.length > 0) {
                _this.monsousgroupe = resultat[0].nom_fr;
                _this.montesteur = 1;
            }
            else {
                _this.montesteur = 0;
            }
        });
        this.services.getAllGroupesById(element.groupe).then(function (resultat) {
            if (resultat.length > 0) {
                _this.mongroupe = resultat[0].nom_fr;
                _this.montesteur = 1;
            }
            else {
                _this.montesteur = 0;
            }
        });
        this.services.getAllEspecesById(element.espece).then(function (resultat) {
            if (resultat.length > 0) {
                _this.monespece = resultat[0].nom_fr;
                _this.montesteur = 1;
            }
            else {
                _this.montesteur = 0;
            }
        });
        this.services.getAllReultatsByObservationId(element.id).then(function (resultat) {
            resultat.forEach(function (element) {
                _this.services.getQuestionById(element.questions).then(function (resultat) {
                    var data = {
                        contenu: element.contenu,
                        questions: resultat[0].titre_fr
                    };
                    _this.listequestions.push(data);
                });
            });
        });
    };
    MesprojetPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    MesprojetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mesprojet',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/mesprojet/mesprojet.html"*/'<ion-header>\n  <navbar [title]= "\'Mes projets\'" ></navbar>\n</ion-header>\n\n\n<ion-content class="backgroundcorps">\n\n\n\n  <ion-list  *ngIf="testeur == 0">                                                                                               \n       <ion-card (click)="findObservationByProjet(element.id)" class="observation" *ngFor="let element of mesprojets">\n          \n          <ion-row>\n               <ion-col col-9>\n                   <p class="pnom"> {{element.nom}}</p>\n                   <p class="plieu">{{element.lieu}}</p>\n               </ion-col>\n               <ion-col col-3>\n                <button class="monbuttoncheck">\n                   <ion-icon name="checkmark"></ion-icon>\n                </button>\n               </ion-col>\n          </ion-row>\n      \n          \n       </ion-card>\n  </ion-list>\n\n\n  <ion-list *ngIf="testeur == 1">                                                                                               \n      <ion-card class="observation" *ngFor="let element of mesobservations">\n         \n         <ion-row (click)="details(element)">\n              <ion-col col-9>\n                <p class="pnom">{{element.dateo | date: \'short\'}} </p>\n                  <p class="plieu">{{element.note}}</p>\n              </ion-col>\n              <ion-col  col-3>\n               <button class="monbuttoncheck" style="color: #1b9eea;">\n                 <ion-icon name="checkmark"></ion-icon>  \n               </button>\n               \n              </ion-col>\n         </ion-row>\n     \n         \n      </ion-card>\n </ion-list>\n\n\n <ion-list *ngIf="testeur == 2">                                                                                               \n      <ion-card class="observation" *ngFor="let element of mesobservations">\n      \n      <ion-row (click)="details(element)">\n              <ion-col col-9>\n              <p class="pnom">{{element.dateo | date: \'short\'}} </p>\n                  <p class="plieu">{{element.note}}</p>\n              </ion-col>\n              <ion-col col-3 >\n              <button *ngIf="element.etat == 1" class="monbuttoncheck" style="color: #1b9eea;">\n              <ion-icon name="checkmark"></ion-icon>  \n              </button>\n              <button *ngIf="element.etat == 0" class="monbuttoncheck" style="color: red;">\n              <ion-icon  name="md-close-circle"></ion-icon>  \n              </button>\n              </ion-col>\n      </ion-row>\n      \n      </ion-card>\n  </ion-list>\n\n  <div *ngIf="detail != 0">\n\n\n          <p class="titre">{{ \'RECAPITULATIF\' | translate }}</p>\n\n          <br>\n         \n      \n          <ion-row>\n             <ion-col class="dateheure2">\n                Date et heure :\n             </ion-col>\n             <ion-col class="dateheure3">\n              {{observationchoisie.dateo | date: \'short\'}} \n             </ion-col>\n          </ion-row>\n      \n          <ion-row>\n              <ion-col class="dateheure2">\n                Localisation :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{observationchoisie.coordX}}  | {{observationchoisie.coordY}}\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col class="dateheure2">\n                Type d\'observation :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{montypeObservations}}\n              </ion-col>\n          </ion-row>\n          <ion-row *ngIf="montesteur == 1">\n              <ion-col class="dateheure2">\n                Groupe :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{mongroupe}}\n              </ion-col>\n          </ion-row>\n          <ion-row *ngIf="montesteur == 1">\n              <ion-col class="dateheure2">\n                Sous-groupe :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{monsousgroupe}}\n              </ion-col>\n          </ion-row>\n          <ion-row *ngIf="montesteur == 1">\n              <ion-col class="dateheure2">\n                Espèce :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{monespece}}\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col class="dateheure2">\n                Description :\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{observationchoisie.note}}\n              </ion-col>\n          </ion-row>\n      \n          <!--<ion-row *ngIf="question!=0">\n              <ion-col class="dateheure2">\n                {{question}}\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{reponse}}\n              </ion-col>\n          </ion-row>-->\n      \n          <ion-row *ngFor="let item of listequestions">\n              <ion-col class="dateheure2">\n                {{item.questions}}\n              </ion-col>\n              <ion-col class="dateheure3">\n                {{item.contenu}}\n              </ion-col>\n          </ion-row>\n      \n      \n        \n  </div>\n\n\n  <button *ngIf="detail != 0" class="monbutonvalide" (click)="retours()" ion-fab right bottom  color="positive"> <ion-icon name="md-checkmark"></ion-icon> </button>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/mesprojet/mesprojet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], MesprojetPage);
    return MesprojetPage;
}());

//# sourceMappingURL=mesprojet.js.map

/***/ })

});
//# sourceMappingURL=9.js.map