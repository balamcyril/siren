import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { currentHost } from '../../host/host';
import { Http } from '@angular/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServicesProvider {

  host: any;
  headers: any;
  myheaders: any;

  

 

  constructor(public http: HttpClient, public toastCtrl: ToastController, public https: Http, private sqlite: SQLite) {

      this.initHeaders().then(next => {
          this.headers = next;
      });

      this.initHeaders2().then(next => {
        this.myheaders = next;
    });
    this.host = currentHost;
  }


  

  initHeaders() {
    return new Promise((resolve, reject) => {
        resolve({
              headers: new HttpHeaders({
            })
        });
    });
  }



  initHeaders2() {
    
    return new Promise((resolve, reject) => {
        resolve({
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin':'*'
            })
        });
    });

  }

  /**
   * Retourne les détails sur un article
   * @returns {Observable<any>}
   */
  login(utilisateur: any): Observable<any> {
    return this.http.get(
      this.host + 'connexion/' + utilisateur.login +'/' + utilisateur.password,
      this.headers
    );
  }



  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  acceuil(): Observable<any> {
    return this.http.get(
      this.host + 'typeobservations',
      this.headers
    );
  }

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  projet_typeObservation(projetId): Observable<any> {
    return this.http.get(
      this.host + 'typeobservations/' + projetId,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  allquestions(): Observable<any> {
    return this.http.get(
      this.host + 'questions',
      this.headers
    );
  }



  


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  groupes(): Observable<any> {
    return this.http.get(
      this.host + 'groupes',
      this.headers
    );
  }
 

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  sousgroupes(id): Observable<any> {
    return this.http.get(
      this.host + 'sousgroupesgroupes/' + id,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  allsousgroupes(): Observable<any> {
    return this.http.get(
      this.host + 'sousgroupes',
      this.headers
    );
  }


  /*private currentMenusProjet = new BehaviorSubject<any>(null);
  public currentMenusListner = this.currentMenusProjet.asObservable();
  setCurrentMenuProjet(listemenus){
    this.currentMenusProjet.next(listemenus);
  }*/

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  allreponses(): Observable<any> {
    return this.http.get(
      this.host + 'reponses',
      this.headers
    );
  }




  


   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  especeBySousGroupe(projetId, especeId): Observable<any> {
    return this.http.get(
      this.host + 'especessousgroupes/' + projetId + '/' + especeId,
      this.headers
    );
  }

   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findQuestionById(questionId): Observable<any> {
    return this.http.get(
      this.host + 'questionsid/' + questionId,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findReponseByQuestionId(questionId): Observable<any> {
    return this.http.get(
      this.host + 'reponsesquestions/' + questionId,
      this.headers
    );
  }

  //http://siren.ammco.org/web/fr/api/reponsesquestions/230

  /**
   * créer une obseravtion
   * @param 
   * @returns {Observable<any>}
   */
  saveObservation(observation): Observable<any> {
    return this.http.post(
      this.host + 'observation',
      observation,
      this.myheaders
    );
  }

  

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findProjetsByUserId(utilisateurid): Observable<any> {
    return this.http.get(
      this.host + 'projetutilisateur/' + utilisateurid,
      this.headers
    );
  }

  

   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findAllProjets(): Observable<any> {
    return this.http.get(
      this.host + 'projetutilisateur',
      this.headers
    );
  }

  


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findObervationByProjetId(utilisateurID, projetID): Observable<any> {
    return this.http.get(
      this.host + 'observationsuser/' + utilisateurID +'/' + projetID,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  /*saveresulatat(resultat): Observable<any> {
    return this.http.get(
      this.host + 'resultats/' + resultat.contenu + '/' + resultat.questions + '/' + resultat.observations,
      this.headers
    );
  }*/

  saveresulatat(resultat): Observable<any> {
    
    return this.http.post(
      this.host + 'resultats',
      resultat,
      this.myheaders
    );
  }



   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  savepositions(latitude, longitude, utilisateurId, projetId): Observable<any> {
    return this.http.get(
      this.host + 'position/' + latitude +'/' + longitude +'/' + utilisateurId +'/' + projetId,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findEspecesByProjetId(projetId): Observable<any> {
    return this.http.get(
      this.host + 'especes/' + projetId,
      this.headers
    );
  }


  



  

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  changerProjet(utilisateurid, projetId): Observable<any> {
    return this.http.get(
      this.host + 'changerprojet/' + utilisateurid + '/' + projetId,
      this.headers
    );
  }

  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  getprojet_typeObservation(projetId): Observable<any> {
    console.log(this.host + 'menu_projet/' + projetId);
    return this.http.get(
      this.host + 'menu_projet/' + projetId,
      this.headers
    );
  }




   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findObservationByUserId(utilisateurid, projetId): Observable<any> {
    return this.http.get(
      this.host + 'observationsuser/' + utilisateurid + '/' + projetId,
      this.headers
    );
  }


   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  findNotificationByUserId(utilisateurid): Observable<any> {
    return this.http.get(
      this.host + 'notificationsprojet/' + utilisateurid,
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  saveCommentaire(useremail, values): Observable<any> {
    return this.http.get(
      this.host + 'feedback/' + useremail + '/' + useremail + '/' + values,
      this.headers
    );
  }


  

   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  getpays(): Observable<any> {
    return this.http.get(
      this.host + 'pays',
      this.headers
    );
  }


   /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  getfonctions(): Observable<any> {
    return this.http.get(
      this.host + 'fonctions',
      this.headers
    );
  }


  /**
   * Retourne la liste des questions
   * @returns {Observable<any>}
   */
  saveUsers(utilisateur): Observable<any> {
    return this.http.get(
      this.host +  'inscription/'+ utilisateur.email +'/' + utilisateur.email + '/' + utilisateur.telephone + '/' + utilisateur.password + '/' + utilisateur.ville + '/' + utilisateur.pays + '/' + utilisateur.fonction
      ,
      this.headers
    );
  }






  ///////////////////////////////////////////////////////////////

         ///// base de données local ///////////
      
  //////////////////////////////////////////////////////////////



  createPays(pays) {

    return new Promise((resolve)=>{
            this.sqlite.create({
              name: 'sirendb.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              db.executeSql('INSERT INTO PAYS VALUES(?,?,?,?,?)',[pays.id, pays.nom_fr, pays.nom_en, pays.code_tel, pays.sigle])
                .then(res => {
                     
                    resolve(res);
                })
                .catch(e => {
                  //this.presentToast('table pays donst exist!');
                  resolve(e);
                });
            }).catch(e => {
                  //this.presentToast('database donst exist!');
                  resolve(e);
            });
      });
  }


  getAllPays(){

    return new Promise((resolve)=>{

      this.sqlite.create({
        name: 'sirendb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

            db.executeSql('SELECT * FROM PAYS ', {})
            .then(res => {
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id,nom_fr:res.rows.item(i).nom_fr,nom_en:res.rows.item(i).nom_en,code_tel:res.rows.item(i).code_tel,sigle:res.rows.item(i).sigle})
              } 
              resolve(expenses);
            })
            .catch(e => {
              console.log(e);
              resolve(0);
            });


      }).catch(e => {
        //this.presentToast('database donst exist!');
      });
          
    });
          


  }


  deletteAllPays(){
    return new Promise((resolve)=>{
      this.sqlite.create({
        name: 'sirendb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
            db.executeSql('DELETE FROM PAYS', {})
            .then(res => {
              resolve(1);
            })
            .catch(e => {
              console.log(e);
              resolve(0);
            });
      }).catch(e => {
        //this.presentToast('database donst exist!');
      });   
    });
  }

////////////////////////////////////////////////////////////////////////

createCheckConnexion(checkconnexion) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO CHECKCONNEXION VALUES(NULL,?)',[checkconnexion.valeur])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table CHECKCONNEXION donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}

getAllCheckConnexion(){

  return new Promise((resolve)=>{

    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM CHECKCONNEXION ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, valeur:res.rows.item(i).valeur})
            } 

            if(expenses.length != 0){
              resolve(1);
            }else{
               resolve(0);
            }
            
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}

deletteAllcheckconnexion(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM CHECKCONNEXION', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createFonction(fonction) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO FONCTIONS VALUES(?,?,?)',[fonction.id, fonction.nom_fr, fonction.nom_en])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table FONCTIONS donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllFonction(){

  return new Promise((resolve)=>{

    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM FONCTIONS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}

deletteAllFonctions(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM FONCTIONS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
   });
}

////////////////////////////////////////////////////////////////////////



createProjetMenu(projetmenu) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO PROJETMENU VALUES(?,?,?,?)',[projetmenu.id, projetmenu.nomFr, projetmenu.nomEn, projetmenu.title])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table PROJETMENU donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllProjetMenu(){

  return new Promise((resolve)=>{

    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJETMENU ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nomFr:res.rows.item(i).nomFr, nomEn:res.rows.item(i).nomEn, title:res.rows.item(i).title})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllProjetMenu(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM PROJETMENU', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
   });
}


////////////////////////////////////////////////////////////////////////

createInscription(utilisateur) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO INSCRIPTION VALUES(NULL,?,?,?,?,?,?,?,?)',[utilisateur.username, utilisateur.email, utilisateur.telephone, utilisateur.password, utilisateur.ville, utilisateur.pays, utilisateur.fonction, utilisateur.monuserid])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table INSCRIPTION donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllInscription(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM INSCRIPTION ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, username:res.rows.item(i).username, email:res.rows.item(i).email, telephone:res.rows.item(i).telephone, password:res.rows.item(i).password, ville:res.rows.item(i).ville, pays:res.rows.item(i).pays, fonction:res.rows.item(i).fonction, monuserid:res.rows.item(i).monuserid})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllInscription(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM INSCRIPTION', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createGroupe(groupe) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO GROUPES VALUES(?,?,?,?,?,?)',[groupe.id, groupe.nom_fr, groupe.nom_en, groupe.image, groupe.description_fr, groupe.description_en])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table GROUPES donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}

getAllGroupes(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM GROUPES ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllGroupesById(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM GROUPES WHERE id = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllGroupes(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM GROUPES', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}


////////////////////////////////////////////////////////////////////////

createSousGroupe(sousgroupe) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO SOUS_GROUPES VALUES(?,?,?,?,?,?,?)',[sousgroupe.id,sousgroupe.nom_fr, sousgroupe.nom_en, sousgroupe.image, sousgroupe.description_fr, sousgroupe.description_en, sousgroupe.groupes])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table SOUS_GROUPES donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}

getAllSousGroupes(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM SOUS_GROUPES ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, groupes:res.rows.item(i).groupes})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllSousGroupesById(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM SOUS_GROUPES WHERE id = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, groupes:res.rows.item(i).groupes})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}




deletteAllSousGroupes(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM SOUS_GROUPES', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createQuestion(question) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            
            db.executeSql('INSERT INTO QUESTIONS VALUES(?,?,?,?,?,?,?,?,?)',[question.id, question.titre_fr, question.titre_en, question.type_reponse, question.interval, question.type_debut, question.image, question.isPicture, question.questions])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table QUESTIONS donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}

getAllQuestions(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM QUESTIONS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, titre_fr:res.rows.item(i).titre_fr, titre_en:res.rows.item(i).titre_en, type_reponse:res.rows.item(i).type_reponse, interval:res.rows.item(i).interval, type_debut:res.rows.item(i).type_debut, image:res.rows.item(i).image, isPicture:res.rows.item(i).isPicture, questions:res.rows.item(i).questions})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}

deletteAllQuestions(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM QUESTIONS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}



////////////////////////////////////////////////////////////////////////

createReponse(reponse) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO REPONSES VALUES(?,?,?,?,?,?,?)',[reponse.id, reponse.titre_fr, reponse.titre_en, reponse.questions, reponse.questions_next, reponse.image, reponse.taille])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table REPONSES donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllReponses(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM REPONSES ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, titre_fr:res.rows.item(i).titre_fr, titre_en:res.rows.item(i).titre_en, question:res.rows.item(i).question, questions_next:res.rows.item(i).questions_next, image:res.rows.item(i).image, taille:res.rows.item(i).taille})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllReponses(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM REPONSES', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createProjetTypeObservation(projettypeobservation) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {


            db.executeSql('INSERT INTO PROJET_TYPE_OBSERVATIONS VALUES(?,?,?,?,?,?,?)',[projettypeobservation.id, projettypeobservation.nom_fr, projettypeobservation.nom_en, projettypeobservation.image, projettypeobservation.type_question, projettypeobservation.questions.id,projettypeobservation.projet.id])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table PROJET_TYPE_OBSERVATIONS donst exist!');
                resolve(e);
              });
          }).catch(e => {
               // this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllProjetTypeObsevations(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, type_question:res.rows.item(i).type_question, questions:res.rows.item(i).questions, projet:res.rows.item(i).projet})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllProjetTypeObsevationsById(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS WHERE id = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, type_question:res.rows.item(i).type_question, questions:res.rows.item(i).questions, projet:res.rows.item(i).projet})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllProjetTypeObsevationsByProjetId(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS WHERE projet = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, type_question:res.rows.item(i).type_question, questions:res.rows.item(i).questions, projet:res.rows.item(i).projet})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllProjettypeObservation(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM TYPE_OBSERVATIONS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
     // this.presentToast('database donst exist!');
    });   
  });
}




createTypeObservation(typeobservation) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {


            db.executeSql('INSERT INTO TYPE_OBSERVATIONS VALUES(?,?,?,?,?,?)',[typeobservation.id, typeobservation.nom_fr, typeobservation.nom_en, typeobservation.image, typeobservation.type_question, typeobservation.questions.id])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table TYPE_OBSERVATIONS donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}

getAllTypeObsevations(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM TYPE_OBSERVATIONS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, type_question:res.rows.item(i).type_question, questions:res.rows.item(i).questions})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}



getAllTypeObsevationsById(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM TYPE_OBSERVATIONS WHERE id = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, type_question:res.rows.item(i).type_question, questions:res.rows.item(i).questions})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAlltypeObservation(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM TYPE_OBSERVATIONS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createEspece(espece) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO ESPECES VALUES(?,?,?,?,?,?,?,?,?,?,?)',[espece.id, espece.nom_fr, espece.nom_en, espece.image, espece.description_fr, espece.description_en, espece.sous_groupes, espece.questions_animal, espece.questions_menaces, espece.questions_signe, espece.questions_alimentation])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table ESPECES donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllEspeces(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM ESPECES ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, sous_groupes:res.rows.item(i).sous_groupes, questions_animal:res.rows.item(i).questions_animal, questions_menaces:res.rows.item(i).questions_menaces, questions_signe:res.rows.item(i).questions_signe, questions_alimentation:res.rows.item(i).questions_alimentation})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllEspecesById(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM ESPECES WHERE id = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, sous_groupes:res.rows.item(i).sous_groupes, questions_animal:res.rows.item(i).questions_animal, questions_menaces:res.rows.item(i).questions_menaces, questions_signe:res.rows.item(i).questions_signe, questions_alimentation:res.rows.item(i).questions_alimentation})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}






deletteAllEspeces(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM ESPECES', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

////////////////////////////////////////////////////////////////////////

createProjet(projet) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO PROJET VALUES(?,?,?,?,?,?,?,?,?,?)',[projet.id ,projet.nom, projet.lieu, projet.public, projet.pays, projet.utilisateurs, projet.rowid, projet.logo, projet.mention, projet.note])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
                //this.presentToast('table PROJET donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllProjets(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJET ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, rowid:res.rows.item(i).rowid,logo:res.rows.item(i).logo, note:res.rows.item(i).note, mention:res.rows.item(i).mention, nom:res.rows.item(i).nom, lieu:res.rows.item(i).lieu, public:res.rows.item(i).public, pays:res.rows.item(i).pays, utilisateurs:res.rows.item(i).utilisateurs})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}

deletteAllProjets(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM PROJET', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}


////////////////////////////////////////////////////////////////////////

createUtilisateur(utilisateur) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO UTILISATEURS VALUES(NULL,?,?,?,?,?,?,?,?,?)',[utilisateur.username, utilisateur.email, utilisateur.password, utilisateur.ville, utilisateur.telephone, utilisateur.pays, utilisateur.fonctions, utilisateur.projet, utilisateur.monuserid])
              .then(res => {
                  resolve(res);
              })
              .catch(e => {
               // this.presentToast('table UTILISATEURS donst exist!');
                resolve(e);
              });
          }).catch(e => {
               // this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllUtilisateur(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM UTILISATEURS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, username:res.rows.item(i).username, email:res.rows.item(i).username, password:res.rows.item(i).password, ville:res.rows.item(i).password, telephone:res.rows.item(i).telephone, pays:res.rows.item(i).pays, fonctions:res.rows.item(i).fonctions , projet:res.rows.item(i).projet , monuserid:res.rows.item(i).monuserid })
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


deletteAllUtilisateur(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM UTILISATEURS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}



////////////////////////////////////////////////////////////////////////

createObservation(observation) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO OBSERVATIONS VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[observation.dateo , observation.coordX, observation.coordY,  observation.img1File, observation.img2File, observation.img3File, observation.img4File, observation.note, observation.etat, observation.typeObservations, observation.utilisateur, observation.projet, observation.groupe, observation.sousgroupe, observation.espece, observation.madate])
              .then(res => {

                    let result = res["insertId"];
                    resolve(result);
                  
              })
              .catch(e => {
                //this.presentToast('table OBSERVATIONS donst exist!');
                resolve(0);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(0);
          });
    });
}


getAllObservations(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM OBSERVATIONS ORDER BY id DESC', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, dateo:res.rows.item(i).dateo , coordX:res.rows.item(i).coordX, coordY:res.rows.item(i).coordY,  img1File:res.rows.item(i).img1File, img2File:res.rows.item(i).img2File, img3File:res.rows.item(i).img3File, img4File:res.rows.item(i).img4File, note:res.rows.item(i).note, etat:res.rows.item(i).etat, typeObservations:res.rows.item(i).typeObservations , utilisateur:res.rows.item(i).utilisateur, projet:res.rows.item(i).projet, groupe:res.rows.item(i).groupe, sousgroupe:res.rows.item(i).sousgroupe, espece:res.rows.item(i).espece, date:res.rows.item(i).madate})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      ///this.presentToast('database donst exist!');
    });
        
  });
}

deletteAllObservations(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM OBSERVATIONS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}

deletteObservationByID(id){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM OBSERVATIONS WHERE id = ?', [id])
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}



getAllObservationByEtat(etat){


  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM OBSERVATIONS WHERE etat = ?', [etat])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, dateo:res.rows.item(i).dateo , coordX:res.rows.item(i).coordX, coordY:res.rows.item(i).coordY,  img1File:res.rows.item(i).img1File, img2File:res.rows.item(i).img2File, img3File:res.rows.item(i).img3File, img4File:res.rows.item(i).img4File, note:res.rows.item(i).note, etat:res.rows.item(i).etat, typeObservations:res.rows.item(i).typeObservations , utilisateur:res.rows.item(i).utilisateur, projet:res.rows.item(i).projet, groupe:res.rows.item(i).groupe, sousgroupe:res.rows.item(i).sousgroupe, espece:res.rows.item(i).espece, madate:res.rows.item(i).madate})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });


}

////////////////////////////////////////////////////////////////////////

createResultat(resultat) {

  return new Promise((resolve)=>{
          this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO RESULTATS VALUES(?,?,?,?,?)',[resultat.id, resultat.contenu, resultat.questions, resultat.observations, resultat.etat])
              .then(res => {

                  //alert("INSERTED error " + JSON.stringify(res));
                  let result = res["insertId"];
                  resolve(result);

              })
              .catch(e => {
                //this.presentToast('table RESULTATS donst exist!');
                resolve(e);
              });
          }).catch(e => {
                //this.presentToast('database donst exist!');
                resolve(e);
          });
    });
}


getAllReultats(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM RESULTATS ', {})
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, contenu:res.rows.item(i).contenu, questions:res.rows.item(i).questions, observations:res.rows.item(i).observations, etat:res.rows.item(i).etat})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });
}


getAllReultatsByObservationId(id){
    return new Promise((resolve)=>{
      this.sqlite.create({
        name: 'sirendb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {


            db.executeSql('SELECT * FROM RESULTATS WHERE observations = ?', [id])
            .then(res => {
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, contenu:res.rows.item(i).contenu, questions:res.rows.item(i).questions, observations:res.rows.item(i).observations, etat:res.rows.item(i).etat})
              } 
              resolve(expenses);
            })
            .catch(e => {
              console.log(e);
              resolve(0);
            });


      }).catch(e => {
        //this.presentToast('database donst exist!');
      });
          
    });
}

deletteAllReultats(){
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM RESULTATS', {})
          .then(res => {
            resolve(1);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });
    }).catch(e => {
      //this.presentToast('database donst exist!');
    });   
  });
}




findresultatByObservationId(observations) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM RESULTATS WHERE observations = ?', [observations])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, contenu:res.rows.item(i).contenu, question:res.rows.item(i).questions, observation:res.rows.item(i).observations, etat:res.rows.item(i).etat})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  RESULTATS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database RESULTATS donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////


findUtilisateursByLoginAndPassword(utilisateur) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM UTILISATEURS WHERE (email = ? or telephone = ?) and password = ?', [utilisateur.login, utilisateur.login, utilisateur.password])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, username:res.rows.item(i).username, email:res.rows.item(i).email, password:res.rows.item(i).password, ville:res.rows.item(i).ville, telephone:res.rows.item(i).telephone, pays:res.rows.item(i).pays, fonctions:res.rows.item(i).fonctions , projet:res.rows.item(i).projet , monuserid:res.rows.item(i).monuserid })
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database donst exist!');
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database donst exist!');
        });
      });
}


///////////////////////////////////////////////////////////////////


getAllEspecesBySousGroupeId(sous_groupeId) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM ESPECES WHERE sous_groupes = ?', [sous_groupeId])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, sous_groupes:res.rows.item(i).sous_groupes, questions_animal:res.rows.item(i).questions_animal, questions_menaces:res.rows.item(i).questions_menaces, questions_signe:res.rows.item(i).questions_signe, questions_alimentation:res.rows.item(i).questions_alimentation})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  ESPECES donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database ESPECES donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////


getAllSousGroupesBygroupeId(groupeId) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM SOUS_GROUPES WHERE groupes = ?', [groupeId])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en, image:res.rows.item(i).image, description_fr:res.rows.item(i).description_fr, description_en:res.rows.item(i).description_en, groupes:res.rows.item(i).groupes})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database SOUS_GROUPES donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database SOUS_GROUPES donst exist!');
          resolve(0);
        });
      });
}


///////////////////////////////////////////////////////////////////


getQuestionById(questionId) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM QUESTIONS WHERE id = ?', [questionId])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {        
                expenses.push({id:res.rows.item(i).id, titre_fr:res.rows.item(i).titre_fr, titre_en:res.rows.item(i).titre_en, type_reponse:res.rows.item(i).type_reponse, interval:res.rows.item(i).interval, type_debut:res.rows.item(i).type_debut,image:res.rows.item(i).image,isPicture:res.rows.item(i).isPicture, questions:res.rows.item(i).questions})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
             // this.presentToast('database QUESTIONS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database QUESTIONS donst exist!');
          resolve(0);
        });
      });
}


///////////////////////////////////////////////////////////////////


getReponseByQuestionId(questionId) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM REPONSES WHERE questions = ?', [questionId])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {       
                expenses.push({id:res.rows.item(i).id, titre_fr:res.rows.item(i).titre_fr, titre_en:res.rows.item(i).titre_en, questions:res.rows.item(i).questions, questions_next:res.rows.item(i).questions_next, image: res.rows.item(i).image, taille:res.rows.item(i).taille})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database QUESTIONS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database QUESTIONS donst exist!');
          resolve(0);
        });
      });
}


getAllObservationByUserIdAndProjet(utilisateurId, projetId){
  
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM OBSERVATIONS WHERE utilisateur = ? and projet = ?', [utilisateurId, projetId])
          
          .then(res => {
            let expenses = [];
            
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, dateo:res.rows.item(i).dateo , coordX:res.rows.item(i).coordX, coordY:res.rows.item(i).coordY,  img1File:res.rows.item(i).img1File, img2File:res.rows.item(i).img2File, img3File:res.rows.item(i).img3File, img4File:res.rows.item(i).img4File, note:res.rows.item(i).note, etat:res.rows.item(i).etat, typeObservations:res.rows.item(i).typeObservations , utilisateur:res.rows.item(i).utilisateur, projet:res.rows.item(i).projet, groupe:res.rows.item(i).groupe, sousgroupe:res.rows.item(i).sousgroupe, espece:res.rows.item(i).espece})
            } 
            resolve(expenses);
          })
          .catch(e => {
            alert("big big error");
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database OBSERVATIONS donst exist!');
    });
        
  });

}


////////////////////////////////////////////////////////////////////////////



getAllPaysById(id) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM PAYS WHERE id = ?', [id])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, nom:res.rows.item(i).nom, lieu:res.rows.item(i).lieu, public:res.rows.item(i).public, pays:res.rows.item(i).pays, utilisateurs:res.rows.item(i).utilisateurs})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  PAYS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database PAYS donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////


getAllFonctionById(id) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM FONCTIONS WHERE id = ?', [id])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en})
          
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  FONCTIONS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database FONCTIONS donst exist!');
          resolve(0);
        });
      });
}




///////////////////////////////////////////////////////////////////


getAllProjetsByUserId(id) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM PROJET WHERE utilisateurs = ?', [id])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, nom_fr:res.rows.item(i).nom_fr, nom_en:res.rows.item(i).nom_en})
          
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  PROJET donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database PROJET donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////


getObervationByProjetId(userId, projetId) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('SELECT * FROM OBSERVATIONS WHERE utilisateur = ?', [userId])
            .then(res => {
              
              let expenses = [];
              for(var i=0; i<res.rows.length; i++) {
                expenses.push({id:res.rows.item(i).id, dateo:res.rows.item(i).dateo , coordX:res.rows.item(i).coordX, coordY:res.rows.item(i).coordY,  img1File:res.rows.item(i).img1File, img2File:res.rows.item(i).img2File, img3File:res.rows.item(i).img3File, img4File:res.rows.item(i).img4File, note:res.rows.item(i).note, etat:res.rows.item(i).etat, typeObservations:res.rows.item(i).typeObservations , utilisateur:res.rows.item(i).utilisateur, projet:res.rows.item(i).projet, groupe:res.rows.item(i).groupe, sousgroupe:res.rows.item(i).sousgroupe, espece:res.rows.item(i).espece, date:res.rows.item(i).madate})
              } 
              
              if(expenses.length != 0){
                resolve(expenses);
              }else{
                 resolve(0);
              }


            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  OBSERVATIONS donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database OBSERVATIONS donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////




updateObservation(id) {
  return new Promise((resolve)=>{
        this.sqlite.create({
          name: 'sirendb.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('UPDATE OBSERVATIONS SET etat=?  WHERE id = ?', [1, id])
            .then(res => {
              
               resolve(res);
               

            })
            .catch(e => {
              console.log(e);
              //this.presentToast('database  PROJET donst exist!');
              resolve(0);
            });
        }).catch(e => {
          console.log(e);
          //this.presentToast('database PROJET donst exist!');
          resolve(0);
        });
      });
}



///////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////


getAllProjetById(id) {
 
  return new Promise((resolve)=>{
    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

          db.executeSql('SELECT * FROM PROJET WHERE rowid = ?', [id])
          .then(res => {
            let expenses = [];
            for(var i=0; i<res.rows.length; i++) {
              expenses.push({id:res.rows.item(i).id, rowid:res.rows.item(i).rowid, nom:res.rows.item(i).nom, lieu:res.rows.item(i).lieu, public:res.rows.item(i).public, pays:res.rows.item(i).pays, utilisateurs:res.rows.item(i).utilisateurs})
            } 
            resolve(expenses);
          })
          .catch(e => {
            console.log(e);
            resolve(0);
          });


    }).catch(e => {
      //this.presentToast('database donst exist!');
    });
        
  });

}











  presentToast(message: any) {
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000
      });
      toast.present();
  }


}


 




