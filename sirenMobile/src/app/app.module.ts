import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { ServicesProvider } from '../providers/services/services';
import { HttpClientModule } from '@angular/common/http';
import { LocalStorageProvider } from '../providers/localstorage/localstorage';
import { IonicStorageModule } from '@ionic/storage';
import { LanguageService } from '../providers/services/language';
import {Geolocation} from '@ionic-native/geolocation';
//translate
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
import { Http } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
export function translateLoader(http: Http) { return new TranslateStaticLoader(http, './assets/i18n', '.json');}




@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: translateLoader,
      deps: [Http]
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    Geolocation,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicesProvider,
    LanguageService,
    LocalStorageProvider,
    ImagePicker,
    ConnectivityProvider
  ]
})
export class AppModule {}
