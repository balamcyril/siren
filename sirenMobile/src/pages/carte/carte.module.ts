import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartePage } from './carte';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    CartePage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(CartePage),
  ],
})
export class CartePageModule {}
