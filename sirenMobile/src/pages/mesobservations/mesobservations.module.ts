import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesobservationsPage } from './mesobservations';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    MesobservationsPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(MesobservationsPage),
  ],
})
export class MesobservationsPageModule {}
