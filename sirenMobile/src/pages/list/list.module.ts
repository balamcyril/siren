import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { ListPage } from './list';

@NgModule({
  declarations: [
    ListPage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(ListPage),
  ],
})
export class ListPageModule {}
