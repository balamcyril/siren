import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupePage } from './groupe';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';




@NgModule({
  declarations: [
    GroupePage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(GroupePage),
  ],
})
export class GroupePageModule {}
