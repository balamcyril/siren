import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeprojetPage } from './changeprojet';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    ChangeprojetPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(ChangeprojetPage),
  ],
})
export class ChangeprojetPageModule {}
