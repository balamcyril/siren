import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NombrepetitPage } from './nombrepetit';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    NombrepetitPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(NombrepetitPage),
  ],
})
export class NombrepetitPageModule {}
