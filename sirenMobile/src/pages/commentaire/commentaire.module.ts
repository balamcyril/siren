import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentairePage } from './commentaire';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    CommentairePage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(CommentairePage),
  ],
})
export class CommentairePageModule {}
