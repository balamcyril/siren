import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';
import { LanguageService } from '../../providers/services/language';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';

/**
 * Generated class for the SousEspecePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sous-espece',
  templateUrl: 'sous-espece.html',
})
export class SousEspecePage {

  public langue: any; 
  public listesousespeces: any; 
  public idgroupe:any;
  public sousgroupeelement: any;
  public myuserconnecte: any;

  constructor(public navCtrl: NavController, public mylocalstorage: LocalStorageProvider, public language:LanguageService, private geolocation: Geolocation, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
    this.idgroupe = this.navParams.get("id");
    this.sousgroupeelement = Parcourt.groupe;
    
    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });
    
  }

  ionViewDidLoad() {

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.language.loadLanguage().then((result) =>{
      if(result == 'en'){
        this.langue = 1;
          }else{
            this.langue = 0;
          }
    });
    
    if(navigator.onLine){
          /*this.sousgroupeelement = Parcourt.groupe;
          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.sousgroupes(this.idgroupe).subscribe((result) =>{  
                this.listesousespeces = result;
          }, (error)=>{
            console.log(error);
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });*/

          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.getAllSousGroupesBygroupeId(this.idgroupe).then((result) =>{
                loading.dismiss();
                this.listesousespeces = result;
          });

    }else{
          
          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.getAllSousGroupesBygroupeId(this.idgroupe).then((result) =>{
                loading.dismiss();
                this.listesousespeces = result;
          });

    }
          


  }

  espece(sousgroupe){
      Parcourt.sousgroupe = sousgroupe;
      this.navCtrl.push('EspecePage');
  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }

}
