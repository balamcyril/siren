import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { LanguageService } from '../../providers/services/language';


/**
 * Generated class for the DescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-description',
  templateUrl: 'description.html',
})
export class DescriptionPage {

  public monobservation = {
    description:''
  };

  public manobservation = {
    description:''
  };

  public question: any;
  public reponse: any; 
  public idquestion: any;
  public type_question: any;
  public myuserconnecte: any;
  public titrequestion:any;
  public nextquestion:any;
  public myimage:any;
  public printpicture =0;
  public noteobservation = 0;
  public myprojet:any;
  public langue = 0;
  public reponses = {
    questions:{
      titre_fr:"",
      titre_en:""
    },
    titre_fr : "",
    titre_en : ""
  }

  public monprojet={
    note:""
  };



  constructor(public navCtrl: NavController, public language:LanguageService, public services: ServicesProvider, public loadingCtrl: LoadingController, public mylocalstorage: LocalStorageProvider,private geolocation: Geolocation, public navParams: NavParams) {
  }

  ionViewWillLeave() {
    this.noteobservation = 0;
  }

  ionViewDidLoad() {

    this.language.loadLanguage().then((result) =>{
      
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });

    this.noteobservation = 0;
    console.log(Parcourt.listesReponses);

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          this.myprojet = this.myuserconnecte.projet;
          this.monprojet.note = this.myprojet.note;
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);

          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })


    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });


    this.question = this.navParams.get("question");
    this.reponse = this.navParams.get("reponse");

    this.idquestion = this.navParams.get("idquestion");
    this.type_question = this.navParams.get("type_question");

    if(navigator.onLine){

      /////////////////////////////////////////////////////////// ONLINE ////////////////////////////////

      /*console.log(this.idquestion);
      console.log(this.type_question);

      if((this.question!=0)||(this.question!='0')){

          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.findQuestionById(this.idquestion).subscribe((result) =>{  
            console.log(result);
            this.nextquestion = result;
            
            if(result.image!=undefined){
              this.printpicture =1;
              this.myimage =result.image;
            }else{
              //alert("le mokai");
            }
            
            if(result.type_reponse != 'select'){
              if(this.langue==0){
                this.titrequestion = result.titre_fr;
              }else{
                this.titrequestion = result.titre_en;
              }
              
            }else{
              this.noteobservation = 1;
            }
            
          }, (error)=>{ 
            console.log(error);
            //votre opération a  echoué!!
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });
      }else{
        this.noteobservation = 1;
      }*/

      ////////////////////////////////////////////////////////////////////////////////

      if((this.question!=0)||(this.question!='0')){

        let loading = this.loadingCtrl.create();
        loading.present();
        this.services.getQuestionById(this.idquestion).then((result) =>{
              loading.dismiss();

              this.nextquestion = result[0];
          
              if(result[0].image!=undefined){
                this.printpicture =1;
                this.myimage =result[0].image;
              }else{
                //alert("le mokai");
              }


              if(result[0].type_reponse != 'select'){
                if(this.langue==0){
                  this.titrequestion = result[0].titre_fr;
                }else{
                  this.titrequestion = result[0].titre_en;
                }
                
              }else{
                this.noteobservation = 1;
              }

             
              });
          }else{
            this.noteobservation = 1; 
          }


    }else{


      if((this.question!=0)||(this.question!='0')){

          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.getQuestionById(this.idquestion).then((result) =>{
                loading.dismiss();

                this.nextquestion = result[0];
            
                if(result[0].image!=undefined){
                  this.printpicture =1;
                  this.myimage =result[0].image;
                }else{
                  //alert("le mokai");
                }


                if(result[0].type_reponse != 'select'){
                  if(this.langue==0){
                    this.titrequestion = result[0].titre_fr;
                  }else{
                    this.titrequestion = result[0].titre_en;
                  }
                  
                }else{
                  this.noteobservation = 1;
                }

               
          });
      }else{
        this.noteobservation = 1; 
      }

    }

   
  }


  suivant(){

     if((this.question!=0)||(this.question!='0')){


      if(navigator.onLine){

          /*if(this.nextquestion.questions!=undefined){
            this.type_question = this.nextquestion.questions.type_reponse;
            this.idquestion = this.nextquestion.questions.id;

            this.reponses.questions = this.nextquestion;
            if(this.langue == 0){
              this.reponses.titre_fr = this.monobservation.description;
            }else{
              this.reponses.titre_en = this.monobservation.description;
            }
            

            var listereponses = [];
            if(Parcourt.listesReponses != null){
              listereponses = Parcourt.listesReponses;
              listereponses.push(this.reponses);
              Parcourt.listesReponses = listereponses;
            }else{
              
              listereponses.push(this.reponses);
              Parcourt.listesReponses = listereponses;
            }
            
            if(this.type_question == "entier"){
              this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
            }else if(this.type_question == "text"){
              this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
            }else if(this.type_question == "select"){
              this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
            }else if(this.type_question == "rapide"){
              this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
            }else if(this.type_question == "annuler"){
              this.navCtrl.setRoot('AcceuilPage').then(()=>{});
            }else{
              this.navCtrl.push('SirinPage', {question: 0, reponse:0});
              //this.navCtrl.push('GroupePage');
            }

          }else{
            if(this.noteobservation == 0){

              if(this.langue==0){
                this.reponses.questions = this.nextquestion;
                //this.reponses.questions.titre_fr = this.titrequestion;
                this.reponses.titre_fr = this.monobservation.description;
              }else{
                this.reponses.questions = this.nextquestion;
                //this.reponses.questions.titre_en = this.titrequestion;
                this.reponses.titre_en = this.monobservation.description;
              }
                

              var listereponses = [];
              if(Parcourt.listesReponses != null){
                listereponses = Parcourt.listesReponses;
                listereponses.push(this.reponses);
                Parcourt.listesReponses = listereponses;
              }else{
                
                listereponses.push(this.reponses);
                Parcourt.listesReponses = listereponses;
              }
              this.noteobservation = 1;
            }else{
              Parcourt.description = this.manobservation.description;
              this.navCtrl.push('SirinPage', {question: 0, reponse:0});
            } 

          }*/

          if(this.nextquestion.questions!="null"){

            this.services.getQuestionById(this.nextquestion.questions).then((result) =>{
                var questions_next = result[0];

                this.type_question = questions_next.type_reponse;
                this.idquestion = questions_next.id;

                this.reponses.questions = questions_next;
                if(this.langue == 0){
                  this.reponses.titre_fr = this.monobservation.description;
                }else{
                  this.reponses.titre_en = this.monobservation.description;
                }

                var listereponses = [];
                if(Parcourt.listesReponses != null){
                  listereponses = Parcourt.listesReponses;
                  listereponses.push(this.reponses);
                  Parcourt.listesReponses = listereponses;
                }else{
                  
                  listereponses.push(this.reponses);
                  Parcourt.listesReponses = listereponses;
                }

                if(this.type_question == "entier"){
                  this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
                }else if(this.type_question == "text"){
                  this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
                }else if(this.type_question == "select"){
                  this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
                }else if(this.type_question == "rapide"){
                  this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
                }else if(this.type_question == "annuler"){
                  this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                }else{
                  //this.navCtrl.push('GroupePage');
                  this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                }

            })

        }else{
          if(this.noteobservation == 0){

            if(this.langue==0){
              this.reponses.questions = this.nextquestion;
              //this.reponses.questions.titre_fr = this.titrequestion;
              this.reponses.titre_fr = this.monobservation.description;
            }else{
              this.reponses.questions = this.nextquestion;
              //this.reponses.questions.titre_en = this.titrequestion;
              this.reponses.titre_en = this.monobservation.description;
            }
              

            var listereponses = [];
            if(Parcourt.listesReponses != null){
              listereponses = Parcourt.listesReponses;
              listereponses.push(this.reponses);
              Parcourt.listesReponses = listereponses;
            }else{
              
              listereponses.push(this.reponses);
              Parcourt.listesReponses = listereponses;
            }
            this.noteobservation = 1;
          }else{
            Parcourt.description = this.manobservation.description;
            this.navCtrl.push('SirinPage', {question: 0, reponse:0});
          } 

        }

        }else{



          if(this.nextquestion.questions!="null"){

              this.services.getQuestionById(this.nextquestion.questions).then((result) =>{
                  var questions_next = result[0];

                  this.type_question = questions_next.type_reponse;
                  this.idquestion = questions_next.id;

                  this.reponses.questions = questions_next;
                  if(this.langue == 0){
                    this.reponses.titre_fr = this.monobservation.description;
                  }else{
                    this.reponses.titre_en = this.monobservation.description;
                  }

                  var listereponses = [];
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(this.reponses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    
                    listereponses.push(this.reponses);
                    Parcourt.listesReponses = listereponses;
                  }

                  if(this.type_question == "entier"){
                    this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "text"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
                  }else if(this.type_question == "select"){
                    this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "rapide"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
                  }else if(this.type_question == "annuler"){
                    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                  }else{
                    //this.navCtrl.push('GroupePage');
                    this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                  }

              })

          }else{
            if(this.noteobservation == 0){

              if(this.langue==0){
                this.reponses.questions = this.nextquestion;
                //this.reponses.questions.titre_fr = this.titrequestion;
                this.reponses.titre_fr = this.monobservation.description;
              }else{
                this.reponses.questions = this.nextquestion;
                //this.reponses.questions.titre_en = this.titrequestion;
                this.reponses.titre_en = this.monobservation.description;
              }
                

              var listereponses = [];
              if(Parcourt.listesReponses != null){
                listereponses = Parcourt.listesReponses;
                listereponses.push(this.reponses);
                Parcourt.listesReponses = listereponses;
              }else{
                
                listereponses.push(this.reponses);
                Parcourt.listesReponses = listereponses;
              }
              this.noteobservation = 1;
            }else{
              Parcourt.description = this.manobservation.description;
              this.navCtrl.push('SirinPage', {question: 0, reponse:0});
            } 

          }



        }



        }else{
              Parcourt.description = this.manobservation.description;
              this.navCtrl.push('SirinPage', {question: 0, reponse:0});
        }

    

    //this.navCtrl.push('SirinPage', {question: this.question, reponse:this.reponse});
  /*  if(this.type_question == "text"){
      this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion});
    }else if(this.type_question == "select"){
      this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion});
    }else{
      this.navCtrl.push('GroupePage');
    }*/

    /*if(this.type_question == "entier"){
      this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
    }else if(this.type_question == "text"){
      this.navCtrl.push('DescriptionPage',{idquestion: 0, type_question: this.type_question});  
    }else if(this.type_question == "select"){
      this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
    }else if(this.type_question == "rapide"){
      this.navCtrl.push('DescriptionPage',{idquestion: 0, type_question: this.type_question});  
    }else if(this.type_question == "annuler"){
      this.navCtrl.setRoot('AcceuilPage').then(()=>{});
    }else{
      this.navCtrl.push('GroupePage');
    }*/

    
 }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }


  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }

}
