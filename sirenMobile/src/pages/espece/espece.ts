import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Parcourt } from '../../configs/configs';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { LanguageService } from '../../providers/services/language';
import { TranslateService } from 'ng2-translate';
import {Geolocation} from '@ionic-native/geolocation';

/**
 * Generated class for the EspecePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-espece',
  templateUrl: 'espece.html',
})
export class EspecePage {

  public langue: any; 
  public listeespeces: any; 
  public especeelement: any;
  public questionsid: any;
  public typeobservation: any;
  public internet:any;
  public userconnecte = {};
  public myuserconnecte: any;
  

  constructor(public navCtrl: NavController,private geolocation: Geolocation , public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider, public translate:TranslateService, public language:LanguageService) {
    this.especeelement = Parcourt.sousgroupe;
    this.typeobservation = Parcourt.typeobservation;
    this.questionsid = 0;
    if(navigator.onLine){
      this.internet = 1;
    }else{
      this.internet = 0;
    }
    
  }

  ionViewDidLoad() {


    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });

    this.language.loadLanguage().then((result) =>{
      if(result == 'en'){
        this.langue = 1;
          }else{
            this.langue = 0;
          }
      });

    this.especeelement = Parcourt.sousgroupe;
    this.typeobservation = Parcourt.typeobservation;

       if(navigator.onLine){

          this.langue = 'fr';
          /*let loading = this.loadingCtrl.create();
          loading.present();
          this.mylocalstorage.getSession().then((result) =>{
            
            this.userconnecte = result;
                this.services.especeBySousGroupe(this.userconnecte["projet"].id, this.especeelement.id).subscribe((result) =>{  
                      this.listeespeces = result;
                }, (error)=>{
                  console.log(error);
                  loading.dismiss();
                }, ()=>{
                  loading.dismiss();
                });
          });*/

          let loading = this.loadingCtrl.create();
          loading.present();
          this.mylocalstorage.getSession().then((result) =>{
              this.services.getAllEspecesBySousGroupeId(this.especeelement.id).then((result) =>{
                    loading.dismiss();
                    this.listeespeces = result;
              });
          });

        }else{
          let loading = this.loadingCtrl.create();
          loading.present();
          this.mylocalstorage.getSession().then((result) =>{
              this.services.getAllEspecesBySousGroupeId(this.especeelement.id).then((result) =>{
                    loading.dismiss();
                    this.listeespeces = result;
              });
          });
        }
  }


  nombrepetit(espece){

    Parcourt.espece = espece;

    if(this.typeobservation.nom_fr== "Animal"){
      /*if(this.internet==0){
        this.questionsid = espece.questions_animal;
      }else{
        this.questionsid = espece.questions_animal.id;
      }*/
      this.questionsid = espece.questions_animal
   }
   if(this.typeobservation.nom_fr== "Menaces"){
     /* if(this.internet == 0){
        this.questionsid = espece.questions_menaces;
      }else{
        this.questionsid = espece.questions_menaces.id;
      }*/
      this.questionsid = espece.questions_menaces;
      
   }
   if(this.typeobservation.nom_fr== "Alimetation"){
     /* if(this.internet == 0){
        this.questionsid = espece.questions_alimentation;
      }else{
        this.questionsid = espece.questions_alimentation.id;
      }*/
      this.questionsid = espece.questions_alimentation;
      
   }
   if(this.typeobservation.nom_fr == "Signe de présence"){
      /*if(this.internet == 0){
        this.questionsid = espece.questions_signe;
      }else{
        this.questionsid = espece.questions_signe.id;
      }*/
      this.questionsid = espece.questions_signe;
      
   }

    if(this.questionsid!=0){


      if(navigator.onLine){

        let loading = this.loadingCtrl.create();
        loading.present();
        this.services.findQuestionById(this.questionsid).subscribe((result) =>{  
          
          if(result.type_reponse == "text"){
              this.navCtrl.push('NombrepetitPage',{idquestion: result.id, type_question: "text"});
          }
          if(result.type_reponse == "select"){ 
              this.navCtrl.push('OuestanimalPage',{idquestion: result.id, type_question: "select"});
            }


        }, (error)=>{
          console.log(error);
          //votre opération a  echoué!!
          loading.dismiss();
        }, ()=>{
          loading.dismiss();
        });

      }else{

        let loading = this.loadingCtrl.create();
          loading.present();
          this.services.getQuestionById(this.questionsid).then((result) =>{
                loading.dismiss();
                if(result[0].type_reponse == "text"){
                  this.navCtrl.push('NombrepetitPage',{idquestion: result[0].id, type_question: "text"});
                }
                if(result[0].type_reponse == "select"){ 
                    this.navCtrl.push('OuestanimalPage',{idquestion: result[0].id, type_question: "select"});
                  }
          });
        

      }



                     
      }

  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }



}
