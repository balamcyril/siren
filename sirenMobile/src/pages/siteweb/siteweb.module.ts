import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SitewebPage } from './siteweb';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    SitewebPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(SitewebPage),
  ],
})
export class SitewebPageModule {}
