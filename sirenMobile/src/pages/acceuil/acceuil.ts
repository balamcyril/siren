import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { Parcourt } from '../../configs/configs';
import { TranslateService } from 'ng2-translate';
import { LanguageService } from '../../providers/services/language';
import {Geolocation} from '@ionic-native/geolocation';
import { Events } from 'ionic-angular';



/**
 * Generated class for the AcceuilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-acceuil',
  templateUrl: 'acceuil.html',
})
export class AcceuilPage {

  public langue = 0;

  public listetypeobservations: any;
  public resultat = {};
  public listpays: any;
  public questionsid: any;
  public userconnecte = {};
  public myuserconnecte: any;
  public myprojet:any;
  public monprojet={
    note:""
  };

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public events: Events, private geolocation: Geolocation, public loadingCtrl: LoadingController, public navParams: NavParams, public mylocalstorage: LocalStorageProvider, public services: ServicesProvider, public translate:TranslateService, public language:LanguageService) {
    
  }

  

  ionViewDidLoad() {

    this.events.publish('user:created', {}, Date.now());

    /*this.services.currentMenusListner.subscribe(next =>{
       alert(JSON.stringify(next));
    })

    this.services.getAllProjetMenu().then((myresult:any) =>{
      alert(JSON.stringify(myresult));
    })*/

    /*var initialHref = window.location.href;
    navigator.splashscreen.show();
    // Reload original app url (ie your index.html file)
    window.location = initialHref;
    navigator.splashscreen.hide();*/
    



    //window.location.reload();

    Parcourt.listesReponses = [];
    let loading2 = this.loadingCtrl.create();
          loading2.present();

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        this.myprojet = this.myuserconnecte.projet;
        this.monprojet.note = this.myprojet.note;
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){

          let loading = this.loadingCtrl.create();
          loading.present();

          
          
              /*this.services.projet_typeObservation(this.myprojet.id).subscribe((result) =>{
                    console.log(result);
                    this.listetypeobservations = result;
              }, (error)=>{
                loading.dismiss();
              }, ()=>{
                loading.dismiss();
              });*/

              /*this.services.getAllProjetTypeObsevationsByProjetId(this.myprojet.id).then((result) =>{
                    console.log(result);
                    this.listetypeobservations = result;
                    loading.dismiss();
              });*/

             
              
              this.services.getAllTypeObsevations().then((result) =>{
                    loading.dismiss();
                    this.listetypeobservations = result;
                    loading2.dismiss();
              })

          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

  

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });
    
    /*this.mylocalstorage.getSession().then((result) =>{

      console.log("========================================");
      console.log(result);
      console.log("========================================");
      
    })*/


    /*let resultat = {
      "contenu":"le resultat", 
      "questions":1, 
      "observations":1, 
      "etat":1
    }
    this.services.createResultat(resultat).then((result) =>{
       
      
    });*/

    //var timestamp = Math.round(new Date().getTime()/1000);

    //alert(timestamp);

    /*setInterval(() => { 

      if(navigator.onLine){

              this.mylocalstorage.getSession().then((result) =>{

                    this.userconnecte = result;

                    
                    this.geolocation.getCurrentPosition().then( (resp) =>{ 
                    
                          var utilisateurId = this.userconnecte["id"];
                          var projetId = this.userconnecte["projet"].id;
                          this.services.savepositions(resp.coords.latitude, resp.coords.longitude, utilisateurId, projetId).subscribe((result) =>{
                              
                            
                          
                          }, (error)=>{
                          }, ()=>{
                          });

                    }).catch( (error) =>{   
                        console.log("error could not get position");
                    });

              });

        }
      
    }, 120000);*/


    this.language.loadLanguage().then((result) =>{
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });
   
    
    if(navigator.onLine){

        

          /*let loading = this.loadingCtrl.create();
          loading.present();
          this.services.acceuil().subscribe((result) =>{
                console.log(result);
                this.listetypeobservations = result;
          }, (error)=>{
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });*/

          
          

          

          /*let loading = this.loadingCtrl.create();
          loading.present();
            this.services.getAllTypeObsevations().then((result) =>{
                  loading.dismiss();
                  this.listetypeobservations = result;
          });*/



    }else{
      /*let loading = this.loadingCtrl.create();
      loading.present();
        this.services.getAllTypeObsevations().then((result) =>{
              loading.dismiss();
              this.listetypeobservations = result;
        });*/

    }

  }





  groupe(observation){

   if(Parcourt.latitude != null){
            if(observation.type_question == "groupe"){
                Parcourt.typeobservation = observation;
                Parcourt.indicateur = 1;
                
                //this.navCtrl.push('GroupePage');
                this.navCtrl.push('PhotosPage',{idquestion: 0, type_question: "groupe"});

            }else{

              Parcourt.indicateur = 2;
              Parcourt.typeobservation = observation;
              

              if(navigator.onLine){

                            //////////////////////////////////// for test ////////////////////////////////////////////
                            /* this.questionsid =  observation.questions.id;

                              let loading = this.loadingCtrl.create();
                              loading.present();
      
                              if(undefined === this.questionsid){

                                      loading.dismiss();
                                      Parcourt.indicateur = 3;
                                      this.navCtrl.push('PhotosPage',{idquestion: 0, type_question: "rapide"});
                
                              }else{
                                    this.services.findQuestionById(this.questionsid).subscribe((result) =>{  

                                        
                                          
                                    if(result.type_reponse == "text"){
                                    
                                        //this.navCtrl.push('NombrepetitPage',{idquestion: result.id});
                                        this.navCtrl.push('PhotosPage',{idquestion: result.id, type_question: "text"});
                                    }
                                    if(result.type_reponse == "select"){ 
                                      
                                      // this.navCtrl.push('OuestanimalPage',{idquestion: result.id});
                                      this.navCtrl.push('PhotosPage',{idquestion: result.id, type_question: "select"});
                                      }
                                  }, (error)=>{
                                    console.log(error);
                                    //votre opération a  echoué!!
                                    loading.dismiss();
                                  }, ()=>{
                                    loading.dismiss();
                                  });
                              }   */
                            

                          //////////////////////////////////////////// for server ////////////////////////////////////////
                          this.questionsid =  observation.questions;
                          if(null === this.questionsid){

                            Parcourt.indicateur = 3;
                            this.navCtrl.push('PhotosPage',{idquestion: 0, type_question: "rapide"});
                
                          }else{

                              
              
                                  let loading = this.loadingCtrl.create();
                                    loading.present();

                                    this.services.getQuestionById(this.questionsid).then((result) =>{

                                          loading.dismiss();
                                          if(result[0].type_reponse == "text"){
                                            //this.navCtrl.push('NombrepetitPage',{idquestion: result[0].id});
                                            this.navCtrl.push('PhotosPage',{idquestion:  result[0].id, type_question: "text"});
                                          }
                                          if(result[0].type_reponse == "select"){ 
                                            // this.navCtrl.push('OuestanimalPage',{idquestion: result[0].id});
                                              this.navCtrl.push('PhotosPage',{idquestion:  result[0].id, type_question: "select"});
                                            }
                                    });



                          }

                        

                


        
              }else{



                this.questionsid =  observation.questions;

              


                if(null === this.questionsid){

                            Parcourt.indicateur = 3;
                            this.navCtrl.push('PhotosPage',{idquestion: 0, type_question: "rapide"});

                }else{

                            
                      
                              let loading = this.loadingCtrl.create();
                                loading.present();

                                this.services.getQuestionById(this.questionsid).then((result) =>{

                                      loading.dismiss();
                                      if(result[0].type_reponse == "text"){
                                        //this.navCtrl.push('NombrepetitPage',{idquestion: result[0].id});
                                        this.navCtrl.push('PhotosPage',{idquestion:  result[0].id, type_question: "text"});
                                      }
                                      if(result[0].type_reponse == "select"){ 
                                        // this.navCtrl.push('OuestanimalPage',{idquestion: result[0].id});
                                          this.navCtrl.push('PhotosPage',{idquestion:  result[0].id, type_question: "select"});
                                        }
                                });
                  
                }
        
              }
            }

          
   }else{

        this.translate.get(['LOCALISATIONERROR', 'MESSAGELOCALISATION']).subscribe((langs:Array<string>) => {
          let alert = this.alertCtrl.create({
            title: langs['LOCALISATIONERROR'],
            message: langs['MESSAGELOCALISATION'],
            buttons: [
              {
                text: 'OK',
                role: 'cancel',
                handler: () => {
                  
                  this.geolocation.getCurrentPosition().then( (resp) =>{ 
                        Parcourt.latitude = resp.coords.latitude;
                        Parcourt.longitude = resp.coords.longitude;
                  }).catch( (error) =>{   
                       console.log("error could not get position");
                  });

                }
              }
            ]
          });
          alert.present();
        })

      }
     

     
  }

  /*savePays(){
    let pays = {

      nom_fr: "Cameroun", 
      nom_en: "Cameroon", 
      code_tel: "237", 
      sigle: "CMR"
    }
    this.services.createPays(pays).then((result1) =>{
        this.services.getAllPays().then((result) =>{
              this.listpays = result;   
        })
    });
  }*/


  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }




}
