<?php

namespace Siren\StatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SirenStatBundle:Default:index.html.twig');
    }
}
