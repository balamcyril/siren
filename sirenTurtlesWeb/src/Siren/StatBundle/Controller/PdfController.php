<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\StatBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Response;
//    use Knp\Bundle\SnappyBundle\Snappy\Response;

/**
 * Description of Pdf
 *
 * @author georges
 */
class PdfController extends Controller{
    //put your code here
    public function pdfAction($resultats, $cpt){
        $resultats=json_decode($resultats, $assoc = true);
//        print_r($resultats);
//        echo $cpt;
        // affichage des pdf
        if ($cpt== 1){
           $html = $this->renderView('SirenStatBundle:Pdf:areaChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'filename="areaChart.pdf"'
                    ));
        }
        if ($cpt== 2){
            $html = $this->renderView('SirenStatBundle:Pdf:donutChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'filename="pieChart.pdf"'
                    ));
        }
        if ($cpt== 3){
            $html = $this->renderView('SirenStatBundle:Pdf:barChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'filename="barChart.pdf"'
                    ));
        }
        if ($cpt== 4){
            $html = $this->renderView('SirenStatBundle:Pdf:lineChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'filename="lineChart.pdf"'
                    ));
        }        
                
    }
    public function imageAction($resultats, $cpt){
        $resultats=json_decode($resultats, $assoc = true);
//        print_r($resultats);
        if ($cpt == 1){
            $html = $this->renderView('SirenStatBundle:Pdf:areaChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.image')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'image/jpg',
                        'Content-Disposition'   => 'filename="areaChart.jpg"'
                    ));
        }
        if ($cpt == 2){
            $html = $this->renderView('SirenStatBundle:Pdf:donutChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.image')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'image/jpg',
                        'Content-Disposition'   => 'filename="pieChart.jpg"'
                    ));
        }
        if ($cpt == 3){
            $html = $this->renderView('SirenStatBundle:Pdf:barChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.image')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'image/jpg',
                        'Content-Disposition'   => 'filename="barChart.jpg"'
                    ));
        }
        if ($cpt == 4){
            $html = $this->renderView('SirenStatBundle:Pdf:lineChart.html.twig', array(
                'resultats'=> $resultats
            ));

            return new response(
            $this->get('knp_snappy.image')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'image/jpg',
                        'Content-Disposition'   => 'filename="lineChart.jpg"'
                    ));
        }
    }
}
