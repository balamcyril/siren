<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\StatBundle\Service;

/**
 * Description of StatPrevision
 *
 * @author georges
 */
class StatPrevision {
    public function moindreCareAnnee($classe,$res){
 //première étape
        // calcul des moyenne
        $X = 6.5;
        $ys=0;
  // deuxieme étape: trouver l'équation y =ax +b
        $xi =array();
        $yi =array();
        $yj =array();
    for($i=1; $i<=12; $i++){
        if (count($res[$i])!= 0){
            $bool=true;
            for($j=0; $j< count($res[$i]); $j++){
                if($res[$i][$j]['attribut'] == $classe){
                    $yi[$i]= $res[$i][$j]['nombre'];
                    $ys += $yi[$i];
                    $bool=false;
                    break;
                }
                 
            }
            if ($bool)
            {
                $yi[$i]=0;
            }
        }else{
            $yi[$i]=0;

        }
        $xi[$i]=$i;
        
    }
        $Y =$ys/12;

    $a1=0;
    $a2=0;
    for($i=1; $i<=12; $i++){
        $a1 += ($xi[$i]-$X)*($yi[$i]-$Y);
        $a2 += ($xi[$i]-$X)*($xi[$i]-$X);
    }
    $a= $a1/$a2;
    
    // calcul de "b"
    $b1= $a*$X;
    $b = $Y-$b1;
    
    //calcul des "y"
    $x=13;
    $pre =array();
    for($i=1; $i<=12; $i++){
        $r1= ($a)*($x);
        $r2=$b;
        $r= $r1+$r2;
        $t1[$i]= $r;
        $x++;
      
    }
    
//    
    for($i=1; $i<=12; $i++){
        $egal=array();
         $egal =array('prevision'=>$t1[$i], 'normal'=> $yi[$i] );
        array_push($pre, $egal);
    }
    
   return $pre; 
       
    }
}
