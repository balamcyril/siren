<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\TypeRepository")
 */
class Type
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_fr", type="string", length=255)
     */
    private $nom_fr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom_en", type="string", length=255, nullable=true)
     */
    private $nom_en;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="boolean")
     */
    private $valeur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Patrouille", mappedBy="segment")
     */
    private $patrouilles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom
     * 
     * @param string $locale 
     * @return string 
     */
    public function getNom($locale = "fr")
    {
        if($locale == "fr"){
            return $this->nom_fr;
        }
        else if($locale == "en"){
            return $this->nom_en;
        }
    }
   

    /**
     * Set valeur
     *
     * @param boolean $valeur
     * @return Type
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return boolean 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Type
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->patrouilles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     * @return Type
     */
    public function addPatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles[] = $patrouilles;

        return $this;
    }

    /**
     * Remove patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     */
    public function removePatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles->removeElement($patrouilles);
    }

    /**
     * Get patrouilles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPatrouilles()
    {
        return $this->patrouilles;
    }

    /**
     * Set nom_fr
     *
     * @param string $nomFr
     * @return Type
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nom_fr
     *
     * @return string 
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nom_en
     *
     * @param string $nomEn
     * @return Type
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nom_en
     *
     * @return string 
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
}
