<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SautQuestion
 *
 * @ORM\Table(name="saut_question")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\SautQuestionRepository")
 */
class SautQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

     /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Saut", inversedBy="sautQuestions")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $saut;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Question", inversedBy="sautQuestions")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $question;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return SautQuestion
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set saut
     *
     * @param \Siren\AppBundle\Entity\Saut $saut
     * @return SautQuestion
     */
    public function setSaut(\Siren\AppBundle\Entity\Saut $saut = null)
    {
        $this->saut = $saut;

        return $this;
    }

    /**
     * Get saut
     *
     * @return \Siren\AppBundle\Entity\Saut 
     */
    public function getSaut()
    {
        return $this->saut;
    }

    /**
     * Set question
     *
     * @param \Siren\AppBundle\Entity\Question $question
     * @return SautQuestion
     */
    public function setQuestion(\Siren\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Siren\AppBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
