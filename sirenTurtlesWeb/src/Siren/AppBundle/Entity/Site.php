<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 *
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\SiteRepository")
 */
class Site
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text")
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Segment", mappedBy="site")
     */
    private $segments;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Projet", inversedBy="projet")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $projet;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        $this->segments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateAdd = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Site
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Site
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Add segment
     *
     * @param \Siren\AppBundle\Entity\Segment $segment
     * @return Site
     */
    public function addSegment(\Siren\AppBundle\Entity\Segment $segment)
    {
        $this->segments[] = $segment;

        return $this;
    }

    /**
     * Remove segment
     *
     * @param \Siren\AppBundle\Entity\Segment $segment
     */
    public function removeSegment(\Siren\AppBundle\Entity\Segment $segment)
    {
        $this->segments->removeElement($segment);
    }

    /**
     * Get segments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * @return mixed
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * @param mixed $projet
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    }


}
