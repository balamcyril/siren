<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Suggestion
 *
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\SuggestionRepository")
 */
class Suggestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Categorie", inversedBy="suggestions")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Question", inversedBy="suggestions")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $question;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Reponse", mappedBy="suggestion")
     */
    private $reponses;
    
    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Reponse", inversedBy="fils", cascade={"remove"})
     */
    protected $parent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set categorie
     *
     * @param \Siren\AppBundle\Entity\Categorie $categorie
     * @return Suggestion
     */
    public function setCategorie(\Siren\AppBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Siren\AppBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set question
     *
     * @param \Siren\AppBundle\Entity\Question $question
     * @return Suggestion
     */
    public function setQuestion(\Siren\AppBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Siren\AppBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add reponses
     *
     * @param \Siren\AppBundle\Entity\Reponse $reponses
     * @return Suggestion
     */
    public function addReponse(\Siren\AppBundle\Entity\Reponse $reponses)
    {
        $this->reponses[] = $reponses;

        return $this;
    }

    /**
     * Remove reponses
     *
     * @param \Siren\AppBundle\Entity\Reponse $reponses
     */
    public function removeReponse(\Siren\AppBundle\Entity\Reponse $reponses)
    {
        $this->reponses->removeElement($reponses);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * Set parent
     *
     * @param \Siren\AppBundle\Entity\Reponse $parent
     * @return Suggestion
     */
    public function setParent(\Siren\AppBundle\Entity\Reponse $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Siren\AppBundle\Entity\Reponse 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
