<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Patrouille
 *
 * @ORM\Table(name="patrouille")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\PatrouilleRepository")
 */
class Patrouille
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSite", type="string", length=255, nullable=true)
     */
    private $nomSite;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="coordXDebut", type="float", nullable=true)
     */
    private $coordXDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="coordYDebut", type="float", nullable=true)
     */
    private $coordYDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="coordXFin", type="float", nullable=true)
     */
    private $coordXFin;

    /**
     * @var string
     *
     * @ORM\Column(name="coordYFin", type="float", nullable=true)
     */
    private $coordYFin;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Segment", inversedBy="patrouilles")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $segment;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Type", inversedBy="patrouilles")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $type;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Observation", mappedBy="patrouille")
     */
    private $observations;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Utilisateur", inversedBy="patrouilles")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $patrouilleur;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Projet", inversedBy="patrouilles")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $projet;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomSite
     *
     * @param string $nomSite
     * @return Patrouille
     */
    public function setNomSite($nomSite)
    {
        $this->nomSite = $nomSite;

        return $this;
    }

    /**
     * Get nomSite
     *
     * @return string 
     */
    public function getNomSite()
    {
        return $this->nomSite;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Patrouille
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Patrouille
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        if($this->getProjet()->getId() == 51) {
            $this->dateDebut->setTimezone(new \DateTimeZone("Asia/Dubai"));
        }
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Patrouille
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        if($this->getProjet()->getId() == 51) {
            $this->dateFin->setTimezone(new \DateTimeZone("Asia/Dubai"));
        }
 
        return $this->dateFin;
    }

    /**
     * Set coordXDebut
     *
     * @param string $coordXDebut
     * @return Patrouille
     */
    public function setCoordXDebut($coordXDebut)
    {
        $this->coordXDebut = $coordXDebut;

        return $this;
    }

    /**
     * Get coordXDebut
     *
     * @return string 
     */
    public function getCoordXDebut()
    {
        return $this->coordXDebut;
    }

    /**
     * Set coordYDebut
     *
     * @param string $coordYDebut
     * @return Patrouille
     */
    public function setCoordYDebut($coordYDebut)
    {
        $this->coordYDebut = $coordYDebut;

        return $this;
    }

    /**
     * Get coordYDebut
     *
     * @return string 
     */
    public function getCoordYDebut()
    {
        return $this->coordYDebut;
    }

    /**
     * Set coordXFin
     *
     * @param string $coordXFin
     * @return Patrouille
     */
    public function setCoordXFin($coordXFin)
    {
        $this->coordXFin = $coordXFin;

        return $this;
    }

    /**
     * Get coordXFin
     *
     * @return string 
     */
    public function getCoordXFin()
    {
        return $this->coordXFin;
    }

    /**
     * Set coordYFin
     *
     * @param string $coordYFin
     * @return Patrouille
     */
    public function setCoordYFin($coordYFin)
    {
        $this->coordYFin = $coordYFin;

        return $this;
    }

    /**
     * Get coordYFin
     *
     * @return string 
     */
    public function getCoordYFin()
    {
        return $this->coordYFin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->observations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set segment
     *
     * @param \Siren\AppBundle\Entity\Segment $segment
     * @return Patrouille
     */
    public function setSegment(\Siren\AppBundle\Entity\Segment $segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \Siren\AppBundle\Entity\Segment 
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set type
     *
     * @param \Siren\AppBundle\Entity\Type $type
     * @return Patrouille
     */
    public function setType(\Siren\AppBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Siren\AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add observations
     *
     * @param \Siren\AppBundle\Entity\Observation $observations
     * @return Patrouille
     */
    public function addObservation(\Siren\AppBundle\Entity\Observation $observations)
    {
        $this->observations[] = $observations;

        return $this;
    }

    /**
     * Remove observations
     *
     * @param \Siren\AppBundle\Entity\Observation $observations
     */
    public function removeObservation(\Siren\AppBundle\Entity\Observation $observations)
    {
        $this->observations->removeElement($observations);
    }

    /**
     * Get observations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set patrouilleur
     *
     * @param \Siren\AppBundle\Entity\Utilisateur $patrouilleur
     * @return Patrouille
     */
    public function setPatrouilleur(\Siren\AppBundle\Entity\Utilisateur $patrouilleur)
    {
        $this->patrouilleur = $patrouilleur;

        return $this;
    }

    /**
     * Get patrouilleur
     *
     * @return \Siren\AppBundle\Entity\Utilisateur 
     */
    public function getPatrouilleur()
    {
        return $this->patrouilleur;
    }

    /**
     * Set projet
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Patrouille
     */
    public function setProjet(\Siren\AppBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \Siren\AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
