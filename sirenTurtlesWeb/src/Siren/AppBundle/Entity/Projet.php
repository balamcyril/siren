<?php

namespace Siren\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="villes", type="text", nullable=true)
     */
    private $villes;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Utilisateur", inversedBy="projetsInities")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $auteur;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Participer", mappedBy="projet")
     */
    private $patrouilleurs;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Pays", inversedBy="projets")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Patrouille", mappedBy="projet")
     */
    private $patrouilles;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Question", mappedBy="projet")
     */
    private $questions;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Site", mappedBy="projet")
     */
    private $sites;

    /**
     * Projet constructor.
     */
    public function __construct()
    {
        $this->patrouilles = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->segments = new ArrayCollection();
        $this->patrouilleurs = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Projet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Projet
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set villes
     *
     * @param string $villes
     * @return Projet
     */
    public function setVilles($villes)
    {
        $this->villes = $villes;

        return $this;
    }

    /**
     * Get villes
     *
     * @return string 
     */
    public function getVilles()
    {
        return $this->villes;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Projet
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Projet
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Projet
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set pays
     *
     * @param \Siren\AppBundle\Entity\Pays $pays
     * @return Projet
     */
    public function setPays(\Siren\AppBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Siren\AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set auteur
     *
     * @param \Siren\AppBundle\Entity\Utilisateur $auteur
     * @return Projet
     */
    public function setAuteur(\Siren\AppBundle\Entity\Utilisateur $auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return \Siren\AppBundle\Entity\Utilisateur
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Add patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     * @return Projet
     */
    public function addPatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles[] = $patrouilles;

        return $this;
    }

    /**
     * Remove patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     */
    public function removePatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles->removeElement($patrouilles);
    }

    /**
     * Get patrouilles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatrouilles()
    {
        return $this->patrouilles;
    }

    /**
     * Get patrouilles
     *
     * @return mixed
     */
    public function getPatrouillesOrder()
    {
        $result = array();
        if(count($this->patrouilles) > 0) {
            $result[0] = $this->patrouilles[0];
            for ($i = 1; $i < count($this->patrouilles); $i++) {
                if ($this->patrouilles[$i]->getDateDebut() > $result[0]->getDateDebut())
                    array_unshift($result, $this->patrouilles[$i]);
                else
                    array_push($result, $this->patrouilles[$i]);
            }
        }
        return $result;
    }

    /**
     * Add patrouilleurs
     *
     * @param \Siren\AppBundle\Entity\Participer $patrouilleur
     * @return Projet
     */
    public function addPatrouilleur(\Siren\AppBundle\Entity\Participer $patrouilleur)
    {
        $this->patrouilleurs[] = $patrouilleur;

        return $this;
    }

    /**
     * Remove patrouilles
     *
     * @param \Siren\AppBundle\Entity\Participer $patrouilleur
     */
    public function removePatrouilleur(\Siren\AppBundle\Entity\Participer $patrouilleur)
    {
        $this->patrouilleurs->removeElement($patrouilleur);
    }

    /**
     * Get patrouilleurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatrouilleurs()
    {
        return $this->patrouilleurs;
    }

    /**
     * Get active patrouilleurs
     *
     * @return mixed
     */
    public function getActivePat()
    {
        $result = array();
        foreach ($this->patrouilleurs as $participer){
            if($participer->getEtat() == 1)
                array_push($result, $participer);
        }
        return $result;
    }

    /**
     * Add questions
     *
     * @param \Siren\AppBundle\Entity\Question $question
     * @return Projet
     */
    public function addQuestion(\Siren\AppBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \Siren\AppBundle\Entity\Question $question
     */
    public function removeQuestion(\Siren\AppBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get patrouilles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add site
     *
     * @param \Siren\AppBundle\Entity\Site $site
     * @return Projet
     */
    public function addSite(\Siren\AppBundle\Entity\Site $site)
    {
        $this->sites[] = $site;

        return $this;
    }

    /**
     * Remove site
     *
     * @param \Siren\AppBundle\Entity\Site $site
     */
    public function removeSite(\Siren\AppBundle\Entity\Site $site)
    {
        $this->sites->removeElement($site);
    }

    /**
     * Get sites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSites()
    {
        return $this->sites;
    }



    /**
     * Get if a $user is a patrol boat
     *
     * @return bool
     */

    public function isPatrouilleur(\Siren\AppBundle\Entity\Utilisateur $user){
        foreach ($this->patrouilleurs as $participer){
            if($participer->getPatrouilleur()->getId() == $user->getId()){
                return true;
            }
        }
        return false;
    }

    /**
     * Get questions order by code
     *
     * @return mixed
     */
    public function getQuestionsOrderByCode($categorie = NULL){
        $result = array();
        $continue = true;
        for($i = 0; $i < $this->questions->count(); $i++){
            if($categorie == NULL) {
                $result[0] = $this->questions[$i];
                break;
            }else if($this->questions[$i]->getCategorie()->getId() == $categorie->getId()){
                $result[0] = $this->questions[$i];
                if($i == $this->questions->count()-1)
                    $continue = false;
                break;
            }
        }

        if($continue) {
            for ($i = 1; $i < $this->questions->count(); $i++) {
                if ($categorie == NULL) {
                    if (strcmp($this->questions[$i]->getCode(), $result[0]->getCode()) < 0) {
                        array_unshift($result, $this->questions[$i]);
                    } else {
                        array_push($result, $this->questions[$i]);
                    }
                } else if ($this->questions[$i]->getCategorie()->getId() == $categorie->getId()) {
                    if (strcmp($this->questions[$i]->getCode(), $result[0]->getCode()) < 0) {
                        array_unshift($result, $this->questions[$i]);
                    } else {
                        array_push($result, $this->questions[$i]);
                    }
                }
            }
        }

        return $result;
    }

}
