<?php

namespace Siren\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pays
 *
 * @ORM\Table(name="pays")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomPays_fr", type="string", length=255)
     */
    private $nomPays_fr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nomPays_en", type="string", length=255, nullable=true)
     */
    private $nomPays_en;

    /**
     * @var string
     *
     * @ORM\Column(name="indicatif", type="string", length=255)
     */
    private $indicatif;

    /**
     * @var string
     *
     * @ORM\Column(name="drapeau", type="string", length=255)
     */
    private $drapeau;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Projet", mappedBy="pays")
     */
    private $projets;

    /**
     * Pays constructor.
     * @param $projets
     */
    public function __construct()
    {
        $this->projets = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nomPays
     *
     * @return string 
     */
    public function getNomPays()
    {
        return $this->nomPays_fr;
    }

    /**
     * Set indicatif
     *
     * @param string $indicatif
     * @return Pays
     */
    public function setIndicatif($indicatif)
    {
        $this->indicatif = $indicatif;

        return $this;
    }

    /**
     * Get indicatif
     *
     * @return string 
     */
    public function getIndicatif()
    {
        return $this->indicatif;
    }

    /**
     * Set drapeau
     *
     * @param string $drapeau
     * @return Pays
     */
    public function setDrapeau($drapeau)
    {
        $this->drapeau = $drapeau;

        return $this;
    }

    /**
     * Get drapeau
     *
     * @return string 
     */
    public function getDrapeau()
    {
        return $this->drapeau;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Pays
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    public function getNom($locale = "fr")
    {
        if($locale == "en")
            return $this->nomPays_en."(".$this->indicatif.")";
        return $this->nomPays_fr."(".$this->indicatif.")";
    }

public function __toString()
    {
        return $this->nomPays_fr."(+".$this->indicatif.")";
    }
    /**
     * Set nomPays_fr
     *
     * @param string $nomPaysFr
     * @return Pays
     */
    public function setNomPaysFr($nomPaysFr)
    {
        $this->nomPays_fr = $nomPaysFr;

        return $this;
    }


    /**
     * Get nomPays_fr
     *
     * @return string 
     */
    public function getNomPaysFr()
    {
        return $this->nomPays_fr;
    }

    /**
     * Set nomPays_en
     *
     * @param string $nomPaysEn
     * @return Pays
     */
    public function setNomPaysEn($nomPaysEn)
    {
        $this->nomPays_en = $nomPaysEn;

        return $this;
    }

    /**
     * Get nomPays_en
     *
     * @return string 
     */
    public function getNomPaysEn()
    {
        return $this->nomPays_en;
    }

    /**
     * Add projets
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Pays
     */
    public function addProjet(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projets[] = $projet;

        return $this;
    }

    /**
     * Remove projets
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     */
    public function removeProjet(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projets->removeElement($projet);
    }

    /**
     * Get projets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjets()
    {
        return $this->projets;
    }
}
