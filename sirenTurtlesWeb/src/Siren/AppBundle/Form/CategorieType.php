<?php

namespace Siren\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Siren\AppBundle\Form\ImageType;

class CategorieType extends AbstractType
{
    private $locale;
    
    public function __construct($locale = "fr") {
        $this->locale = $locale;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom_fr', 'text', array("required" => false))
                ->add('nom_en', 'text', array("required" => false))
                ->add('types', 'entity', array(
                                'class' => 'SirenAppBundle:Type',
                                'property' => 'nom_'.$this->locale,
                                'multiple' => true, 
                                'required' => false))
                ->add('image', new ImageType());
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Siren\AppBundle\Entity\Categorie'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'siren_appbundle_categorie';
    }


}
