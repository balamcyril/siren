<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;

/**
 * Description of MapController
 *
 * @author Dorian
 */
class MapController extends Controller {

    //put your code here

    public function getAllPatAction() {
        //$request = $this->get("request");
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->findBy(array('etat' => '1'));

        $i = 0;
        foreach ($patrouilles as $patrouille) {
            $result[$i]["id"] = $patrouille->getId();
            $result[$i]["site"] = $patrouille->getSegment()->getSite()->getNom();
            $result[$i]["segment"] = $patrouille->getSegment()->getNom();
            $result[$i]["traces"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Trace");
            $result[$i]["carcasses"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Carcasse");
            $result[$i]["nids"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Nid");
            $result[$i]["morts"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Mort");
            $result[$i]["vivants"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Vivant");
            $result[$i]["dateDebut"] = $patrouille->getDateDebut()->format("d/m/Y");
            $result[$i]["heureDebut"] = $patrouille->getDateDebut()->format("H:i");
            $result[$i]["dateFin"] = $patrouille->getDateFin()->format("d/m/Y");
            $result[$i]["heureFin"] = $patrouille->getDateFin()->format("H:i");
            $result[$i]["coordXDebut"] = $patrouille->getCoordXDebut();
            $result[$i]["coordYDebut"] = $patrouille->getCoordYDebut();
            $result[$i]["coordXFin"] = $patrouille->getCoordXFin();
            $result[$i]["coordYFin"] = $patrouille->getCoordYFin();
            $result[$i]["nbObs"] = $patrouille->getObservations()->count();
            $result[$i++]["userAdd"] = $patrouille->getPatrouilleur()->getNom();
        }

        return new JsonResponse($result);
    }

    public function getMarkersAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get("request");
        
        $result = array();
        
        $type = $request->request->get("type");
        $debut = $request->request->get("debut");
        $fin = $request->request->get("fin");

        $nid = filter_var($request->request->get("nid"), FILTER_VALIDATE_BOOLEAN);
        $trace = filter_var($request->request->get("trace"), FILTER_VALIDATE_BOOLEAN);
        $carcasse = filter_var($request->request->get("carcasse"), FILTER_VALIDATE_BOOLEAN);
        $vivant = filter_var($request->request->get("vivant"), FILTER_VALIDATE_BOOLEAN);
        $mort = filter_var($request->request->get("mort"), FILTER_VALIDATE_BOOLEAN);

        
        //Transformation des dates en DateTime si nécessaire;

        $dateDebut = new DateTime();
        $dateFin = new DateTime();

        $dateFin->setDate(substr($fin, strrpos($fin, "/") + 1), substr($fin, strpos($fin, "/") + 1, (strrpos($fin, "/") - strpos($fin, "/") - 1)), substr($fin, 0, strpos($fin, "/")));


        if ($debut != "") {
            $dateDebut->setDate(substr($debut, strrpos($debut, "/") + 1), substr($debut, strpos($debut, "/") + 1, (strrpos($debut, "/") - strpos($debut, "/") - 1)), substr($debut, 0, strpos($debut, "/")));
        } else {
            $dateDebut = NULL;
        }

        if ($fin != "") {
            $dateFin->setDate(substr($fin, strrpos($fin, "/") + 1), substr($fin, strpos($fin, "/") + 1, (strrpos($fin, "/") - strpos($fin, "/") - 1)), substr($fin, 0, strpos($fin, "/")));
        } else {
            $dateFin = NULL;
        }

        if ($type == "patrouille") {
            if (($carcasse && $mort && $nid && $vivant && $trace) || (!$carcasse && !$mort && !$nid && !$vivant && !$trace)) {
                $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->getBetween($dateDebut, $dateFin);
            } else {
                $param["carcasse"] = $carcasse;
                $param["mort"] = $mort;
                $param["nid"] = $nid;
                $param["vivant"] = $vivant;
                $param["trace"] = $trace;

                $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->getBetweenWithParam($dateDebut, $dateFin, $param);
            }

            $i = 0;
            foreach ($patrouilles as $patrouille) {
                $result[$i]["id"] = $patrouille->getId();
                $result[$i]["site"] = $patrouille->getSegment()->getSite()->getNom();
                $result[$i]["segment"] = $patrouille->getSegment()->getNom();
                $result[$i]["traces"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Trace");
                $result[$i]["carcasses"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Carcasse");
                $result[$i]["nids"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Nid");
                $result[$i]["morts"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Mort");
                $result[$i]["vivants"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Vivant");
                $result[$i]["dateDebut"] = $patrouille->getDateDebut()->format("d/m/Y");
                $result[$i]["heureDebut"] = $patrouille->getDateDebut()->format("H:i");
                $result[$i]["dateFin"] = $patrouille->getDateFin()->format("d/m/Y");
                $result[$i]["heureFin"] = $patrouille->getDateFin()->format("H:i");
                $result[$i]["coordXDebut"] = $patrouille->getCoordXDebut();
                $result[$i]["coordYDebut"] = $patrouille->getCoordYDebut();
                $result[$i]["coordXFin"] = $patrouille->getCoordXFin();
                $result[$i]["coordYFin"] = $patrouille->getCoordYFin();
                $result[$i]["nbObs"] = $patrouille->getObservations()->count();
                $result[$i++]["userAdd"] = $patrouille->getPatrouilleur()->getNom();
            }
        }
        else{
            if (($carcasse && $mort && $nid && $vivant && $trace) || (!$carcasse && !$mort && !$nid && !$vivant && !$trace)) {
                $observations = $em->getRepository("SirenAppBundle:Observation")->getBetween($dateDebut, $dateFin);
            } else {
                $param["carcasse"] = $carcasse;
                $param["mort"] = $mort;
                $param["nid"] = $nid;
                $param["vivant"] = $vivant;
                $param["trace"] = $trace;

                $observations = $em->getRepository("SirenAppBundle:Observation")->getBetweenWithParam($dateDebut, $dateFin, $param);
            }
            
            $i = 0;
            foreach ($observations as $observation) {
                $result[$i]["id"] = $observation->getId();
                $result[$i]["site"] = $observation->getPatrouille()->getSegment()->getSite()->getNom();
                $result[$i]["segment"] = $observation->getPatrouille()->getSegment()->getNom();
                $result[$i]["type"] = $observation->getCategorie()->getNom($this->get("request")->getLocale());
                $result[$i]["typeid"] = $observation->getCategorie()->getId();
                $result[$i]["coordXDebut"] = $observation->getCoordX();
                $result[$i]["coordYDebut"] = $observation->getCoordY();

                if($observation->getCategorie()->getNom() == "Individu"){
                    if($observation->isMort())
                        $result[$i]["mort"] = true;
                    else
                        $result[$i]["mort"] = false;
                }
                
                $j = 0;
                foreach($observation->getImages() as $image){
                    $result[$i]["images"][$j++] = $image->getWebPath();
                }
                
                $result[$i++]["userAdd"] = $observation->getPatrouille()->getPatrouilleur()->getNom();
            }
        }
        
        return new JsonResponse($result);
    }

}
