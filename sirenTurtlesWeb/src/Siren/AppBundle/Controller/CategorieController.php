<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Siren\AppBundle\Entity\Categorie;
use Siren\AppBundle\Form\CategorieType;
use Siren\AppBundle\Entity\Type;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Description of CategorieController
 *
 * @author Dorian
 */
class CategorieController extends Controller {

    //put your code here
    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $categorie = new Categorie;
        
        $request = $this->get('request');
        $form = $this->createForm(new CategorieType($request->getLocale()), $categorie);

        $result = false;
        if ($request->getMethod() == 'POST') {
            //echo "toto";
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données
                $categorie->setDateAdd(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($categorie);
                $em->flush();
                $result = true;
            }
        }

        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();
        return $this->render('SirenAppBundle:Admin/Patrouille:categorie.html.twig', array(
                    'liste_categories' => $categories,
                    'form' => $form->createView(),
                    'enreg' => $result
        ));
    }

    public function getCategorieAction(Categorie $categorie) {
        $result["result"] = false;
        if ($categorie != null) {
            $result["id"] = $categorie->getId();
            $result["nomFr"] = $categorie->getNomFr();
            $result["nomEn"] = $categorie->getNomEn();

            $i = 0;
            foreach ($categorie->getTypes() as $type) {
                $result["type"][$i]["id"] = $type->getId();
                $result["type"][$i++]["nom"] = $type->getNom();
            }
            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    public function modifAction(Categorie $categorie) {

        $result = false;
        
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CategorieType($request->getLocale()), $categorie);


        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données

                $em->persist($categorie);
                $em->flush();
                $result = true;
            }
        }

        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();
        return $this->render('SirenAppBundle:Admin/Patrouille:categorie.html.twig', array(
                    'liste_categories' => $categories,
                    'form' => $form->createView(),
                    'enreg' => $result
        ));
    }

    public function deleteAction(Categorie $categorie) {

        $result = false;
        if ($categorie != null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush();
            $result = true;
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }
    
    public function getIdAction($titre){
        $json["result"] = false;
        $em = $this->getDoctrine()->getManager();
        
        $categorie = $em->getRepository("SirenAppBundle:Categorie")->findBy(array("nom_fr" => $titre));
        
        if($categorie != null){
            $json["id"] = $categorie[0]->getId();
            $json["result"] = true;
        }
        
        return new JsonResponse($json);
    }

}
