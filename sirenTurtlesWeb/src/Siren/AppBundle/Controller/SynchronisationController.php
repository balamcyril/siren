<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Image;
use Siren\AppBundle\Entity\Observation;
use Siren\AppBundle\Entity\Patrouille;
use Siren\AppBundle\Entity\Projet;
use Siren\AppBundle\Entity\Repondre;
use Siren\AppBundle\Entity\Segment;
use Siren\AppBundle\Entity\Site;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Description of SynchronisationController
 *
 * @author Dorian
 */
class SynchronisationController extends Controller
{

    public function getDatabaseAction(){
        $em = $this->getDoctrine()->getManager();
        $postdata = file_get_contents("php://input");
        $values = (array)json_decode($postdata);

        $userVals = (array) $values["user"];
        $user = $em->getRepository("SirenAppBundle:Utilisateur")->findOneBy(array("email" => $userVals["email"]));
        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        $json = array();

        if (!$encoder->isPasswordValid($user->getPassword(), $userVals["password"], $salt)) {
            $json["success"] = false;
            $json["error"] = "authentification";
        }
        else{
            $types = $em->getRepository("SirenAppBundle:Type")->findAll();
            $i = 0;
            foreach ($types as $type){
                $json["types"][$i]["id"] = $type->getId();
                $json["types"][$i]["nomFr"] = $type->getNomFr();
                $json["types"][$i]["nomEn"] = $type->getNomEn();
                $json["types"][$i]["valeur"] = $type->getValeur();
                $json["types"][$i++]["dateAdd"] = $type->getDateAdd()->getTimestamp();
            }

            $categories = $em->getRepository("SirenAppBundle:Categorie")->findAll();
            $i = 0;
            foreach ($categories as $categorie){
                $json["categories"][$i]["id"] = $categorie->getId();
                $json["categories"][$i]["nomFr"] = $categorie->getNomFr();
                $json["categories"][$i]["nomEn"] = $categorie->getNomEn();
                $json["categories"][$i]["dateAdd"] = $categorie->getDateAdd()->getTimestamp();
                if($categorie->getImage() != null){
                    $path = $categorie->getImage()->getWebPath();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);
                    $json["categories"][$i]["image"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                }

                $j = 0;
                foreach ($categorie->getTypes() as $type) {
                    $json["categories"][$i]["types"][$j++]["id"] = $type->getId();
                }
                $i++;
            }

            $pays = $em->getRepository("SirenAppBundle:Pays")->findAll();
            $i = 0;
            foreach ($pays as $country){
                $json["pays"][$i]["id"] = $country->getId();
                $json["pays"][$i]["nomFr"] = $country->getNomPaysFr();
                $json["pays"][$i]["nomEn"] = $country->getNomPaysEn();
                $json["pays"][$i++]["indicatif"] = $country->getIndicatif();
            }
            $projVals = (array) $values["projet"];
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($projVals["id"]);

            if($projet == NULL){
                $json["success"] = false;
                $json["error"] = "projet";
            }else{
                $i = 0;
                $json["sites"] = array();
                foreach ($projet->getSites() as $site){
                    $json["sites"][$i]["id"] = $site->getId();
                    $json["sites"][$i]["nom"] = $site->getNom();
                    $json["sites"][$i++]["dateAdd"] = $site->getDateAdd()->getTimestamp();
                }

                $segments = $em->getRepository("SirenAppBundle:Segment")->getByProjet($projet);
                $i = 0;
                $json["segments"] = array();
                foreach ($segments as $segment){
                    $json["segments"][$i]["id"] = $segment->getId();
                    $json["segments"][$i]["nom"] = $segment->getNom();
                    $json["segments"][$i]["dateAdd"] = $segment->getDateAdd()->getTimestamp();

                    if ($segment->getPeriodeDebut() != NULL) {
                        $json["segments"][$i]["testDebut"] = true;
                        $json["segments"][$i]["periodeDebut"] = $segment->getPeriodeDebut();
                        $json["segments"][$i]["moisDebut"] = $segment->getMoisDebut();
                    }else
                        $json["segments"][$i]["testDebut"] = NULL;

                    if ($segment->getPeriodePic() != NULL) {
                        $json["segments"][$i]["testPic"] = true;
                        $json["segments"][$i]["periodePic"] = $segment->getPeriodePic();
                        $json["segments"][$i]["moisPic"] = $segment->getMoisPic();
                    }else
                        $json["segments"][$i]["testPic"] = NULL;

                    if ($segment->getPeriodeFin() != NULL) {
                        $json["segments"][$i]["testFin"] = true;
                        $json["segments"][$i]["periodeFin"] = $segment->getPeriodeFin();
                        $json["segments"][$i]["moisFin"] = $segment->getMoisFin();
                    }else
                        $json["segments"][$i]["testFin"] = NULL;

                    $json["segments"][$i++]["idSite"] = $segment->getSite()->getId();

                }

                $questions = $em->getRepository("SirenAppBundle:Question")->getQuestionsProjet($projet);
                $i = 0;
                foreach ($questions as $question){
                    $json["questions"][$i]["id"] = $question->getId();
                    $json["questions"][$i]["code"] = $question->getCode();
                    $json["questions"][$i]["titreFr"] = $question->getTitreFr();
                    $json["questions"][$i]["titreEn"] = $question->getTitreEn();
                    $json["questions"][$i]["type"] = $question->getType();
                    //$json["questions"][$i]["niveau"] = $question->Niveau();
                    $json["questions"][$i]["categorie"] = $question->getCategorie()->getId();
                    $json["questions"][$i]["dateAdd"] = $question->getDateAdd()->getTimestamp();
                    if($question->getImage() != null){
                        $path = $question->getImage()->getWebPath();
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($path);
                        $json["questions"][$i]["image"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }

                    if($question->getProjet() != null)
                        $json["questions"][$i]["idProjet"] = $question->getProjet()->getId();
                    else
                        $json["questions"][$i]["idProjet"] = NULL;

                    $i++;
                }

                $reponses = $em->getRepository("SirenAppBundle:Reponse")->getReponsesProjet($projet);
                $i = 0;
                foreach ($reponses as $reponse){
                    $json["reponses"][$i]["id"] = $reponse->getId();;
                    $json["reponses"][$i]["valeurFr"] = $reponse->getValeurFr();
                    $json["reponses"][$i]["valeurEn"] = $reponse->getValeurEn();
                    $json["reponses"][$i]["question"] = $reponse->getQuestion()->getId();
                    $json["reponses"][$i]["dateAdd"] = $reponse->getDateAdd()->getTimestamp();
                    if($reponse->getImage() != null){
                        $path = $reponse->getImage()->getWebPath();
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($path);
                        $json["reponses"][$i]["image"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }

                    if($reponse->getSaut() != null)
                        $json["reponses"][$i]["idSaut"] = $reponse->getSaut()->getId();
                    else
                        $json["reponses"][$i]["idSaut"] = NULL;

                    if($reponse->getQuestionSuivante() != null)
                        $json["reponses"][$i]["idQuestionSuivante"] = $reponse->getQuestionSuivante()->getId();
                    else
                        $json["reponses"][$i]["idQuestionSuivante"] = NULL;

                    if($reponse->getSuggestion() != null)
                        $json["reponses"][$i]["idSuggestion"] = $reponse->getSuggestion()->getId();
                    else
                        $json["reponses"][$i]["idSuggestion"] = NULL;

                    if($reponse->getFils() != null)
                        $json["reponses"][$i]["idFils"] = $reponse->getFils()->getId();
                    else
                        $json["reponses"][$i]["idFils"] = NULL;

                    $i++;
                }

                $sauts = $em->getRepository("SirenAppBundle:Saut")->findAll();
                $i = 0;
                foreach ($sauts as $saut){
                    $json["sauts"][$i]["id"] = $saut->getId();
                    $json["sauts"][$i++]["dateAdd"] = $saut->getDateAdd()->getTimestamp();
                }

                $sautQuestions = $em->getRepository("SirenAppBundle:SautQuestion")->findAll();
                $i = 0;
                foreach ($sautQuestions as $sautQuestion){
                    $json["sautQuestions"][$i]["id"] = $sautQuestion->getId();
                    $json["sautQuestions"][$i]["position"] = $sautQuestion->getPosition();
                    $json["sautQuestions"][$i]["idQuestion"] = $sautQuestion->getQuestion()->getId();
                    $json["sautQuestions"][$i++]["idSaut"] = $sautQuestion->getSaut()->getId();
                }

                $json["result"] = true;
            }

        }

        return new JsonResponse($json);
    }

    public function getProjetUserAction(){
        $em = $this->getDoctrine()->getManager();
        $postdata = file_get_contents("php://input");
        $values = (array)json_decode($postdata);

        $userVals = (array) $values["user"];
        $user = $em->getRepository("SirenAppBundle:Utilisateur")->findOneBy(array("email" => $userVals["email"]));
        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        $json = array();

        if (!$encoder->isPasswordValid($user->getPassword(), $userVals["password"], $salt)) {
            $json["success"] = false;
            $json["error"] = "authentification";
        }
        else{
            $projets = array();
            $i = 0;
            foreach ($user->getProjetsInities() as $monProjet){
                $projets[$i]["id"] = $monProjet->getId();
                $projets[$i]["nom"] = $monProjet->getNom();
                $projets[$i]["lieu"] = $monProjet->getLieu();
                $projets[$i]["etat"] = $monProjet->getEtat();
                $projets[$i]["nbPatrouilles"] = count($monProjet->getPatrouilles());
                $projets[$i]["dateAdd"] = $monProjet->getDateAdd()->getTimestamp();
                $projets[$i]["dateDebut"] = $monProjet->getDateDebut()->getTimestamp();
                $projets[$i]["nomAuteur"] = $monProjet->getAuteur()->getNom();
                $projets[$i++]["prenomAuteur"] = $monProjet->getAuteur()->getPrenom();
            }

            foreach ($user->getProjetsParticipes() as $monProjet){
                $projets[$i]["id"] = $monProjet->getProjet()->getId();
                $projets[$i]["nom"] = $monProjet->getProjet()->getNom();
                $projets[$i]["lieu"] = $monProjet->getProjet()->getLieu();
                $projets[$i]["etat"] = $monProjet->getProjet()->getEtat();
                $projets[$i]["nbPatrouilles"] = count($monProjet->getProjet()->getPatrouilles());
                $projets[$i]["dateAdd"] = $monProjet->getProjet()->getDateAdd()->getTimestamp();
                $projets[$i]["dateDebut"] = $monProjet->getProjet()->getDateDebut()->getTimestamp();
                $projets[$i]["nomAuteur"] = $monProjet->getProjet()->getAuteur()->getNom();
                $projets[$i++]["prenomAuteur"] = $monProjet->getProjet()->getAuteur()->getPrenom();
            }
            $json["projets"] = $projets;
            $json["success"] = true;
        }

        return new JsonResponse($json);
    }

    public function synchroniseAction()
    {
        $em = $this->getDoctrine()->getManager();
        $postdata = file_get_contents("php://input");

        $values = (array)json_decode($postdata);

        $json["error"] = true;
        $patrouilles = (array) $values["patrouilles"];
        //$json["recu"] = var_dump($patrouilles);
        $i = 0;
        if (is_array($patrouilles) && count($patrouilles) > 0) {
            foreach ($patrouilles as $patValue) {
                $patValue = (array) $patValue;
                $i++;
                //$json["recu"] = var_dump($patValue);
                $patrouille = new Patrouille();
                //var_dump($i);
                $patrouille->setCoordXDebut($patValue["coordXDebut"]);
                $patrouille->setCoordXFin($patValue["coordXFin"]);
                $patrouille->setCoordYDebut($patValue["coordYDebut"]);
                $patrouille->setCoordYFin($patValue["coordYFin"]);
                $patrouille->setNomSite($patValue["nomSite"]);
                $patrouille->setEtat($patValue["etat"]);
                //$envoi=$patValue["envoi"];
                $envoi=false;
                
                $date = new \DateTime();
                $date->setTimestamp($patValue["dateDebut"]);
                $patrouille->setDateDebut($date);
                $date->setTimestamp($patValue["dateFin"]);
                $patrouille->setDateFin($date);

                $patrouilleur = $em->getRepository("SirenAppBundle:Utilisateur")->find($patValue["patrouilleur_id"]);
                $patrouille->setPatrouilleur($patrouilleur);

                $segment = $em->getRepository("SirenAppBundle:Segment")->findOneBy(array("nom" => $patValue["segment_nom"]));
                if ($segment == null) {
                    $segment = new Segment();
                    $segment->setNom($patValue["segment_nom"]);
                    $em->persist($segment);
                }
                $patrouille->setSegment($segment);

                $type = $em->getRepository("SirenAppBundle:Type")->find($patValue["type_id"]);
                $patrouille->setType($type);

                $em->persist($patrouille);
                 $trace=0;
                $nid=0;
                $individu=0;
                $reste=0;
                
                foreach ($patValue["observations"] as $obs) {
                    $obs = (array) $obs;
                    $observation = new Observation();

                    $date->setTimestamp($obs["dateAdd"]);
                    $observation->setDateAdd($date);
                    $observation->setEtat($obs["etat"]);
                    $observation->setCoordX($obs["coordX"]);
                    $observation->setCoordY($obs["coordY"]);

                     if($myObs["categorie_id"]==1)
                    {
                            $trace+=1;
                    }
                    if($myObs["categorie_id"]==3)
                    {
                            $nid+=1;
                    }
                    if($myObs["categorie_id"]==4)
                    {
                            $individu+=1;
                    }
                    if($myObs["categorie_id"]==2)
                    {
                            $reste+=1;
                    }
                    
                    $categorie = $em->getRepository("SirenAppBundle:Categorie")->find($obs["categorie_id"]);
                    $observation->setCategorie($categorie);

                    /*foreach ($obs["reponses"] as $valeur) {
                        $repondre = new Repondre();

                        $question = $em->getRepository("SirenAppBundle:Question")->find($valeur["question_id"]);
                        $repondre->setQuestion($question);

                        if ($question->getType() == "qcm") {
                            $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($valeur["reponse_id"]);
                            $repondre->setReponse($reponse);
                        }
                        elseif ($question->getType() == "qro"){
                            $repondre->setReponseText($valeur["reponseText"]);
                        }
                        $em->persist($repondre);
                        $observation->addReponse($repondre);
                    }*/
                    $observation->setPatrouille($patrouille);
                    $em->persist($observation);
                }
                if($envoi)
                {
                    $this->send($patrouille->getId(),$trace,$nid,$individu,$reste);
                }
            }
            $json["error"] = false;
            $em->flush();
        }

        return new JsonResponse($json);
    }

    public function connexionAction(){

        $postdata = file_get_contents("php://input");
        $values = (array)json_decode($postdata);

        $request = $this->get("request");
        $em = $this->getDoctrine()->getManager();

        $json = array();

        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');

        /// Start retrieve user
        // Let's retrieve the user by its username:
        // If you are using FOSUserBundle:
        $user_manager = $this->get('fos_user.user_manager');

        $user = $user_manager->findUserByUsernameOrEmail($values["email"]);
        /// End Retrieve user

        // Check if the user exists !
        if(!$user){
            $json["success"] = false;
        }else {

            /// Start verification
            $encoder = $factory->getEncoder($user);
            $salt = $user->getSalt();

            if (!$encoder->isPasswordValid($user->getPassword(), $values["password"], $salt)) {
                $json["success"] = false;
            }else {
                /// End Verification

                // The password matches ! then proceed to set the user in session

                //Handle getting or creating the user entity likely with a posted form
                // The third parameter "main" can change according to the name of your firewall in security.yml
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);

                // If the firewall name is not main, then the set value would be instead:
                // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
                $this->get('session')->set('_security_main', serialize($token));

                // Fire the login event manually
                $event = new InteractiveLoginEvent($request, $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

                /*
                 * Now the user is authenticated !!!!
                 * Do what you need to do now, like render a view, redirect to route etc.
                 */
                $json["success"] = true;
                $json["user"]["id"] = $user->getId();
                $json["user"]["username"] = $user->getUsername();
                $json["user"]["email"] = $user->getEmail();

                if($user->getImage() != null){
                    $path = $user->getImage()->getWebPath();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);
                    $json["user"]["image"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                }

                if(count($user->getProjetsInities()) == 0){
                    $projet = new Projet();
                    $projet->setDateAdd(new \DateTime());
                    $projet->setNom("Mon premier projet");
                    $projet->setAuteur($user);
                    $projet->setDateDebut(new \DateTime());
                    $projet->setEtat(1);

                    $em->persist($projet);
                    $em->flush();
                }

                $lastProjet = $user->getLastProjet();
                if($lastProjet != NULL){
                    $json["user"]["lastProjet"]["id"] = $lastProjet->getId();
                    $json["user"]["lastProjet"]["nom"] = $lastProjet->getNom();
                    $json["user"]["lastProjet"]["lieu"] = $lastProjet->getLieu();
                    $json["user"]["lastProjet"]["etat"] = $lastProjet->getEtat();
                    $json["user"]["lastProjet"]["nbPatrouilles"] = count($lastProjet->getPatrouilles());
                    $json["user"]["lastProjet"]["dateAdd"] = $lastProjet->getDateAdd()->getTimestamp();
                    $json["user"]["lastProjet"]["dateDebut"] = $lastProjet->getDateDebut()->getTimestamp();
                    $json["user"]["lastProjet"]["nomAuteur"] = $lastProjet->getAuteur()->getNom();
                    $json["user"]["lastProjet"]["prenomAuteur"] = $lastProjet->getAuteur()->getPrenom();
                }
                else
                    $json["user"]["lastProjet"] = NULL;

            }
        }

        return new JsonResponse($json);

    }

    public function savePatrouilleAction(){
        $postdata = file_get_contents("php://input");
        $values = (array)json_decode($postdata);
        $em = $this->getDoctrine()->getManager();
        $userVals = (array) $values["user"];
        $user = $em->getRepository("SirenAppBundle:Utilisateur")->findOneBy(array("email" => $userVals["email"]));
        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        $json = array();

       // $json["tata"] = $userVals;
        if (!$encoder->isPasswordValid($user->getPassword(), $userVals["password"], $salt)) {
            $json["success"] = false;
            $json["error"] = "authentification";
        }
        else{
            $patVals = (array) $values["patrouille"];
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($patVals["projet_id"]);
            if($projet != NULL){
                $type = $em->getRepository("SirenAppBundle:Type")->find($patVals["type_id"]);
                //$json["tata"] = $type;
                if($type != NULL){
                    $site = $em->getRepository("SirenAppBundle:Site")->findOneBy(array("nom" => $patVals["nomSite"], "projet" => $projet));
                    if($site == NULL){
                        $site = new Site();
                        $site->setNom($patVals["nomSite"]);
                        $site->setProjet($projet);

                        $em->persist($site);
                    }

                    $segment = $em->getRepository("SirenAppBundle:Segment")->findOneBy(array("nom" => $patVals["nomSegment"], "site" => $site));
                    if($segment == NULL){
                        $segment = new Segment();
                        $segment->setNom($patVals["nomSegment"]);

                        $testDebut = (isset($patVals["testDebut"])?$patVals["testDebut"]:false);
                        $testPic = (isset($patVals["testPic"])?$patVals["testPic"]:false);
                        $testFin = (isset($patVals["testFin"])?$patVals["testFin"]:false);

                        if ($testDebut) {
                            $periode = $patVals["periodeDebut"];
                            $mois = $patVals["moisDebut"];
                            $segment->setPeriodeDebut($periode);
                            $segment->setMoisDebut($mois);
                        }

                        if ($testPic) {
                            $periode = $patVals["periodePic"];
                            $mois = $patVals["moisPic"];
                            $segment->setPeriodePic($periode);
                            $segment->setMoisPic($mois);
                        }

                        if ($testFin) {
                            $periode = $patVals["periodeFin"];
                            $mois = $patVals["moisFin"];
                            $segment->setPeriodeFin($periode);
                            $segment->setMoisFin($mois);
                        }
                        
                        $segment->setSite($site);

                        $em->persist($segment);
                    }


                    $patrouille = new Patrouille();
                    $patrouille->setProjet($projet);
                    $patrouille->setSegment($segment);
                    $patrouille->setPatrouilleur($user);
                    $patrouille->setType($type);
                    $patrouille->setEtat($patVals["etat"]);
                    $patrouille->setCoordXDebut($patVals["coordXDebut"]);
                    $patrouille->setCoordYDebut($patVals["coordYDebut"]);
                    $patrouille->setCoordXFin($patVals["coordXFin"]);
                    $patrouille->setCoordYFin($patVals["coordYFin"]);
                    //$envoi=$patVals["envoi"];
                    $envoi=false;
                
                    $date = new \DateTime();
                    $date->setTimestamp($patVals["dateDebut"] / 1000);
                    $patrouille->setDateDebut($date);

                    $dateFin = new \DateTime();
                    $dateFin->setTimestamp($patVals["dateFin"] / 1000);
                    $patrouille->setDateFin($dateFin);
                    $em->persist($patrouille);
                    $trace=0;
                    $nid=0;
                    $individu=0;
                    $reste=0;
                
                    $obsVals = (array) $values["observations"];
                    foreach ($obsVals as $obs){
                        $obs = (array) $obs;
                        $observation = new Observation();
                        $observation->setEtat($patVals["etat"]);
                        $observation->setPatrouille($patrouille);
                        $myObs = (array) $obs["observation"];
                        $observation->setCoordX($myObs["coordX"]);
                        $observation->setCoordY($myObs["coordY"]);
                        $dateAdd = new \DateTime();
                        $dateAdd->setTimestamp($myObs["dateAdd"] / 1000);
                        $observation->setDateAdd($dateAdd);

                         if($myObs["categorie_id"]==1)
                    {
                            $trace+=1;
                    }
                    if($myObs["categorie_id"]==3)
                    {
                            $nid+=1;
                    }
                    if($myObs["categorie_id"]==4)
                    {
                            $individu+=1;
                    }
                    if($myObs["categorie_id"]==2)
                    {
                            $reste+=1;
                    }
                    
                        $categorie = $em->getRepository("SirenAppBundle:Categorie")->find($myObs["categorie_id"]);
                        $observation->setCategorie($categorie);

                        $em->persist($observation);

                        $repVals = (array) $obs["reponses"];
                        foreach ($repVals as $value){
                            $value = (array) $value;
                            $repondre = new Repondre();
                            $repondre->setObservation($observation);

                            $question = $em->getRepository("SirenAppBundle:Question")->find($value["question_id"]);
                            $repondre->setQuestion($question);
                            if($question->getType() == "qcm"){
                                $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($value["reponse_id"]);
                                $repondre->setReponse($reponse);
                            }else{
                                $repondre->setReponseText($value["reponseText"]);
                            }

                            $em->persist($repondre);
                        }
                        

                        $em->persist($site);
                        
                        
                        if (isset($obs["images"]))
                        {
                            $imVals = (array) $obs["images"];
                        } else {
                            $imVals  = array();
                        }
                        //$json["tata"] = $imVals;

                        foreach ($imVals as $value){
                            $value = (array) $value;
                            $image = new Image();
                            $photo = $value["chemin"];

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/uploads/images/';
                                    //$webPath = str_replace('app\\', '', $webPath);
                                    $sourcename = $webPath . uniqid() . '.' . $extension;

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode($photo[1]));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $image->setChemin($photoUrl);
                                    $image->setObservation($observation);

                                    $em->persist($image);
                                    //$json["source"] = $sourcename;
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
                        }
                    }
                    if($envoi)
                    {
                        $this->send($patrouille->getId(),$trace,$nid,$individu,$reste);
                    }
                
                    $json["success"] = true;
                    $em->flush();
                }else{
                    $json["success"] = false;
                    $json["error"] = "type";
                }
            }else{
                $json["success"] = false;
                $json["error"] = "projet";
            }
        }

        return new JsonResponse($json);
    }

    /**
     * This method registers an user in the database manually.
     *
     **/
    public function registerAction(){
        $postdata = file_get_contents("php://input");
        $values = (array) json_decode($postdata);

        $json = array();

        $userManager = $this->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getManager();

        $email = $values["email"];
        $password = $values["password"];
        $username = $values["username"];

        // Or you can use the doctrine entity manager if you want instead the fosuser manager
        // to find
        //$em = $this->getDoctrine()->getManager();
        //$usersRepository = $em->getRepository("mybundleuserBundle:User");
        // or use directly the namespace and the name of the class
        // $usersRepository = $em->getRepository("mybundle\userBundle\Entity\User");
        //$email_exist = $usersRepository->findOneBy(array('email' => $email));

        $email_exist = $userManager->findUserByEmail($email);
        $username_exist = $userManager->findUserByUsername($username);

        // Check if the user exists to prevent Integrity constraint violation error in the insertion
        if($email_exist || $username_exist){
            $json["error"] = "utilise";
            $json["success"] = false;
        }else {
            $nom = $values["nom"];
            $prenom = $values["prenom"];

            $user = $userManager->createUser();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setEmailCanonical($email);
            $user->setLocked(0); // don't lock the user
            $user->setEnabled(0); // enable the user or enable it later with a confirmation token in the email
            // this method will encrypt the password with the default settings :)
            $user->setPlainPassword($password);

            $user->setNom($nom);
            $user->setPrenom($prenom);
            $user->setOrganisation($values["organisation"]);
            $user->addRole("ROLE_USER");
            $user->setTelephone($values["telephone"]);
            $user->setVille($values["ville"]);

            $json["user"]["id"] = $user->getId();
            $json["user"]["username"] = $user->getUsername();
            $json["user"]["email"] = $user->getEmail();
            if ($user->getImage() != null){
                $path = $user->getImage()->getWebPath();
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $json["user"]["image"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }

            $mailer = $this->container->get('fos_user.mailer');

            // Create token
            $token = sha1(uniqid(mt_rand(), true)); // Or whatever you prefer to generate a token
            $user->setConfirmationToken($token);
            $mailer->sendConfirmationEmailMessage($user);
            $userManager->updateUser($user);

            $json["success"] = true;
        }
        return new JsonResponse($json);
    }

    function resetPasswordAction(){
        $postdata = file_get_contents("php://input");
        $values = (array) json_decode($postdata);

        $json = array();

        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($values["email"]);

        if (null === $user) {
            $json["error"] = "not exist";
            $json["success"] = false;
        }else if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            $json["error"] = "sented";
            $json["success"] = false;
        }else{
            if (null === $user->getConfirmationToken()) {
                /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
            $user->setPasswordRequestedAt(new \DateTime());
            $this->container->get('fos_user.user_manager')->updateUser($user);
            $json["success"] = true;
        }

        return new JsonResponse($json);

    }
    public function get_distance_m($lat1, $lng1, $lat2, $lng2) {
	  $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
	  $rlo1 = deg2rad($lng1);
	  $rla1 = deg2rad($lat1);
	  $rlo2 = deg2rad($lng2);
	  $rla2 = deg2rad($lat2);
	  $dlo = ($rlo2 - $rlo1) / 2;
	  $dla = ($rla2 - $rla1) / 2;
	  $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo
	));
	  $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
	  return ($earth_radius * $d);
	}
	
	public function send($pat,$trace,$nid,$individu,$reste)
	{
	    $em = $this->getDoctrine()->getManager();
	    $patrouille = $em->getRepository("SirenAppBundle:Patrouille")->find($pat);
	    
	    $dd1=$patrouille->getDateDebut();
		$dd2=$patrouille->getDateFin();
		$interval = $dd1->diff($dd2);
		
	    if(1)
		{
			$objet='Recapitulatif de la patrouille '.$patrouille->getSegment()->getNom().'du ';
		}
		else
		{
			$objet='Summary of the patrol';
		}
		
		$distance=1;
		
	    if(1){$tt='Détails de la patrouille';}else{$tt='Details of the patrol';}
		if(1){$sg='Segment';}else{$sg='Segment';}
		if(1){$st='Nom du site';}else{$st='Site name';}
		if(1){$hd='Heure de début';}else{$hd='Start time';}
		if(1){$hf='Heure de fin';}else{$hf='End time';}
		if(1){$dp='Durée de la patrouille';}else{$dp='Duration of the patrol';}
		if(1){$dsp='Distance parcourue';}else{$dsp='Distance traveled';}
		if(1){$ro='Récapitulatif des observations';}else{$ro='Summary of observations';}
		if(1){$nt='Nombre de traces';}else{$nt='Number of traces';}
		if(1){$nn='Nombre de nids';}else{$nn='Number of nests';}
		if(1){$ni='Nombre d\'individus vivants ou morts';}else{$ni='Number of living or dead individuals';}
		if(1){$nc='Nombre de carcasses(restes)';}else{$nc='Number of carcasses(remains)';}
		
		
		$message = (new \Swift_Message($objet))
        ->setFrom('ammcosiren@gmail.com')
        ->setTo(['wtakoutsing@gmail.com','grmcteam@gmail.com'])
        ->setBody('<html><head><style>
    @page
    {
        margin:14px;
        padding:10px;
    }
    .form_connect table{
        margin: 3px;
        width:100%;
        border-collapse: collapse;

    }
    .form_connect table th{
        border: 1px #999999 outset;
        text-align: center;
        color: black;
        background-color:white;
        font-weight: bold;

    }
    .form_connect table tr td{
        background-color: white;
        border: 1px #999999 solid;
        text-align: center;
        font-size:12px;
        padding:2px;
    }

    .form_connect2 table{
        margin: 3px;

    }
    .form_connect2 table th{
        background-color: #37c0fb ;

    }
    .form_connect2 table tr td{
        background-color: white;

    }
    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }

</style></head><body>

<div style="width:700px; margin:auto; font-size:12px;">
    <div class="form_connect box-body" style="text-align:center">
        <h3 align="center" >'.$tt.'</h3>

        <div class="table-responsive table-striped" >
            <table class="border: 1px #000 solid;">
                <tr><th>'.$sg.'</th><td>odfe</td></tr>
                <tr><th>'.$st.'</th><td>dslke</td></tr>
                <tr><th>'.$hd.'</th><td>d1</td></tr>
                <tr><th>'.$hf.'</th><td>d2</td></tr>
                <tr><th>'.$dp.'</th><td>d3</td></tr>
                <tr><th>'.$dsp.'</th><td>'.$distance.' m</td></tr>

            </table>
        </div><br>
        <h3 align="center" >'.$ro.'</h3>

        <div class="table-responsive table-striped" >
            <table class="border: 1px #000 solid;">
                <tr><th>'.$nt.'</th><td>'.$trace.'</td></tr>
                <tr><th>'.$nn.'</th><td>'.$nid.'</td></tr>
                <tr><th>'.$ni.'</th><td>'.$individu.'</td></tr>
                <tr><th>'.$nc.'</th><td>'.$reste.'</td></tr>

            </table>
        </div><br>

    </div>

</div>

</body></html>', 'text/html');

    $this->get('mailer')->send($message);

    return 1;

	}
}
