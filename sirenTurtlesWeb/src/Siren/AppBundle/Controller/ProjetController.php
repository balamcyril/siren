<?php
/**
 * Created by PhpStorm.
 * User: njami
 * Date: 07/11/2017
 * Time: 04:33
 */

namespace Siren\AppBundle\Controller;


use DateTime;
use Siren\AppBundle\Entity\Participer;
use Siren\AppBundle\Entity\Projet;
use Siren\AppBundle\Entity\Utilisateur;
use Siren\AppBundle\Form\ProjetType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProjetController extends Controller
{
    function mesProjetsAction(){
        $user = $this->get('security.context')->getToken()->getUser();

        $projetsInities = $user->getProjetsInities();
        $projetsParticipes = $user->getProjetsParticipes();


        $projet = new Projet();
        $form = $this->createForm(new ProjetType, $projet);

        return $this->render('SirenAppBundle:User/Projet:projets.html.twig', array(
            'projets_inities' => $projetsInities,
            'projets_participes' => $projetsParticipes,
            'form' => $form->createView()
        ));
    }

    function getProjetAction(Projet $projet){
        $result["result"] = false;
        if ($projet != null) {
            $result["id"] = $projet->getId();
            $result["nom"] = $projet->getNom();
            $result["lieu"] = $projet->getLieu();
            $result["pays"] = $projet->getPays()->getId();
            $result["dateDebut"] = $projet->getDateDebut()->format("Y-m-d");

            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    function newAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->get("request");
        $dateDebut = $request->request->get("dateDebut");

        $projet = new Projet();
        $form = $this->createForm(new ProjetType($request->getLocale()), $projet);

        $result = false;
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données
                $projet->setDateAdd(new \DateTime());
                $projet->setAuteur($user);
                $projet->setDateDebut(new DateTime($dateDebut));
                $projet->setEtat(1);

                $em->persist($projet);
                $em->flush();
                $result = true;
            }
        }

        $projetsInities = $user->getProjetsInities();
        $projetsParticipes = $user->getProjetsParticipes();

        return $this->render('SirenAppBundle:User/Projet:projets.html.twig', array(
            'projets_inities' => $projetsInities,
            'projets_participes' => $projetsParticipes,
            'form' => $form->createView(),
            'enreg' => $result
        ));
    }

    function addUserAction(Projet $projet, $userval){
        $result = false;
        $em = $this->getDoctrine()->getManager();
        $username = explode("-", $userval);
        $username = $username[0];

        $user = $em->getRepository("SirenAppBundle:Utilisateur")->findOneBy(array("username" => $username));
        if($user != NULL){

            $participer = new Participer($user, $projet);
            $participer->setEtat(1);

            $em->persist($participer);
            $em->flush();

            $result=true;
            $json["id"] = $participer->getId();
            $json["nom"] = $user->getNom()." ".$user->getPrenom();
            $json["username"] = "@".$user->getUsername();
        }

        $json["result"] = $result;
        return new JsonResponse($json);
    }

    function removeUserAction(Participer $participer){
        $result = false;
        $em = $this->getDoctrine()->getManager();
        if($participer != NULL){
            $participer->setEtat(0);

            $em->persist($participer);
            $em->flush();

            $result=true;
        }

        $json["result"] = $result;
        return new JsonResponse($json);
    }

    function patrouillesFromAction(Projet $projet){

    }

    function modifAction(Projet $projet){
        $user = $this->get('security.context')->getToken()->getUser();
        $result = false;
        $request = $this->get("request");
        $form = $this->createForm(new ProjetType($request->getLocale()), $projet);

        $dateDebut = $request->request->get("dateDebut");

        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données
                $projet->setDateDebut(new DateTime($dateDebut));

                $em = $this->getDoctrine()->getManager();
                $em->persist($projet);
                $em->flush();
                $result = true;
            }
        }

        $projetsInities = $user->getProjetsInities();
        $projetsParticipes = $user->getProjetsParticipes();
        return $this->render('SirenAppBundle:User/Projet:projets.html.twig', array(
            'projets_inities' => $projetsInities,
            'projets_participes' => $projetsParticipes,
            'form' => $form->createView(),
            'enreg' => $result
        ));
    }

    function deleteAction(Projet $projet){
        $result = false;
        if ($projet != null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($projet);
            $em->flush();
            $result = true;
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }

}