<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Projet;
use Siren\AppBundle\Entity\Site;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Siren\AppBundle\Entity\Type;
use Siren\AppBundle\Form\TypeType;
use Siren\AppBundle\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\JsonResponse;
use Siren\AppBundle\Entity\Image;
use Siren\AppBundle\Entity\Patrouille;
use Siren\AppBundle\Entity\Observation;
use Siren\AppBundle\Entity\Segment;
use Siren\AppBundle\Entity\Repondre;
use DateTime;

/**
 * Description of PatrouilleController
 *
 * @author Dorsin
 */
class PatrouilleController extends Controller {
    
    public function typeAction() {
        $em = $this->getDoctrine()->getManager();
        
        $types = $em->getRepository('SirenAppBundle:Type')->findAll();
              
        $type = new Type;
        $form = $this->createForm(new TypeType, $type);
        
        return $this->render('SirenAppBundle:Admin/Patrouille:type.html.twig', array(
            'liste_types' => $types,
            'form' => $form->createView()
        ));
    }
    
    public function categorieAction() {
        return $this->redirect($this->generateUrl('siren_useradmin_patrouille_categorie_new'));
    }
    
    public function questionAction() {
        return $this->redirect($this->generateUrl('siren_useradmin_patrouille_question_new'));
    }
    
    public function patrouillesFromAction(Projet $projet){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $session->set("idProjet", $projet->getId());

        $patrouilles = $projet->getPatrouillesOrder();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("SirenAppBundle:Utilisateur")->findAll();

        $goodUsers = array();
        foreach ($users as $aUser){
            if(!in_array("ROLE_ADMIN", $aUser->getRoles()) && $user->getId() != $aUser->getId() && !$projet->isPatrouilleur($aUser)){
                array_push($goodUsers, $aUser);
            }
        }
        $session->remove("valueSet");
        $isAuthor = false;
        if($user->getId() == $projet->getAuteur()->getId())
            $isAuthor = true;
        
        return $this->render('SirenAppBundle:User/Patrouille:patrouille.html.twig', array(
            'liste_patrouilles' => $patrouilles,
            'projet' => $projet,
            'usersAdd' => $goodUsers,
            'auteur' => $isAuthor
        ));
    }
    
    public function startAction(){
        $session = $this->get("session");
        $request = $this->get("request");
        $em = $this->getDoctrine()->getManager();

        if(($session->get("valueSet") == NULL)) {
            $session->set("nomSite", $request->request->get("nomSite"));
            $session->set("nomSegment", $request->request->get("nomSegment"));
            $session->set("dateDebut", $request->request->get("dateDebut"));
            $session->set("heureDebut", $request->request->get("heureDebut"));
            $session->set("minDebut", $request->request->get("minDebut"));
            $session->set("dateFin", $request->request->get("dateFin"));
            $session->set("heureFin", $request->request->get("heureFin"));
            $session->set("minFin", $request->request->get("minFin"));
            $session->set("distance", $request->request->get("distance"));
            $session->set("coordX", $request->request->get("coordX"));
            $session->set("coordY", $request->request->get("coordY"));

            $session->set("valueSet", true);
        }
        $session->remove("projetTest");

        $user = $this->get('security.context')->getToken()->getUser();
        $projet = $session->get("idProjet");
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($projet);
        $isAuthor = false;
        if($user->getId() == $projet->getAuteur()->getId())
            $isAuthor = true;

        $categories = $em->getRepository("SirenAppBundle:Categorie")->findAll();
        //var_dump($session->all());
        return $this->render('SirenAppBundle:User/Patrouille:startPatrouille.html.twig', array(
            "liste_categories" => $categories,
            'projet' => $projet,
            'auteur' => $isAuthor
        ));
    }
    
    public function saveTempImageAction($num){
        $result["result"] = false;
        
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $this->get("session");
        
        $files = $request->files->get("image");
        $i = 0;
        $valeur = "";
        foreach ($files as $file){
            $image = new Image();
            $image->setFile($file);
            
            $em->persist($image);
            $result["result"] = true;
            
            //$session->set("image-".$i++, );
            $valeur .= $image->getId() . "|";
        }
        $session->set("image-".$num, $valeur);
        $em->flush();
        
        return new JsonResponse($result);
    }
    
    public function saveAction(){
        $json["result"] = false;
        $session = $this->get("session");

        $request = $this->get("request");
        $user= $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        $dateAdd = new DateTime();

        $type = $em->getRepository("SirenAppBundle:Type")->find(1);
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));
        $segment = $em->getRepository("SirenAppBundle:Segment")->findOneBy(array("nom" => $session->get("nomSegment")));
        $site = $em->getRepository("SirenAppBundle:Site")->findOneBy(array("nom" => $session->get("nomSite"), "projet" => $projet));


        if($site == NULL){
            $site = new Site();
            $site->setNom($session->get("nomSite"));
            $site->setProjet($projet);
            $em->persist($site);
        }

        if($segment == NULL){
            $segment = new Segment();
            $segment->setNom($session->get("nomSegment"));
            $segment->setSite($site);
            $em->persist($segment);
        }

        $temp = new \Symfony\Component\Serializer\Encoder\JsonDecode(true);
        $data = $temp->decode($request->getContent(), \Symfony\Component\Serializer\Encoder\JsonEncoder::FORMAT);
        $tabFinal = $data["data"];
        $patrouille = new Patrouille();
        //$patrouille->setNomSite($session->get("nomSite"));
        $patrouille->setEtat($tabFinal["public"] == true ? 1 : 0 );
        $patrouille->setPatrouilleur($user);
        $patrouille->setType($type);
        $patrouille->setSegment($segment);
        $patrouille->setProjet($projet);
        $patrouille->setCoordXDebut($session->get("coordX"));
        $patrouille->setCoordYDebut($session->get("coordY"));

        $dateDebut = new DateTime();
        $dateFin = new DateTime();
        $debut = $session->get("dateDebut");
        $fin = $session->get("dateFin");
        
        $dateDebut->setDate(substr($debut, strrpos($debut, "/")+1), substr($debut, strpos($debut, "/")+1, (strrpos($debut, "/")-strpos($debut, "/")-1)), substr($debut, 0, strpos($debut, "/")));
        $dateDebut->setTime($session->get("heureDebut"), $session->get("minDebut"), 0);
        
        $dateFin->setDate(substr($fin, strrpos($fin, "/")+1), substr($fin, strpos($fin, "/")+1, (strrpos($fin, "/")-strpos($fin, "/")-1)), substr($fin, 0, strpos($fin, "/")));
        $dateFin->setTime($session->get("heureFin"), $session->get("minFin"), 0);
        
        $patrouille->setDateDebut($dateDebut);
        $patrouille->setDateFin($dateFin);
        
        $em->persist($patrouille);
        if(count($tabFinal["observations"]) == 0){
            $json["error"] = "observation";
        }else {
            for ($i = 0; $i < count($tabFinal["observations"]); $i++) {
                $observation = new Observation();
                $observation->setDateAdd($dateAdd);

                $categorie = $em->getRepository("SirenAppBundle:Categorie")->find($tabFinal["observations"][$i]["categorie"]);
                $observation->setCategorie($categorie);
                $observation->setCoordX($tabFinal["observations"][$i]["coordX"]);
                $observation->setCoordY($tabFinal["observations"][$i]["coordY"]);

                for ($j = 0; $j < count($tabFinal["observations"][$i]["question"]); $j++) {
                    $question = $em->getRepository("SirenAppBundle:Question")->find($tabFinal["observations"][$i]["question"][$j]);
                    $repondre = new Repondre();
                    $repondre->setQuestion($question);
                    if ($question->getType() == "qro") {
                        $rep = $question->getReponses();
                        if (count($rep) == 0)
                            break;
                        if ($rep[0]->getValeurFr() != "texte")
                            continue;
                        $repondre->setReponseText($tabFinal["observations"][$i]["reponses"][$j]);
                    } else {
                        $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($tabFinal["observations"][$i]["reponses"][$j]);
                        $repondre->setReponse($reponse);
                    }
                    $repondre->setDateAdd($dateAdd);
                    $repondre->setObservation($observation);
                    $em->persist($repondre);
                    $observation->addReponse($repondre);
                    $observation->setPatrouille($patrouille);
                }
                $valImg = $session->get("image-" . $i);
                $listImg = explode("|", $valImg);
                foreach ($listImg as $img) {
                    if (is_int($img)) {
                        $image = $em->getRepository("SirenAppBundle:Image")->find($img);
                        $image->setObservation($observation);
                        $observation->addImage($image);
                    }
                }
                $em->persist($observation);
            }

            $em->flush();

            $json["result"] = true;
        }
        return new JsonResponse($json);
    }
    
    public function getRecapAction(Patrouille $patrouille){
        $result["result"] = false;
        $em = $this->getDoctrine()->getManager();
        
        if($patrouille != null){
            $result["segment"] = $patrouille->getSegment()->getNom();
            $result["site"] = $patrouille->getSegment()->getSite()->getNom();
            $result["dateDebut"] = $patrouille->getDateDebut()->format("d/m/Y");
            $result["heureDebut"] = $patrouille->getDateDebut()->format("H:i");
            $result["dateFin"] = $patrouille->getDateFin()->format("d/m/Y");
            $result["heureFin"] = $patrouille->getDateFin()->format("H:i");
            //$result["distance"] = $patrouille->getD
            
            $result["nbTraces"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Trace");
            $result["nbNids"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Nid");
            $result["nbCarcasses"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Carcasse");
            $result["nbVivants"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Vivant");
            $result["nbMorts"] = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Mort");
            $result["public"] = ($patrouille->getEtat()==1?"Oui":"Non");
            $result["result"] = true;
        }
        
        return new JsonResponse($result);
    }



    public function mapShowAction(){
        $em = $this->getDoctrine()->getManager();
        $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->findBy(array('etat' => '1'));
        $i = 0;

        return $this->render('SirenAppBundle:Home/Cyrille:map.html.twig', array(
            "liste_patrouilles" => $patrouilles
        ));
    }

    public function getAllAction(){
        $em = $this->getDoctrine()->getManager();
        $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->findBy(array('etat' => '1'));
        $i = 0;
        foreach ($patrouilles as $patrouille) {
            $result[$i]["id"] = $patrouille->getId();
            $result[$i]["site"] = $patrouille->getSegment()->getSite()->getNom();

            $result[$i]["dateDebut"] = $patrouille->getDateDebut()->format("d/m/Y");
            $result[$i]["heureDebut"] = $patrouille->getDateDebut()->format("H:i");
            $result[$i]["dateFin"] = $patrouille->getDateFin()->format("d/m/Y");
            $result[$i]["heureFin"] = $patrouille->getDateFin()->format("H:i");
            $result[$i]["coordXDebut"] = $patrouille->getCoordXDebut();
            $result[$i]["coordYDebut"] = $patrouille->getCoordYDebut();
            $result[$i]["coordXFin"] = $patrouille->getCoordXFin();
            $result[$i]["coordYFin"] = $patrouille->getCoordYFin();
            $result[$i]["nbObs"] = $patrouille->getObservations()->count();
            $result[$i++]["userAdd"] = $patrouille->getPatrouilleur()->getNom();
        }

        return new JsonResponse($result);
    }

    public function getCoordAction(Patrouille $patrouille){
        $result["error"] = 1;
        if($patrouille!=null){
            $result["error"] = 0;
            $result["x"] = $patrouille->getCoordXDebut();
            $result["y"] = $patrouille->getCoordYDebut();
        }
        return new JsonResponse($result);
    }

    public function removeAction(Patrouille $patrouille){
        $result = false;
        $em = $this->getDoctrine()->getManager();
        if($patrouille != null){
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($patrouille);
                $em->flush();
                $result = true;
            }catch (\Exception $e){
                $result = false;
                $json["error"] = $e->getMessage();
            }
        }
        
        $json["result"] = $result;
        return new JsonResponse($json);
    }
}
