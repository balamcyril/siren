<?php

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Projet;
use Siren\AppBundle\Entity\Site;
use Siren\AppBundle\Form\SiteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SiteController extends Controller
{
    public function getFromProjetAction(Projet $projet)
    {
        $result = false;
        $json = array();
        $json["data"] = array();

        if($projet != NULL){
            $sites = $projet->getSites();
            $result = true;
            foreach ($sites as $site){
                $json["data"][$site->getNom()] = NULL;
            }
        }
        $json["result"] = $result;

        return new JsonResponse($json);
    }

    public function sitesFromProjetAction(){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        $isAuthor = false;

        $site = new Site;
        $form = $this->createForm(new SiteType, $site);
        $request = $this->get('request');

        if($projet != null){
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            $result = false;
            $nom = false;
            if ($request->getMethod() == 'POST') {
                $form->bind($request);

                if ($form->isValid()) {
                    $exist = $em->getRepository("SirenAppBundle:Site")->findOneBy(array("nom" => $site->getNom(), "projet" => $projet));
                    if($exist == NULL) {
                        // On l'enregistre notre objet $article dans la base de données
                        $site->setDateAdd(new \DateTime());
                        $site->setProjet($projet);
                        $em->persist($site);
                        $em->flush();
                        $result = true;
                    }else{
                        $nom = true;
                        $result = false;
                    }
                }
            }

            return $this->render('SirenAppBundle:User/Projet:site.html.twig', array(
                'liste_sites' => $projet->getSites(),
                'projet' => $projet,
                'form' => $form->createView(),
                'auteur' => $isAuthor,
                'noProjet' => false,
                'enreg' => $result,
                'nom' => $nom
            ));
        }
        else{
            return $this->render('SirenAppBundle:User/Projet:site.html.twig', array(
                'liste_sites' => array(),
                'noProjet' => true,
                'projet' => $projet,
                'auteur' => $isAuthor
            ));
        }

    }

    public function modifAction(Site $site){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        $isAuthor = false;

        $form = $this->createForm(new SiteType, $site);
        $request = $this->get('request');

        if($projet != null){
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            $result = false;
            $nom = false;
            if ($request->getMethod() == 'POST') {
                $form->bind($request);

                if ($form->isValid()) {
                    $exist = $em->getRepository("SirenAppBundle:Site")->findOneBy(array("nom" => $site->getNom(), "projet" => $projet));
                    if($exist == NULL) {
                        // On l'enregistre notre objet $article dans la base de données
                        $em->persist($site);
                        $em->flush();
                        $result = true;
                    }else{
                        $nom = true;
                        $result = false;
                    }
                }
            }

            return $this->render('SirenAppBundle:User/Projet:site.html.twig', array(
                'liste_sites' => $projet->getSites(),
                'projet' => $projet,
                'form' => $form->createView(),
                'auteur' => $isAuthor,
                'noProjet' => false,
                'enreg' => $result,
                'nom' => $nom
            ));
        }
        else{
            return $this->render('SirenAppBundle:User/Projet:site.html.twig', array(
                'liste_sites' => array(),
                'noProjet' => true,
                'projet' => $projet,
                'auteur' => $isAuthor
            ));
        }
    }

    public function getSiteAction(Site $site){
        $result = true;
        if($site != NULL)
        {
            $json["id"] = $site->getId();
            $json["nom"] = $site->getNom();
            $json["projet"] = $site->getProjet()->getId();

            $result = true;
        }
        $json["result"] = $result;

        return new JsonResponse($json);
    }

    public function deleteAction(Site $site){
        $result = false;
        if ($site != null) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($site);
                $em->flush();
                $result = true;
            }catch (\Exception $e){
                $result = false;
                $json["error"] = $e->getMessage();
            }
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }
}
