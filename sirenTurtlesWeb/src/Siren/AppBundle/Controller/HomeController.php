<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of HomeController
 *
 * @author Dorian
 */
class HomeController extends Controller {

    public function indexAction() {

        $request = $this->get("request");
        if ($request->getLocale() == "") {
            return $this->redirect($this->generateUrl('SirenTortue_homepage'));
        }
        return $this->render('SirenAppBundle:Home:index.html.twig');
    }

    public function dashboardAction() {

        $request = $this->get("request");
        if ($request->getLocale() == "") {
            return $this->redirect($this->generateUrl('SirenTortue_homepage_stat'));
        }
        return $this->render('SirenAppBundle:Home:dashboard.html.twig');
    }
    public function dashAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_homePlus_dashboard'));
        }
        return $this->render('SirenAppBundle:Home:dash.html.twig');
    }
    public function dashboardTempsAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_homeTemps_dashboard'));
        }
        return $this->render('SirenAppBundle:Home:dashboardTemps.html.twig');
    }
    public function indexNoLocaleAction() {

        return $this->redirect($this->generateUrl('SirenTortue_homepage'));
    }


}
