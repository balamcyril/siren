<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Siren\AppBundle\Entity\Question;
use Siren\AppBundle\Form\QuestionType;
use Siren\AppBundle\Form\ReponseType;
use Siren\AppBundle\Entity\Reponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Description of QuestionController
 *
 * @author Dorian
 */
class QuestionController extends Controller {

    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get("session");
        $idProjet = $session->get("idProjet");

        $question = new Question;
        $reponse = new Reponse();

        $request = $this->get('request');
        $form = $this->createForm(new QuestionType($request->getLocale()), $question);
        $formRep = $this->createForm(new ReponseType($request->getLocale()), $reponse);

        $isProjet = $request->query->get("projet");
        $isProjet = ($isProjet==NULL)?false:(($isProjet==1 && $idProjet!=NULL)?true:false);
        if($isProjet)
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);

        $result = false;
        $code = false;
        if ($request->getMethod() == 'POST') {
            //echo "toto";
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet dans la base de données
                $testCode = $em->getRepository("SirenAppBundle:Question")->findBy(array("code" => $question->getCode()));
                if (count($testCode) > 0) {
                    $code = true;
                } else {
                    $question->setDateAdd(new \DateTime());
                    if($isProjet)
                        $question->setProjet($projet);
                    $em->persist($question);
                    $em->flush();
                    $result = true;
                }
            }
        }

        if($isProjet){
            $user = $this->get('security.context')->getToken()->getUser();
            $questions = $projet->getQuestionsOrderByCode();
            $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();

            $isAuthor = false;
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            return $this->render('SirenAppBundle:User/Projet:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'projet' => $projet,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'auteur' => $isAuthor,
                'enreg' => $result,
                'code' => $code
            ));
        }else{
            $questions = $em->getRepository('SirenAppBundle:Question')->getOrderByCode();
            $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();
            return $this->render('SirenAppBundle:Admin/Patrouille:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result,
                'code' => $code
            ));
        }

    }

    public function getQuestionAction(Question $question) {
        $result["result"] = false;
        if ($question != null) {
            $result["id"] = $question->getId();
            $result["titre_fr"] = $question->getTitreFr();
            $result["titre_en"] = $question->getTitreEn();
            $result["type"] = $question->getType();
            $result["code"] = $question->getCode();
            $result["categorie"] = $question->getCategorie()->getId();

            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    public function modifAction(Question $question) {
        $em = $this->getDoctrine()->getManager();

        $result = false;
        $reponse = new Reponse;
        $request = $this->get('request');

        $session = $this->get("session");
        $idProjet = $session->get("idProjet");
        $isProjet = $request->query->get("projet");
        $isProjet = ($isProjet==NULL)?false:(($isProjet==1 && $idProjet!=NULL)?true:false);

        $form = $this->createForm(new QuestionType($request->getLocale()), $question);
        $formRep = $this->createForm(new ReponseType($request->getLocale()), $reponse);


        $code = false;
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données
                $testCode = $em->getRepository("SirenAppBundle:Question")->isCodeUsedForModif($question);
                if ($testCode) {
                    $code = true;
                } else {
                    $em->persist($question);
                    $em->flush();
                    $result = true;
                }
            }
        }

        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();

        if($isProjet){
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);
            $user = $this->get('security.context')->getToken()->getUser();
            $questions = $projet->getQuestionsOrderByCode();

            $isAuthor = false;
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            return $this->render('SirenAppBundle:User/Projet:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'projet' => $projet,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'auteur' => $isAuthor,
                'enreg' => $result,
                'code' => $code
            ));
        }else {
            $questions = $em->getRepository('SirenAppBundle:Question')->getOrderByCode();
            return $this->render('SirenAppBundle:Admin/Patrouille:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result,
                'code' => $code
            ));
        }
    }

    public function deleteAction(Question $question) {

        $result = false;
        if ($question != null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($question);
            $em->flush();
            $result = true;
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }

    public function getQuestionPatAction($id, $cat, $pos, $idLast) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get("request");
        $result["result"] = false;
        $question = null;

        $session = $this->get("session");

        //Est ce qu'il s'agit d'un saut ?
        if ($pos != -1 && $idLast != -1) {
            $repInitiale = $em->getRepository("SirenAppBundle:Reponse")->find($idLast);
            $sautQuestion = $repInitiale->getSaut()->getQuestionAt($pos + 1);

            if ($sautQuestion == null) {
                $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($id);
                $question = $reponse->getQuestionSuivante();
                $pos = -1;
            } else {
                $question = $sautQuestion->getQuestion();
                $pos = $sautQuestion->getPosition();
            }
        } else {
            if ($cat != -1) {
                $categorie = $em->getRepository("SirenAppBundle:Categorie")->find($cat);
                $session->set("catPat", $categorie->getId());
                $session->remove("projetTest");
                $question = $em->getRepository("SirenAppBundle:Question")->getFirstQuestion($categorie);
            } else if ($id != 0) {
                $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($id);
                $question = $reponse->getQuestionSuivante();
            }
        }

        if($question == null){
            if($reponse != null){
                $sautQuestion = $reponse->getSaut()->getQuestionAt(1);

                if ($sautQuestion == null) {
                    $reponse = $em->getRepository("SirenAppBundle:Reponse")->find($id);
                    $question = $reponse->getQuestionSuivante();
                    $pos = -1;
                    $idLast = -1;
                } else {
                    $question = $sautQuestion->getQuestion();
                    $pos = $sautQuestion->getPosition();
                    $idLast = $id;
                }
            }
        }
        
        if ($question != null) {
            $type = "qcm";
            $repSolo = null;

            if ($question->getType() == "qro") {
                if (count($question->getReponses()) > 0) {
                    $type = $question->getReponses()[0]->getValeur($this->get("request")->getLocale());
                    $repSolo = $question->getReponses()[0];
                } else{
                    $type = "end";
                }

                //var_dump($session->get("projetTest"));
                if(($type == "end") && ($session->get("projetTest") == NULL)){
                    $idProjet = $session->get("idProjet");
                    $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);
                    $session->set("projetTest", true);
                    $categorie = $em->getRepository("SirenAppBundle:Categorie")->find($session->get("catPat"));
                    //var_dump($projet->getQuestionsOrderByCode($categorie));
                    if(count($projet->getQuestionsOrderByCode($categorie)) > 0) {
                        $question = $projet->getQuestionsOrderByCode($categorie)[0];
                        if (count($question->getReponses()) > 0) {
                            $type = $question->getReponses()[0]->getValeur($this->get("request")->getLocale());
                            $repSolo = $question->getReponses()[0];
                        } else{
                            $type = "end";
                        }
                    }
                }
            }

            $result["id"] = $question->getId();
            $result["titre"] = $question->getTitre($this->get("request")->getLocale());
            if($question->getImage() != NULL)
                $result["imQuest"] = $request->getBasePath() . '/' . $question->getImage()->getWebPath();
            else
                $result["imQuest"] = NULL;
            $result["code"] = $question->getCode();
            $result["type"] = $question->getType();

            $result["reponses"] = $this->renderView('SirenAppBundle:User/Patrouille:reponsePat.html.twig', array(
                'type' => $type,
                'repSolo' => $repSolo,
                'position' => $pos,
                'idLast' => $idLast,
                'liste_reponses' => $question->getReponses()
            ));

            $result["result"] = true;
        }
        
        return new JsonResponse($result);
    }
    
    public function getIdAction($code){
        $json["result"] = false;
        $em = $this->getDoctrine()->getManager();
        
        $question = $em->getRepository("SirenAppBundle:Question")->findBy(array("code" => $code));
        if($question != null){
            $json["id"] = $question[0]->getId();
            $json["result"] = true;
        }
        
        return new JsonResponse($json);
    }

    public function questionsProjetAction(){
        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $idProjet = $session->get("idProjet");
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);

        $user = $this->get('security.context')->getToken()->getUser();

        $question = new Question;
        $reponse = new Reponse();

        $request = $this->get('request');
        $form = $this->createForm(new QuestionType($request->getLocale()), $question);
        $formRep = $this->createForm(new ReponseType($request->getLocale()), $reponse);

        $questions = $projet->getQuestionsOrderByCode();
        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();

        $isAuthor = false;
        if($user->getId() == $projet->getAuteur()->getId())
            $isAuthor = true;

        return $this->render('SirenAppBundle:User/Projet:question.html.twig', array(
            'liste_questions' => $questions,
            'liste_categories' => $categories,
            'projet' => $projet,
            'form' => $form->createView(),
            'formRep' => $formRep->createView(),
            'auteur' => $isAuthor
        ));
    }

}
