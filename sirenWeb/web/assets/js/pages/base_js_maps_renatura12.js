/*
Document: base_js_maps.js
Author: Rustheme
Description: Custom JS code used in Maps Page
 */

var map;
var contents = new Array();
var markers = new Array();
var infowindow;
var directionsDisplay, directionsService;
var pret = false;

var BaseJsMaps = function() {

	// Init Markers Map
	var initMapMarkers = function() {
        detectBrowser();
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsService = new google.maps.DirectionsService();
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -4.847516, lng: 11.882749},
            zoom: 11
        });
        directionsDisplay.setMap(map);

        var adresse = document.getElementById("pathmapall").value;
        infowindow = new google.maps.InfoWindow();
        
                                                            
        $.getJSON(adresse, function (data) {
            var nb = data.length;
            
            for (var i = 0; i < nb; i++) {
                if (document.getElementById("admin").value)
                                                            {
                                                             var textadmin= '<a class="btn btn-danger btn-xs" href="http://siren.ammco.org/web/fr/observations/'+ data[i]["id"] +'/edit"><i class="fa fa-close"></i></a>';   
                                                            }
                                                            
                var text = '<div class="card" style="margin: 5px; clear: both;">' +
                                '<h4>Détails de l\'observation</h4>' +
                                '<img style="margin: 10px" width="120" height="90"  src="' + imageChemin + data[i]["image"] +'" />'+
                                '<ul class="list" style="font-size:12px; float: left;">' +
                                    '<li><b>N°:</b> ' + data[i]["alpha"] + 
                                     '&ensp;<a class="btn btn-success btn-xs" href="http://siren.ammco.org/web/fr/observations/'+ data[i]["id"] +'/show2"><i class="fa fa-eye"></i></a>&ensp;' + textadmin + '</li>'+
                                    '<li><b>Date:</b> ' + data[i]["date"] + ' à ' + data[i]["heure"] + '</li>' +
                                    '<li><b>Type:</b> ' + data[i]["type"] + '</li>';
                    if (data[i]["typequestion"]=="groupe")
                {
                var text1 ='<li><b>Espece:</b> ' + data[i]["espece"] + '</li>'+
                                    '<li><b>Groupe:</b> ' + data[i]["groupe"] + '</li>'+
                                    '<li><b>Sous-groupe:</b> ' + data[i]["sousgroupe"] + '</li>';
                text = text.concat(text1);
                }
                var text3 ='<li><b>Auteur:</b> ' + data[i]["collecteur"] + '</li>'+
                                '</ul>' +
                               
                            '</div>';
                text = text.concat(text3);
                
                contents[i] = text;
                var image;
                switch (data[i]["typeid"]) {
                    case 11:
                        image = cat1;
                        break;
                    case 12:
                        image = cat2;
                        break;
                    case 13:
                        image = cat3;
                        break;
                    case 14:
                        image = cat4;
                        break;
                    default:
                        image = defaut;
                        break;
                }

                markers[i] = new google.maps.Marker({
                    position: {lat: data[i]["coordX"], lng: data[i]["coordY"]},
                    map: map,
                    icon: image,
                    title: data[i]["collecteur"]
                });

                markers[i].addListener('click', function () {
                    for (var x = 0; x < markers.length; x++) {
                        if (markers[x] == this) {
                            break;
                        }
                    }
                    infowindow.setContent(contents[x]);
                    infowindow.open(map, markers[x]);
                });

                // Add a marker clusterer to manage the markers.
                var markerCluster = new MarkerClusterer(map, markers,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m1.png'});

            }

            map.addListener('click', function () {
                infowindow.close();
            })
        });
	};

    var detectBrowser = function() {
        var useragent = navigator.userAgent;
        var mapdiv = document.getElementById("map");

        if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1) {
            mapdiv.style.width = '100%';
            mapdiv.style.height = '300px';
        } else {
            $(mapdiv).css('width', '100%');
            mapdiv.style.height = '700px';
        }
    };

    var selectMarkers = function() {
        /*$("#modalWaith").modal({dismissible: false});
        $("#modalWaith").modal("open");

        $("#modalWaith").css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)");
        $("#modalWaith").css("filter", "alpha(opacity=50)");
        $("#modalWaith").css("-moz-opacity", "0.5");
        $("#modalWaith").css("-khtml-opacity", "0.5");
        $("#modalWaith").css("opacity", "0.5");*/
        
        //var type = $("input[name='type']:checked").val();
        var tortues_marines = $("#tortues_marines").is(":checked");
        var autre_espece_marine = $("#autre_espece_marine").is(":checked");
        var production_de_peche = $("#production_de_peche").is(":checked");
        var menace_renatura = $("#menace_renatura").is(":checked");
        var rapide = $("#rapide").is(":checked");

        var debut = $("#datedebut").val();
        var fin = $("#datefin").val();
        
        var projet = $("#projet").val();
        
        
        var pathmarker = document.getElementById("pathmarker").value;
        //alert(pathmarker);
        var data = {
            tortues_marines: tortues_marines,
            autre_espece_marine: autre_espece_marine,
            production_de_peche: production_de_peche,
            menace_renatura: menace_renatura,
            rapide: rapide,
            debut: debut,
            fin: fin,
            projet: projet
        };
        $.ajax({
            url: pathmarker,
            type: "POST",
            dataType: 'json', // selon le retour attendu
            data: data,
            success: function (donnees) {
                 //alert(nbeobservation +markers.length);
                // La réponse du serveur
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
                markers = [];

                var nb = donnees.length;
                
                var html;
                for (var i = 0; i < nb; i++) {
                
                
                
                if (document.getElementById("admin").value)
                                                            {
                                                             var textadmin= '<a class="btn btn-danger btn-xs" href="http://siren.ammco.org/web/fr/observations/'+ donnees[i]["id"] +'/edit"><i class="fa fa-close"></i></a>';   
                                                            }
                                                            
                var html = '<div class="card" style="margin: 5px; clear: both;">' +
                                '<h4>'+ labeltitre + '</h4>' +
                                '<img style="margin: 10px" width="120" height="90" src="' + imageChemin + donnees[i]["image"] +'" />'+
                                '<ul class="list" style="font-size:12px; float: left;">' +
                                    '<li><b>N°:</b> ' + donnees[i]["alpha"] + 
                                     '&ensp;<a class="btn btn-success btn-xs" href="http://siren.ammco.org/web/fr/observations/'+ donnees[i]["id"] +'/show2"><i class="fa fa-eye"></i></a>&ensp;' + textadmin + '</li>'+
                                    '<li><b>Date:</b> ' + donnees[i]["date"] + ' à ' + donnees[i]["heure"] + '</li>'+'<li><b>Type:</b> ' + donnees[i]["type"] + '</li>';
                if (donnees[i]["typequestion"]=="groupe")
                {
                var html1 ='<li><b>Espece:</b> ' + donnees[i]["espece"] + '</li>'+
                                    '<li><b>Groupe:</b> ' + donnees[i]["groupe"] + '</li>'+
                                    '<li><b>Sous-groupe:</b> ' + donnees[i]["sousgroupe"] + '</li>';
                html =  html.concat(html1);
                }
                var html3 ='<li><b>Auteur:</b> ' + donnees[i]["collecteur"] + '</li>'+
                                '</ul>' +
                               
                            '</div>';
                html = html.concat(html3);
                        
                    contents[i] = html;

                    var image;
                    switch (donnees[i]["typeid"]) {
                        case 11:
                            image = cat1;
                            break;
                        case 12:
                            image = cat2;
                            break;
                        case 13:
                            image = cat3;
                            break;
                        case 14:
                            image = cat4;
                            break;
                        default:
                            image = defaut;
                            break;
                    }
                    markers[i] = new google.maps.Marker({
                        position: {lat: donnees[i]["coordX"], lng: donnees[i]["coordY"]},
                        map: map,
                        icon: image,
                        title: donnees[i]["collecteur"]
                    });

                    markers[i].addListener('click', function () {
                        for (var x = 0; x < markers.length; x++) {
                            if (markers[x] == this) {
                                break;
                            }
                        }
                        infowindow.setContent(contents[x]);
                        infowindow.open(map, markers[x]);
                    });



                }

                //$("#modalWaith").modal("close");
            }
        });
    };


	return {
		init: function () {
			// Gmaps.js: https://hpneo.github.io/gmaps/

			initMapMarkers();

			var btMap = document.getElementById("generer");

			btMap.addEventListener("click", selectMarkers, false);
		}
	};
}();

// Initialize when page loads
jQuery( function() {
	$(".showDetail").click(function (e) {
	    //alert("x = " + parseFloat($(this).attr("obx")) + " & y = " + parseFloat($(this).attr("oby")));
        map.setCenter({lat: parseFloat($(this).attr("obx")), lng: parseFloat($(this).attr("oby"))});
	    map.setZoom(17);

	    console.log(markers[2]);
    });
});
