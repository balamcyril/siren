<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Segments;
use Doctrine\ORM\EntityManager;

class SegmentsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Segments $segment) {
        $this->em->persist($segment);
        $this->em->flush();
    }
    
    public function update(Segments $segment) {
        $this->em->merge($segment);
        $this->em->flush();
    }
    
    public function delete($id) {
        $segment = $this->getRepository()->find($id);
        if ($segment) {
            $this->em->remove($segment);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    public function findName($name) {
        return $this->getRepository()->findOneByNom($name);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Segments");
    }

    private function findBySites($id) {
        return $this->getRepository()->findBySites($id);
    }
}
