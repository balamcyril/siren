<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Sites;
use Doctrine\ORM\EntityManager;

class SitesMetier {
private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function create(Sites $e) {
        $this->em->persist($e);
        $this->em->flush();
    }

    public function update(Sites $e) {
        $this->em->merge($e);
        $this->em->flush();
    }

    public function delete($id) {
        $e = $this->getRepository()->find($id);
        if ($e) {
            $this->em->remove($e);
            $this->em->flush();
        }
    }

    public function findAll() {
        return $this->getRepository()->findAll();
    }

    public function find($id) {
        return $this->getRepository()->find($id);
    }


    private function getRepository() {
        return $this->em->getRepository("AppBundle:Sites");
    }

    public function findByProjet($projet) {
        return $this->getRepository()->findByProjet($projet);
    }
}
