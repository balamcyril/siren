<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Observations;
use Doctrine\ORM\EntityManager;

class ObservationsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Observations $obs) {
        $this->em->persist($obs);
        $this->em->flush();
    }
    
    public function update(Observations $obs) {
        $this->em->merge($obs);
        $this->em->flush();
    }
    
    public function delete($id) {
        $obs = $this->getRepository()->find($id);
        if ($obs) {
            $this->em->remove($obs);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findBy($id) {
        return $this->getRepository()->findBy(array('typeObservations' => $id ));
    }
    public function findByProjet($projet) {
        return $this->getRepository()->findByProjet($projet);
    }
    
    public function findByAlpha($alpha) {
        return $this->getRepository()->findByAlpha($alpha);
    }
    
    public function findByProjetPage($projet, $page) {
        return $this->getRepository()->findBy(array('projet' => $projet),array('id' => 'DESC'),100, $page-1);
    }

    public function user($utilisateurs,$projet) {
        return $this->getRepository()->findBy(array('utilisateurs' => $utilisateurs,'projet' => $projet ));
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Observations");
    }
}
