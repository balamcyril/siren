<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Projet_utilisateur;
use Doctrine\ORM\EntityManager;

class Projet_utilisateurMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Projet_utilisateur $projet_utilisateur) {
        $this->em->persist($projet_utilisateur);
        $this->em->flush();
    }
    
    public function update(Projet_utilisateur $projet_utilisateur) {
        $this->em->merge($projet_utilisateur);
        $this->em->flush();
    }
    
    public function delete($id) {
        $projet_utilisateur = $this->getRepository()->find($id);
        if ($projet_utilisateur) {
            $this->em->remove($projet_utilisateur);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findByUtilisateurs($id) {
        return $this->getRepository()->findByUtilisateurs($id);
    }
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Projet_utilisateur");
    }
}
