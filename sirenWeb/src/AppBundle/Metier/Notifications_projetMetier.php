<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Notifications_projet;
use Doctrine\ORM\EntityManager;

class Notifications_projetMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Notifications_projet $notifications_projet) {
        $this->em->persist($notifications_projet);
        $this->em->flush();
    }
    
    public function update(Notifications_projet $notifications_projet) {
        $this->em->merge($notifications_projet);
        $this->em->flush();
    }
    
    public function delete($id) {
        $notifications_projet = $this->getRepository()->find($id);
        if ($notifications_projet) {
            $this->em->remove($notifications_projet);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function getnew($id) {
        return $this->getRepository()->findByProjet($id);
    }
    
    
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Notifications_projet");
    }
}
