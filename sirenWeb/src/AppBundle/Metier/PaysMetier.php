<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Pays;
use Doctrine\ORM\EntityManager;

class PaysMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Pays $pays) {
        $this->em->persist($pays);
        $this->em->flush();
    }
    
    public function update(Pays $pays) {
        $this->em->merge($pays);
        $this->em->flush();
    }
    
    public function delete($id) {
        $pays = $this->getRepository()->find($id);
        if ($pays) {
            $this->em->remove($pays);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }

    public function findAllFr() {
        return $this->getRepository()->findAllFr();
    }

    public function findAllEn() {
        return $this->getRepository()->findAllEn();
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Pays");
    }
}
