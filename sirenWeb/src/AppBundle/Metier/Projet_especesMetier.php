<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Projet_especes;
use Doctrine\ORM\EntityManager;

class Projet_especesMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Projet_especes $projet_especes) {
        $this->em->persist($projet_especes);
        $this->em->flush();
    }
    
    public function update(Projet_especes $projet_especes) {
        $this->em->merge($projet_especes);
        $this->em->flush();
    }
    
    public function delete($id) {
        $projet_especes = $this->getRepository()->find($id);
        if ($projet_especes) {
            $this->em->remove($projet_especes);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findByProjet($projet) {
        return $this->getRepository()->findByProjet($projet);
    }
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Projet_especes");
    }
}
