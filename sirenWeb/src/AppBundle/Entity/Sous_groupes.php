<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Sous_groupes
 *
 * @ORM\Table(name="sous_groupes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Sous_groupesRepository")
 * @Vich\Uploadable
 */
class Sous_groupes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_fr", type="string", length=255)
     */
    private $nom_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_en", type="string", length=255)
     */
    private $nom_en;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Nom_pt", type="string", length=255)
     */
    private $nom_pt;

    /**
     * @var string
     *
     * @ORM\Column(name="Description_fr", type="string", length=255)
     */
    private $descriptionFr;

    /**
     * @var string
     *
     * @ORM\Column(name="Description_en", type="string", length=255)
     */
    private $descriptionEn;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Description_pt", type="string", length=255)
     */
    private $descriptionPt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255)
     */
    private $image;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="sousGroupes_image", fileNameProperty="image")
     */
    private $imageFile;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Groupes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $groupes; 
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE")
    */
    private $projet;
    
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set descriptionFr
     *
     * @param string $descriptionFr
     *
     * @return Sous_groupes
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }

    /**
     * Get descriptionFr
     *
     * @return string
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return Sous_groupes
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set descriptionPt
     *
     * @param string $descriptionPt
     *
     * @return Sous_groupes
     */
    public function setDescriptionPt($descriptionPt)
    {
        $this->descriptionPt = $descriptionPt;

        return $this;
    }

    /**
     * Get descriptionPt
     *
     * @return string
     */
    public function getDescriptionPt()
    {
        return $this->descriptionPt;
    }
    
    /**
     * Set groupes
     *
     * @param \AppBundle\Entity\Groupes $groupes
     *
     * @return Sous_groupes
     */
    public function setGroupes(\AppBundle\Entity\Groupes $groupes)
    {
        $this->groupes = $groupes;

        return $this;
    }

    /**
     * Get groupes
     *
     * @return \AppBundle\Entity\Groupes
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     *
     * @return Sous_groupes
    */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    /**
     * Set image
     *
     * @param string $image
     *
     * @return Sous_groupes
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    
    

    /**
     * Set nomFr
     *
     * @param string $nomFr
     *
     * @return Sous_groupes
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nomFr
     *
     * @return string
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nomEn
     *
     * @param string $nomEn
     *
     * @return Sous_groupes
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nomEn
     *
     * @return string
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
    
    public function __toString() {
        return $this->nom_fr;
    }
    
    /**
     * Set nomPt
     *
     * @param string $nomPt
     *
     * @return Sous_groupes
     */
    public function setNomPt($nomPt)
    {
        $this->nom_pt = $nomPt;

        return $this;
    }

    /**
     * Get nomPt
     *
     * @return string
     */
    public function getNomPt()
    {
        return $this->nom_pt;
    }
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Notifications_projet
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
