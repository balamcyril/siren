<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Reponses
 *
 * @ORM\Table(name="reponses")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReponsesRepository")
 * @Vich\Uploadable
 */
class Reponses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_fr", type="string", length=255)
     */
    private $titre_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_en", type="string", length=255)
     */
    private $titre_en;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Titre_pt", type="string", length=255)
     */
    private $titre_pt;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $questions; 
    
     /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE") 
    */
    private $questions_next; 
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255, nullable=True)
     */
    private $image;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="reponse_image", fileNameProperty="image")
     */
    private $imageFile;
    
    
    public function __construct() {
        $this->updatedAt = new \DateTime('now');
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questions
     *
     * @param \AppBundle\Entity\Questions $questions
     *
     * @return Reponses
     */
    public function setQuestions(\AppBundle\Entity\Questions $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set titreFr
     *
     * @param string $titreFr
     *
     * @return Reponses
     */
    public function setTitreFr($titreFr)
    {
        $this->titre_fr = $titreFr;

        return $this;
    }

    /**
     * Get titreFr
     *
     * @return string
     */
    public function getTitreFr()
    {
        return $this->titre_fr;
    }

    /**
     * Set titreEn
     *
     * @param string $titreEn
     *
     * @return Reponses
     */
    public function setTitreEn($titreEn)
    {
        $this->titre_en = $titreEn;

        return $this;
    }

    /**
     * Get titreEn
     *
     * @return string
     */
    public function getTitreEn()
    {
        return $this->titre_en;
    }
    
    /**
     * Set titrePt
     *
     * @param string $titrePt
     *
     * @return Reponses
     */
    public function setTitrePt($titrePt)
    {
        $this->titre_pt = $titrePt;

        return $this;
    }

    /**
     * Get titrePt
     *
     * @return string
     */
    public function getTitrePt()
    {
        return $this->titre_pt;
    }
    
    public function __toString() {
        return $this->titre_fr;
    }
    
    /**
     * Set questions_next
     *
     * @param \AppBundle\Entity\Questions $questions_next
     *
     * @return Reponses
     */
    public function setQuestionsNext(\AppBundle\Entity\Questions $questions_next)
    {
        $this->questions_next = $questions_next;

        return $this;
    }

    /**
     * Get questions_next
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestionsnext()
    {
        return $this->questions_next;
    }
    
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Reponses
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }



    /**
     * Set image
     *
     * @param string $image
     *
     * @return Reponses
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     *
     * @return Reponses
    */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
}
