<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Marche
 *
 * @ORM\Table(name="marche")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MarcheRepository")
 * @Vich\Uploadable
 */
class Marche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="Type_de_poisson", type="string", length=1024)
     */
    private $typeDePoisson;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=1024)
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Debarcadaire", type="string", length=1024)
     */
    private $debarcadaire;

    /**
     * @var string
     *
     * @ORM\Column(name="Site_de_peche", type="string", length=1024)
     */
    private $siteDePeche;


    /**
     * @var int
     *
     * @ORM\Column(name="Quantite", type="integer")
     */
    private $quantite;
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="Prix", type="integer")
     */
    private $prix;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Telephone", type="string", length=1024)
     */
    private $telephone;


    /**
     * @var text
     *
     * @ORM\Column(name="Img1", type="text", nullable=True)
     */
    private $img1;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="marche_image1", fileNameProperty="img1")
     */
    private $img1File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img2", type="text", nullable=True)
     */
    private $img2;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="marche_image2", fileNameProperty="img2")
     */
    private $img2File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img3", type="text", nullable=True)
     */
    private $img3;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="marche_image3", fileNameProperty="img3")
     */
    private $img3File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img4", type="text", nullable=True)
     */
    private $img4;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="marche_image4", fileNameProperty="img4")
     */
    private $img4File;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateurs")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $utilisateurs; 
    
    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat = 1;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    public function __construct() {
        $this->date = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }
   
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Marche
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    
    /**
     * Set img1
     *
     * @param string $img1
     *
     * @return Marche
     */
    public function setImg1($img1)
    {
        $this->img1 = $img1;

        return $this;
    }

    /**
     * Get img1
     *
     * @return string
     */
    public function getImg1()
    {
        return $this->img1;
    }
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img1File
     *
     * @return Marche
    */
    public function setImg1File(File $img1File = null)
    {
        $this->img1File = $img1File;
        if ($img1File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg1File()
    {
        return $this->img1File;
    }
    
    /**
     * Set img2
     *
     * @param string $img2
     *
     * @return Marche
     */
    public function setImg2($img2)
    {
        $this->img2 = $img2;

        return $this;
    }

    /**
     * Get img2
     *
     * @return string
     */
    public function getImg2()
    {
        return $this->img2;
    }
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img2File
     *
     * @return Marche
    */
    public function setImg2File(File $img2File = null)
    {
        $this->img2File = $img2File;
        if ($img2File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg2File()
    {
        return $this->img2File;
    }
    
    /**
     * Set img3
     *
     * @param string $img3
     *
     * @return Marche
     */
    public function setImg3($img3)
    {
        $this->img3 = $img3;

        return $this;
    }

    /**
     * Get img3
     *
     * @return string
     */
    public function getImg3()
    {
        return $this->img3;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img3File
     *
     * @return Marche
    */
    public function setImg3File(File $img3File = null)
    {
        $this->img3File = $img3File;
        if ($img3File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg3File()
    {
        return $this->img3File;
    }
    
    /**
     * Set img4
     *
     * @param string $img4
     *
     * @return Marche
     */
    public function setImg4($img4)
    {
        $this->img4 = $img4;

        return $this;
    }

    /**
     * Get img4
     *
     * @return string
     */
    public function getImg4()
    {
        return $this->img4;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img4File
     *
     * @return Marche
    */
    public function setImg4File(File $img4File = null)
    {
        $this->img4File = $img4File;
        if ($img4File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg4File()
    {
        return $this->img4File;
    }
    
     public function __toString() {
        return $this->description;
    }


    /**
     * Set typeDePoisson
     *
     * @param string $typeDePoisson
     *
     * @return Marche
     */
    public function setTypeDePoisson($typeDePoisson)
    {
        $this->typeDePoisson = $typeDePoisson;

        return $this;
    }

    /**
     * Get typeDePoisson
     *
     * @return string
     */
    public function getTypeDePoisson()
    {
        return $this->typeDePoisson;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Marche
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set debarcadaire
     *
     * @param string $debarcadaire
     *
     * @return Marche
     */
    public function setDebarcadaire($debarcadaire)
    {
        $this->debarcadaire = $debarcadaire;

        return $this;
    }

    /**
     * Get debarcadaire
     *
     * @return string
     */
    public function getDebarcadaire()
    {
        return $this->debarcadaire;
    }

    /**
     * Set siteDePeche
     *
     * @param string $siteDePeche
     *
     * @return Marche
     */
    public function setSiteDePeche($siteDePeche)
    {
        $this->siteDePeche = $siteDePeche;

        return $this;
    }

    /**
     * Get siteDePeche
     *
     * @return string
     */
    public function getSiteDePeche()
    {
        return $this->siteDePeche;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Marche
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return Marche
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Marche
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return Marche
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set utilisateurs
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateurs
     *
     * @return Marche
     */
    public function setUtilisateurs(\AppBundle\Entity\Utilisateurs $utilisateurs)
    {
        $this->utilisateurs = $utilisateurs;

        return $this;
    }

    /**
     * Get utilisateurs
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }
    
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Marche
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}
