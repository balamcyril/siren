<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet_typeObservations
 *
 * @ORM\Table(name="projet_typeObservations")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Projet_typeObservationsRepository")
 */
class Projet_typeObservations
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;   

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $projet;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Type_observations")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $typeObservations;
    

    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Projet_typeObservations
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set typeObservations
     *
     * @param \AppBundle\Entity\Type_observations $typeObservations
     *
     * @return Projet_typeObservations
     */
    public function setTypeObservations(\AppBundle\Entity\Type_observations $typeObservations)
    {
        $this->typeObservations = $typeObservations;

        return $this;
    }

    /**
     * Get typeObservations
     *
     * @return \AppBundle\Entity\Type_observations
     */
    public function getTypeObservations()
    {
        return $this->typeObservations;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
