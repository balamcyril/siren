<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Especes
 *
 * @ORM\Table(name="especes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EspecesRepository")
 * @Vich\Uploadable
 */
class Especes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_fr", type="string", length=255)
     */
    private $nom_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_en", type="string", length=255)
     */
    private $nom_en;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Nom_pt", type="string", length=255)
     */
    private $nom_pt;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_inaturalist", type="string", length=255, nullable=True)
     */
    private $nom_inaturalist;


    /**
     * @var string
     *
     * @ORM\Column(name="Description_fr", type="string", length=255)
     */
    private $descriptionFr;

    /**
     * @var string
     *
     * @ORM\Column(name="Description_en", type="string", length=255)
     */
    private $descriptionEn;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Description_pt", type="string", length=255)
     */
    private $descriptionPt;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255)
     */
    private $image;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="especes_image", fileNameProperty="image")
     */
    private $imageFile;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sous_groupes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $sous_groupes;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $questions_animal;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $questions_menaces;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $questions_signe;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $questions_alimentation;
    
    /**
     * @var int
     *
     * @ORM\Column(name="Defaut", type="integer")
     */
    private $defaut;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime", nullable=True)
     */
    private $date;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE")
    */
    private $projet;
    
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set descriptionFr
     *
     * @param string $descriptionFr
     *
     * @return Especes
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }

    /**
     * Get descriptionFr
     *
     * @return string
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return Especes
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set descriptionPt
     *
     * @param string $descriptionPt
     *
     * @return Especes
     */
    public function setDescriptionPt($descriptionPt)
    {
        $this->descriptionPt = $descriptionPt;

        return $this;
    }

    /**
     * Get descriptionPt
     *
     * @return string
     */
    public function getDescriptionPt()
    {
        return $this->descriptionPt;
    }
    
    function getSousGroupes() {
        return $this->sous_groupes;
    }

    function setSousGroupes($sous_groupes) {
        $this->sous_groupes = $sous_groupes;
    }

    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     *
     * @return Especes
    */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    /**
     * Set image
     *
     * @param string $image
     *
     * @return Especes
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set questions_animals
     *
     * @param \AppBundle\Entity\Questions $questions_animals
     *
     * @return Especes
     */
    public function setQuestionsAnimal(\AppBundle\Entity\Questions $questions_animals = null)
    {
        $this->questions_animal = $questions_animals;

        return $this;
    }

    /**
     * Get questions_animal
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestionsAnimal()
    {
        return $this->questions_animal;
    }
    
    /**
     * Set questions_menaces
     *
     * @param \AppBundle\Entity\Questions $questions_menaces
     *
     * @return Especes
     */
    public function setQuestionsMenaces(\AppBundle\Entity\Questions $questions_menaces = null)
    {
        $this->questions_menaces = $questions_menaces;

        return $this;
    }

    /**
     * Get questions_menaces
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestionsMenaces()
    {
        return $this->questions_menaces;
    }
    
    /**
     * Set questions_menaces
     *
     * @param \AppBundle\Entity\Questions $questions_menaces
     *
     * @return Especes
     */
    public function setQuestionsSigne(\AppBundle\Entity\Questions $questions_menaces = null)
    {
        $this->questions_signe = $questions_menaces;

        return $this;
    }

    /**
     * Get questions_signe
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestionsSigne()
    {
        return $this->questions_signe;
    }
    
    /**
     * Set questions_alimentation
     *
     * @param \AppBundle\Entity\Questions $questions_alimentation
     *
     * @return Especes
     */
    public function setQuestionsAlimentation(\AppBundle\Entity\Questions $questions_alimentation = null)
    {
        $this->questions_alimentation = $questions_alimentation;

        return $this;
    }

    /**
     * Get questions_alimentation
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestionsAlimentation()
    {
        return $this->questions_alimentation;
    }
 
    /**
     * Set nomFr
     *
     * @param string $nomFr
     *
     * @return Especes
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nomFr
     *
     * @return string
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nomEn
     *
     * @param string $nomEn
     *
     * @return Especes
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nomEn
     *
     * @return string
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
    
    /**
     * Set nomPt
     *
     * @param string $nomPt
     *
     * @return Especes
     */
    public function setNomPt($nomPt)
    {
        $this->nom_pt = $nomPt;

        return $this;
    }

    /**
     * Get nomPt
     *
     * @return string
     */
    public function getNomPt()
    {
        return $this->nom_pt;
    }
    
    /**
     * Set nomInaturalist
     *
     * @param string $nomInaturalist
     *
     * @return Especes
     */
    public function setNomInaturalist($nomInaturalist= null)
    {
        $this->nom_inaturalist = $nomInaturalist;

        return $this;
    }

    /**
     * Get nomInaturalist
     *
     * @return string
     */
    public function getNomInaturalist()
    {
        return $this->nom_inaturalist;
    }


    public function __toString() {
        return $this->nom_fr;
    }
     public function __construct() {
        $this->date = new \DateTime('now');
    }
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Set defaut
     *
     * @param integer $defaut
     *
     * @return Especes
     */
    public function setDefaut($defaut)
    {
        $this->defaut = $defaut;

        return $this;
    }

    /**
     * Get defaut
     *
     * @return integer
     */
    public function getDefaut()
    {
        return $this->defaut;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Especes
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Especes
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Notifications_projet
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
