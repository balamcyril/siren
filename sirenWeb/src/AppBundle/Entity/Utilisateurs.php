<?php

namespace AppBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Utilisateurs
 *
 * @ORM\Table(name="utilisateurs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UtilisateursRepository")
 */
class Utilisateurs extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var bigint
     *
     * @ORM\Column(name="Telephone", type="bigint")
     */
    private $telephone;

    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fonctions")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $fonctions;  
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $pays; 
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $projet;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime" , nullable=True)
     */
    private $date;
    
    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
        $this->date = new \DateTime('now');
        $this->enabled = 1;
       
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

 /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Notifications_projet
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Utilisateurs
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return Utilisateurs
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }


    /**
     * Set fonctions
     *
     * @param \AppBundle\Entity\Fonctions $fonctions
     *
     * @return Utilisateurs
     */
    public function setFonctions(\AppBundle\Entity\Fonctions $fonctions)
    {
        $this->fonctions = $fonctions;

        return $this;
    }

    /**
     * Get fonctions
     *
     * @return \AppBundle\Entity\Fonctions
     */
    public function getFonctions()
    {
        return $this->fonctions;
    }
    
    
    
    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Utilisateurs
     */
    public function setPays(\AppBundle\Entity\Pays $pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    

    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Utilisateurs
     */
    public function setProjet(\AppBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
