<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Segments
 *
 * @ORM\Table(name="Segments")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SegmentsRepository")
 * @Vich\Uploadable
 */

class Segments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sites")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $sites;

    /**
    * @var text
    *
    * @ORM\Column(name="Nom", type="text")
    */
    private $nom;

    /**
    * @var text
    *
    * @ORM\Column(name="Ville", type="text")
    */
    private $ville;

    /**
    * @var text
    *
    * @ORM\Column(name="Region", type="text")
    */
    private $region;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="Date", type="datetime")
    */
    private $date;

    public function __construct() {
        $this->date = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
    * Get id
    *
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param text $nom
     *
     * @return Segments
    */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
    * Get nom
    *
    * @return text
    */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ville
     *
     * @param text $ville
     *
     * @return Segments
    */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
    * Get ville
    *
    * @return text
    */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set region
     *
     * @param text $region
     *
     * @return Segments
    */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return text
     */
    public function getRegion()
    {
        return $this->region;
    }
    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Segments
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sites
     *
     * @param \AppBundle\Entity\Sites $sites
     *
     * @return Segments
     */
    public function setSites(\AppBundle\Entity\Sites $sites)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return \AppBundle\Entity\Sites
     */
    public function getSites()
    {
        return $this->sites;
    }

}