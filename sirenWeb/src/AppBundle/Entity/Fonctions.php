<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Fonctions
 *
 * @ORM\Table(name="fonctions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FonctionsRepository")
 */
class Fonctions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_fr", type="string", length=255)
     */
    private $nom_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_en", type="string", length=255)
     */
    private $nom_en;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Nom_pt", type="string", length=255)
     */
    private $nom_pt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nomFr
     *
     * @param string $nomFr
     *
     * @return Fonctions
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nomFr
     *
     * @return string
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nomEn
     *
     * @param string $nomEn
     *
     * @return Fonctions
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nomEn
     *
     * @return string
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
    
    /**
     * Set nomPt
     *
     * @param string $nomPt
     *
     * @return Fonctions
     */
    public function setNomPt($nomPt)
    {
        $this->nom_pt = $nomPt;

        return $this;
    }

    /**
     * Get nomPt
     *
     * @return string
     */
    public function getNomPt()
    {
        return $this->nom_pt;
    }
    
    public function __toString() {
        return $this->nom_fr;
    }
}
