<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet_especes
 *
 * @ORM\Table(name="projet_especes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Projet_especesRepository")
 */
class Projet_especes
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;   

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $projet;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Especes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $especes;
    

    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Projet_especes
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set especes
     *
     * @param \AppBundle\Entity\Especes $especes
     *
     * @return Projet_especes
     */
    public function setEspeces(\AppBundle\Entity\Especes $especes)
    {
        $this->especes = $especes;

        return $this;
    }

    /**
     * Get especes
     *
     * @return \AppBundle\Entity\Especes
     */
    public function getEspeces()
    {
        return $this->especes;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
