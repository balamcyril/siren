<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Sites
 *
 * @ORM\Table(name="sites")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SitesRepository")
 * @Vich\Uploadable
 */

class Sites 
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $projet;

    /**
    * @var text
    *
    * @ORM\Column(name="Nom", type="text")
    */
    private $nom;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="Date", type="datetime")
    */
    private $date;

    public function __construct() {
        $this->date = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
    * Get id
    *
    * @return integer
    */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Sites
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set nom
     *
     * @param text $nom
     *
     * @return Sites
    */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
    * Get nom
    *
    * @return text
    */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Sites
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

}