<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Projet_utilisateur;
use AppBundle\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Projet_utilisateur controller.
 *
 */
class Projet_utilisateurController extends Controller
{
    /**
     * Lists all projet_utilisateur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projet_utilisateurs = $em->getRepository('AppBundle:Projet_utilisateur')->findAll();

        return $this->render('projet_utilisateur/index.html.twig', array(
            'projet_utilisateurs' => $projet_utilisateurs,
        ));
    }

    /**
     * Creates a new projet_utilisateur entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $projet_utilisateur = new Projet_utilisateur();
        $form = $this->createForm('AppBundle\Form\Projet_utilisateurType', $projet_utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projet_utilisateur->setProjet($projet);
            $em->persist($projet_utilisateur);
            $em->flush();

            return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
        }
        
        return $this->render('projet_utilisateur/new.html.twig', array(
            'projet_utilisateur' => $projet_utilisateur,
            'form' => $form->createView(),
            'projet' => $projet,
            
        ));
    }

    /**
     * Finds and displays a projet_utilisateur entity.
     *
     */
    public function showAction(Projet_utilisateur $projet_utilisateur,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $deleteForm = $this->createDeleteForm($projet_utilisateur);

        return $this->render('projet_utilisateur/show.html.twig', array(
            'projet_utilisateur' => $projet_utilisateur,
            'delete_form' => $deleteForm->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Displays a form to edit an existing projet_utilisateur entity.
     *
     */
    public function editAction(Request $request, Projet_utilisateur $projet_utilisateur)
    {
        $deleteForm = $this->createDeleteForm($projet_utilisateur);
        $editForm = $this->createForm('AppBundle\Form\Projet_utilisateurType', $projet_utilisateur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_utilisateur_edit', array('id' => $projet_utilisateur->getId()));
        }

        return $this->render('projet_utilisateur/edit.html.twig', array(
            'projet_utilisateur' => $projet_utilisateur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projet_utilisateur entity.
     *
     */
    public function deleteAction(Request $request, Projet_utilisateur $projet_utilisateur)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $form = $this->createDeleteForm($projet_utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($projet_utilisateur);
            $em->flush();
        }

        return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
    }

    /**
     * Creates a form to delete a projet_utilisateur entity.
     *
     * @param Projet_utilisateur $projet_utilisateur The projet_utilisateur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Projet_utilisateur $projet_utilisateur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projet_utilisateur_delete', array('id' => $projet_utilisateur->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
