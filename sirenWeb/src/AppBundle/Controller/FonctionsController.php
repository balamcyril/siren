<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fonctions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fonction controller.
 *
 */
class FonctionsController extends Controller
{
    /**
     * Lists all fonction entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fonctions = $em->getRepository('AppBundle:Fonctions')->findAll();

        return $this->render('fonctions/index.html.twig', array(
            'fonctions' => $fonctions,
        ));
    }

    /**
     * Creates a new fonction entity.
     *
     */
    public function newAction(Request $request)
    {
        $fonction = new Fonctions();
        $form = $this->createForm('AppBundle\Form\FonctionsType', $fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fonction);
            $em->flush();

            return $this->redirectToRoute('fonctions_show', array('id' => $fonction->getId()));
        }

        return $this->render('fonctions/new.html.twig', array(
            'fonction' => $fonction,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fonction entity.
     *
     */
    public function showAction(Fonctions $fonction)
    {
        $deleteForm = $this->createDeleteForm($fonction);

        return $this->render('fonctions/show.html.twig', array(
            'fonction' => $fonction,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fonction entity.
     *
     */
    public function editAction(Request $request, Fonctions $fonction)
    {
        $deleteForm = $this->createDeleteForm($fonction);
        $editForm = $this->createForm('AppBundle\Form\FonctionsType', $fonction);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fonctions_edit', array('id' => $fonction->getId()));
        }

        return $this->render('fonctions/edit.html.twig', array(
            'fonction' => $fonction,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fonction entity.
     *
     */
    public function deleteAction(Request $request, Fonctions $fonction)
    {
        $form = $this->createDeleteForm($fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fonction);
            $em->flush();
        }

        return $this->redirectToRoute('fonctions_index');
    }

    /**
     * Creates a form to delete a fonction entity.
     *
     * @param Fonctions $fonction The fonction entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fonctions $fonction)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fonctions_delete', array('id' => $fonction->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
