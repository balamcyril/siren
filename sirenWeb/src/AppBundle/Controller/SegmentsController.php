<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Segments;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Segment controller.
 *
 */
class SegmentsController extends Controller
{
    /**
     * Lists all Segment entities for a site.
     *
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        $site = $request->query->get("id");

        $em = $this->getDoctrine()->getManager();

        $segments = $em->getRepository('AppBundle:Segments')->findBy(
            array('sites' => $site),
            array('id' => 'ASC')
        );
        return $this->render('segments/index.html.twig', array(
            'segments' => $segments,
        ));
    }

    /**
     * Creates a new Segment entity.
     *
     */
    public function newAction(Request $request)
    {
        $segment = new Segments();
        $form = $this->createForm('AppBundle\Form\SegmentsType', $segment);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
            
        $session = $request->getSession();
        $site=$em->getRepository('AppBundle:Sites')->find($session->get("site"));
        

        if ($form->isSubmitted() && $form->isValid()) {
            $segment->setSites($site);
            $em->persist($segment);
            $em->flush();
            return $this->redirectToRoute('segments_show', array('id' => $segment->getId()));
        }

        return $this->render('segments/new.html.twig', array(
            'segment' => $segment,
            'site' => $session->get("site"),
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Segment entity.
     *
     */
    public function showAction(Segments $segment)
    {
        $deleteForm = $this->createDeleteForm($segment);
        

        return $this->render('segments/show.html.twig', array(
            'segment' => $segment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Segment entity.
     *
     */
    public function editAction(Request $request, Segments $segment)
    {
        $deleteForm = $this->createDeleteForm($segment);
        $editForm = $this->createForm('AppBundle\Form\SegmentsType', $segment);
        $editForm->handleRequest($request);
        
        $site= $segment->getSites();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('sites_show', array('id' => $site->getId()));
        }

        return $this->render('segments/edit.html.twig', array(
            'segment' => $segment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a segment entity.
     *
     */
    public function deleteAction(Request $request, Segments $segment)
    {
        $form = $this->createDeleteForm($segment);
        $form->handleRequest($request);
        $site= $segment->getSites();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($segment);
            $em->flush();
        }
        
        $segments = $em->getRepository('AppBundle:Segments')->findBySites($site);
        
        //set something maybe

        return $this->redirectToRoute('sites_show', array('id' => $site->getId()));
    }

    /**
     * Creates a form to delete a Segment entity.
     *
     * @param Segments $segment The Segment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Segments $segment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('segments_delete', array('id' => $segment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
