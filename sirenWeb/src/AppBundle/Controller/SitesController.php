<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sites;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Site controller.
 *
 */
class SitesController extends Controller
{
    /**
     * Lists all site entities.
     *
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        $session->remove('site');
        $session->remove('projet');

        $em = $this->getDoctrine()->getManager();
        $sites = $em->getRepository('AppBundle:Sites')->findAll(array('id' => 'ASC'));
        return $this->render('sites/index.html.twig', array(
            'sites' => $sites,
        ));
    }

    /**
     * Creates a new site entity.
     *
     */
    public function newAction(Request $request)
    {
        $site = new Sites();

        $form = $this->createForm('AppBundle\Form\SitesType', $site);
        $form->handleRequest($request);

        $session = $request->getSession();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if($session->get("projet") !=NULL)
            {
                $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
                $site->setProjet($projet);
                $projet->setSite($site);
            }

            $em->persist($site);
            $em->flush();

            return $this->redirectToRoute('sites_show', array('id' => $site->getId()));
        }

        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }


        return $this->render('sites/new.html.twig', array(
            'site' => $site,
            'retour' => $retour,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a site entity.
     *
     */
    public function showAction(Sites $site, Request $request)
    {
        $session = $request->getSession();
        $deleteForm = $this->createDeleteForm($site);

        $em = $this->getDoctrine()->getManager();
        $segments = $em->getRepository('AppBundle:Segments')->findBySites($site);

        $session->set('site', $site->getId());

        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }


        return $this->render('sites/show.html.twig', array(
            'site' => $site,
            'segments' => $segments,
            'retour' => $retour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing site entity.
     *
     */
    public function editAction(Request $request, Sites $site)
    {
        $site2=$site;
        $session = $request->getSession();

        $deleteForm = $this->createDeleteForm($site);
        $editForm = $this->createForm('AppBundle\Form\SitesType', $site);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sites_edit', array('id' => $site->getId()));
        }

        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }


        return $this->render('sites/edit.html.twig', array(
            'site' => $site,
            'retour' => $retour,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a site entity.
     *
     */
    public function deleteAction(Request $request, Sites $site)
    {
        $form = $this->createDeleteForm($site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($site);
            $em->flush();
        }
        return $this->redirectToRoute('sites_index');
    }

    /**
     * Creates a form to delete a site entity.
     *
     * @param Sites $site The site entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sites $site)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sites_delete', array('id' => $site->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
