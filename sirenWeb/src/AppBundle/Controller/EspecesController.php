<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Especes;
use AppBundle\Entity\Sous_groupes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Espece controller.
 *
 */
class EspecesController extends Controller
{
    /**
     * Lists all espece entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $especes = $em->getRepository('AppBundle:Especes')->findAll();

        return $this->render('especes/index.html.twig', array(
            'especes' => $especes,
        ));
    }

    /**
     * Creates a new espece entity.
     *
     */
    public function newAction(Request $request, Sous_groupes $sous_groupe)
    {
        $espece = new Especes();
        $form = $this->createForm('AppBundle\Form\EspecesType', $espece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $espece->setSousGroupes($sous_groupe);
            $em->persist($espece);
            $em->flush();

            return $this->redirectToRoute('sous_groupes_show', array('id' => $sous_groupe->getId()));
        }

        return $this->render('especes/new.html.twig', array(
            'espece' => $espece,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a espece entity.
     *
     */
    public function showAction(Especes $espece)
    {
        $deleteForm = $this->createDeleteForm($espece);

        return $this->render('especes/show.html.twig', array(
            'espece' => $espece,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing espece entity.
     *
     */
    public function editAction(Request $request, Especes $espece)
    {
        $espece2=$espece;
        $deleteForm = $this->createDeleteForm($espece);
        $editForm = $this->createForm('AppBundle\Form\EspecesType', $espece);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($espece->getImage() == NULL)
            {
                $espece->setImage($espece2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('especes_edit', array('id' => $espece->getId()));
        }

        return $this->render('especes/edit.html.twig', array(
            'espece' => $espece,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a espece entity.
     *
     */
    public function deleteAction(Request $request, Especes $espece)
    {
        $form = $this->createDeleteForm($espece);
        $form->handleRequest($request);
        $sous_groupe=$espece->getSousGroupes();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($espece);
            $em->flush();
        }

        return $this->redirectToRoute('sous_groupes_show', array('id' => $sous_groupe->getId()));
    }

    /**
     * Creates a form to delete a espece entity.
     *
     * @param Especes $espece The espece entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Especes $espece)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('especes_delete', array('id' => $espece->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
     public function deleteImageAction(Request $request, Especes $especes)
    {
        $especes->setImage(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('especes_edit', array('id' => $especes->getId()));
        
    }
}
