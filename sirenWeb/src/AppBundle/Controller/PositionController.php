<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Position;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
/**
 * Position controller.
 *
 */
class PositionController extends Controller
{
    /**
     * Lists all position entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $positions = $em->getRepository('AppBundle:Position')->findAll();

        return $this->render('position/index.html.twig', array(
            'positions' => $positions,
        ));
    }

    /**
     * Creates a new position entity.
     *
     */
    public function newAction(Request $request)
    {
        $position = new Position();
        $form = $this->createForm('AppBundle\Form\PositionType', $position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($position);
            $em->flush();

            return $this->redirectToRoute('position_show', array('id' => $position->getId()));
        }

        return $this->render('position/new.html.twig', array(
            'position' => $position,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a position entity.
     *
     */
    public function showAction(Position $position)
    {
        $deleteForm = $this->createDeleteForm($position);

        return $this->render('position/show.html.twig', array(
            'position' => $position,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing position entity.
     *
     */
    public function editAction(Request $request, Position $position)
    {
        $deleteForm = $this->createDeleteForm($position);
        $editForm = $this->createForm('AppBundle\Form\PositionType', $position);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('position_edit', array('id' => $position->getId()));
        }

        return $this->render('position/edit.html.twig', array(
            'position' => $position,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a position entity.
     *
     */
    public function deleteAction(Request $request, Position $position)
    {
        $form = $this->createDeleteForm($position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($position);
            $em->flush();
        }

        return $this->redirectToRoute('position_index');
    }

    /**
     * Creates a form to delete a position entity.
     *
     * @param Position $position The position entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Position $position)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('position_delete', array('id' => $position->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public function exportAction(Request $request){
            $user = $this->getUser();

            $em = $this->getDoctrine()->getManager();
            $observations= new Position();
        
            $projet = $request->request->get("projet");
            $pro = $em->getRepository('AppBundle:Projet')->find($projet);
            $dateDebut = $request->request->get("datedebut");
            $dateFin = $request->request->get("datefin");
            
            $dateDebut = ($dateDebut == "") ? NULL : $dateDebut;
            $dateFin = ($dateFin == "") ? NULL : $dateFin;

            if($dateDebut != NULL){
                $debut = $dateDebut;
                $dateDebut = new \DateTime();
                $dateDebut->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }

            if($dateFin != NULL){
                $debut = $dateFin;
                $dateFin = new \DateTime();
                $dateFin->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }
            
            // ask the service for a Excel5
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            // Pour traduire dans la locale de l'utilisateur :
            $titre = 'Exportation positions du';
            $description = "Ce fichier Excel 2007 a été généré par l'application Siren et contient des données des positions qui y ont été sauvegardées";

            $dateExport = date("d-m-Y H:i:s");
            $phpExcelObject->getProperties()->setCreator($user->getUsername())
                ->setLastModifiedBy("Siren")
                ->setTitle($titre.' '.$dateExport)
                ->setSubject($titre)
                ->setDescription($description);

                
                $observations = $em->getRepository("AppBundle:Position")->getexport($projet, $dateDebut, $dateFin);
                
                $phpExcelObject->setActiveSheetIndex(0);

                
                $sheet = $phpExcelObject->getActiveSheet();
                $sheet->setTitle("Observation");
                $texte = 'Position';
                $sheet->setCellValue('A1', $texte);
                $texte = 'Projet';
                $sheet->setCellValue('B1', $texte);
                $texte = 'Utilisateur';
                $sheet->setCellValue('C1', $texte);
                $texte = "Latitude (x)";
                $sheet->setCellValue('D1', $texte);
                $texte = 'Longitude (y)';
                $sheet->setCellValue('E1', $texte);
                $texte = 'Date';
                $sheet->setCellValue('F1', $texte);
                
                
               
                $i = 2;
                foreach ($observations as $observation){
                    $sheet->setCellValue('A'.$i, ($i - 1));
                    $sheet->setCellValue('B'.$i, $observation->getProjet()->getNom());
                    $sheet->setCellValue('C'.$i, $observation->getUtilisateurs()->getUsername());
                    $sheet->setCellValue('D'.$i, $observation->getCoordX());
                    $sheet->setCellValue('E'.$i, $observation->getCoordY());
                    $sheet->setCellValue('F'.$i, $observation->getDate()->format("d/m/Y H:i:s"));
                    
                    $i++;
                }
            
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers

            
            if($pro!=NULL)
            {
                $clearstring=filter_var($pro->getNom(), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'export observations '.$clearstring.' du '.$dateExport.'.xls'
            );
            }
            else
            {
             $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'export observations total du '.$dateExport.'.xls'
            );   
            }
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        
    }
}
