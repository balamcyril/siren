<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Sites;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Jsonsegment;
use Symfony\Component\HttpFoundation\Segment;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class SitesController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/sites/{projet}", requirements={"projet":"\d+"})
     */
    public function getSitesAction( Request $request, $projet) {
        $this->metier = $this->get("app.sites.metier");
        $pro= $this->get("app.projet.metier");
        $newprojet=$pro->find((int)$projet);
        if($newprojet==null)
        {
            return null;
        }
        if(!$newprojet->getPublic()) {
            return null;
        }

        $gg = $this->metier->findByProjet($newprojet);
        $i=0;
        $groupe = array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["date"]=$g->getDate();
            $groupe[$i]["nom"]=$g->getNom();
            $groupe[$i]["projet"]=$g->getProjet();
            $i++;
        }
        return $groupe;
    }
    /**
     * @Rest\View()
     * @Rest\Get("/sitesid/{id}")
     */
    public function getSiteAction($id) {
        $this->metier = $this->get("app.sites.metier");
        $g=$this->metier->find($id);
        $groupe["id"]=$g->getId();
        $groupe["date"]=$g->getDate();
        $groupe["nom"]=$g->getNom();
        $groupe["projet"]=$g->getProjet();

        return $groupe;
    }
}
