<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Segments;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class SegmentsController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/segments")
     */
    public function getSegmentsAction($site, Request $request) {
        $this->metier = $this->get("app.segments.metier");
        $newSite = $this->get("app.sites.metier")->find($site);

        if($newSite ==null)
        {
            return null;
        }
        if(!$newSite->getPublic()) {
            return null;
        }

        $gg = $this->metier->findBySites($newSite);
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom"]=$g->getNom();
            $groupe[$i]["sites"]=$g->getSites();
            $groupe[$i]["ville"]=$g->getVille();
            $groupe[$i]["region"]=$g->getRegion();
            $i++;
        }
        return $groupe;
    }
    
    
    /**
     * @Rest\View()
     * @Rest\Get("/segmentid/{id}")
     */
    public function getSegmentAction($id) {      
        $this->metier = $this->get("app.segments.metier");
        $g=$this->metier->find($id);
        $groupe["id"]=$g->getId();
        $groupe[$i]["nom"]=$g->getNom();
        $groupe[$i]["sites"]=$g->getSites();
        $groupe[$i]["ville"]=$g->getVille();
        $groupe[$i]["region"]=$g->getRegion();
        
        return $groupe;
    }   
}
