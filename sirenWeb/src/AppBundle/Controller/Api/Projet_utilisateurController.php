<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Projet_utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Projet_utilisateurController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/projetutilisateur/{utilisateur}")
     */
    public function getprojetAction($utilisateur) {
        $this->metier = $this->get("app.projet_utilisateur.metier");
        $newutil= new \AppBundle\Entity\Utilisateurs();
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        
        $liste_projet=$this->metier->findByUtilisateurs($newutil);
        
        $nb = count($liste_projet);
        $result= NULL;
        for($i = 0; $i < $nb; $i++){
            $projet = $liste_projet[$i];
            $result[$i]= $projet->getProjet();
        }
        
        $gg= $result;
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom"]=$g->getNom();
            $groupe[$i]["lieu"]=$g->getLieu();
            $groupe[$i]["maplink"]=$g->getMaplink();
            $groupe[$i]["site"]=$g->getSite();
            $groupe[$i]["organization"]=$g->getOrganization();
            $groupe[$i]["public"]=$g->getPublic();
            $groupe[$i]["pays"]=$g->getPays();
            $groupe[$i]["utilisateurs"]=$g->getUtilisateurs();
            $groupe[$i]["mention"]=$g->getMention();
            $groupe[$i]["mention_en"]=$g->getMentionEn();
            $groupe[$i]["mention_pt"]=$g->getMentionPt();
            $groupe[$i]["note"]=$g->getNote();
            $groupe[$i]["note_en"]=$g->getNoteEn();
            $groupe[$i]["note_pt"]=$g->getNotePt();
            $groupe[$i]["couleur"]=$g->getCouleur();
            if ($g->getQuestions() != NULL)
            {
                $groupe[$i]["questions"]=$g->getQuestions();
            }
            else
            {
                $groupe[$i]["questions"]=null;
            }
            
            if ($g->getImage() != NULL)
            {
                $path =  $this->get('kernel')->getRootDir().'/../web/images/projet/'.$g->getImage();
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);            
                $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            }
            
            $i++;
        }
        return $groupe;

    }
}
