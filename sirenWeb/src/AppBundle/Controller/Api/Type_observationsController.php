<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Type_observations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Type_observationsController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/typeobservations")
     */
    public function getTypeObsAction(Request $request) {
        $this->metier = $this->get("app.typeobsevations.metier");
        
        $gg=$this->metier->niveau();
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom_fr"]=$g->getNomFr();
            $groupe[$i]["nom_en"]=$g->getNomEn();
            $groupe[$i]["nom_pt"]=$g->getNomPt();
            $groupe[$i]["type_question"]=$g->getTypeQuestion();
            if ($g->getQuestions() != NULL)
            {
                $groupe[$i]["questions"]=$g->getQuestions();
            }
            else
            {
                $groupe[$i]["questions"]=0;
            }
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/typeObservations/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            
            
            $i++;
        }
        return $groupe;
    }
    
}
