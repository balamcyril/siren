<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Boussole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class BoussoleController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/getBoussoles")
     */
    public function getBoussolesAction(Request $request) {
        $this->metier = $this->get("app.boussole.metier");
        
            return $this->metier->findAll();
        
    }
    
    /**
     * @Rest\View()
     * @Rest\Get("/getBoussole/{id}")
     */
    public function getBoussoleAction($id) {
        $this->metier = $this->get("app.boussole.metier");
        
        return $this->metier->find($id);
        
    }
    
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/postBoussole")
     */
    public function postBoussoleAction(Request $request) {
        
		$titre=urldecode($request->request->get('titre'));
		$coordx=urldecode($request->request->get('coordx'));
		$coordy=urldecode($request->request->get('coordy'));
		$dateo=urldecode($request->request->get('dateo'));
		$utilisateur=urldecode($request->request->get('utilisateur'));
		
		$boussole = new Boussole();
        $boussole->setTitre($titre);
        
        $date = new \DateTime();
        $date->setTimestamp((int)$dateo);
        $boussole->setDate($date);
        
        $boussole->setCoordX($coordx);
        
        $boussole->setCoordY($coordy);
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        $boussole->setUtilisateurs($newutil);
        
        $boussole->setEtat(true);

        $this->metier = $this->get("app.boussole.metier");
        $this->metier->create($boussole);
            
        return 1;
    }   
	 
}
