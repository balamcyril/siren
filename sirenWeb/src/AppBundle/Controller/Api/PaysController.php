<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Pays;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class PaysController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/test")
     */
    public function testAction() {
        return true;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/pays")
     */
    public function getPaysAction(Request $request) {
        $this->metier = $this->get("app.pays.metier");
        
            return $this->metier->findAll();
            
    }
        
}
