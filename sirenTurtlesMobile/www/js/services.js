angular.module('starter.services', ['ngCordova'])
  .factory('UserDataService', function ($cordovaSQLite, $ionicPlatform,$q,$timeout) {
    var db, dbName = "siren12.db"
    var first;
    function useWebSql() {
      db = window.openDatabase(dbName, "1.0", "Utilisateur database", 200000)
      console.info('Using webSql')
    }
 
    function useSqlLite() {
      db = $cordovaSQLite.openDB({name: dbName, location : 1})
      console.info('Using SQLITE')
    }
 
 
    function initDatabase(){
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS saut ( id integer primary key, dateAdd datetime)')
      .then(function(res){ 
        }, onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS categorie (id integer primary key,image_id integer ,nom_fr varchar(255) ,dateAdd datetime ,nom_en varchar(255) , FOREIGN KEY(image_id) REFERENCES image(id) )').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS utilisateur (id integer primary key, username varchar(20), password varchar(20), etat integer, email varchar(30))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS categorie_type (categorie_id integer ,type_id integer , PRIMARY KEY(categorie_id,type_id), FOREIGN KEY(categorie_id) REFERENCES categorie ( id ), FOREIGN KEY(type_id) REFERENCES type(id) )').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS image (id integer primary key,observation_id integer ,chemin varchar(255) ,dateAdd datetime , FOREIGN KEY(observation_id) REFERENCES observation(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS observation (id integer primary key,patrouille_id integer ,categorie_id integer ,coordX decimal(10,5) ,coordY decimal(10,5) ,etat integer ,dateAdd datetime , FOREIGN KEY(patrouille_id) REFERENCES patrouille ( id ), FOREIGN KEY(categorie_id) REFERENCES categorie(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS patrouille (id integer primary key,segment_id integer ,type_id integer ,patrouilleur_id integer ,nomSite varchar(255) ,etat integer ,dateDebut datetime ,dateFin datetime ,coordXDebut decimal(10,5) ,coordYDebut decimal(10,5) ,coordXFin decimal(10,5) ,coordYFin decimal(10,5) , FOREIGN KEY(segment_id) REFERENCES segment ( id ), FOREIGN KEY(type_id) REFERENCES type ( id ), FOREIGN KEY(patrouilleur_id) REFERENCES patrouilleur(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS pays ( id integer primary key, nomPays_fr varchar(255) , indicatif varchar(255) , drapeau varchar(255) , dateAdd datetime , nomPays_en varchar(255))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS question (id integer primary key,categorie_id integer ,titre_fr varchar(255) ,type  varchar(255) ,dateAdd datetime ,titre_en varchar(255) ,code varchar(255) , FOREIGN KEY(categorie_id) REFERENCES categorie(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS repondre (id integer primary key,observation_id integer ,question_id integer ,reponse_id integer ,reponseText varchar(255) ,dateAdd datetime , FOREIGN KEY(observation_id) REFERENCES observation ( id ), FOREIGN KEY(question_id) REFERENCES question ( id ), FOREIGN KEY(reponse_id) REFERENCES reponse(id) )').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS saut_question (id integer primary key,saut_id integer ,question_id integer ,position integer , FOREIGN KEY(saut_id) REFERENCES saut(id) )').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS segment ( id integer primary key, nom varchar(255) , dateAdd datetime)').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS suggestion (id integer primary key,categorie_id integer ,parent_id integer ,valeur_fr varchar(255) ,valeur_en varchar(255) ,dateAdd datetime , FOREIGN KEY(categorie_id) REFERENCES categorie ( id ), FOREIGN KEY(parent_id) REFERENCES parent(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS type ( id integer primary key, nom_fr varchar(255) , dateAdd datetime , nom_en varchar(255) , valeur tinyint(1))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS reponse (id integer primary key,question_id integer ,question_suivante_id integer ,image_id integer ,valeur_fr varchar(255) ,dateAdd datetime ,valeur_en varchar(255) ,saut_id integer ,suggestion_id integer , FOREIGN KEY(question_id) REFERENCES question ( id ), FOREIGN KEY(question_suivante_id) REFERENCES question ( id ), FOREIGN KEY(image_id) REFERENCES image ( id ), FOREIGN KEY(saut_id) REFERENCES saut ( id ), FOREIGN KEY(suggestion_id) REFERENCES suggestion(id))').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS suggestion (id integer primary key,categorie_id integer ,parent_id integer ,valeur_fr varchar(255) ,valeur_en varchar(255) ,dateAdd datetime , FOREIGN KEY(categorie_id) REFERENCES categorie ( id ), FOREIGN KEY(parent_id) REFERENCES parent(id) )').then(function(res){

      },onErrorQuery);
      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS type ( id integer primary key, nom_fr varchar(255) , dateAdd datetime , nom_en varchar(255) , valeur tinyint(1))').then(function(res){
      },onErrorQuery);
      
      first=true;
      }

    function bd1(){
      $cordovaSQLite.execute(db, 'INSERT INTO categorie (id, image_id, nom_fr, dateAdd, nom_en) VALUES(?,?,?,?,?)', [1, 1, "Trace", "2017-02-01 02:45:03", "Trace"]);
      $cordovaSQLite.execute(db, 'INSERT INTO categorie (id, image_id, nom_fr, dateAdd, nom_en) VALUES(?,?,?,?,?)', [2, 2, "Cacasse", "2017-02-01 03:05:17", "Carcass"]);
      $cordovaSQLite.execute(db, 'INSERT INTO categorie (id, image_id, nom_fr, dateAdd, nom_en) VALUES(?,?,?,?,?)', [3, 3, "Nid", "2017-02-01 03:07:17", "Nest"]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie (id, image_id, nom_fr, dateAdd, nom_en) VALUES(?,?,?,?,?)', [4, 4, "Individu", "2017-02-01 03:08:32", "Individual"]);   
      
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [1,1]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [2,1]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [2,3]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [3,1]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [4,1]); 
      $cordovaSQLite.execute(db, 'INSERT INTO categorie_type (categorie_id, type_id) VALUES(?,?)', [4,3]); 

    }

    function bd2(){
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [1, 1, "La Trace est-elle associée à un nid?", "qcm", "2017-02-02 12:42:03", "Track connected to a nest ?", "A1"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [2, 1, "Trace symetrique?", "qcm", "2017-02-02 18:08:13", "Symetrical track ?", "A2"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [3, 1, "Largeur de la trace (cm)", "qro", "2017-02-02 18:41:00", "Track  width (cm)", "A3"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [4, 1, "Il s’agit de quelle espèce de tortue?", "qcm", "2017-02-02 18:58:08", "Turtle species name ?", "A4"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [5, 1, "Photo de la Trace", "qro", "2017-02-02 18:59:16", "Traces Photo", "A5"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [6, 1, "Note", "qro", "2017-02-02 19:00:30", "Note", "A6"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [7, 3, "Age de Nid", "qcm", "2017-02-02 19:02:19", "Nest age ?", "B1"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [8, 3, "Le nid est transloqué?", "qcm", "2017-02-02 19:04:48", "Translocated ?", "B3"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [9, 3, "Nid associé à une trace?", "qcm", "2017-02-02 19:10:54", "Nest connected to a track ? ", "B4"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [10, 3, "Photo du Nid", "qro", "2017-02-03 16:30:26", "Photo of the nest", "B5"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [11, 3, "Note", "qro", "2017-02-03 16:30:52", "Note", "B6"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [12, 4, "Longueur courbe de la carapace (cm)", "qro", "2017-02-03 16:31:48", "Carapace curved Length (cm)", "C1"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [13, 4, "Taille de la queue", "qcm", "2017-02-03 16:33:42", "Tail size ?", "C2"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [14, 4, "Griffe sur palette natatoire", "qcm", "2017-02-03 16:34:40", "Claws on the flippers ?", "C3"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [15, 4, "Bague ou puce présente?", "qcm", "2017-02-03 16:35:42", "Ring or PIT tag present ?", "C4"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [16, 4, "Numero de bague/puce", "qcm", "2017-02-03 16:36:25", "Ring or PIT tag number ?", "C5"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [17, 4, "Nombre de paires d’écailles costales", "qcm", "2017-02-03 16:37:23", "Number of lateral scutes ?", "C6"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [18, 4, "Présence d’un bec pointu courbé?", "qcm", "2017-02-03 16:39:36", "Pointed face ?", "C7"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [19, 4, "Forme de la carapace", "qcm", "2017-02-03 16:40:16", "Carapace shape ?", "C8"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [20, 4, "Nom de l’espèce", "qcm", "2017-02-03 16:42:27", "Species name ?", "C9"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [21, 4, "Etat de l’animal", "qcm", "2017-02-03 16:43:14", "Animal state ?", "C10"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [22, 4, "Type de marquage que vous effectuer?", "qcm", "2017-02-03 16:44:22", "What type of marking ?", "C11"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [23, 4, "Numero bague/puce", "qcm", "2017-02-03 16:45:32", "Ring or PIT tag number ", "C12"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [24, 4, "Où est l’animal à votre depart?", "qcm", "2017-02-03 16:46:27", "Where is the animal as you leave ?", "C13"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [25, 4, "Cause de la mort", "qcm", "2017-02-03 16:48:00", "Cause of the death ?", "C14"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [26, 4, "Cocher les anomalie externe visible sur l’animal", "qcm", "2017-02-03 16:49:05", "Check any external anormalities you see on the animal", "C15"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [27, 4, "Photo de la carapace et dautres élements importants", "qro", "2017-02-03 16:49:55", "Photo of carapace and other important elements", "C16"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [28, 4, "Note", "qro", "2017-02-03 16:50:29", "Note", "C17"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [29, 2, "Type de carcasses", "qcm", "2017-02-03 16:51:15", "Type of carcass", "C18"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [30, 2, "Photo de la carcasse et d’autres élements importants", "qro", "2017-02-03 16:52:03", "Photo of carcass and other important elements", "C19"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [31, 2, "Note", "qro", "2017-02-03 16:52:39", "Note", "C20"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [32, 3, "Etat du Nid", "qcm", "2017-02-09 17:56:28", "Nest state ?", "B2"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [33, 4, "Numéro", "qro", "2017-02-13 14:23:43", "Number", "C5i"]);
      $cordovaSQLite.execute(db, 'INSERT INTO question (id, categorie_id,titre_fr,type,dateAdd,titre_en,code) VALUES(?,?,?,?,?,?,?)', [34, 2, "Numéro", "qro", "2017-02-13 14:24:20", "Number", "C12i"]);
      
    }

    function bd3(){
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [1, 1, 2, null, "Oui", "2017-02-02 18:05:55", "Yes", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [2, 1, 2, null, "Non", "2017-02-02 18:06:13", "No", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [3, 2, 3, null, "Oui", "2017-02-09 17:20:43", "Yes", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [4, 2, 3, null, "Non", "2017-02-09 17:28:57", "No", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [5, 4, 5, null, "Olivâtre", "2017-02-09 17:47:26", "Olive Ridley", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [6, 4, 5, null, "Imbriquée", "2017-02-09 17:48:41", "Hawksbill", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [7, 4, 5, null, "Verte", "2017-02-09 17:49:38", "Green", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [8, 4, 5, null, "Luth", "2017-02-09 17:50:33", "Leatherback", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [9, 4, 5, null, "Indéterminée", "2017-02-09 17:51:54", "Undetermined", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [10, 4, 5, null, "Autre", "2017-02-09 17:52:31", "Other", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [11, 7, 8, null, "Frais (nuit precédente)", "2017-02-09 17:55:03", "Fresh (from last night)", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [12, 7, 32, null, "Ancien", "2017-02-09 18:06:24", "Old", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [13, 7, 32, null, "Indéterminée", "2017-02-09 18:08:44", "Undetermined", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [14, 32, 8, null, "Oeufs collectées", "2017-02-09 18:09:38", "Eggs collected", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [15, 32, 8, null, "Nid pertubé", "2017-02-10 15:01:41", "Disturbed nest", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [16, 32, 8, null, "Intacte", "2017-02-10 15:02:58", "Intact", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [17, 32, 8, null, "Autre", "2017-02-10 15:03:29", "Other", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [18, 8, 9, null, "Oui", "2017-02-10 15:04:24", "Yes", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [19, 8, 9, null, "Non", "2017-02-10 15:04:52", "No", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [21, 9, null, null, "Oui", "2017-02-10 15:35:16", "Yes", 2, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [22, 9, null, null, "Non", "2017-02-10 15:52:10", "No", 3, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [23, 13, 14, null, "Petite", "2017-02-10 16:01:37", "Small", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [24, 13, 14, null, "Grosse", "2017-02-10 16:02:12", "Large", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [25, 14, 15, null, "Oui", "2017-02-10 16:03:02", "Yes", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [26, 14, 15, null, "Non", "2017-02-10 16:03:45", "No", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [27, 15, 16, null, "Oui", "2017-02-10 16:04:31", "Yes", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [28, 15, 17, null, "Non", "2017-02-10 16:04:56", "No", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [29, 16, 33, null, "Bague droite", "2017-02-10 16:06:02", "Right ring", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [30, 17, 20, null, "Aucune (recouvert de cuire)", "2017-02-10 16:27:38", "No scutes", null, 1]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [31, 17, 18, null, "4 paires", "2017-02-10 16:28:26", "4 pairs", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [32, 17, 19, null, "5 paires", "2017-02-10 16:29:31", "5 pairs", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [33, 17, 20, null, "6-9 paires", "2017-02-10 16:30:49", "6-9 pairs", null, 13]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [34, 18, 20, null, "Oui", "2017-02-10 16:31:39", "Yes", null, 4]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [35, 18, 20, null, "Non", "2017-02-10 16:32:01", "No", null, 3]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [36, 19, 20, null, "Plutôt ronde", "2017-02-10 16:33:08", "Rather round", null, 5]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [37, 19, 20, null, "Plutôt longue", "2017-02-10 16:33:52", "Rather long", null, 6]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [38, 20, 21, null, "Luth", "2017-02-10 16:37:48", "Leatherback", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [39, 20, 21, null, "Olivâtre", "2017-02-10 16:45:49", "Olive Ridley", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [40, 20, 21, null, "Verte", "2017-02-10 16:55:27", "Green", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [41, 20, 21, null, "Imbriquée", "2017-02-10 16:57:45", "Hawksbill", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [42, 20, 21, null, "Caouanne", "2017-02-10 17:07:25", "Loggerhead", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [43, 20, 21, null, "Kemp", "2017-02-10 17:09:27", "Kemp's", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [44, 20, 21, null, "Indéterminée", "2017-02-10 17:10:44", "Undetermined", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [45, 21, 22, null, "Vivant, femelle en ponte", "2017-02-10 17:28:21", "Alive, nesting female", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [46, 21, 22, null, "Vivant, capture", "2017-02-10 17:29:06", "Alive, captured", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [47, 21, 25, null, "Mort", "2017-02-11 16:42:36", "Dead", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [48, 22, 23, null, "Bagues metallites", "2017-02-11 16:48:09", "Rings", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [49, 22, 23, null, "Puce", "2017-02-11 16:57:03", "PIT tagging", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [50, 22, 24, null, "Photo ID", "2017-02-11 16:58:44", "Photo ID", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [51, 22, 24, null, "Aucun", "2017-02-11 16:59:46", "None", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [52, 23, 24, null, "bague droite", "2017-02-11 17:01:04", "Straight ring", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [53, 23, 24, null, "bague gauche", "2017-02-11 17:01:56", "Left ring", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [54, 23, 24, null, "Puce", "2017-02-11 17:02:57", "Chip", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [55, 24, 25, null, "Toujours en ponte", "2017-02-11 17:03:42", "Still nesting", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [56, 24, 25, null, "Retourné/relaché en mer", "2017-02-11 17:04:36", "Returned/released back to sea", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [57, 24, 25, null, "En capture", "2017-02-11 17:05:27", "In captivity", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [58, 24, 25, null, "Tué", "2017-02-11 17:30:22", "Killed", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [59, 24, 25, null, "Indéterminée", "2017-02-11 20:32:22", "Undetermined", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [60, 25, 26, null, "Echouée", "2017-02-13 12:57:04", "Stranded", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [61, 25, 27, null, "Tuée pendant la ponte", "2017-02-13 12:58:00", "Killed during nesting", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [62, 25, 27, null, "Capturée dans le filet", "2017-02-13 12:59:00", "Captured in a Net", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [63, 25, 27, null, "Autre", "2017-02-13 13:00:07", "Other", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [64, 26, 27, null, "Aucune", "2017-02-13 13:05:02", "None", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [65, 26, 27, null, "Lésion", "2017-02-13 13:12:09", "Lesion", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [66, 26, 27, null, "Masse cutanée (fibro-papillomes)", "2017-02-13 13:13:14", "Skin tumor (fibro-papillomas)", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [67, 26, 27, null, "Autre", "2017-02-13 13:13:40", "Other", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [68, 29, null, null, "Carapace", "2017-02-13 14:18:12", "Carapace", 4, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [69, 5, 6, null, "Photo", "2017-02-13 14:19:34", "Continue", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [70, 10, 11, null, "Photo", "2017-02-13 14:20:06", "Continue", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [71, 30, 31, null, "Photo", "2017-02-13 14:22:00", "Continue", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [72, 27, 28, null, "Photo", "2017-02-13 14:22:41", "Continue", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [73, 3, 4, null, "texte", "2017-02-13 15:20:25", "text", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [74, 29, 30, null, "Os", "2017-02-14 11:00:21", "Bone", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [75, 29, 30, null, "Coquille", "2017-02-14 11:01:16", "Shell", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [76, 29, 30, null, "Autre", "2017-02-14 11:02:00", "Other", null, null]);   
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [77, 16, 33, null, "bague gauche", "2017-03-01 13:39:19", "Left ring", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [78, 16, 33, null, "Puce", "2017-03-01 13:40:09", "PIT tagging", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [79, 12, 13, null, "texte", "2017-03-01 13:41:15", "text", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [81, 34, 24, null, "texte", "2017-03-24 10:30:30", "text", null, null]);
      $cordovaSQLite.execute(db, 'INSERT INTO reponse (id, question_id,question_suivante_id,image_id,valeur_fr,dateAdd,valeur_en,saut_id,suggestion_id) VALUES(?,?,?,?,?,?,?,?,?)', [82, 33, 17, null, "texte", "2017-03-24 10:32:08", "text", null, null]);

    }

    function bd4(){
      $cordovaSQLite.execute(db, 'INSERT INTO saut (id,dateAdd) VALUES(?,?)', [2, "2017-02-10 15:35:16"]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut (id,dateAdd) VALUES(?,?)', [3, "2017-02-10 15:52:10"]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut (id,dateAdd) VALUES(?,?)', [4, "2017-02-13 14:18:12"]);
      
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [1, 2, 2, 1]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [2, 2, 3, 2]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [3, 2, 4, 3]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [4, 2, 10, 4]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [5, 3, 4, 1]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [6, 3, 10, 2]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [7, 4, 17, 1]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [8, 4, 18, 2]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [9, 4, 19, 3]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [10, 4, 20, 4]);
      $cordovaSQLite.execute(db, 'INSERT INTO saut_question (id,saut_id,question_id,position) VALUES(?,?,?,?)', [11, 4, 30, 5]);
      
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [1, 4, 38, 20]);
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [3, 4, 40, 20]);
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [4, 4, 41, 20]);
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [5, 4, 42, 20]);
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [13, 4, 39, 20]);
      $cordovaSQLite.execute(db, 'INSERT INTO suggestion (id,categorie_id,parent_id,question_id) VALUES(?,?,?,?)', [6, 4, 43, 20]);

      $cordovaSQLite.execute(db, 'INSERT INTO type (id,nom_fr,dateAdd,nom_en,valeur) VALUES(?,?,?,?,?)', [1, "Diurne", "2017-01-30 15:41:39", "Diurnal", 1]);
      $cordovaSQLite.execute(db, 'INSERT INTO type (id,nom_fr,dateAdd,nom_en,valeur) VALUES(?,?,?,?,?)', [3, "Nocturne", "2017-01-30 15:53:59", "Nocturnal", 0]);
      first=false;
    }

    $ionicPlatform.ready(function () {
      if(window.cordova){
        useSqlLite();
      } else {
        useWebSql();
      }
      initDatabase();
      if(first){
        bd1();
        bd2();
        bd3();
        bd4();
      }
      
    })
 
    function onErrorQuery(err){
      console.error(err)
    }
    return {
      createUser: function (user) {
        return $cordovaSQLite.execute(db, 'INSERT INTO utilisateur (username, email, password, etat) VALUES(?,?,?,?)', [user.username, user.email, user.password, user.etat])
      },
      createSegment: function (segment) {
        return $cordovaSQLite.execute(db, 'INSERT INTO segment (nom, dateAdd) VALUES(?,?)', [segment.nom, segment.dateAdd])
      },
      createPatrouille: function (patrouille) {
        return $cordovaSQLite.execute(db, 'INSERT INTO patrouille (segment_id, type_id, patrouilleur_id, nomSite, etat, dateDebut,dateFin,coordYDebut,coordXDebut,coordYFin,coordXFin) VALUES(?,?,?,?,?,?,?,?,?,?,?)', [patrouille.segment_id, patrouille.type_id, patrouille.patrouilleur_id, patrouille.nomSite,patrouille.etat,patrouille.dateDebut,patrouille.dateFin,patrouille.coordYDebut,patrouille.coordXDebut,patrouille.coordYFin,patrouille.coordXFin])
      },      
      createObservation: function (observation) {
        observation.categorie_id=observation.categorie;
        //console.log(observation);
        var variable = $cordovaSQLite.execute(db, 'INSERT INTO observation (patrouille_id, categorie_id, coordX, coordY, etat, dateAdd) VALUES(?,?,?,?,?,?)', [observation.patrouille_id, observation.categorie_id, observation.coordX, observation.coordY, observation.etat, observation.dateAdd])
        //console.log(variable.$$state.value.insertId);
        return variable;
        },
      updateUser: function(user){
        return $cordovaSQLite.execute(db, 'UPDATE utilisateur set username = ?, password = ? where id = ?', [user.username, user.password, user.id])
      },
      updateEtatPatrouille: function(patrouille){
        return $cordovaSQLite.execute(db, 'UPDATE patrouille set etat = ? where id = ?', [patrouille.etat , patrouille.id])
      },
      getAll: function(callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM utilisateur').then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getAllPatrouille: function(callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT p.id,p.nomSite, p.etat, p.dateDebut,p.dateFin,p.coordYDebut,p.coordXDebut,p.coordYFin,p.coordXFin,s.nom FROM patrouille p, segment s WHERE s.id=p.segment_id').then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getAllPatrouilleSynchro: function(callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT patrouille.*,observation.*,segment.nom FROM patrouille INNER JOIN observation ON patrouille.id=observation.patrouille_id INNER JOIN segment ON patrouille.segment_id=segment.id  WHERE patrouille.etat=0  ORDER BY patrouille.id').then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getAllRepondre: function(callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM observation').then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getAllObservation: function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM observation o WHERE o.patrouille_id=?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i));
            }
            callback(data);
          }, onErrorQuery)
        })
      },
      getAllSegment: function(callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM segment').then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      
      deleteUser: function(callback){
        $ionicPlatform.ready(function () {
         $cordovaSQLite.execute(db, 'DELETE FROM observation').then(function () {
           
            callback("data");
          }, onErrorQuery)
        })
      },
      getReponse:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM reponse WHERE question_id = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getReponseId:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM reponse WHERE id = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      findSegment:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM segment WHERE nom = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getQuestion:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM question WHERE categorie_id = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getQuestionBySaut:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM saut_question WHERE saut_id = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getQuestionId:function(id,callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM question WHERE id = ?',[id]).then(function (results) {
            var data = []
 
            for (i = 0, max = results.rows.length; i < max; i++) {
              data.push(results.rows.item(i))
            }
 
            callback(data)
          }, onErrorQuery)
        })
      },
      getById: function(id, callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM utilisateur where id = ?', [id]).then(function (results) {
            callback(results.rows.item(0))
          })
        })
      },
      getByEmail: function(username, callback){
        $ionicPlatform.ready(function () {
          $cordovaSQLite.execute(db, 'SELECT * FROM utilisateur where email = ?', [username]).then(function (results) {
            
            try{
              callback(results.rows.item(0));
            }
            catch(err){
              callback(null);
            }

             })
        })
      }

    };
})
  