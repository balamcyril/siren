import csv 
from datetime import date
import os, json

# script creating arb files out of csv and save them in a folder named by the date

english_version = "en"
french_version = "fr"
arab_version = "ar"
portugese_version = "pt"
german_version = "de"

csv_filename = 'siren_leo_locale_strings.csv'

language_friendly_string = {"en":"English", "fr":"French", "de":"German", "ar":"Arabic", "pt":"Portuguese" }

def create_day_folder():
    path = str(date.today())
    try:
        os.mkdir(path)
    except Exception:
        print('folder already exists')
    return path

    
def create_arb_file(data_map, path, locale):
    file = open(path + "/app_"+ locale+".arb", 'w')
    file.write('{\n')
    for key in data_map:
        file.write('  "'+key+'": "'+data_map[key]+'",\n')
    file.write('}')
    file.close()
    
def create_map(filename):
    locale_map = {}
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for locale in language_friendly_string:
                language = language_friendly_string[locale]
                if not language in locale_map:
                    locale_map[language] = {}
                if language in row and len(row[language])>0:
                    locale_map[language][row['String ID']]=row[language]
    return locale_map


path = create_day_folder()
for locale in language_friendly_string:
    create_arb_file(create_map(csv_filename)[language_friendly_string[locale]], path, locale)
    print(language_friendly_string[locale] + ' created in folder ' + path)
