class Observation:
    def __init__(self):
        self.id = None
        self.project_id = None
        self.project_name = None
        self.inaturalist_id = None
        self.coord_x = None
        self.coord_y = None
        self.note = None
        self.alpha = None
        self.collector = None
        self.images = {'img1': None, 'img2': None, 'img3': None, 'img4': None}
        self.dead = None
        self.status = None
        self.type_observation_id = None
        self.type_observation = None
        self.specie_id = None
        self.specie = None
        self.family_id = None
        self.family = None
        self.group_id = None
        self.group = None
        self.user_id = None
        self.date = None
        self.segment_id = None
        self.site_id = None
        self.segment = None
        self.site = None

    def to_dict(self):
        observation_dict = {
            'id': self.id,
            'project_id': self.project_id,
            'projectName': self.project_name,
            'idInaturalist': self.inaturalist_id,
            'coordX': self.coord_x,
            'coordY': self.coord_y,
            'note': self.note,
            'alpha': self.alpha,
            'collector': self.collector,
            'img1': self.images['img1'],
            'img2': self.images['img2'],
            'img3': self.images['img3'],
            'img4': self.images['img4'],
            'dead': self.dead,
            'status': self.status,
            'type_observation_id': self.type_observation_id,
            'TypeObservation': self.type_observation,
            'specie_id': self.specie_id,
            'specie': self.specie,
            'familyId': self.family_id,
            'family': self.family,
            'groupe': self.group_id,
            'group': self.group,
            'user_id': self.user_id,
            'date': self.date,
            'segment_id': self.segment_id,
            'site_id': self.site_id,
            'segment': self.segment,
            'site': self.site
        }
        # Remove keys with None values
        observation_dict = {key: value for key, value in observation_dict.items() if value is not None}

        return observation_dict

    @classmethod
    def from_dict(cls, data):
        observation = cls()
        observation.id = data.get('id')
        observation.project_id = data.get('projectId')
        observation.project_name = data.get('projectName')
        observation.inaturalist_id = data.get('idInaturalist')
        observation.coord_x = data.get('coordX')
        observation.coord_y = data.get('coordY')
        observation.note = data.get('note')
        observation.alpha = data.get('alpha')
        observation.collector = data.get('collector')
        observation.images = data.get('images', observation.images)
        observation.dead = data.get('dead')
        observation.status = data.get('status')
        observation.type_observation_id = data.get('TypeObservationId')
        observation.type_observation = data.get('TypeObservation')
        observation.specie_id = data.get('specieId')
        observation.specie = data.get('specie')
        observation.family_id = data.get('familyId')
        observation.family = data.get('family')
        observation.group_id = data.get('groupId')
        observation.group = data.get('group')
        observation.user = data.get('user')
        observation.date = data.get('date', observation.date)
        observation.segment = data.get('segment', observation.segment)
        observation.site = data.get('site_id', observation.segment)

        return observation

    def __str__(self):
        image_str = ''
        if self.images:
            image_str = ''
            for key, value in self.images.items():
                if key and value:
                    image_str += ' ,' + f'{key}: {value[:150]}'
        date_str = str(self.date)

        return (
            f"Observation ID: {self.id}, "
            f"Project ID: {self.project_id}, "
            f"Project Name: {self.project_name}, "
            f"iNaturalist ID: {self.inaturalist_id}, "
            f"Coordinates: ({self.coord_x}, {self.coord_y}), "
            f"Note: {self.note}, "
            f"Alpha: {self.alpha}, "
            f"Collector: {self.collector}, "
            f"Images: {image_str}, "
            f"Dead: {self.dead}, "
            f"Status: {self.status}, "
            f"Type Observation ID: {self.type_observation_id}, "
            f"Type Observation: {self.type_observation}, "
            f"Species ID: {self.specie_id}, "
            f"Species: {self.specie}, "
            f"Family ID: {self.family_id}, "
            f"Family: {self.family}, "
            f"Group ID: {self.group_id}, "
            f"Group: {self.group}, "
            f"User: {self.user_id}, "
            f"Date: {date_str}, "
            f"Segment ID: {self.segment_id}, "
            f"Site ID: {self.site_id}, "
            f"Segment: {self.segment}, "
            f"Site: {self.site}"
        )

    def set_image(self, img_num, image_url):
        if 1 <= img_num <= 4:
            img_key = f'img{img_num}'
            if image_url != None:
                pass
            else:
                image_url = ''
            self.images[img_key] = image_url
        return self

    def set_coordinates(self, coord_x, coord_y):
        self.coord_x = coord_x
        self.coord_y = coord_y
        return self

    def set_utc_timestamp(self, timestamp):
        self.date = timestamp
        return self