import requests

import siren_data
from observation import Observation
from observation_builder import ObservationBuilder

class SirenAPI:
    def __init__(self, host, verbose = True):
        self.token = ''
        self.host = host
        self.headers = { 'Content-Type': 'application/json; charset=UTF-8', 'Authorization' : ''}
        self.headersQa = { 'Content-Type': 'charset=UTF-8', 'Authorization' : ''}

        self.verbose = True
        self.email = ''
        self.login_data = ''


    def login(self, email, password):
        if self.verbose:
            print(f'*** Login with {email}')
        self.email = email
        r = requests.get(self.host + 'mobile/login?email=' + email + '&password=' + password)
        if r.status_code != 200:
            print(r)
        assert r.status_code == 200
        response_json = r.json()
        self.login_data = response_json
        if self.verbose:
            print(self.login_data)
        self.token = response_json['data']['token']
        assert len(self.token) > 0
        self.headers['Authorization'] = 'Bearer ' + self.token
        self.headersQa['Authorization'] = 'Bearer ' + self.token
        open('token.txt', 'w').write(self.token)
        return response_json['data']

    def get_projects(self, user_id):
        if self.verbose:
            print(f'*** Retrieve projects for user {self.email} id: {user_id}')
        r = requests.get(self.host + 'user/project', headers=self.headers)
        assert r.status_code == 200
        projects = []
        response_json = r.json()
        for s in response_json['data']:
            projects.append(s)
        return projects

    def get_type_observation(self):
        if self.verbose:
            print(f'*** Retrieve types of observation')
        r = requests.get(self.host + 'typeObservation/list', headers=self.headers)
        response_json = r.json()
        assert r.status_code == 200
        return response_json['data']

    def get_users(self, project_id):
        if self.verbose:
            print(f'*** Retrieve users for project id: {project_id}')
        r = requests.get(self.host + 'userproject/list?project_id=' + project_id , headers=self.headers)
        assert r.status_code == 200
        users = []
        response_json = r.json()
        print(response_json)
        for s in response_json['data']:
            users.append(s)
        return users

    def get_species(self):
        if self.verbose:
            print(f'*** Retrieve species')
        r = requests.get(self.host + 'specie/list', headers=self.headers)
        assert r.status_code == 200
        species = []
        response_json = r.json()
        for s in response_json['data']:
            species.append(s)
        return species

    def get_sites(self, project_name, project_id ):
        if self.verbose:
            print(f'*** Retrieve sites (beaches) for project {project_name}')
        r = requests.get(self.host + 'mobile/site?project_id=' + project_id, headers=self.headers)
        assert r.status_code == 200
        sites = []
        response_json = r.json()
        for site in response_json['data']:
            sites.append(site)
        return sites

    def get_groups(self ):
        if self.verbose:
            print(f'*** Retrieve groups')
        r = requests.get(self.host + 'group/list', headers=self.headers)
        assert r.status_code == 200
        groups = []
        response_json = r.json()
        for g in response_json['data']:
            groups.append(g)
        return groups

    def get_questions(self ):
        if self.verbose:
            print(f'*** Retrieve questions')
        r = requests.get(self.host + 'question/list', headers=self.headers)
        assert r.status_code == 200
        q = []
        response_json = r.json()
        for g in response_json['data']:
            q.append(g)
        return q

    def create_question(self, project_id, title, type):
        if self.verbose:
            print(f'*** Create question {title} type {type} for project {project_id}')
        title.replace(' ','%20')
        r = requests.post(self.host + 'question/create?title=' + title + '&typeAnswer=' + type + '&typeBegin=personnel&interval=0&projectId=' + str(project_id), headers=self.headers)
        response_json = ''
        if 'application/json' in r.headers.get('Content-Type', ''):
            response_json = r.json()
        else:
            self.print_no_json(r)
        assert r.status_code == 200
        return response_json

    def create_site(self, name, project_id):
        if self.verbose:
            print(f"*** Create site {name} for project {project_id}")
        r = requests.post(self.host + f'site/create?name={name}&projectId={project_id}', headers=self.headers)
        response_json = ''
        if 'application/json' in r.headers.get('Content-Type', ''):
            response_json = r.json()
        else:
            self.print_no_json(r)
        assert r.status_code == 200
        return response_json

    def add_result_to_observation(self, obs_id, question_id, content):
        result_payload = {
        'observation_id': obs_id,
        'question_id': question_id,
        'content': content
        }
        if self.verbose:
            question_str = siren_data.get_question_name(question_id)
            print(f"*** Add a result to {obs_id}, question: {question_id} {question_str} content: {content}")
        r = requests.post(self.host + 'mobile/result/create', json=result_payload, headers=self.headers)
        response_json = r.json()
        assert r.status_code == 200
        assert 'successfully' in response_json['message']
        param = response_json['param']
        id = response_json['data']['id']
        return response_json['data']

    def get_all_results(self, observation_id):
        if self.verbose:
            print("*** List all results")
        r = requests.get( f'{self.host}resultByObservation/list?observationId={observation_id}', headers=self.headers)
        assert r.status_code == 200
        return r.json()['data']

    def submit_one_observation_qa(self, observation):
        obs_dict = observation.to_dict()
        files = {}
        print("*** Submit one observation")
        if self.verbose:
            obs_str = ''
            for k, v in obs_dict.items():
                obs_str += k + ': ' + str(v) + ' '
                files[k] = v
            print(files)
        #files['img1'] = ('image.jpg', open(obs_dict['img1'], 'rb'), 'image/jpeg')
        files['img1'] = ''
        #files['qa_number'] = 0
        r = requests.post(self.host + 'mobile/observationQa/create', data=files, headers=self.headersQa)
        print(r.request.body)
        print(r.request.url)
        print(r.request.headers)
        if r.status_code != 200:
            print(r.status_code)
            print(r.text)
        assert r.status_code == 200
        try:
            response_json = r.json()
        except Exception as e:
            print('Json expected, exception caught')
            print(e)
            print(r.text)
            raise Exception('Submit observation failed')
        assert('successfully' in response_json['message'])
        server_observation__id = response_json['data']['id']
        assert (server_observation__id > 10000)
        return server_observation__id

    def submit_one_observation(self, observation):
        obs_dict = observation.to_dict()
        if self.verbose:
            print("*** Submit one observation")
            obs_str = ''
            for k, v in obs_dict.items():
                if 'img' in k:
                    obs_str += k + ': ' + v[:20] + '... '
                else:
                    obs_str += k + ': ' + str(v) + ' '
            print(obs_str)
        r = requests.post(self.host + 'mobile/observation/create', json=obs_dict, headers=self.headers)
        if r.status_code != 200:
            print('error while uploading', r.status_code)
            print(r.text)
            return -1
        server_observation__id = int(r.text)
        assert (server_observation__id > 10000)
        return server_observation__id

    def validate_observation(self, observation__id):
        if self.verbose:
            print(f'*** Validate the observation {observation__id}')
        r = requests.post(self.host + f'OneObservation/validate?observation_id={observation__id}', headers=self.headers)
        response_json = r.json()
        assert r.status_code == 200
        assert 'successfully' in response_json['message']

    def get_one_observation(self, observation_id):
        if self.verbose:
            print(f'Retrieve one observation {observation_id}')
        r = requests.get(self.host + 'observation/readOne?id=' + str(observation_id), headers=self.headers)
        print(r.text)
        assert r.status_code == 200
        observation_data = r.json()['data']
        builder = ObservationBuilder()
        return builder.with_id(observation_data['id']) \
            .with_project_id(observation_data.get('projectId')) \
            .with_project_name(observation_data.get('projectName')) \
            .with_inaturalist_id(observation_data.get('idInaturalist')) \
            .with_coordinates(observation_data.get('coordX'), observation_data.get('coordY')) \
            .with_note(observation_data.get('note')) \
            .with_alpha(observation_data.get('alpha')) \
            .with_collector(observation_data.get('collector')) \
            .with_image(1, observation_data.get('img1')) \
            .with_image(2, observation_data.get('img2')) \
            .with_image(3, observation_data.get('img3')) \
            .with_image(4, observation_data.get('img4')) \
            .with_dead(observation_data.get('dead')) \
            .with_status(observation_data.get('status')) \
            .with_type_observation(observation_data.get('TypeObservationId'), observation_data.get('TypeObservation')) \
            .with_user_id(observation_data.get('user')) \
            .with_date(observation_data.get('date')) \
            .with_segment(observation_data.get('segmentId'), observation_data.get('segment')) \
            .with_specie(observation_data.get('specieId'), observation_data.get('specie')) \
            .with_site(observation_data.get('siteId'), observation_data.get('site')) \
            .with_family(observation_data.get('familyId'), observation_data.get('family')) \
            .with_group(observation_data.get('groupId'), observation_data.get('group')) \
            .build()

    def print_no_json(self, request):
        print(f'No Json object returned, text (len:{len(request.text)}):\n{request.text[:200]} \n...')