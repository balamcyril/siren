import os
import base64
from PIL import Image
import exifread

def get_b64_image_message(filepath):
    with open(filepath, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        base64_message = base64_encoded_data.decode('utf-8')
    return base64_message

def get_decimal_from_dms(dms):
    degrees, minutes, seconds = dms
    decimal = degrees + (minutes / 60.0) + (seconds / 3600.0)
    return decimal

def extract_gps_info(gps_info):
    latitude = get_decimal_from_dms(gps_info[2])
    if gps_info[1] == 'S':
        latitude = -latitude

    longitude = get_decimal_from_dms(gps_info[4])
    if gps_info[3] == 'W':
        longitude = -longitude

    return "{:.8f}".format(latitude), "{:.8f}".format(longitude)

def extract_gps_from_image(photo_path):
    try:
        gps_info = {
            1: 'N',
            2: (2.0, 56.0, 14.5067),
            3: 'E',
            4: (9.0, 54.0, 25.2359),
            # ... other GPS tags
        }

        extracted_gps = extract_gps_info(gps_info)
        return extracted_gps

    except Exception as e:
        return None


def extract_date_time_from_image(photo_path):
    img = Image.open(photo_path)
    # Extract Exif metadata
    exif_data = img._getexif()
    # Find the Exif tag that corresponds to date and time
    date_time_tag = 0x0132  # Exif tag for Date and Time
    # Check if the Exif data contains the date and time information
    if exif_data and date_time_tag in exif_data:
        date_time = exif_data[date_time_tag]
        return date_time
    return None


# return a list of full path
def find_images(date, picture_code):
    path = os.path.join(os.getcwd(), date.split('/')[0], picture_code)
    images_path = []
    if not os.path.exists(path):
        return []
    for f in os.listdir(path):
        images_path.append(os.path.join(path,f))
    return images_path

#must be launch from the root of the search
#return full path of the picture
def find_photo_path(filename, verbose = False):
    path = os.getcwd()
    if verbose:
        print(f'Looking for {filename} in {path}')
    return recurse_find(filename, path, verbose)

def recurse_find(filename, path, verbose):
    if verbose:
        print(f'Looking in {path}')
    files = os.listdir(path)
    if filename in files:
        if verbose:
            print(f'Found in {path}')
        return os.path.join(path, filename)
    for f in files:
        if verbose:
            print(f'File {f}')
        if os.path.isdir(os.path.join(path, f)):
            found_path = recurse_find(filename, os.path.join(path,f), verbose)
            if found_path is not None:
                return found_path
    return None

test_gps = False
test_find_photo_by_name = False
test_find_photo_by_code = False
test_date_time = False


if test_find_photo_by_name:
    photo_path = "https://drive.google.com/drive/folders/IMG-20230801-WA0156.jpg"
    filename = photo_path.split('/')[-1]
    print(f'Looking for {filename}')
    os.chdir('/Users/bonansea/Tech/Siren Photos/ID Photos')
    print(find_photo_path(filename, False))

if test_find_photo_by_code:
    picture_code = '1'
    complete_date = '13/07/2023'
    os.chdir('/Users/bonansea/Tech/Siren Photos/ID Photos')
    files = find_images(complete_date, picture_code)
    for f in files:
        print(os.path.getsize(f), f)

if test_gps:
    # Replace with the actual path to your photo
    photo_path = "shark.jpg"
    extracted_gps = extract_gps_from_image(photo_path)

    if extracted_gps:
        latitude, longitude = extracted_gps
        print(f"Latitude: {latitude}, Longitude: {longitude}")
    else:
        print("Error extracting GPS data or no GPS data found.")

if test_date_time:
    photo_path = "shark.jpg"
    print(extract_date_time_from_image(photo_path))