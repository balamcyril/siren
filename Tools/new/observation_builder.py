from observation import Observation


class ObservationBuilder:
    def __init__(self):
        self.observation = Observation()

    def with_id(self, id):
        self.observation.id = id
        return self

    def with_project_id(self, project_id):
        self.observation.project_id = project_id
        return self

    def with_project_name(self, project_name):
        self.observation.project_name = project_name
        return self

    def with_inaturalist_id(self, inaturalist_id):
        self.observation.inaturalist_id = inaturalist_id
        return self

    def with_coordinates(self, coord_x, coord_y):
        self.observation.coord_x = coord_x
        self.observation.coord_y = coord_y
        return self

    def with_note(self, note):
        self.observation.note = note
        return self

    def with_alpha(self, alpha):
        self.observation.alpha = alpha
        return self

    def with_collector(self, collector):
        self.observation.collector = collector
        return self

    def with_images(self, images):
        self.observation.images = images
        return self

    def with_image(self, img_num, image_url):
        if 1 <= img_num <= 4:
            img_key = f'img{img_num}'
            if image_url != None:
                pass
            else:
                image_url = ''
            self.observation.images[img_key] = image_url
        return self

    def with_dead(self, dead):
        self.observation.dead = dead
        return self

    def with_status(self, status):
        self.observation.status = status
        return self

    def with_type_observation(self, type_observation_id, type_observation):
        self.observation.type_observation_id = type_observation_id
        self.observation.type_observation = type_observation
        return self

    def with_specie(self, specie_id, specie):
        self.observation.specie_id = specie_id
        self.observation.specie = specie
        return self

    def with_family(self, family_id, family):
        self.observation.family_id = family_id
        self.observation.family = family
        return self

    def with_group(self, group_id, group):
        self.observation.group_id = group_id
        self.observation.group = group
        return self

    def with_user_id(self, user_id):
        self.observation.user_id = user_id
        return self

    def with_date(self, date_data):
        self.observation.date = date_data
        return self

    def with_segment(self, segment_id, segment):
        self.observation.segment_id = segment_id
        self.observation.segment = segment
        return self

    def with_site(self, site_id, site):
        self.observation.site_id = site_id
        self.observation.site = site
        return self

    def build(self):
        return self.observation
