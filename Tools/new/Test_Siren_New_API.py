#Test New APIs

import requests
import datetime
import math
from siren_data import users, types_observation, host_api, get_project_info, host_test_api, types_answers
from siren_api import SirenAPI
import base64
from observation_builder import ObservationBuilder
from image_metadata import get_b64_image_message, extract_gps_from_image

test_run_all = False
test_create_site = False
test_sites_list = False
test_specie_list = False
test_group_list = False
test_read_one_observation = False
test_answer_list = False
test_get_all_observations = False
test_submit_one_observation = False
test_submit_one_observation_qa = False
test_validate_observation = False #Submit must occur before
test_add_result = False
test_result_list = False
test_project_list = True
test_user_list = True
test_list_type_observation = False
test_create_question = False
test_question_list = False

project_name = 'leo'
test_server = False

project_info = get_project_info(project_name)
project_id = project_info['project_id']

host = host_api
if test_server:
    host = host_test_api

print(f'Login to {host} project {project_name}, id: {project_id}')
siren_api = SirenAPI(host)
siren_api.login(project_info['email'], project_info['password'])

if test_user_list:
    project_id = '62184'
    users_list = siren_api.get_users(project_id)
    for p in users_list:
        print(p['id'], p['email'], p['username'], p['firstname'], p['role'])

if test_project_list or test_run_all:
    projects = siren_api.get_projects(None)
    for p in projects:
        print(p['id'], p['name'])

if test_create_site or test_run_all:
    print(siren_api.create_site('Ngoyé',project_id))

if test_sites_list or test_run_all:
    sites = siren_api.get_sites(project_name, project_id)
    for s in sites:
        print(s['id'], s['name'])

if test_answer_list or test_run_all:
    print("List of quesions")
    r = requests.get(host_api + 'answer/list', headers=siren_api.headers)
    print(r.text)
    response_json = r.json()
    assert r.status_code == 200
    print('OK ')
    for t in response_json['data']:
        print(t)

server_observation__id = 0
if test_submit_one_observation or test_run_all:
    no_image = False
    image_file_name = 'shark.jpg'
    fullpath = '/Users/bonansea/Tech/siren-develop-Tools-new/Tools/new/' + image_file_name
    if no_image:
        image_str = ''
    else:
        base64_message = get_b64_image_message(fullpath)
        image_str = 'data:image/jpeg;base64,' + base64_message
    (latitude,longitude)= extract_gps_from_image(fullpath)
    builder = ObservationBuilder()
    observation = builder.with_project_id(project_id) \
                         .with_coordinates(latitude, longitude) \
                         .with_specie(-1, None) \
                         .with_group(2, None) \
                         .with_user_id(users['glabyedh@ammco.org']) \
                         .with_type_observation(types_observation['Flash'], None) \
                         .with_note(f"test python with {image_file_name.split('.')[0]} image, {datetime.datetime.now()} ") \
                         .with_image(1, image_str) \
                         .with_image(2, '') \
                         .with_image(3, '') \
                         .with_image(4, '') \
                         .with_site(10, None) \
                         .with_date(math.floor(datetime.datetime.now().timestamp())) \
                         .build()
    server_observation__id = siren_api.submit_one_observation(observation)
    print(f'Obs created on server id: {server_observation__id}')

    if test_validate_observation or test_run_all:
        siren_api.validate_observation(server_observation__id)
        print('Done')

if test_submit_one_observation_qa or test_run_all:
    no_image = False
    image_file_name = 'shark.jpg'
    fullpath = '/Users/bonansea/Tech/siren-develop-Tools-new/Tools/new/' + image_file_name
    if no_image:
        fullpath = ''
    (latitude,longitude)= extract_gps_from_image(fullpath)
    builder = ObservationBuilder()
    observation = builder.with_project_id(project_id) \
                         .with_coordinates(latitude, longitude) \
                         .with_specie(-1, None) \
                         .with_group(2, None) \
                         .with_user_id(users['glabyedh@ammco.org']) \
                         .with_type_observation(types_observation['Flash'], None) \
                         .with_note(f"test python with {image_file_name.split('.')[0]} image, {datetime.datetime.now()} ") \
                         .with_image(1, fullpath) \
                         .with_image(2, '') \
                         .with_image(3, '') \
                         .with_image(4, '') \
                         .with_site(10, None) \
                         .with_date(math.floor(datetime.datetime.now().timestamp())) \
                         .build()
    server_observation__id = siren_api.submit_one_observation_qa(observation)
    print(f'Obs created on server id: {server_observation__id}')

    if test_validate_observation or test_run_all:
        siren_api.validate_observation(server_observation__id)
        print('Done')

obs_id = project_info['known_observation_id']
if server_observation__id > 0:
    obs_id = server_observation__id

if test_add_result or test_run_all:
    print(siren_api.add_result_to_observation(obs_id, 309, "this is an answers"))

if test_result_list or test_run_all:
    # results are answers linked to questions and observations
    results = siren_api.get_all_results(obs_id)
    print(f'Answers found for observation {obs_id}')
    for r in results:
        print(r['id'],r['question']['title'],r['content'])

if test_read_one_observation or test_run_all:
    observation = siren_api.get_one_observation(obs_id)
    print(observation)

if test_get_all_observations or test_run_all:
    print(f'List of observations validated {test_validate_observation}')
    if test_validate_observation:
        r = requests.get(host_api + f'user/project/observation/byPage/list?numPage=1&id={project_id}&itemsPerPage=1000', headers=siren_api.headers)
    else:
        r = requests.get(host_api + f'userObservationNoValidate/project?1&id={project_id}&itemsPerPage=1000', headers=siren_api.headers)
    data = r.json()['data']
    assert r.status_code == 200
    print(f'OK {len(data)} observations')
    for observation_data in data:
        if True:
            if observation_data['id'] == server_observation__id:
                assert observation_data['status'] == test_validate_observation
            builder = ObservationBuilder()
            observation = builder.with_id(observation_data['id']) \
                .with_project_id(observation_data.get('projectId')) \
                .with_project_name(observation_data.get('projectName')) \
                .with_inaturalist_id(observation_data.get('idInaturalist')) \
                .with_coordinates(observation_data.get('coordX'), observation_data.get('coordY')) \
                .with_note(observation_data.get('note')) \
                .with_alpha(observation_data.get('alpha')) \
                .with_collector(observation_data.get('collector')) \
                .with_dead(observation_data.get('dead')) \
                .with_status(observation_data.get('status')) \
                .with_type_observation(observation_data.get('TypeObservationId'), observation_data.get('TypeObservation')) \
                .with_user_id(observation_data.get('user')) \
                .with_date(observation_data.get('date')) \
                .with_segment(observation_data.get('segmentId'), observation_data.get('segment')) \
                .with_specie(observation_data.get('specieId'), observation_data.get('specie')) \
                .with_site(observation_data.get('siteId'), observation_data.get('site')) \
                .with_family(observation_data.get('familyId'), observation_data.get('family')) \
                .with_group(observation_data.get('groupId'), observation_data.get('group')) \
                .build()

            print(observation)

if test_specie_list or test_run_all:
    print("List of species")
    species = siren_api.get_species()
    for s in species:
        print(s['id'], s['name'])

if test_group_list or test_run_all:
    print("List of groups")
    groups = siren_api.get_groups()
    for g in groups:
        print(g['id'],g['name'])

if test_list_type_observation or test_run_all:
    print("List type of observations")
    types= siren_api.get_type_observation()
    for t in types:
        print(t['id'], t['name'])

if test_create_question:
    print(siren_api.create_question(project_id, 'Measurment Data', types_answers['text']))

if test_question_list or test_run_all:
    print("List of questions")
    questions = siren_api.get_questions()
    for q in questions:
        print(q)









