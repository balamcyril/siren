
project_name = "leo"

host_api = 'https://api.sirenammco.org/api/'
host_test_api = 'https://api.testsirenammco.org/api/'

def get_project_info(name):

    if name == 'leo':
        return {
            'project_id': '62155',
            'email': 'lsanchez@ammco.org',
            'password':'AMMCO2023!',
            'known_observation_id': 30046
        }
    elif name == 'p6':
        return {
            'project_id': '62191',
            'email': 'lab126bonansea@gmail.com',
            'password': 'michel2024',
            'known_observation_id': 30321
        }
    elif name == 'ammco':
        return {
            'project_id': '39',
            'email': 'glabyedh@ammco.org',
            'password': '52208490La*',
            'known_observation_id': 30437
        }
    elif name == 'emirat':
        return {
            'project_id': '62156',
            'email': 'maitha.darwish@epaa.shj.ae',
            'password': 'EPAA',
            'known_observation_id': 0
        }
    raise Exception("Unknown project name")


def get_question_name(id):
    for q in column_to_question_mapping:
        if column_to_question_mapping[q] == id:
            return q
    return ''

groups = {
    'mammals': 1,
    'reptiles': 2,
    'sharks': 3,
    'shark':3,
    'fish': 4,
    'birds': 5,
    'others': 6,
    'rays': 7,
    'ray': 7
}

species = {
    'sphyrna lewini' : 18,
    'gymnura altavela' : 40,
    'rhinobatos irvinei': 51,
    'mustelus mustelus' : 58,
    'glaucostegus cemiculus' : 71,
    'dasyatis marmorata':78,
    'raja parva':80,
    'zanobatus maculatus':81,
    'rhinobatos albomaculatus':85,
    'dasyatis margaritella':86,
    'dasyatis margarita': 124
}

# These are the site for project 39
sites = {
    'batoke' : 247,
    'bakingili': 248,
    'mbouamanga': 294,
    'londji': 295,
    'ngoye':291
}

users = {
    'gmengoue@ammco.org': 171287,
    'cbiankeu@ammco.org': 171282,
    'glabyedh@ammco.org': 171281,
    'lsanchez@ammco.org': 204,
    'borisfeze9@gmail.com': 171266,
    'cfogwan@gmail.com': 62278,
    'lab126bonansea@gmail.com': 170949
}

types_observation = {
    'Nest': 1,
    'Crawl': 2,
    'Female Encounter': 3,
    'Animal': 4,
    'Signs of presence': 5,
    'Flash': 6,
    'Threat': 7,
    'Other': 8
}

types_answers = {
    'text':'text',
    'choice': 'select',
    'number': 'entier'
}

type_answers_begin = {
    'commune': 'commune',
    'personnel':'personnel'
}

type_answers_default_interval = 0

qa_columns = [
    'measurment_data',
    'site',
    'zone',
    'price',
    'dw',
    'genre',
    'family',
    'species',
    'tl',
    'male_maturity ',
    'sex',
    'weight '
]

column_to_question_mapping = {
    'measurment_data': 337,
    'site': 336,
    'zone': 335,
    'price': 334,
    'dw': 333,
    'genre': 332,
    'family': 331,
    'species': 330,
    'tl': 329,
    'male_maturity ': 328,
    'sex': 327,
    'weight ': 326,
}

all_questions = [
{'id': 336, 'title': 'Site', 'TypeAnswer': 'text', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:26:13+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 335, 'title': 'Zone', 'TypeAnswer': 'text', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:26:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 334, 'title': 'Price', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:24:53+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 333, 'title': 'Disk Width (DW) ', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:12:10+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 332, 'title': 'Genre', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:09:22+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 331, 'title': 'Family', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:09:12+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 330, 'title': 'Species', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:08:42+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 329, 'title': 'Total Length (TL)', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:08:06+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 328, 'title': 'Male maturity', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:06:42+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 327, 'title': 'Sex', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:05:22+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 326, 'title': 'Weight', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2023-09-23T17:02:35+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 325, 'title': 'Unhatched term or pipped', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 50, 'required': False, 'updatedAt': '2022-02-13T23:10:11+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 324, 'title': 'Top to Bottom Depth', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 50, 'required': False, 'updatedAt': '2022-02-13T23:10:11+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 323, 'title': 'Predated Eggs', 'TypeAnswer': 'text', 'typeBegin': 'personnel', 'intervale': 20, 'required': False, 'updatedAt': '2022-02-13T23:09:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 322, 'title': 'Top\xa0to\xa0first\xa0egg\xa0depth', 'TypeAnswer': 'entier', 'typeBegin': 'personnel', 'intervale': 25, 'required': False, 'updatedAt': '2022-02-13T23:08:03+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 321, 'title': 'Nest ID', 'TypeAnswer': 'text', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2021-06-07T17:23:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 320, 'title': 'Site', 'TypeAnswer': 'text', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2021-06-07T17:23:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 319, 'title': 'Pipped Eggs dead Hatchlings', 'TypeAnswer': 'number', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2021-06-07T17:23:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 318, 'title': 'Small Eggshells', 'TypeAnswer': 'number', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2021-06-07T17:23:05+00:00', 'isPicture': False, 'image': 'question.png'},
{'id': 317, 'title': 'Turtle Species', 'TypeAnswer': 'select', 'typeBegin': 'personnel', 'intervale': 0, 'required': False, 'updatedAt': '2021-06-07T17:23:05+00:00', 'isPicture': False, 'image': 'question.png'},
]
