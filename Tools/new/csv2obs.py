#################################################################################
# Import observations from Gofrane Excel to Ammco project
# Observation site will be left empty for these observations
# Sites from the excel will be added as question
# observations will all be Animal
#################################################################################

import csv
import os
from datetime import datetime, timezone
import math
import json
from observation_builder import ObservationBuilder
from image_metadata import get_b64_image_message, extract_gps_from_image, find_images, extract_date_time_from_image
from siren_api import SirenAPI
from siren_data import get_question_name, users, types_observation, sites, species, get_project_info, host_api, groups, column_to_question_mapping, qa_columns

global_logs_file = '/Users/bonansea/Tech/siren-develop-Tools-new/Tools/new/script_logs.txt'

# Read CVS with observations
def get_data_from_csv(file_path):
    csv_data = []
    with open(file_path, mode='r', encoding='iso-8859-1', newline='') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            csv_data.append(row)
    return csv_data


# return obs already uploaded
def get_existing_observations(file_path):
    observation_existing = {}
    if os.path.exists(file_path):
        with open(file_path, 'r') as json_file:
            observation_existing = json.load(json_file)
    return observation_existing


def save_existing_observations(observations, file_path):
    # Save the new combined existing one
    with open(file_path, 'w') as json_file:
        json.dump(observations, json_file, indent=4)


def is_observation_already_exist(existing_observations, excel_row):
    for oe in existing_observations:
        if excel_row['picture_code'] == existing_observations[oe]['picture_code']:
            return True
    return False


def get_comment(row, csv_path):
    comment = row['comments']
    if len(comment):
        comment += '\n'
    comment += 'Created from ' + csv_path.split('/')[-1]
    return comment


def get_timestamp(row):
    date_format = "%d/%m/%Y"
    date = datetime.strptime(row['complete_date'], date_format)
    return date.replace(tzinfo=timezone.utc).timestamp()

def get_timestamp_from_datetime(dt_str):
    date_format = "%Y:%m:%d %H:%M:%S"
    date_time_obj = datetime.strptime(dt_str, date_format)
    return date_time_obj.replace(tzinfo=timezone.utc).timestamp()

def get_group_id(row):
    return groups[row['group'].lower()]


def get_species_id(row):
    return species[row['species'].lower()]


def get_site_id(row):
    return sites[row['site'].lower()]


def get_zone_id(row):
    return sites[row['zone'].lower()]

def get_question(column):
    return column_to_question_mapping[column]


def submit_observation_from_row(csv_path, siren_api, row, project_id, validated, type, user_id, skip_images):
    files_path = find_images(row['complete_date'], row['picture_code'])

    builder = ObservationBuilder()
    observation = builder.with_project_id(project_id) \
        .with_specie(get_species_id(row), None) \
        .with_group(get_group_id(row), None) \
        .with_type_observation(type, None) \
        .with_note(get_comment(row, csv_path)) \
        .with_site(get_site_id(row), row['site']) \
        .with_user_id(user_id) \
        .with_date(get_timestamp(row)) \
        .build()

    # add 4 images
    count = 1
    max = 4
    for f in files_path:
        base64_message = get_b64_image_message(f)
        if not skip_images:
            observation.set_image(count, 'data:image/jpeg;base64,' + base64_message)
        else:
            observation.set_image(count, '')
        if count == 1:
            (latitude, longitude) = extract_gps_from_image(f)
            observation.set_coordinates(latitude, longitude)
            image_date_time = extract_date_time_from_image(f)
            if image_date_time:
                observation.set_utc_timestamp(get_timestamp_from_datetime(image_date_time))
        count +=1
        if count > max:
            break
    server_observation__id = siren_api.submit_one_observation(observation)
    print(f'Obs created on server id: {server_observation__id}')
    if server_observation__id == -1:
        return -1,observation
    # add Q&A
    for column in qa_columns:
        content = row[column].rstrip()
        if len(content):
            siren_api.add_result_to_observation(server_observation__id, get_question(column), content)

    # validate observation
    if validated:
        siren_api.validate_observation(server_observation__id)
    return server_observation__id,observation

# max obs to upload
def ingest_new_observations(project_name, csv_path, photo_path, type, observer_email, max, skip_images):

    csv_data = get_data_from_csv(csv_path)

    # Read existing created observations
    existing_json_file_path = os.getcwd() + '/observation_created.json'
    observation_existing = get_existing_observations(existing_json_file_path)

    # Login
    project_info = get_project_info(project_name)
    siren_api = SirenAPI(host_api)
    siren_api.login(project_info['email'], project_info['password'])
    user_id = users[observer_email]
    type_id = types_observation[type]
    count = 0
    observation_created = {}
    row_count = 0
    # set to the photo root dir
    os.chdir(photo_path)
    ignored_row = []
    failed_row = []
    last_row = {}
    last_exception = ''
    missing_species = ['hypanus rudis']
    try:
    # ingest max observations if it does not exist yet
        for row in csv_data:
            row_count +=1
            print("Row CSV " + str(row_count) + '  ' + str(row))
            if row['picture_code'] == '273':
                print('for debugging')
            last_row = row
            # to be removed when we have the mapping
            if len(row['species']) == 0 or row['species'].lower() in missing_species:
                ignored_row.append(row)
                continue
            if is_observation_already_exist(observation_existing, row):
                print(str(row['picture_code']) + ' found, ignored')
            else:
                (id, obs) = submit_observation_from_row(csv_path, siren_api, row, project_info['project_id'], True, type_id, user_id, skip_images)
                if id == -1:
                    failed_row.append((row,obs))
                else:
                    observation_created[id] = row
                    log_results(siren_api, id)
                    count += 1
            if count >= max:
                break
    except Exception as e:
        last_exception = "Exception caught " + str(e)
        print(last_exception)
        print(last_row)
    # Append to existing ones
    observation_existing.update(observation_created)
    save_existing_observations(observation_existing, existing_json_file_path)
    print("ignored rows:")
    for r in ignored_row:
        print(r['species'], r)
    print("Upload failure rows:")
    for (r,o) in failed_row:
        print('------------')
        print(r['picture_code'], r)
        print(str(o))
    print(f'Total ignored rows due to species not supported or missing: {len(ignored_row)},({missing_species})' )
    print('Total upload failures', len(failed_row))
    if last_exception:
        print(last_exception)
        print(last_row)

def log_results(siren_api, obs_id):
    file = open(global_logs_file,'a')
    log = '\n Results from DB for ' + str(obs_id)
    log += str(siren_api.get_one_observation(obs_id))
    log += '\nQ&A for ' + str(obs_id)
    results = siren_api.get_all_results(obs_id)
    for r in results:
        log += '\n' + str(r)
    print(log)
    file.write(log)

if __name__ == '__main__':

    photo_path = '/Users/bonansea/Tech/Siren Photos/ID Photos'
    csv_path = '/Users/bonansea/Tech/Siren Photos/Shark & ray July data.csv'
    project_name = 'ammco' #change de ammco
    type = 'Animal'
    observer_email = 'glabyedh@ammco.org'
    skip_images = False
    ingest_new_observations(project_name, csv_path, photo_path, type, observer_email, 100, skip_images)



