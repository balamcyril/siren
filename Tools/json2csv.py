import csv 
import json

# script creating a CSV out of the .arb language files that flutter uses. app_pt.arb, app_en.arb...
# launch this script from the folder containing the arb files

english_version = "en"
french_version = "fr"
arab_version = "ar"
portugese_version = "pt"
german_version = "de"

language_friendly_string = {"en":"English", "fr":"French", "de":"German", "ar":"Arabic", "pt":"Portuguese" }

def escape(data_str):
    #print(data_str)
    return data_str.replace("\\","\\\\")

def create_id_map():
    
    id_map = {}
    json_data = escape(open("app_"+ english_version+".arb").read())
    data_dict = json.loads(json_data)
    for key in data_dict:
        id_map[key] = { english_version:data_dict[key] } 
    return id_map

def append_language_to_map(id_map, locale):
    
    json_data = escape(open("app_"+ locale+".arb").read())
    data_dict = json.loads(json_data)
    for key in data_dict:
        if key in id_map:
            id_map[key][locale] = data_dict[key]
        else:
            print("Missing English Key " +  key)
            id_map[key] = { locale:data_dict[key] } 

def create_csv(id_map):
    columns = ["String ID"]
    columns.extend(language_friendly_string.keys())
    with open('siren_leo_locale_strings.csv', 'w') as f:  
        w = csv.writer(f)
        
        #header
        header = []
        for locale in columns:
            if "String" in locale:
                header.append(locale)
            else:
                header.append(language_friendly_string[locale])
        w.writerow(header)
        
        #row
        for id in id_map:
            row = [id]
            for locale in columns:
                if "String" in locale:
                    continue
                if locale in id_map[id]:
                    row.append(id_map[id][locale])
                else:
                    row.append("")
            w.writerow(row) 
      
id_map = create_id_map()
append_language_to_map(id_map,french_version)
append_language_to_map(id_map,german_version)
append_language_to_map(id_map,portugese_version)
append_language_to_map(id_map,arab_version)

print(id_map)
create_csv(id_map)
