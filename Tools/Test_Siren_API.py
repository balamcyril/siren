#Test API for project emirats

import requests
import json

# no API available to create a new project
# no API for new user
#project emirats

project = '62156'
email = 'maitha.darwish@epaa.shj.ae'
password = 'EPAA'
                 
print('login')
r = requests.get('https://siren.ammco.org/web/fr/api/connexion/'+ email + '/' + password)
assert r.status_code == 200
response_json = r.json()
assert response_json['username'] == 'maitha al kaisari'
print('OK')

print('Check Types of observations')
r = requests.get('https://siren.ammco.org/web/fr/api/typeobservations/'+project)
assert r.status_code == 200
response_json = r.json()
assert len(response_json) == 4
print('OK')

print('Retrieve sites (beaches)')
r = requests.get('https://siren.ammco.org/web/fr/api/sites/'+project)
assert r.status_code == 200
response_json = r.json()
sites = []
for site in response_json:
    sites.append(site['nom'])
assert 'B4' in sites
print('OK')


# get all observations
# get obs by id
# https://siren.ammco.org/web/fr/api/observationById/21578 

print('Retrieve observations')
r = requests.get('https://siren.ammco.org/web/fr/api/observations/'+project)
assert r.text

response_json = r.json()
assert len(response_json) > 0
print('OK')
print('Check image download')
r = requests.get('https://siren.ammco.org/web/images/observations/tof1/620e52f0ebbfb.jpg')
f = open('620e52f0ebbfb.jpg',"wb")
f.write(r.content)
f.close()
print('OK')
