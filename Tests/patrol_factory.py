from class_SQLPatrol import SQLPatrol
# from class_Service import SERVICEPatrol
# from class_FilePatrol import FilePatrol
from class_SQLObservation import SQLObservation
def createClassPatrol(backend):
    patrol = None;
    if backend == 'SQL':
        patrol = SQLPatrol()
    # elif backend == 'File':
    #     patrol = FilePatrol()
    # elif backend == 'CRUD':
    #     patrol = ServicePatrol()
    else:
        raise Exception("No such class!")
    return patrol;

def createClassObservation(backend):
    observation = None;
    if backend == 'SQL':
        observation = SQLObservation()
    # elif backend == 'File':
    #     observation = FileObservation()
    # elif backend == 'CRUD':
    #     observation = ServiceObservation()
    else:
        raise Exception("No such class!")
    return observation;