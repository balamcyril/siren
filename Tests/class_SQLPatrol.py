from class_patrol import Patrol
from class_SQLQuery import SQLQuery
import datetime

class SQLPatrol(Patrol):
    def __init__(self):
        super().__init__
        self.backend = 'SQL'
        self.query = SQLQuery('patrouille')
        self.cols = '''(id, 
                    segment_id, 
                    type_id, 
                    patrouilleur_id, 
                    nomSite, 
                    etat, 
                    dateDebut, 
                    dateFin, 
                    coordXDebut, 
                    coordYDebut, 
                    coordXFin, 
                    coordYFin, 
                    projet_id)'''

        
    def add(self, cur, conn, data):
        self.query.add_a_row(cur, conn, self.cols, data)
       
        
    def delete(self, cur, conn, id):
        self.query.delete_a_row(cur, conn, id)

    def get_all(self, cur, limit=None, offset=None):
        return self.query.get_all(cur, limit, offset)

    def get_by_id(self, cur, id):
        return self.query.get_by_id(cur, id)

    def get_all_by_project(self, cur, projet_id):
        return self.query.get_by_projet(cur, projet_id)
        
    def get_count(self, cur):
        return self.query.get_count(cur)

    def get_date(self, cur, col, offset=0):
        return self.query.get_col(cur, col)[offset][0].date()

    def get_time(self, cur, col, offset=0):
        return self.query.get_col(cur, col)[offset][0].time()

    def get_join_table(self, cur, data):
        return self.query.get_join_table(cur, data)

    def does_exist(self, cur, id):
        return self.get_by_id(cur, id) is not None

    def is_empty(self, cur):
        return self.get_all(cur) == []
