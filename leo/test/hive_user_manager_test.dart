import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:siren_flutter/data/db_user_manager.dart';
import 'package:siren_flutter/data/hive_user_manager.dart';
import 'package:siren_flutter/data/user_data.dart';
import 'package:siren_flutter/screens/geo_map.dart';

void main() {
  test('Save user data', () async {
    Map loginResponse = {
      'id': 170949,
      'username': 'lab126bonansea@gmail.com',
      'email': 'lab126bonansea@gmail.com',
      'password': 'michel',
      'ville': 'Berlin',
      'telephone': '1732754499',
      'pays': {
        'id': 84,
        'nom_fr': 'Allemagne',
        'code_tel': 49,
        'sigle': 'DEU',
        'nom_en': 'Germany',
        'nom_pt': ''
      },
      'fonctions': {
        'id': 1,
        'nom_fr': 'Pêcheur',
        'nom_en': 'Fisherman',
        'nom_pt': ''
      },
      'projet': {
        'id': 62155,
        'nom': 'Leo test project',
        'lieu': 'All around the world',
        'organization': 'Ammco',
        'public': 1,
        'pays': {
          'id': 55,
          'nom_fr': 'Croatie',
          'code_tel': 385,
          'sigle': 'HRV',
          'nom_en': 'Croatia',
          'nom_pt': ''
        },
        'utilisateurs': {
          'id': 170949,
          'username': 'lab126bonansea@gmail.com',
          'username_canonical': 'lab126bonansea@gmail.com',
          'email': 'lab126bonansea@gmail.com',
          'email_canonical': 'lab126bonansea@gmail.com',
          'enabled': "True",
          'salt': 'PWzIFUyH0QCwAXFsc.gqXEOGCWPzp4mZFnHo3OMG/NU',
          'password':
              'f/mwzL43jAvXMnO+s87RTcdIcGGzPc/nTwaa+gsnzCLCtfn4B4eZSMkgpQ/5rzqVGqzlV/4FJJEWTabEugg+5Q==',
          'last_login': '2021-12-30T07:59:06+0000',
          'confirmation_token': 'JGRQmn-sIHujlgiDbHV8SFNDPofBogWgwJKWrGz6SGw',
          'password_requested_at': '2021-12-30T07:45:09+0000',
          'roles': ['ROLE_USER'],
          'ville': 'Berlin',
          'telephone': 1732754499,
          'fonctions': {
            'id': 1,
            'nom_fr': 'Pêcheur',
            'nom_en': 'Fisherman',
            'nom_pt': ''
          },
          'pays': {
            'id': 84,
            'nom_fr': 'Allemagne',
            'code_tel': 49,
            'sigle': 'DEU',
            'nom_en': 'Germany',
            'nom_pt': ''
          },
          'projet': {
            'id': 62155,
            'nom': 'Leo test project',
            'note_en': 'Thanks for submitting your observation',
            'lieu': 'All around the world',
            'organization': 'Ammco',
            'public': 1,
            'pays': {
              'id': 55,
              'nom_fr': 'Croatie',
              'code_tel': 385,
              'sigle': 'HRV',
              'nom_en': 'Croatia',
              'nom_pt': ''
            },
            'date': '2021-12-30T07:32:47+0000',
            'couleur': '0000FF',
            'note': 'Merci pour votre observation',
            'updated_at': '2021-12-30T07:32:47+0000'
          },
          'date': '2021-05-15T19:46:07+0000'
        },
        'note': 'Merci pour votre observation',
        'note_en': 'Thanks for submitting your observation',
        'couleur': '0000FF'
      }
    };

    final UserData userData = UserData();
    final DBUserManager hiveUserManager = HiveUserManager();

    hiveUserManager.init(test: true);

    userData.fillFromLoginResponse(loginResponse);
    userData.email = "lab126bonansea@gmail.com";

    hiveUserManager.set(userData);
    Map data = await hiveUserManager.get();
    print('HiveUserData get: ${data}');
    expect(userData.id, data['id']);
    expect(userData.email, data['email']);
    expect(userData.currentProjectData.name, data['projet']['nom']);
  });
  test('save user position', () async {
    final UserData userData = UserData();
    final DBUserManager hiveUserManager = HiveUserManager();
    geoPoint initialPoint = geoPoint(20.0, -122.23);
    hiveUserManager.init(test: true);
    hiveUserManager.setPosition(initialPoint);
    geoPoint newPoint = await hiveUserManager.getPosition();
    expect(newPoint.latitude, initialPoint.latitude);
    expect(newPoint.longitude, initialPoint.longitude);
  });
}
