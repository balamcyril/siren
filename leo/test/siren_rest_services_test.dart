import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

double longitude = 52.393909;
double latitude = 13.056914;
int userId = 171086;
int projectId = 62155;
//170949 me

/* Run from main until we fixed the context issue here
void main() {
  test('check rest API login', () async {
    final SirenRestServices restServices = SirenRestServices();
    final r = await restServices.login('lab126bonansea@gmail.com', 'michel');
    expect(r['id'], 170949);
    expect(r['projet']['nom'], "Leo test project");
  });
  test('check rest API login failed', () async {
    try {
      final SirenRestServices restServices = SirenRestServices();
      final r = await restServices.login('lab126bonansea@gmail.com', 'bla');
      expect("false", "Exception should have been thrown");
    } catch (e) {
      if (e.toString().contains("Exception should have been thrown")) {
        throw Exception("Exception should have been thrown");
      }
    }
  });
  test("Get sites", () async {
    final SirenRestServices restServices = SirenRestServices();
    final r = await restServices.getSites(projectId);
    expect(r.length > 0, true);
    expect(r[0].name, "Beach 66");
  });
  test('Submit simple observation', () async {
    final SirenRestServices restServices = SirenRestServices();
    ObservationData observationData = FemaleEncounterFlowObservationData();
    observationData.comment =
        "Submit observation from flutter testing " + DateTime.now().toString();
    observationData.project_id = 62155;
    observationData. = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    observationData.longitude = longitude;
    observationData.latitude = latitude;
    observationData.user_id = userId;
    observationData.species = Species.leatherbackTurtle;
    observationData.group_id = 1;
    observationData.sub_group_id = 1;
    observationData.site_id = 3;
    final r = await restServices.submitObservation(observationData);
    print(r);
    expect(r > 20900, true);
  });
  test('Submit images', () async {
    final SirenRestServices restServices = SirenRestServices();
    ObservationData observationData = FemaleEncounterFlowObservationData();
    observationData.comment =
        "Submit observation with images from flutter testing " +
            DateTime.now().toString();
    observationData.project_id = 62155;
    observationData.timestamp = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    observationData.longitude = longitude;
    observationData.latitude = latitude;
    observationData.user_id = userId;
    observationData.species = Species.leatherbackTurtle;
    observationData.group_id = 1;
    observationData.sub_group_id = 1;
    observationData.site_id = 3;
    observationData.add_media(MediaData("./test0.jpg", -1));
    observationData.add_media(MediaData("./test1.jpg", -1));
    observationData.add_media(MediaData("./test2.jpg", -1));
    observationData.add_media(MediaData("./test3.jpg", -1));
    observationData
        .add_media(MediaData("./test4.jpg", -1)); // should not be sent

    print("Submit observation with images: ");
    print(observationData.mediaList[0]);
    print(observationData.mediaList[1]);
    print(observationData.mediaList[2]);
    print(observationData.mediaList[3]);

    var r = await restServices.submitObservation(observationData);
    print(r);
    expect(r > 20900, true);

    //Get new observation, check that the image is there
    var response = await restServices.getMap('observationById/' + r.toString());
    expect(response['img1'].contains("jpg"), true);
    expect(response['img2'].contains("jpg"), true);
    expect(response['img3'].contains("jpg"), true);
    expect(response['img4'].contains("jpg"), true);
  });
  test('Submit answer', () async {
    final SirenRestServices restServices = SirenRestServices();
    ObservationData observationData = FemaleEncounterFlowObservationData();
    observationData.comment =
        "Submit observation with answers to questions from flutter testing " +
            DateTime.now().toString();
    observationData.project_id = 62155;
    observationData.timestamp = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    observationData.longitude = longitude;
    observationData.latitude = latitude;
    observationData.user_id = userId;
    observationData.species = Species.leatherbackTurtle;
    observationData.group_id = 1;
    observationData.sub_group_id = 1;
    observationData.site_id = 3;
    observationData.add_media(MediaData("./test1.jpg", -1));

    observationData.add_answer(AnswerData(10, 22));
    observationData.add_answer(AnswerData(83, "there is 2 adult turtles"));

    observationData.update_answer(AnswerData(83, "there is 3 adult turtles"));

    print("Submit observation with answers: ");
    print(
        "Quesion ${observationData.answerList[0].questionId} answer: ${observationData.answerList[0].answer}");
    print(
        "Quesion ${observationData.answerList[1].questionId} answer: ${observationData.answerList[1].answer}");

    var r = await restServices.submitObservation(observationData);
    print(r);
    expect(r > 20900, true);
    // TODO build an API to get the answers. No way to check that answers were correct
    // or manually in the website
  });
}
*/
