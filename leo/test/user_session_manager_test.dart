import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:siren_flutter/user_session_manager.dart';

void main() {
  test('Validate user session start', () async {
    UserSessionManager userSessionManager = UserSessionManager();
    userSessionManager.unitTesting = true;
    var success = await userSessionManager.start();
    print("session start completed success $success");
    if (!success) {
      bool connected =
          await userSessionManager.login("lab126bonansea@gmail.com", "michel");
      print("User logged in? $success");
    }
    Map data = await userSessionManager.dbUserManager.get();
    print("Get data and compare with data previously set");

    expect(data['email'], userSessionManager.userData.email);
    expect(data['projet']['nom'],
        userSessionManager.userData.currentProjectData.name);
    userSessionManager.dbUserManager.delete();
  });
  test("Logout user", () async {
    UserSessionManager userSessionManager = UserSessionManager();
    userSessionManager.unitTesting = true;
    var success = await userSessionManager.start();
    print("session start completed success $success");
    if (!success) {
      bool connected =
          await userSessionManager.login("lab126bonansea@gmail.com", "michel");
      print("User logged in? $success");
    }
    Map data = await userSessionManager.dbUserManager.get();
    print("Get user data ${data['username']}");
    userSessionManager.logout();
    try {
      data = await userSessionManager.dbUserManager.get();
    } catch (e) {
      print("Expexted exception after logout $e");
    }
  });
  test("Fetch sites", () async {
    debugPrint("Fetch sites test");
    UserSessionManager userSessionManager = UserSessionManager();
    userSessionManager.unitTesting = true;
    var success = await userSessionManager.start();
    if (!success) {
      bool connected =
          await userSessionManager.login("lab126bonansea@gmail.com", "michel");
    }
    var sites = await userSessionManager.fetchSites();
    debugPrint(sites.toString());
    expect(sites.length > 0, true);
  });
  test("Refresh sites", () async {
    debugPrint("Refresh sites test");
    UserSessionManager userSessionManager = UserSessionManager();
    userSessionManager.unitTesting = true;
    var success = await userSessionManager.start();
    if (!success) {
      bool connected =
          await userSessionManager.login("lab126bonansea@gmail.com", "michel");
    }
    var sites = await userSessionManager.refreshSites();
    debugPrint(sites.toString());
    expect(sites.length > 0, true);
  });
}
