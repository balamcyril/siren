import 'package:flutter_test/flutter_test.dart';
import 'package:siren_flutter/data/observation_data.dart';

void main() {
  test('check not being able to instantiate base class observation data', () {
    try {
      final ObservationData observationData = ObservationData();
      observationData.comment == "you shouldn't be here";
      expect(false,
          "Exception should have been thrown as ObservationData cannot be instantiated");
    } catch (e) {
      if (e.toString().contains("Exception should have been thrown")) {
        throw Exception("Exception should have been thrown");
      }
    }
  });
}
