import 'dart:js';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/question_flow_manager.dart';

// Does not work as no context
// TODO find a way to build a mock context
/*
void main() {
  testWidgets('Flow testing', (WidgetTester tester) async {
    try {
      Map typeMap = {
        "femaleEncounter": ObservationTypeName.femaleEncouter,
        "crawls": ObservationTypeName.crawls,
        "nest": ObservationTypeName.nest
      };
      final QuestionFlowManager flowManager = QuestionFlowManager(context);

      for (var type in typeMap.keys) {
        List<ObservationStep> steps = flowManager.getSteps(typeMap[type]);
        debugPrint("Flow $type ${steps.length} steps");
        for (var step in steps) {
          debugPrint(step.type.toString());
          for (var question in step.questions) {
            debugPrint(question.toString());
          }
        }
      }
    } catch (e) {
      debugPrint("Exception ${e.toString()}");
    }
  });
}
*/
