import 'package:flutter_test/flutter_test.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';
import 'package:siren_flutter/data/media_data.dart';

void main() {
  test('set observation data', () {
    final FemaleEncounterFlowObservationData femaleEncounterObservationData =
        FemaleEncounterFlowObservationData();

    femaleEncounterObservationData.comment = "this is a simple observation";

    expect(
        femaleEncounterObservationData.comment, "this is a simple observation");
  });
  test('add media to observation', () {
    final FemaleEncounterFlowObservationData femaleEncounterObservationData =
        FemaleEncounterFlowObservationData();
    const imagePath = "path to image";

    femaleEncounterObservationData.add_media(MediaData(imagePath, -1));

    expect(femaleEncounterObservationData.mediaList.length, 1);
    expect(femaleEncounterObservationData.mediaList[0], imagePath);
  });
}
