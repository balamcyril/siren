import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:siren_flutter/data/hive_observatio_manager.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';

void main() {
  test('Save observation data', () async {
    final FemaleEncounterFlowObservationData femaleEncounterObservationData =
        FemaleEncounterFlowObservationData();
    final HiveObservationManager hiveObservationManager =
        HiveObservationManager();

    hiveObservationManager.init(test: true);

    femaleEncounterObservationData.comment =
        "this is a unit test simple observation";
    femaleEncounterObservationData.id =
        await hiveObservationManager.create(femaleEncounterObservationData);
    print('id ${femaleEncounterObservationData.id}');
    Map data =
        await hiveObservationManager.get(femaleEncounterObservationData.id);
    print('Map: ${data}');
    expect(femaleEncounterObservationData.comment, data['comment']);
  });
}
