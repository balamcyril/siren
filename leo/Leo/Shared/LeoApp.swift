//
//  LeoApp.swift
//  Shared
//
//  Created by Bonansea, Michel on 11/17/21.
//

import SwiftUI

@main
struct LeoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
