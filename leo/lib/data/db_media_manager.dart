import 'package:siren_flutter/data/media_data.dart';

abstract class DBMediaManager {
  Future<int> create(MediaData mediaData);
  Future<int> update(MediaData mediaData);
  Future<int> delete(int mediaId);

  //Future<List<MediaData>> get(int observationId);
  Future<List<MediaData>> getMediaDataList(int observationId);
}
