import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:siren_flutter/data/db_user_manager.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/user_data.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive_flutter/hive_flutter.dart';
import 'package:siren_flutter/screens/geo_map.dart';

import 'observation_type_data.dart';

class HiveUserManager extends DBUserManager {
  late Box<dynamic> box;
  final String boxName = 'UserBox8';
  final String keyUserName = 'User';
  final String keyPatrolId = 'PatrolId';
  final String keyPosition = 'Position';
  final String keySites = 'Sites';
  final String keySync = 'Sync';
  final String keyFlashSupport = 'Flash';
  final String keyObservationsType = 'ObservationsType';
  // final String keyConnect = 'isConnect';

  @override
  Future<dynamic> init({bool test = false}) async {
    if (!kIsWeb && !Hive.isBoxOpen(boxName)) {
      WidgetsFlutterBinding.ensureInitialized();
      if (!test) {
        final appDocumentDirectory =
            await path_provider.getApplicationDocumentsDirectory();
        Hive.init(appDocumentDirectory.path);
        debugPrint('HiveUserManager init ${appDocumentDirectory.path}');
      } else {
        Hive.init('.');
      }
    }
    return Hive.openBox(boxName);
  }

  @override
  void set(UserData data) async {
    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set data for key $keyUserName');
    box.put(keyUserName, data.loginResponse);
  }

  @override
  Future<Map> get() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keyUserName);
    if (data == null) {
      throw Exception("HiveUserManager - No data found for key $keyUserName");
    }
    return data;
  }

  @override
  void delete() async {
    box.delete(keyUserName);
    debugPrint('HiveUserManager delete data for key $keyUserName');
    box.delete(keySites);
    debugPrint('HiveUserManager delete data for key $keySites');
    box.delete(boxName);
    debugPrint('HiveUserManager delete data for key $boxName');
    box.delete(keyFlashSupport);
    debugPrint('HiveUserManager delete data for key $keyFlashSupport');
    box.delete(keyObservationsType);
    debugPrint('HiveUserManager delete data for key $keyObservationsType');
    box.delete(keyPosition);
    debugPrint('HiveUserManager delete data for key $keyPosition');
  }

  @override
  void setPosition(geoPoint point) async {
    Map positionMap = {
      'latitude': point.latitude,
      'longitude': point.longitude
    };

    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set position');
    box.put(keyPosition, positionMap);
  }

  @override
  Future<geoPoint> getPosition() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keyPosition);
    if (data == null) {
      throw Exception("HiveUserManager - No data found for key $keyPosition");
    }
    return geoPoint(data['latitude'], data['longitude']);
  }

  @override
  void setSites(List<SiteData> sites) async {
    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set sites');
    box.put(keySites, jsonEncode(sites));
  }

  @override
  Future<List<SiteData>> getSites() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keySites);
    if (data == null) {
      throw Exception("HiveUserManager - No data found for key $keyPosition");
    }
    List<SiteData> sites = [];
    var jsonList = jsonDecode(data);
    jsonList.forEach((item) {
      sites.add(SiteData(item["id"], item["name"]));
    });
    return sites;
  }

  @override
  void setLastSync(String sync) async {
    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set sync date');
    box.put(keySync, sync);
  }

  @override
  Future<String> getLastSync() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keySync);
    if (data == null) {
      // throw Exception("HiveUserManager - No data found for key $keyPosition");
      return "";
    }
    return data;
  }

  @override
  void setObservationsType(List<ObservationsTypeData> ObservationsType) async {
    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set observation type list');
    box.put(keyObservationsType, jsonEncode(ObservationsType));
  }

  @override
  Future<List<ObservationsTypeData>> getObservationsType() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keyObservationsType);
    if (data == null) {
      throw Exception("HiveUserManager - No data found for key $keyPosition");
    }

    List<ObservationsTypeData> ObservationsTypes = [];
    var jsonList = jsonDecode(data);
    jsonList.forEach((item) {
      ObservationsTypes.add(ObservationsTypeData(
          item["id"], item["nom_fr"], item["nom_en"], item["nom_pt"]));
    });
    return ObservationsTypes;
  }

  @override
  void setPatrolId(int patrolId) async {
    box = await Hive.openBox(boxName);
    debugPrint('HiveUserManager set data for key $keyPatrolId');
    box.put(keyPatrolId, patrolId);
  }

  @override
  Future<int> getPatrolId() async {
    box = await Hive.openBox(boxName);
    var data = box.get(keyPatrolId);
    if (data == null) {
      debugPrint('HIVE VALUE IS NULL $keyPatrolId');
      return -1;
      // throw Exception("HiveUserManager - No data found for key $keyPatrolId");
    }
    return data;
  }
}
