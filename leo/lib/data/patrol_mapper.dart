import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/patrol_column_mapper.dart';

import 'package:siren_flutter/data/patrol_data.dart';

import '../data/observation_type_data.dart';

class PatrolMapper {
  PatrolColumnMapper getColumnMapper(String column) {
    if (column == 'id') return PatrolColumnMapperId();
    if (column == 'projectId') return PatrolColumnMapperProject();
    if (column == 'obsNbr') return PatrolColumnMapperObsNbr();
    if (column == 'userId') return PatrolColumnMapperUser();
    if (column == 'author') return PatrolColumnMapperObserver();
    // if (column == 'date') return PatrolColumnMapperDate();
    if (column == 'startTime') return PatrolColumnMapperStartTime();
    if (column == 'endTime') return PatrolColumnMapperEndTime();
    if (column == 'gpsStartCoordX') return PatrolColumnMapperGPSLatitudeStart();
    if (column == 'gpsStartCoordY')
      return PatrolColumnMapperGPSLongitudeStart();
    if (column == 'gpsEndCoordX') return PatrolColumnMapperGPSLatitudeEnd();
    if (column == 'gpsEndCoordY') return PatrolColumnMapperGPSLongitudeEnd();
    if (column == 'totalPatrolTime') return PatrolColumnMapperPatrolTime();
    if (column == 'totalDistance') return PatrolColumnMapperDistance();

    throw ("PatrolMapper getColumnMapper unknown column $column");
  }

  // return observations built from the observations rest API
  PatrolData fromServer(Map<String, dynamic> PatrolServerMap) {
    Map<String, dynamic> patrol_map = Map();

    patrol_map['status'] = PatrolStatus.uploaded;

    PatrolServerMap.forEach((key, value) {
      debugPrint("key: ${key}");
      debugPrint("value: ${value}");
      PatrolColumnMapper mapper = getColumnMapper(key);

      if (key == 'id') patrol_map['_id'] = -1 * mapper.getData(value);

      if (mapper.runtimeType != PatrolColumnMapperEmpty) {
        patrol_map[mapper.getColumnName()] = mapper.getData(value);
      }
    });

    PatrolData patrol = PatrolData.fromMap(patrol_map);
    return patrol;
  }
}
