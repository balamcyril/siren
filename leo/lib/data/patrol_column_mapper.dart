import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';

abstract class PatrolColumnMapper {
  String getColumnName();
  dynamic getData(data);
}

//use to skip a filed
class PatrolColumnMapperEmpty extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return "Empty";
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperUser extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.user_id;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperId extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.server_id;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperDate extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.timestamp;
  }

  // return a timestamp
  @override
  dynamic getData(data) {
    final formatter = DateFormat(r'''yyyy-MM-dd'T'hh:mm:ss''');
    final dateTimeFromStr = formatter.parse(data);
    // final dateTimeFromStr = formatter.parse((data as String).split('+')[0]);
    return dateTimeFromStr.millisecondsSinceEpoch;
  }
}

class PatrolColumnMapperStartTime extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.start_timestamp;
  }

  // return a timestamp
  @override
  dynamic getData(data) {
    final formatter = DateFormat(r'''yyyy-MM-dd'T'hh:mm:ss''');
    final dateTimeFromStr = formatter.parse(data);
    return dateTimeFromStr.millisecondsSinceEpoch;
  }
}

class PatrolColumnMapperEndTime extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.end_timestamp;
  }

  // return a timestamp
  @override
  dynamic getData(data) {
    final formatter = DateFormat(r'''yyyy-MM-dd'T'hh:mm:ss''');
    final dateTimeFromStr = formatter.parse(data);
    return dateTimeFromStr.millisecondsSinceEpoch;
  }
}

class PatrolColumnMapperGPSLatitudeStart extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.start_latitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperGPSLongitudeStart extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.start_longitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperGPSLatitudeEnd extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.end_latitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperGPSLongitudeEnd extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.end_longitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperGPSLatitudeEndTime extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.end_latitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperGPSLongitudeEndTime extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.end_longitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class PatrolColumnMapperProject extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.project_id;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperObserver extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.observer;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperObsNbr extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.obs_nbr;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperPatrolTime extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.timestamp;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class PatrolColumnMapperDistance extends PatrolColumnMapper {
  @override
  String getColumnName() {
    return PatrolFields.distance;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}
