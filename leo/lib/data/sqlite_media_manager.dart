import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/db_media_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';
import 'global_db.dart';

class SqfliteMediaManager extends DBMediaManager {
  //Global Database Instance (singleton)
  static final SqfliteMediaManager instance = SqfliteMediaManager._init();

  static Database? _database;

  SqfliteMediaManager._init();

  // returns _database if it is not null else call initDB
  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await SqfliteObservationManager.instance.database;
    return _database!;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  //abstracted methods
  @override
  Future<int> create(MediaData mediaData) async {
    final db = await instance.database;
    final id = await db.insert(tableMedias, mediaData.toMap());
    return id;
  }

  @override
  Future<int> update(MediaData mediaData) async {
    final db = await instance.database;
    try {
      return await db.update(tableMedias, mediaData.toMap(),
          where: '${MediaFields.id} = ?', whereArgs: [mediaData.media_id]);
    } catch (e) {
      throw Exception('Error updating media with ID: ${mediaData.media_id}');
    }
  }

  @override
  Future<int> delete(int mediaId) async {
    final db = await instance.database;
    try {
      return await db.delete(tableMedias,
          where: '${MediaFields.id} = ?', whereArgs: [mediaId]);
    } catch (e) {
      throw Exception('Error deleting media with ID: $mediaId');
    }
  }

  @override
  Future<List<MediaData>> getMediaDataList(int observationId) async {
    Database db = await instance.database;
    var medias = await db.rawQuery(
        "SELECT * FROM $tableMedias WHERE  $observationId = $tableMedias.observation_id");
    List<MediaData> mediaArr = medias.isNotEmpty
        ? medias.map((media) => MediaData.fromMap(media)).toList()
        : [];
    return mediaArr;
  }
}
