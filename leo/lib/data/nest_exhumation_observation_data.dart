import 'package:siren_flutter/data/observation_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NestExhumationObservationData extends ObservationData {
  NestExhumationObservationData() {
    type = ObservationType.nestExhumation;
  }
  String getTypeString(context) => AppLocalizations.of(context)!.nestExhumation;
}
