import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/db_observation_manager.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

import 'global_db.dart';

class SqfliteObservationManager extends DBObservationManager {
  //Global Database Instance (singleton)
  static final SqfliteObservationManager instance =
      SqfliteObservationManager._init();

  static Database? _database;

  SqfliteObservationManager._init();

  // returns _database if it is not null else call initDB
  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB(globalDBName);
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    WidgetsFlutterBinding.ensureInitialized();
    Directory dbPath = await getApplicationDocumentsDirectory();
    final path = join(dbPath.path, filePath);
    return await openDatabase(path,
        version: 4, onCreate: _createTables, onUpgrade: _onUpgrade);
  }

  Future _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      var patrol_col = await db.rawQuery(
          "SELECT COUNT(*) AS nbr FROM pragma_table_info('$tableObservations') WHERE name = 'patrol_id'");

      if (patrol_col.first['nbr'] == 0) {
        await db.execute(
            'ALTER TABLE $tableObservations ADD COLUMN ${ObservationFields.patrol_id} INTEGER');

        // await db.execute('DROP TABLE $tablePatrol');
        await db.execute('''
       CREATE TABLE IF NOT EXISTS $tablePatrol(
         ${PatrolFields.id} INTEGER PRIMARY KEY AUTOINCREMENT,
         ${PatrolFields.status} INTEGER,
         ${PatrolFields.server_id} INTEGER,
         ${PatrolFields.project_id} INTEGER,
         ${PatrolFields.observer} VARCHAR(255),
         ${PatrolFields.timestamp} VARCHAR(255),
         ${PatrolFields.distance} DOUBLE,
         ${PatrolFields.start_timestamp} INTEGER,
         ${PatrolFields.end_timestamp} INTEGER,
         ${PatrolFields.obs_nbr} INTEGER,
         ${PatrolFields.user_id} INTEGER,
         ${PatrolFields.start_latitude} DOUBLE,
         ${PatrolFields.start_longitude} DOUBLE,
         ${PatrolFields.end_latitude} DOUBLE,
         ${PatrolFields.end_longitude} DOUBLE

         );
      ''');
      }
    }
  }

  Future _createTables(Database db, int version) async {
    await db.execute('''
       CREATE TABLE IF NOT EXISTS $tablePatrol(
         ${PatrolFields.id} INTEGER PRIMARY KEY AUTOINCREMENT,
         ${PatrolFields.status} INTEGER,
         ${PatrolFields.server_id} INTEGER,
         ${PatrolFields.project_id} INTEGER,
         ${PatrolFields.observer} VARCHAR(255),
         ${PatrolFields.timestamp} VARCHAR(255),
         ${PatrolFields.distance} DOUBLE,
         ${PatrolFields.start_timestamp} INTEGER,
         ${PatrolFields.end_timestamp} INTEGER,
         ${PatrolFields.obs_nbr} INTEGER,
         ${PatrolFields.user_id} INTEGER,
         ${PatrolFields.start_latitude} DOUBLE,
         ${PatrolFields.start_longitude} DOUBLE,
         ${PatrolFields.end_latitude} DOUBLE,
         ${PatrolFields.end_longitude} DOUBLE

         );
      ''');
    await db.execute('''
       CREATE TABLE IF NOT EXISTS $tableObservations(
        ${ObservationFields.patrol_id} INTEGER,
         ${ObservationFields.id} INTEGER PRIMARY KEY AUTOINCREMENT,
         ${ObservationFields.status} INTEGER,
         ${ObservationFields.server_id} INTEGER,
         ${ObservationFields.observer} VARCHAR(255),
         ${ObservationFields.latitude} DOUBLE,
         ${ObservationFields.longitude} DOUBLE,
         ${ObservationFields.timestamp} INTEGER,
         ${ObservationFields.site} VARCHAR(255),
         ${ObservationFields.site_id} INTEGER,
         ${ObservationFields.project_id} INTEGER,
         ${ObservationFields.project} VARCHAR(255),
         ${ObservationFields.species} INTEGER,
         ${ObservationFields.user_id} INTEGER,
         ${ObservationFields.comment} VARCHAR(255),
         ${ObservationFields.group_id} INTEGER,
         ${ObservationFields.sub_group_id} INTEGER,
         ${ObservationFields.type} INTEGER,
         ${ObservationFields.nest_id} VARCHAR(255)

         );
      ''');

    await db.execute('''
       CREATE TABLE IF NOT EXISTS $tableMedias(
         ${MediaFields.id} INTEGER PRIMARY KEY AUTOINCREMENT,
         ${MediaFields.path} VARCHAR(255),
         ${MediaFields.observation_id} INTEGER,
         FOREIGN KEY(${MediaFields.observation_id}) REFERENCES $tableObservations(${ObservationFields.id})
         );
      ''');
    await db.execute('''
       CREATE TABLE IF NOT EXISTS $tableAnswers(
         ${AnswerFields.id} INTEGER PRIMARY KEY AUTOINCREMENT,
         ${AnswerFields.answer} VARCHAR(255),
         ${AnswerFields.question_id} INTEGER,
         ${AnswerFields.observation_id} INTEGER,
         FOREIGN KEY(${AnswerFields.observation_id}) REFERENCES $tableObservations(${ObservationFields.id})
         );
      ''');
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  // for debugging
  void viewContent() async {
    Database db = await instance.database;
    var medias = await db.rawQuery("SELECT * FROM $tableMedias");
    print("Media table");
    print(medias);
    var obs = await db.rawQuery("SELECT * FROM $tableObservations");
    print("Observation table");
    print(obs);
    var answer = await db.rawQuery("SELECT * FROM $tableAnswers");
    print("Answer table");
    print(obs);
    var patrol = await db.rawQuery("SELECT * FROM $tablePatrol");
    print("Patrol table");
    print(patrol);
  }

  // one query joining image table
  Future<List<ObservationData>> getAllObservations() async {
    Database db = await instance.database;
    var observations = await db.rawQuery(
        "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.status} != ${Status.deleted.index} ORDER BY _id DESC");
    return mapObservations(observations);
  }

  Future<List<ObservationData>> getAllObservationsForSite(String site) async {
    Database db = await instance.database;
    site = site.replaceAll("'", "''");
    var observations = await db.rawQuery(
        "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.site} = '$site' AND ${ObservationFields.status} != ${Status.deleted.index} ");
    return mapObservations(observations);
  }

  Future<List<ObservationData>> getAllObservationsForPatrolProject(
      int project_id, int patrol_id, int patrol_server_id, int request) async {
    Database db = await instance.database;
    var observations = [];
    if (patrol_id > 0)
      observations = await db.rawQuery(
          "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.patrol_id} = '${patrol_id}' AND ${ObservationFields.project_id} = '${project_id}' AND ${ObservationFields.status} <> '${Status.deleted.index}' ORDER BY _id DESC");
    else {
      if (request == -1)
        observations = await db.rawQuery(
            "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.patrol_id} = '${patrol_server_id}' AND ${ObservationFields.project_id} = '${project_id}' AND ${ObservationFields.status} = '${Status.draft.index}' ORDER BY _id DESC");
      else
        observations = await db.rawQuery(
            "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.patrol_id} = '${patrol_server_id}' AND ${ObservationFields.project_id} = '${project_id}' AND ${ObservationFields.status} <> '${Status.deleted.index}' ORDER BY _id DESC");
    }

    return mapObservations(observations);
  }

  Future<List<ObservationData>> getUploadedObservations() async {
    return getObservationsByState(Status.uploaded);
  }

  Future<List<ObservationData>> getDraftObservations() async {
    return getObservationsByState(Status.draft);
  }

  Future<List<ObservationData>> getDeletedObservations() async {
    return getObservationsByState(Status.deleted);
  }

  Future<List<ObservationData>> getValidatedObservations() async {
    return getObservationsByState(Status.validated);
  }

  Future<List<ObservationData>> getObservationsByState(Status status) async {
    Database db = await instance.database;
    var observations = await db.rawQuery(
        // ignore: unnecessary_brace_in_string_interps
        "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE ${ObservationFields.status} = ${status.index} ORDER BY _id DESC");
    return mapObservations(observations);
  }

  // Map query result for unique observation with list of images
  List<ObservationData> mapObservations(observations) {
    final Map<String, List<String>> mapIdPaths = Map();
    List<dynamic> observationsArr = observations.isNotEmpty
        ? observations.map((observation) {
            ObservationData observationData =
                ObservationData.fromMap(observation);

            // Put image paths in a map per observation id
            if (observation['path'] != null) {
              if (!mapIdPaths.containsKey(observationData.id.toString())) {
                mapIdPaths[observationData.id.toString()] = <String>[];
              }
              List<String>? paths = mapIdPaths[observationData.id.toString()];
              paths!.add(observation['path'].toString());
            }
            return observationData;
          }).toList()
        : [];
    List<dynamic> uniqueObservations = [];
    Set uniqueIds = Set();
    // map unique observations with multiple images in one object
    observationsArr.forEach((observation) {
      if (!uniqueIds.contains(observation.id)) {
        uniqueObservations.add(observation);
        uniqueIds.add(observation.id);
        if (mapIdPaths.containsKey(observation.id.toString())) {
          mapIdPaths[observation.id.toString()]!.forEach((path) {
            observation.add_media(MediaData(path, observation.id));
          });
        }
      }
    });
    return List<ObservationData>.from(uniqueObservations);
  }

  Future<List<ObservationData>> searchObservations(String searchText,
      {bool trash = false}) async {
    Database db = await instance.database;
    var observations;
    if (trash) {
      observations = await db.rawQuery(
          "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE (site LIKE '%$searchText%' OR  nest_id LIKE '%$searchText%' OR comment LIKE '%$searchText%') AND ${ObservationFields.status} = ${Status.deleted.index} ");
    } else {
      observations = await db.rawQuery(
          "SELECT * FROM $tableObservations LEFT JOIN $tableMedias ON $tableObservations._id = $tableMedias.observation_id WHERE (site LIKE '%$searchText%' OR  nest_id LIKE '%$searchText%' OR comment LIKE '%$searchText%') AND ${ObservationFields.status} != ${Status.deleted.index} ");
    }
    return mapObservations(observations);
  }

  Future<int> getCountBySite(String site) async {
    Database db = await instance.database;
    site = site.replaceAll("'", "''");
    return Sqflite.firstIntValue(await db.rawQuery(
            "SELECT COUNT(*) FROM $tableObservations WHERE (${ObservationFields.site} = '$site') AND ${ObservationFields.status} != ${Status.deleted.index}"))
        as int;
  }

  Future<int> getCount() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery(
            "SELECT COUNT(*) FROM $tableObservations WHERE ${ObservationFields.status} != ${Status.deleted.index}"))
        as int;
  }

  Future<int> getDraftCount() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery(
            "SELECT COUNT(*) FROM $tableObservations WHERE ${ObservationFields.status} != ${Status.draft.index}"))
        as int;
  }

  Future<Set<int>> getAllServerIDs() async {
    Database db = await instance.database;
    var rows = await db.rawQuery(
        "SELECT ${ObservationFields.server_id} FROM $tableObservations WHERE ${ObservationFields.server_id} > 0");
    Set<int> ids = rows.isNotEmpty
        ? rows.map((id) {
            return id['server_id'] as int;
          }).toSet()
        : Set<int>();
    return ids;
  }

  //abstracted methods
  @override
  Future<int> create(ObservationData observation) async {
    debugPrint("--- obs to create");
    debugPrint("$observation");
    final db = await instance.database;
    final id = await db.insert('observations', observation.toMap());
    return id;
  }

  @override
  Future<int> update(ObservationData data) async {
    final db = await instance.database;
    try {
      return await db.update(tableObservations, data.toMap(),
          where: '${ObservationFields.id} = ?', whereArgs: [data.id]);
    } catch (e) {
      throw Exception('Error updating Observation with ID: ${data.id}');
    }
  }

  @override
  Future delete(int observation_id) async {
    final db = await instance.database;
    Map<String, Object?> currentObservation = await get(observation_id);
    Map<String, Object?> observationToBeDeleted = {
      ...currentObservation,
      'status': Status.deleted.index
    };
    String whereString = '${ObservationFields.id} = ?';
    List<dynamic> whereArguments = [observation_id];
    await db.update(tableObservations, observationToBeDeleted,
        where: whereString, whereArgs: whereArguments);
  }

  @override
  Future<Map<String, Object?>> get(int observation_id) async {
    Database db = await instance.database;
    String whereString = '${ObservationFields.id} = ?';
    List<dynamic> whereArguments = [observation_id];
    var observations = await db.query('observations',
        where: whereString, whereArgs: whereArguments);
    if (observations.isEmpty) {
      throw Exception("Observation not found with Id $observation_id");
    }
    Map<String, Object?> observation = observations[0];
    return observation;
  }

  @override
  Future<Map<String, Object?>> getByServerId(int observation_id) async {
    Database db = await instance.database;
    String whereString = '${ObservationFields.server_id} = ?';
    List<dynamic> whereArguments = [observation_id];
    var observations = await db.query('observations',
        where: whereString, whereArgs: whereArguments);
    if (observations.isEmpty) {
      throw Exception("Observation not found with Id $observation_id");
    }
    Map<String, Object?> observation = observations[0];
    return observation;
  }

  Future<void> hardDeletes() async {
    final db = await instance.database;
    try {
      await db.delete(tableObservations);
    } catch (e) {
      throw Exception('erase observations failed');
    }
  }

  Future<int> hardDelete(int id) async {
    final db = await instance.database;
    try {
      return await db.delete(tableObservations,
          where: '${ObservationFields.id} = ?', whereArgs: [id]);
    } catch (e) {
      throw Exception('Error deleting observation with ID: $id');
    }
  }

  //patrol section

  Future<int> createPatrol(PatrolData patrol) async {
    final db = await instance.database;
    final id = await db.insert('patrol', patrol.toMap());
    return id;
  }

  Future<int> updatePatrol(PatrolData data) async {
    final db = await instance.database;
    try {
      return await db.update(tablePatrol, data.toMap(),
          where: '${PatrolFields.id} = ?', whereArgs: [data.id]);
    } catch (e) {
      throw Exception('Error updating Patrol with ID: ${data.id}');
    }
  }

  Future<int> endPatrol(PatrolData data) async {
    final db = await instance.database;
    try {
      return await db.update(tablePatrol, data.toMap(),
          where: '${PatrolFields.id} = ?', whereArgs: [data.id]);
    } catch (e) {
      throw Exception('Error updating Patrol with ID: ${data.id}');
    }
  }

  Future<List<PatrolData>> getPatrolByProject(int project_id) async {
    Database db = await instance.database;
    var patrols = await db.rawQuery(
        "SELECT * FROM $tablePatrol WHERE ${PatrolFields.project_id} = ${project_id} ORDER BY _id DESC");
    return mapPatrols(patrols);
  }

  Future<List<PatrolData>> getPatrolById(int patrol_id) async {
    Database db = await instance.database;
    var patrols = await db.rawQuery(
        "SELECT * FROM $tablePatrol WHERE ${PatrolFields.id} = ${patrol_id}");
    return mapPatrols(patrols);
  }

  List<PatrolData> mapPatrols(patrols) {
    List<dynamic> patrolsArr = patrols.isNotEmpty
        ? patrols.map((patrol) {
            PatrolData patrolData = PatrolData.fromMap(patrol);
            return patrolData;
          }).toList()
        : [];
    List<dynamic> uniquePatrol = [];
    Set uniqueIds = Set();
    // map unique observations with multiple images in one object
    patrolsArr.forEach((patrol) {
      if (!uniqueIds.contains(patrol.id)) {
        uniquePatrol.add(patrol);
        uniqueIds.add(patrol.id);
      }
    });
    return List<PatrolData>.from(uniquePatrol);
  }

  Future<void> erasePatrol() async {
    final db = await instance.database;
    try {
      await db.rawQuery("DELETE FROM $tablePatrol");
    } catch (e) {
      throw Exception('Error deleting observation with ID');
    }
  }

  Future<int> hardDeleteObsPatrol(int id) async {
    final db = await instance.database;
    try {
      return await db.delete(tableObservations,
          where: '${ObservationFields.patrol_id} = ?', whereArgs: [id]);
    } catch (e) {
      throw Exception('Error deleting observation with ID: $id');
    }
  }

  Future<int> hardDeletePatrol(int id, obsCount) async {
    final db = await instance.database;
    int delobs = await hardDeleteObsPatrol(id);

    if (delobs == obsCount)
      try {
        return await db.delete(tablePatrol,
            where: '${PatrolFields.id} = ?', whereArgs: [id]);
      } catch (e) {
        throw Exception('Error deleting observation with ID: $id');
      }

    return -1;
  }
}
