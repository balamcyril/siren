class Job {
  String name;
  int id;

  Job(this.id, this.name);

  @override
  String toString() {
    return '{ ${this.name}, ${this.id} }';
  }

  Job.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];
}
