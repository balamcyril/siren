import 'package:siren_flutter/data/db_answer_manager.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class SqfliteAnswerManager extends DBAnswerManager {
  //Global Database Instance (singleton)
  static final SqfliteAnswerManager instance = SqfliteAnswerManager._init();

  static Database? _database;

  SqfliteAnswerManager._init();

  // returns _database if it is not null else call initDB
  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await SqfliteObservationManager.instance.database;
    return _database!;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  //abstracted methods
  @override
  Future<int> create(AnswerData answerData) async {
    final db = await instance.database;
    final id = await db.insert(tableAnswers, answerData.toMap());
    return id;
  }

  @override
  Future<int> update(AnswerData answerData) async {
    final db = await instance.database;
    try {
      return await db.update(tableAnswers, answerData.toMap(),
          where: '${AnswerFields.id} = ?', whereArgs: [answerData.answerId]);
    } catch (e) {
      throw Exception('Error updating answer with ID: ${answerData.answerId}');
    }
  }

  @override
  Future<int> delete(int answerId) async {
    final db = await instance.database;
    try {
      return await db.delete(tableAnswers,
          where: '${AnswerFields.id} = ?', whereArgs: [answerId]);
    } catch (e) {
      throw Exception('Error deleting answer with ID: $answerId');
    }
  }

  @override
  Future<List<AnswerData>> get(int observationId) async {
    Database db = await instance.database;
    var answers = await db.rawQuery(
        "SELECT * FROM $tableAnswers WHERE  $observationId = $tableAnswers.observation_id");
    List<AnswerData> answerArr = answers.isNotEmpty
        ? answers.map((answer) => AnswerData.fromMap(answer)).toList()
        : [];
    return answerArr;
  }

  Future<String> getFemaleTagID(int observationId) async {
    var answers = await get(observationId);
    for (var answer in answers) {
      if (answer.questionId == questionNameToIDMapper["female_tag_id_left"]) {
        return answer.answer;
      }
      if (answer.questionId == questionNameToIDMapper["female_tag_id_right"]) {
        return answer.answer;
      }
    }
    return "";
  }
}
