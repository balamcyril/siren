const String tableAnswers = 'answers';

class AnswerFields {
  static const String id = 'answer_id';
  static const String answer = 'answer';
  static const String question_id = 'question_id';
  static const String observation_id = 'observation_id';
}

class AnswerData {
  int? answerId;
  late dynamic answer;
  late int questionId;
  late int observationId;

  AnswerData(this.questionId, this.answer);

  AnswerData.namedArgumentConstructor(
      {this.answerId,
      required this.questionId,
      required this.answer,
      required this.observationId});

  factory AnswerData.fromMap(Map<String, dynamic> json) {
    return AnswerData.namedArgumentConstructor(
        answerId: json['answer_id'],
        questionId: json['question_id'],
        answer: json['answer'],
        observationId: json['observation_id']);
  }

  Map<String, dynamic> toMap() {
    return {
      'answer_id': answerId,
      'question_id': questionId,
      'answer': answer,
      'observation_id': observationId
    };
  }

  String toString() {
    return "_ID: $answerId Question ID: $questionId, Answer: $answer, Observation ID: $observationId";
  }

  @override
  bool operator ==(Object other) =>
      other is AnswerData && other.answerId == answerId;
}
