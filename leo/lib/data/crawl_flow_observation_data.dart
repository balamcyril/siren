import 'package:siren_flutter/data/observation_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CrawlFlowObservationData extends ObservationData {
  CrawlFlowObservationData() {
    type = ObservationType.crawl;
  }
  String getTypeString(context) =>
      AppLocalizations.of(context)!.crawlMonitoring;

  late String turtleSpecies;
  late String crawlType;
  late String crawlWidthInCm;
}
