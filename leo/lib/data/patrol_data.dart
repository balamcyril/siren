//prepped variables for key instantiation
import 'package:flutter/material.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';

const String tablePatrol = 'patrol';

class PatrolStatus {
  static int get actif => 0;
  static int get uploaded => 1;
  static int get unactif => -1;
}

class PatrolFields {
  static const String id = '_id';
  static const String project_id = 'project_id';
  static const String user_id = 'user_id';
  static const String status = 'status';
  static const String observer = 'observer';
  static const String server_id = 'server_id';
  static const String timestamp = 'timestamp';
  static const String distance = 'distance';
  static const String start_timestamp = 'start_timestamp';
  static const String end_timestamp = 'end_timestamp';
  static const String obs_nbr = 'obs_nbr';
  static const String start_latitude = 'start_latitude';
  static const String start_longitude = 'start_longitude';
  static const String end_latitude = 'end_latitude';
  static const String end_longitude = 'end_longitude';
}

class PatrolData {
  int? id;
  int server_id = -1;
  String observer = "";
  int user_id = -1;
  int project_id = -1;
  late int status;
  late int start_timestamp;
  String timestamp = "...";
  double distance = 0;
  int end_timestamp = 0;
  int obs_nbr = 0;
  int old_obs_nbr = 0;
  double start_latitude = 0;
  double start_longitude = 0;
  double end_latitude = 0;
  double end_longitude = 0;

  PatrolData(this.project_id);

  //constructor that allows me to use the factory pattern
  PatrolData.namedArgumentConstructor({
    this.id,
    required this.project_id,
    required this.server_id,
    required this.observer,
    required this.user_id,
    required this.status,
    required this.timestamp,
    required this.distance,
    required this.start_timestamp,
    required this.end_timestamp,
    required this.obs_nbr,
    required this.start_latitude,
    required this.start_longitude,
    required this.end_latitude,
    required this.end_longitude,
  });

  factory PatrolData.fromMap(Map<String, dynamic> json) {
    return PatrolData.namedArgumentConstructor(
        id: json['_id'],
        project_id: json['project_id'],
        server_id: json['server_id'],
        observer: json['observer'],
        user_id: json['user_id'],
        status: json['status'] == 1
            ? PatrolStatus.uploaded
            : json['status'] == -1
                ? PatrolStatus.unactif
                : PatrolStatus.actif,
        timestamp: json['timestamp'],
        distance: json['distance'],
        start_timestamp: json['start_timestamp'],
        end_timestamp: json['end_timestamp'],
        obs_nbr: json['obs_nbr'],
        start_latitude: json['start_latitude'],
        start_longitude: json['start_longitude'],
        end_latitude: json['end_latitude'],
        end_longitude: json['end_longitude']);
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'project_id': project_id,
      'server_id': server_id,
      'observer': observer,
      'user_id': user_id,
      'status': status,
      'timestamp': timestamp,
      'distance': distance,
      'start_timestamp': start_timestamp,
      'end_timestamp': end_timestamp,
      'obs_nbr': obs_nbr,
      'start_latitude': start_latitude,
      'start_longitude': start_longitude,
      'end_latitude': end_latitude,
      'end_longitude': end_longitude,
    };
  }

  void updateFromMap(var PatrolDataMap) {
    id = PatrolDataMap['_id'];
    project_id = PatrolDataMap['project_id'];
    observer = PatrolDataMap['observer'];
    user_id = PatrolDataMap['user_id'];
    status = PatrolDataMap['status'];
    timestamp = PatrolDataMap['timestamp'];
    distance = PatrolDataMap['distance'];
    start_timestamp = PatrolDataMap['start_timestamp'];
    end_timestamp = PatrolDataMap['end_timestamp'];
    obs_nbr = PatrolDataMap['obs_nbr'];
    start_latitude = PatrolDataMap['start_latitude'];
    start_latitude = PatrolDataMap['start_longitude'];
    end_latitude = PatrolDataMap['end_latitude'];
    end_longitude = PatrolDataMap['end_longitude'];
  }

  @override
  String toString() {
    return ('''
      Instance of Patrol
      Server ID: $server_id,
      Patrol ID: $id,
      Observer: $observer,
      User ID: $user_id,
      Project ID: $project_id,
      Status: $status,
      Timestamp: $timestamp,
      Distance: $distance,
      Start_timestamp: $start_timestamp,
      End_timestamp: $end_timestamp,
      Obs_nbr: $obs_nbr,
      start_latitude: $start_latitude,
      start_longitude: $start_longitude,
      end_latitude: $end_latitude,
      end_longitude: $end_longitude,
    ''');
  }

  void clear() {
    id = -1;
    project_id = -1;
    observer = "";
    status = PatrolStatus.unactif;
    user_id = 0;
    timestamp = "...";
    distance = 0;
    start_timestamp = 0;
    end_timestamp = 0;
    obs_nbr = 0;
    start_latitude = 0;
    start_latitude = 0;
    end_latitude = 0;
    end_longitude = 0;
  }
}
