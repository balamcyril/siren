import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/media_data.dart';

abstract class DBAnswerManager {
  Future<int> create(AnswerData answerData);
  Future<int> update(AnswerData answerData);
  Future<int> delete(int answerId);
  Future<List<AnswerData>> get(int observationId);
}
