//prepped variables for key instantiation
import 'package:flutter/material.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';

const String tableObservations = 'observations';

class ObservationFields {
  static const String id = '_id';
  static const String status = 'status';
  static const String patrol_id = 'patrol_id';
  static const String observer = 'observer';
  static const String latitude = 'latitude';
  static const String longitude = 'longitude';
  static const String timestamp = 'timestamp';
  static const String site = 'site';
  static const String project = 'project';
  static const String site_id = 'site_id';
  static const String project_id = 'project_id';
  static const String species = 'species';
  static const String user_id = 'user_id';
  static const String comment = 'comment';
  static const String group_id = 'group_id';
  static const String sub_group_id = 'sub_group_id';
  static const String type = 'type';
  static const String server_id = 'server_id';
  static const String nest_id = 'nest_id';
}

// Species
const int speciesOffset = 9;
const int greenTurtle = 9;
const int leatherbackTurtle = 10;
const int oliveRidleyTurtle = 11;
const int hawksbillTurtle = 12;
const int loggerheadSeaTurtle = 13;
const int unknown = 0;

enum Species {
  greenTurtle,
  leatherbackTurtle,
  oliveRidleyTurtle,
  hawksbillTurtle,
  loggerheadSeaTurtle,
  unknown
}

bool isSupported(int id) {
  return (id == greenTurtle ||
          id == leatherbackTurtle ||
          id == oliveRidleyTurtle ||
          id == hawksbillTurtle ||
          id == loggerheadSeaTurtle)
      ? true
      : false;
}

int getSpeciesIdFromServerString(String serverSpecies) {
  if (serverSpecies.toLowerCase() == "green turtle") return greenTurtle;
  // return greenTurtle - speciesOffset;
  if (serverSpecies.toLowerCase() == "hawksbill turtle") return hawksbillTurtle;
  // return hawksbillTurtle - speciesOffset;
  if (serverSpecies.toLowerCase() == "loggerhead turtle")
    return loggerheadSeaTurtle;
  // return loggerheadSeaTurtle - speciesOffset;
  if (serverSpecies.toLowerCase() == "olive ridley turtle")
    return oliveRidleyTurtle;
  // return oliveRidleyTurtle - speciesOffset;
  if (serverSpecies.toLowerCase() == "leatherback turtle")
    return leatherbackTurtle;
  // return leatherbackTurtle - speciesOffset;

  throw ("ObservationData getSpeciesIdFromServerString unknown server species: $serverSpecies");
}

// Status
enum Status { draft, uploaded, validated, deleted, uploading }

// Type
class ObservationType {
  static int get old_femaleEncounter => 16;
  static int get old_crawl => 17;
  static int get old_nesting => 18;
  static int get old_nestExhumation => 19;
  static int get old_flash => 7;

  static int get nesting => 1;
  static int get crawl => 2;
  static int get femaleEncounter => 3;
  static int get nestExhumation => -1;
  static int get animal => 4;
  static int get signOfPresence => 5;
  static int get flash => 6;
  static int get threat => 7;
  static int get other => 8;
}

class ObservationData {
  int? id;
  int server_id = -1;
  int patrol_id = -1;
  Status status = Status.draft;
  String observer = "";
  double latitude = 0;
  double longitude = 0;
  String site = "";
  int site_id = -1;
  late String project;
  late int project_id;
  String nest_id = "";
  late Species species;
  late int timestamp;
  int user_id = 0;
  String comment = "";
  int group_id = 2; // reptiles
  int sub_group_id = 4; // sea turtles
  int type = ObservationType.femaleEncounter;
  List<MediaData> mediaList = [];
  List<AnswerData> answerList = [];
  late double upload_progress = 0.0;
  late int size = 1;
  late int uploadedSize = 1;

  ObservationData() {
    if (runtimeType.toString() == "ObservationData") {
      throw Exception('Base class cannot be instantiated');
    }
  }

  //constructor that allows me to use the factory pattern
  ObservationData.namedArgumentConstructor({
    this.id,
    required this.server_id,
    required this.patrol_id,
    required this.status,
    required this.observer,
    required this.latitude,
    required this.longitude,
    required this.timestamp,
    required this.site,
    required this.project,
    required this.site_id,
    required this.project_id,
    required this.species,
    required this.user_id,
    required this.comment,
    required this.group_id,
    required this.sub_group_id,
    required this.type,
    required this.nest_id,
  });

  factory ObservationData.fromMap(Map<String, dynamic> json) {
    // debugPrint("ObservationData.fromMap : ${json}");
    int local_patrol_id = -1;
    if (json.containsKey('patrol_id')) {
      if (json['patrol_id'] != null) local_patrol_id = json['patrol_id'];
    }
    return ObservationData.namedArgumentConstructor(
        id: json['_id'],
        server_id: json['server_id'],
        patrol_id: local_patrol_id,
        status: Status.values[json['status']],
        observer: json['observer'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        timestamp: json['timestamp'],
        site_id: json['site_id'],
        project_id: json['project_id'],
        site: json['site'],
        project: json['project'],
        // -10 is the flash species id
        species: (json['species'] != -10)
            ? Species.values[json['species']]
            : Species.values[5],
        user_id: json['user_id'],
        comment: json['comment'],
        group_id: json['group_id'],
        sub_group_id: json['sub_group_id'],
        type: json["type"],
        nest_id: json['nest_id']);
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'server_id': server_id,
      'patrol_id': patrol_id,
      'status': status.index,
      'observer': observer,
      'latitude': latitude,
      'longitude': longitude,
      'timestamp': timestamp,
      'site': site,
      'site_id': site_id,
      'project_id': project_id,
      'project': project,
      'species': species.index,
      'user_id': user_id,
      'comment': comment,
      'group_id': group_id,
      'sub_group_id': sub_group_id,
      'type': type,
      'nest_id': nest_id,
    };
  }

  void updateFromMap(var observationDataMap) {
    id = observationDataMap['_id'];
    server_id = observationDataMap['server_id'];
    patrol_id = observationDataMap['patrol_id'];
    status = Status.values[observationDataMap['status']];
    observer = observationDataMap['observer'];
    latitude = observationDataMap['latitude'];
    longitude = observationDataMap['longitude'];
    timestamp = observationDataMap['timestamp'];
    site_id = observationDataMap['site_id'];
    project_id = observationDataMap['project_id'];
    site = observationDataMap['site'];
    project = observationDataMap['project'];
    species = Species.values[observationDataMap['species']];
    user_id = observationDataMap['user_id'];
    comment = observationDataMap['comment'];
    group_id = observationDataMap['group_id'];
    sub_group_id = observationDataMap['sub_group_id'];
    type = observationDataMap["type"];
    nest_id = observationDataMap['nest_id'];
  }

  String getObservationTypeString(BuildContext context) {
    if (type == ObservationType.femaleEncounter ||
        type == ObservationType.old_femaleEncounter) {
      return AppLocalizations.of(context)!.femaleEncounter;
    }
    if (type == ObservationType.crawl || type == ObservationType.old_crawl) {
      return AppLocalizations.of(context)!.crawlMonitoring;
    }
    if (type == ObservationType.nesting ||
        type == ObservationType.old_nesting) {
      return AppLocalizations.of(context)!.nestMonitoring;
    }
    if (type == ObservationType.nestExhumation ||
        type == ObservationType.old_nestExhumation) {
      return AppLocalizations.of(context)!.nestExhumation;
    }
    if (type == ObservationType.flash || type == ObservationType.old_flash) {
      return AppLocalizations.of(context)!.flash;
    }

    //new
    if (type == ObservationType.animal) {
      return AppLocalizations.of(context)!.flash;
    }
    if (type == ObservationType.signOfPresence) {
      return AppLocalizations.of(context)!.flash;
    }

    if (type == ObservationType.threat) {
      return AppLocalizations.of(context)!.flash;
    }

    if (type == ObservationType.other) {
      return AppLocalizations.of(context)!.flash;
    }
    throw Exception(
        "ObservationData getObservationTypeString invalid observation type");
  }

  // Media
  void add_media(MediaData mediaData) {
    mediaList.add(mediaData);
  }

  void remove_media(int index) {
    if (index < 0) {
      return;
    } else {
      mediaList.removeAt(index);
    }
  }

  void update_media(MediaData media) {
    int index = mediaList.indexWhere(
        (element) => element.observation_id == media.observation_id);
    if (index == -1) {
      mediaList.add(media);
    } else {
      mediaList[index] = media;
    }
  }

  void empty_media() {
    mediaList.clear();
  }

  bool hasMediaToShow() {
    return this.mediaList.isNotEmpty &&
        !this.mediaList[0].path.contains('https:');
  }

  bool noPicture() {
    return (!this.mediaList.isNotEmpty) ? true : false;
  }

  bool downloadNotComplete() {
    for (var element in this.mediaList) {
      if (element.path.contains('https:')) return true;
    }
    return false;
  }

  // Answers
  void add_answer(AnswerData answer) {
    answerList.add(answer);
  }

  // create answer if empty
  void update_answer(AnswerData answer) {
    int index = answerList
        .indexWhere((element) => element.questionId == answer.questionId);
    if (index == -1) {
      answerList.add(answer);
    } else {
      answerList[index] = answer;
    }
  }

  void remove_answer(AnswerData answer) {
    int index = answerList
        .indexWhere((element) => element.questionId == answer.questionId);
    if (index == -1) {
      return;
    } else {
      answerList.removeAt(index);
    }
  }

  @override
  String toString() {
    return ('''
      Instance of Observation
      Server ID: $server_id,
      patorl ID: $patrol_id,
      Observation ID: $id,
      Status: $status,
      Observer: $observer,
      Latitude: $latitude,
      Longitude: $longitude,
      timestanp: $timestamp,
      Site: $site,
      Project: $project,
      Site ID: $site_id,
      Project ID: $project_id,
      Species: $species,
      User ID: $user_id,
      Comment: $comment,
      Group ID: $group_id,
      Sub Group ID: $sub_group_id,
      Type: $type,
      Nest ID: $nest_id,
    ''');
  }

  void clear() {
    id = -1;
    server_id = -1;
    patrol_id = -1;
    status = Status.draft;
    observer = "";
    latitude = 0;
    longitude = 0;
    site = "";
    site_id = -1;
    project = "";
    project_id = -1;
    nest_id = "";
    timestamp = 0;
    user_id = 0;
    comment = "";
    group_id = -1;
    sub_group_id = -1;
    type = ObservationType.femaleEncounter;
    mediaList = [];
    answerList = [];
  }
}
