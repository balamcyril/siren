import 'package:siren_flutter/data/crawl_flow_observation_data.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';
import 'package:siren_flutter/data/flash_flow_observation_data.dart';
import 'package:siren_flutter/data/nest_exhumation_observation_data.dart';
import 'package:siren_flutter/data/nest_monitoring_observation_data.dart';

import 'package:siren_flutter/data/observation_data.dart';

class AccountFactory {
  ObservationData create(int type) {
    return FlashFlowObservationData();
  }
}
