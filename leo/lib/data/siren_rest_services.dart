import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:siren_flutter/data/job_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/patrol_mapper.dart';
import 'package:siren_flutter/data/project_data.dart';
import 'package:udp/udp.dart';

import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/observation_mapper.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/screens/show_observation_uploading_page.dart';

import '../utils/platform.dart';
import 'observation_type_data.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

import 'package:flutter_image_compress/flutter_image_compress.dart';

class SirenRestServices {
  final String lasthost = 'https://api.sirenammco.org/api/';
  //final String lasthost = 'https://api.testsirenammco.org/api/';

  Future<Map<String, dynamic>> getMap(String path) async {
    var response = await http.get(Uri.parse(lasthost + path));

    if (response.statusCode != 200) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    // some command return 0 as a character if they fail
    if (response.body.length < 2) {
      throw Exception("Operation failed, body length < 2");
    }
    return jsonDecode(response.body);
  }

  dynamic getList(String path) async {
    final response = await http.get(Uri.parse(lasthost + path), headers: {
      'Content-Type': 'application/json',
      // 'Accept': 'application/json',
      'Authorization':
          "Bearer ${userSessionManager.userData.loginResponse['token']}",
    });

    if (response.statusCode != 200) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    // some command return 0 as a character if they fail
    if (response.body.length < 2) {
      throw Exception("Operation failed, body length < 2");
    }
    return jsonDecode(response.body);
  }

  Future<String> post(String path, Object payload) async {
    var response = await http.post(Uri.parse(lasthost + path),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':
              "Bearer ${userSessionManager.userData.loginResponse['token']}",
        },
        body: jsonEncode(payload));

    debugPrint("object payload ${jsonEncode(payload)}");

    if (response.statusCode != 200 && response.statusCode != 201) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    // some command return 0 as a character if they fail
    if (response.body.isEmpty) {
      throw Exception("Operation failed, body length empty");
    }
    return response.body;
  }

  Future<String> postCreation(String path, Object payload) async {
    var response = await http.post(Uri.parse(lasthost + path),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(payload));

    debugPrint("object payload ${jsonEncode(payload)}");

    if (response.statusCode != 200 && response.statusCode != 201) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    // some command return 0 as a character if they fail
    if (response.body.isEmpty) {
      throw Exception("Operation failed, body length empty");
    }
    return response.body;
  }

  Future<Map<String, dynamic>> login(String email, String password) async {
    if (email.isEmpty || password.isEmpty) {
      throw Exception("Invalid credentials ");
    }

    print(jsonEncode({"email": email, "password": password}));
    print('cici');

    var response = await http.post(Uri.parse(lasthost + 'user/login'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({"email": email, "password": password}));

    print('result---');
    return jsonDecode(response.body);
  }

  Future<List<SiteData>> getSites(int projectId) async {
    List<SiteData> sites = [];

    debugPrint("get site here ..........");
    var response =
        await getList('mobile/site?project_id=' + projectId.toString());

    response['data'].forEach((site) {
      sites.add(SiteData(site['id'], site['name']));
    });

    debugPrint("liste site: ${sites}");
    return sites;
  }

  Future<List<ObservationData>> getObservations(int projectId) async {
    List<ObservationData> observations = [];
    var response = await getList('observations/' + projectId.toString());
    response.forEach((observation) {
      observations.add(ObservationMapper().fromServer(observation));
    });
    return observations;
  }

  Future<List<dynamic>> getPatrolValidatedObservations(
      int projet, List<int> data) async {
    List<ObservationData> observations = [];
    List<PatrolData> patrols = [];
    bool validatetype = false;

    debugPrint("the data to send: ${data}");

    var response =
        await http.post(Uri.parse(lasthost + 'mobile/observationSyncV2'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization':
                  "Bearer ${userSessionManager.userData.loginResponse['token']}",
            },
            body: jsonEncode({"project_id": projet, "data": data}));

    if (response.statusCode != 200 && response.statusCode != 201) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    if (response.body.isEmpty) {
      throw Exception("Operation failed, body length empty");
    }

    jsonDecode(response.body)['observations'].forEach((observation) {
      validatetype = isSupported(observation['especes']['id']);
      debugPrint(" supporte : ${validatetype}");
      if (validatetype || (observation['especes']['id'] == -1))
        observations.add(ObservationMapper().fromServer(observation));
    });

    jsonDecode(response.body)['patrols'].forEach((patrol) {
      patrols.add(PatrolMapper().fromServer(patrol));
    });

    debugPrint("prject len: ${patrols.length}");
    return [patrols, observations];
  }

  Future<List<ObservationData>> getValidatedObservations(
      int projet, List<int> data) async {
    List<ObservationData> observations = [];
    bool validatetype = false;

    debugPrint("the data to send: ${data}");

    var response =
        await http.post(Uri.parse(lasthost + 'mobile/observationSync'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization':
                  "Bearer ${userSessionManager.userData.loginResponse['token']}",
            },
            body: jsonEncode({"project_id": projet, "data": data}));

    if (response.statusCode != 200 && response.statusCode != 201) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    if (response.body.isEmpty) {
      throw Exception("Operation failed, body length empty");
    }

    jsonDecode(response.body).forEach((observation) {
      validatetype = isSupported(observation['especes']['id']);
      debugPrint(" supporte : ${validatetype}");
      if (validatetype || (observation['especes']['id'] == -1))
        observations.add(ObservationMapper().fromServer(observation));
    });

    return observations;
  }

  Future<List<ObservationsTypeData>> getObservationSupported(
      int projectId) async {
    List<ObservationsTypeData> observationsType = [];
    // var response = await getList('typeobservations/' + projectId.toString());//old
    var response = await getList('typeObservation/list');
    response['data'].forEach((data) {
      observationsType.add(ObservationsTypeData(
          // data['id'], data['nom_fr'], data['nom_en'], data['nom_pt']));//old
          data['id'],
          data['name'],
          data['name'],
          data['name']));
    });
    return observationsType;
  }

  Future<List<int>> getUploadedValidatedObservations(List<int> data) async {
    List<int> observations = [];

    debugPrint("all validate upload haut");
    var response = await http.post(
        Uri.parse(lasthost + 'mobile/observationSyncVerif'),
        // var response = await http.post(Uri.parse(host + 'observationsSyncVerif'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':
              "Bearer ${userSessionManager.userData.loginResponse['token']}",
        },
        body: jsonEncode({"data": data}));

    debugPrint("all : ${response.statusCode}");

    if (response.statusCode != 200 && response.statusCode != 201) {
      throw Exception("Bad return code ${response.statusCode}");
    }
    if (response.body.isEmpty) {
      throw Exception("Operation failed, body length empty");
    }

    jsonDecode(response.body).forEach((observation) {
      observations.add(observation);
    });

    debugPrint(" ${observations}");

    return observations;
  }

  Future<int> submitObservationValidate(
      BuildContext context, ObservationData observationData) async {
    Map payload = {
      "projet": observationData.project_id,
      "dateo": (observationData.timestamp / 1000).round(),
      "coordY": observationData.longitude,
      "coordX": observationData.latitude,
      "utilisateur": observationData.user_id,
      "espece": observationData.species.index,
      "groupe": observationData.group_id,
      "sousgroupe": observationData.sub_group_id,
      "typeObservations": observationData.type,
      "note": observationData.comment,
      "site": observationData.site_id,
      "status": 1
    };

    String responseBody = await post('observation', payload);
    int observationId = int.parse(responseBody);

    return observationId;
  }

  Future<int> submitObservation(
      BuildContext context, ObservationData observationData) async {
    var species = -1;

    //espece
    if (observationData.species.index != 5)
      species = observationData.species.index + speciesOffset;

    Map payload = {
      "project_id": observationData.project_id,
      "date": (observationData.timestamp / 1000).round(),
      "coordY": observationData.longitude,
      "coordX": observationData.latitude,
      "specie_id": species,
      "groupe": observationData.group_id,
      "type_observation_id": observationData.type,
      "note": observationData.comment,
      "segment_id": observationData.site_id
    };

    String imageDirectoryPath = await getMobileStoragePath();
    var key;
    File file;
    late Uint8List readImage;
    for (var i = 0; i < observationData.mediaList.length; i++) {
      key = "img" + (i + 1).toString();
      // key = "img" + (i + 1).toString() + "File";
      file = File(imageDirectoryPath + observationData.mediaList[i].path);
      readImage = file.readAsBytesSync();
      payload[key] = 'data:image/jpeg;base64,' + base64.encode(readImage);
      if (i == 3) {
        break;
      }
    }

    payload['qa_number'] = observationData.answerList.length;
    for (var i = 0; i < observationData.answerList.length; i++) {
      // key = "qa" + (i + 1).toString();
      payload["qa"][i] = {
        "question_id": observationData.answerList[i].questionId,
        "content": getAnswerText(context, observationData.answerList[i])
      };
    }

    String responseBody = await post('mobile/observationQa/create', payload);
    // String responseBody = await post('observation', payload);
    int observationId = int.parse(responseBody);
    // debugPrint(responseBody);
    // // send answers if any
    // for (var i = 0; i < observationData.answerList.length; i++) {
    //   payload = {
    //     "observation_id": observationId,
    //     "question_id": observationData.answerList[i].questionId,
    //     "content": getAnswerText(context, observationData.answerList[i])
    //   };
    //   responseBody = await post('mobile/result/create', payload);
    // }

    return observationId;
  }

  Future<int> getFullSize(BuildContext context, ObservationData observationData,
      int index, int patrolId) async {
    var dio = Dio();
    var token = CancelToken();
    var species = -1;
    var objQa = [];

    // dio.interceptors.add(InterceptorsWrapper(
    //   onRequest: (RequestOptions options, requestInterceptorHandler)
    //       // onRequest: (RequestOptions options)
    //       {
    //     print('----- Request -----');
    //     print('Method: ${options.method}');
    //     print('URL: ${options.uri}');
    //     print('Headers: ${options.headers}');
    //     print('Query Parameters: ${options.queryParameters.toString()}');
    //     print('Request Data: ${options.data.toString()}');
    //     print('---------------------');
    //     // return options;
    //   },
    // ));

    //espece
    if (observationData.species.index != 5)
      species = observationData.species.index + speciesOffset;

    // Map payload = {
    var payload = {
      "type_observation_id": observationData.type,
      "specie_id": species,
      "project_id": observationData.project_id,
      "date": (observationData.timestamp / 1000).round(),
      "coordY": observationData.longitude,
      "coordX": observationData.latitude,
      "user_id": observationData.user_id,
      "note": observationData.comment,
      "segment_id": observationData.site_id
    };

    if (patrolId > 0) {
      debugPrint("obs for patrol to send: ");
      payload['patrol_id'] = patrolId;
    }

    String imageDirectoryPath = await getMobileStoragePath();
    var key;
    var keyz;
    File file;
    late Uint8List readImage;

    for (var i = 0; i < observationData.mediaList.length; i++) {
      var compressPath = imageDirectoryPath + i.toString() + "compressSize.jpg";
      // var zipPath = imageDirectoryPath +
      //     observationData.mediaList[i].path.split('.')[0] +
      //     '.zip';
      var result = await FlutterImageCompress.compressAndGetFile(
        imageDirectoryPath + observationData.mediaList[i].path,
        compressPath,
        quality: 50,
      );

      key = "img" + (i + 1).toString();
      // keyz = "zip" + (i + 1).toString();
      file = File(imageDirectoryPath + observationData.mediaList[i].path);
      readImage = file.readAsBytesSync();
      payload[key] = await MultipartFile.fromFile(compressPath,
          // imageDirectoryPath + observationData.mediaList[i].path,
          filename: key + '.jpg');
      // payload[keyz] =
      //     await MultipartFile.fromFile(zipPath, filename: 'image.zip');
      // payload[key] = 'data:image/jpeg;base64,' + base64.encode(readImage);
      if (i == 3) {
        break;
      }
    }
    for (var i = 0; i < observationData.answerList.length; i++) {
      objQa.add({
        "question_id": observationData.answerList[i].questionId,
        "content": getAnswerText(context, observationData.answerList[i])
      });
    }

    payload['qa_number'] = observationData.answerList.length;
    if (payload['qa_number'] != 0) {
      payload["qa"] = objQa;
    }
    FormData formData = new FormData.fromMap(payload);

    debugPrint("all payload formdata: ${formData.fields}}");
    debugPrint(
        "payload to send qa: ${payload["qa"]} -  ${payload['qa_number']}");

    try {
      await dio.post(
        lasthost + 'mobile/observationQa/create',
        data: formData,
        options: Options(
          receiveTimeout: Duration(milliseconds: 1000),
          sendTimeout: Duration(milliseconds: 1000),
          headers: {
            HttpHeaders.contentTypeHeader: "charset=UTF-8",
            HttpHeaders.authorizationHeader:
                "Bearer ${userSessionManager.userData.loginResponse['token']}",
          },
        ),
        cancelToken: token,
        onSendProgress: (int send, int total) {
          userSessionManager.observationOnUploaded[index].size = total;
          token.cancel();
        },
      );
    } catch (e) {
      debugPrint("Calcul size failled: ${e}");
    }

    return 1;
  }

  Future<int> submitPatrol(BuildContext context, int indexPatrol) async {
    int patrolId = -1;
    var payload = {
      "observer": userSessionManager.patrollist[indexPatrol].observer,
      "user_id": userSessionManager.patrollist[indexPatrol].user_id,
      "project_id": userSessionManager.patrollist[indexPatrol].project_id,
      "status": userSessionManager.patrollist[indexPatrol].status,
      "start_timestamp":
          userSessionManager.patrollist[indexPatrol].start_timestamp,
      "timestamp": userSessionManager.patrollist[indexPatrol].timestamp,
      "distance": userSessionManager.patrollist[indexPatrol].distance,
      "end_timestamp": userSessionManager.patrollist[indexPatrol].end_timestamp,
      "obs_nbr": userSessionManager.patrollist[indexPatrol].obs_nbr,
      "start_latitude":
          userSessionManager.patrollist[indexPatrol].start_latitude,
      "start_longitude":
          userSessionManager.patrollist[indexPatrol].start_longitude,
      "end_latitude": userSessionManager.patrollist[indexPatrol].end_latitude,
      "end_longitude": userSessionManager.patrollist[indexPatrol].end_longitude
    };

    String responseBody = await post('mobile/patrol/create', payload);
    debugPrint("patrol resulte:");

    patrolId = jsonDecode(responseBody)['data']['id'];
    // patrolId = int.parse(jsonDecode(responseBody)['data']['id']);

    if (patrolId != -1)
      debugPrint("patrol created with server id: ${patrolId}");
    else
      debugPrint("patrol created faild");

    return patrolId;
  }

  Future<List<Job>> CreateAccount() async {
    List<Job> jobs = [];

    var payload = {
      "email": userSessionManager.userInfo.email,
      "password": userSessionManager.userInfo.password,
      "codeTel": userSessionManager.userInfo.codeTel,
      "phone": userSessionManager.userInfo.phone,
      "sigle": userSessionManager.userInfo.sigle
    };

    debugPrint("creation payload: ${payload}");

    var responseBody = await postCreation('userMobile/create', payload);

    debugPrint("jobs get: ${responseBody}");

    jsonDecode(responseBody)['data']['fonctions'].forEach((jb) {
      jobs.add(Job.fromJson(jb));
    });

    debugPrint("jobs get: ${jobs}");
    userSessionManager.userInfo.id = jsonDecode(responseBody)['data']['id'];
    userSessionManager.jobList = jobs;
    return jobs;
  }

  Future<bool> AssignProject(List<int> projectsId) async {
    List<Job> jobs = [];

    var payload = {
      "userId": userSessionManager.userInfo.id,
      "projects": projectsId
    };

    debugPrint("creation payload: ${payload}");

    var responseBody = await postCreation('userMobile/AssignPojetct', payload);

    debugPrint("assign project result: ${responseBody}");

    return jsonDecode(responseBody)['data'][0] as bool;
  }

  Future<bool> CheckCode(String code) async {
    bool check = false;

    var payload = {"code": code, "email": userSessionManager.userInfo.email};

    debugPrint("code payload: ${payload}");

    var responseBody = await postCreation('userMobile/validate', payload);

    debugPrint("code getss: ${responseBody}");

    return jsonDecode(responseBody)['data'][0] as bool;
    // return responseBody as bool;
  }

  Future<bool> refreshCode() async {
    var payload = {"userId": userSessionManager.userInfo.id};
    var responseBody = await postCreation('userMobile/updateCode', payload);

    debugPrint("refresh code: ${responseBody}");

    return jsonDecode(responseBody)['data'][0] as bool;
  }

  Future<List<ProjectData>> refreshData() async {
    List<ProjectData> projects = [];

    var response = await getList('user/project');

    // debugPrint("response: ${response['data']}");
    response['data'].forEach((project) {
      debugPrint("id: ${project['id']}");
      debugPrint("name: ${project['name']}");
      projects.add(ProjectData(project['id'], project['name']));
    });

    debugPrint("liste site: ${projects}");
    return projects;
  }

  Future<bool> updateUser(var payload) async {
    List<ProjectData> projects = [];
    bool ret = false;
    debugPrint("update payload: ${payload}");
    var responseBody = await postCreation('userMobile/update', payload);
    debugPrint("data content: ${responseBody}");

    jsonDecode(responseBody)['data'].forEach((project) {
      debugPrint("id: ${project['id']}");
      debugPrint("name: ${project['name']}");

      projects.add(ProjectData(project['id'], project['name']));
    });

    userSessionManager.userData.projects = projects;

    debugPrint("liste projet: ${projects}");

    if (jsonDecode(responseBody)['code'] == 200) ret = true;
    // return jsonDecode(responseBody)['data'][0];
    return ret;
  }

  Future<List<Job>> getJob() async {
    List<Job> jobs = [];

    debugPrint("get job ..........");
    var response = await getList('noconnect/function/list');

    if (response.statusCode != 200) {
      throw Exception("Bad return code ${response.statusCode}");
    }

    response['data'].forEach((job) {
      jobs.add(Job(job['id'], job['name']));
    });

    debugPrint("liste site: ${jobs}");
    return jobs;
  }

  Future<int> submitObservationAll(
      BuildContext context,
      ObservationData observationData,
      int index,
      int patrolId,
      MainScreenStatefulWidget parent) async {
    ShowObservationUploadingPage showPage =
        ShowObservationUploadingPage(parent);
    MainScreenState state = parent.getState();

    var dio = Dio();
    var token = CancelToken();
    Response responseBody;
    double calc = 0;
    int totalSend = 0;
    int lastSendData = 0;
    int trueSendData = 0;
    int observationId = 0;
    var objQa = [];
    var species = -1;

    //espece
    if (observationData.species.index != 5)
      species = observationData.species.index + speciesOffset;

    // Map payload = {
    var payload = {
      "type_observation_id": observationData.type,
      "specie_id": species,
      "project_id": observationData.project_id,
      "date": (observationData.timestamp / 1000).round(),
      "coordY": observationData.longitude,
      "coordX": observationData.latitude,
      "user_id": observationData.user_id,
      "note": observationData.comment,
      "segment_id": observationData.site_id
    };

    if (patrolId > 0) {
      debugPrint("obs for patrol to send: ");
      payload['patrol_id'] = patrolId;
    }

    String imageDirectoryPath = await getMobileStoragePath();
    var key;
    var keyz;
    File file;
    late Uint8List readImage;

    for (var i = 0; i < observationData.mediaList.length; i++) {
      var compressPath = imageDirectoryPath + i.toString() + "compress.jpg";
      // var zipPath = imageDirectoryPath +
      //     observationData.mediaList[i].path.split('.')[0] +
      //     '.zip';
      var result = await FlutterImageCompress.compressAndGetFile(
        imageDirectoryPath + observationData.mediaList[i].path,
        compressPath,
        quality: 50,
      );

      key = "img" + (i + 1).toString();
      // keyz = "zip" + (i + 1).toString();
      file = File(imageDirectoryPath + observationData.mediaList[i].path);
      readImage = file.readAsBytesSync();
      payload[key] = await MultipartFile.fromFile(compressPath,
          // imageDirectoryPath + observationData.mediaList[i].path,
          filename: key + '.jpg');
      // payload[keyz] =
      //     await MultipartFile.fromFile(zipPath, filename: 'image.zip');
      if (i == 3) {
        break;
      }
    }

    for (var i = 0; i < observationData.answerList.length; i++) {
      objQa.add({
        "question_id": observationData.answerList[i].questionId,
        "content": getAnswerText(context, observationData.answerList[i])
      });
    }

    payload['qa_number'] = observationData.answerList.length;
    if (payload['qa_number'] != 0) {
      payload["qa"] = objQa;
    }

    FormData formData = new FormData.fromMap(payload);

    debugPrint("all payload image: ${payload['img1']}}");
    debugPrint(
        "payload to send qa: ${payload["qa"]} -  ${payload['qa_number']}");

    try {
      responseBody = await dio.post(
        lasthost + 'mobile/observationQa/create',
        data: formData,
        options: Options(
          receiveTimeout: Duration(milliseconds: 60000),
          sendTimeout: Duration(milliseconds: 60000),
          headers: {
            HttpHeaders.contentTypeHeader: "charset=UTF-8",
            // HttpHeaders.contentTypeHeader: "application/json; charset=UTF-8",
            HttpHeaders.authorizationHeader:
                "Bearer ${userSessionManager.userData.loginResponse['token']}",
          },
        ),
        cancelToken: token,
        onSendProgress: (int send, int total) {
          calc = ((send * 100) / total) / 100;

          // debugPrint("sending ........ ${send} :: total......... ${total}  ");

          totalSend = total;

          if (send <= total) {
            trueSendData = send - lastSendData;
            lastSendData = send;
            userSessionManager.uploadedSize =
                userSessionManager.uploadedSize + trueSendData;

            userSessionManager.observationOnUploaded[index].uploadedSize = send;

            userSessionManager.observationOnUploaded[index].upload_progress =
                double.parse(calc.toStringAsFixed(2));
          }

          state.refeshState();
        },
      );

      state.refeshState();

      observationId = responseBody.data['data']['id'];

      debugPrint("Dio return value");
      debugPrint("${responseBody}");
    } catch (e) {
      debugPrint("Dio uploadall error error: ${e}");

      if (userSessionManager.observationOnUploaded[index].uploadedSize > 0) {
        userSessionManager.allUploadSize = userSessionManager.allUploadSize -
            userSessionManager.observationOnUploaded[index].uploadedSize;
        userSessionManager.uploadedSize = userSessionManager.uploadedSize -
            userSessionManager.observationOnUploaded[index].uploadedSize;
      }

      state.refeshState();

      throw Exception("-Exeption-");
    }

    return observationId;
  }

  String getAnswerText(BuildContext context, AnswerData answerData) {
    String answerText = answerData.answer;
    var question =
        ObservationQuestionManager(context).getById(answerData.questionId);
    if (question.type == questionType.select) {
      // for select type, answer.amswer contains the ID of the selection, we thus need to get actual text from the ID
      answerText = questionSelectionStringFromAnswerID(
          context, question.name, int.parse(answerData.answer));
    }
    return answerText;
  }
}
