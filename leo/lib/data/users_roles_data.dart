class UsersRoles {
  static int get pecheur => 1;
  static int get chercheur => 2;
  static int get plongeur => 3;
  static int get touriste => 4;
  static int get staff => 5;
  static int get autre => 6;
}
