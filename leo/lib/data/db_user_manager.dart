import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/user_data.dart';
import 'package:siren_flutter/screens/geo_map.dart';

import 'observation_type_data.dart';

abstract class DBUserManager {
  Future<dynamic> init({bool test = false});
  void set(UserData data);
  Future<Map> get();
  void delete();
  void setPosition(geoPoint point);
  Future<geoPoint> getPosition();
  void setSites(List<SiteData> sites);
  Future<List<SiteData>> getSites();
  void setLastSync(String sync);
  void setPatrolId(int patrolId);
  Future<int> getPatrolId();
  Future<String> getLastSync();
  void setObservationsType(List<ObservationsTypeData> observationsType);
  Future<List<ObservationsTypeData>> getObservationsType();
}
