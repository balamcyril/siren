class SiteData {
  SiteData(this.id, this.name);
  late int id;
  late String name;
  @override
  String toString() {
    return "Site id: $id, name: $name";
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
  SiteData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];
}
