import 'package:siren_flutter/data/site_data.dart';
import 'observation_type_data.dart';

class ProjectData {
  ProjectData(this.id, this.name);
  late int id;
  late String name;
  List<SiteData> siteDataList = [];
  List<ObservationsTypeData> observationsTypeList = [];
}
