import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:siren_flutter/data/db_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive_flutter/hive_flutter.dart';

class HiveObservationManager extends DBObservationManager {
  late Box<dynamic> box;
  final String boxName = 'ObservationBox1';

  Future<dynamic> init({bool test = false}) async {
    if (!kIsWeb && !Hive.isBoxOpen(boxName)) {
      WidgetsFlutterBinding.ensureInitialized();
      if (!test) {
        final appDocumentDirectory =
            await path_provider.getApplicationDocumentsDirectory();
        Hive.init(appDocumentDirectory.path);
        print('HiveObservationManager init ${appDocumentDirectory.path}');
      } else {
        Hive.init('.');
      }
    }
    return Hive.openBox(boxName);
  }

  Future<int> create(ObservationData data) async {
    Map data_map = {'type': data.type, 'comment': data.comment};
    box = await Hive.openBox(boxName);
    int key = await box.add('row');
    print('HiveObservationManager create $key');
    box.put(key, data_map);
    return key;
  }

  void update(ObservationData data) {}
  void delete(int observation_id) {}

  @override
  Future<Map> get(int? observation_id) async {
    Box box = await Hive.openBox(boxName);
    return box.get(observation_id);
  }
}
