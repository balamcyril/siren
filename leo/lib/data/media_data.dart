import 'dart:io';

const String tableMedias = 'medias';

class MediaFields {
  static const String id = 'media_id';
  static const String path = 'path';
  static const String observation_id = 'observation_id';
}

class MediaData {
  int? media_id;
  late String path;
  late int observation_id;

  MediaData(this.path, this.observation_id);

  //constructor that allows me to use the factory pattern
  MediaData.namedArgumentConstructor(
      {this.media_id, required this.path, required this.observation_id});

  factory MediaData.fromMap(Map<String, dynamic> json) {
    return MediaData.namedArgumentConstructor(
        media_id: json['media_id'],
        path: json['path'],
        observation_id: json['observation_id']);
  }

  Map<String, dynamic> toMap() {
    return {
      'media_id': media_id,
      'path': path,
      'observation_id': observation_id
    };
  }

  @override
  bool operator ==(Object other) =>
      other is MediaData && other.media_id == media_id;
}
