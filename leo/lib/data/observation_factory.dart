import 'package:siren_flutter/data/crawl_flow_observation_data.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';
import 'package:siren_flutter/data/flash_flow_observation_data.dart';
import 'package:siren_flutter/data/nest_exhumation_observation_data.dart';
import 'package:siren_flutter/data/nest_monitoring_observation_data.dart';

import 'package:siren_flutter/data/observation_data.dart';

class ObservationFactory {
  ObservationData create(int type) {
    if (type == ObservationType.crawl || type == ObservationType.old_crawl) {
      return CrawlFlowObservationData();
    }
    if (type == ObservationType.femaleEncounter ||
        type == ObservationType.old_femaleEncounter) {
      return FemaleEncounterFlowObservationData();
    }
    if (type == ObservationType.nesting ||
        type == ObservationType.old_nesting) {
      return NestMonitoringObservationData();
    }
    if (type == ObservationType.nestExhumation ||
        type == ObservationType.old_nestExhumation) {
      return NestExhumationObservationData();
    }
    if (type == ObservationType.flash || type == ObservationType.old_flash) {
      return FlashFlowObservationData();
    }
    throw Exception("ObservationFactory - create invalid observation type");
  }
}
