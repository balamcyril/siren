import 'package:flutter/material.dart';
import 'observation_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/leo_custom_icons.dart';

class ObservationsTypeData {
  ObservationsTypeData(this.id, this.nom_fr, this.nom_en, this.nom_pt);

  late int id;
  late String nom_fr;
  late String nom_en;
  late String nom_pt;

  @override
  String toString() {
    return "ObservationType id: $id";
  }

  Map<String, dynamic> toJson() =>
      {'id': id, 'nom_fr': nom_fr, 'nom_en': nom_en, 'nom_pt': nom_pt};

  ObservationsTypeData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nom_fr = json['nom_fr'],
        nom_en = json['nom_en'],
        nom_pt = json['nom_pt'];

  bool isSupported(int id) {
    return (id == ObservationType.femaleEncounter ||
            id == ObservationType.crawl ||
            id == ObservationType.nesting ||
            id == ObservationType.nestExhumation)
        ? true
        : false;
  }

  String getMenuText(BuildContext context, int id) {
    if (id == ObservationType.femaleEncounter) {
      return AppLocalizations.of(context)!.femaleEncounter;
    }
    if (id == ObservationType.crawl) {
      return AppLocalizations.of(context)!.crawlMonitoring;
    }
    if (id == ObservationType.nesting) {
      return AppLocalizations.of(context)!.nestMonitoring;
    }
    if (id == ObservationType.nestExhumation) {
      return AppLocalizations.of(context)!.nestExhumation;
    }
    throw Exception(
        "ObservationData getObservationTypeString invalid observation type");
  }

  IconData getIcon(int id) {
    if (id == ObservationType.femaleEncounter) {
      return LeoCustomIcons.sea_turtle;
    }
    if (id == ObservationType.crawl) {
      return LeoCustomIcons.crawl__track_;
    }
    if (id == ObservationType.nesting) {
      return LeoCustomIcons.new_nest;
    }
    if (id == ObservationType.nestExhumation) {
      return LeoCustomIcons.excavate;
    }
    throw Exception("ObservationIcon invalid observation icon type");
  }
}
