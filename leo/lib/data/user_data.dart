import 'dart:convert';

import 'package:siren_flutter/data/project_data.dart';
import 'package:siren_flutter/screens/geo_map.dart';
import 'package:flutter/material.dart';

class UserData {
  late String nameAndSurname;
  late String email;
  late String password;
  late String username;
  late int roleId;
  late String roleName;
  late String phone;
  late int id;
  late ProjectData currentProjectData;
  late geoPoint currentLocation;
  late bool isAdmin = false;
  late String sigle;
  late String codeTel;

  late List<ProjectData> projects = [];
  late Map loginResponse;

  void fillFromLoginResponse(response) {
    loginResponse = response;
    nameAndSurname = response['firstname'] +
        " " +
        response[
            'lastname']; //replace with name and surname once response supports it
    email = response['email'];
    username = response['firstname'];
    id = response['id'];

    if (response['fonction'].toString().toLowerCase() == "researcher")
      isAdmin = true;

    currentProjectData =
        ProjectData(response['projectId'], response['projectName']);
    if (response.containsKey('fonctionId')) {
      roleId = response['fonctionId'];
    } else {
      debugPrint("Warning, functionId missing from response");
      roleId = 0;
    }
    // TODO: add localized role name support
    phone = response['codeTel'] != null
        ? "+" + response['codeTel'].toString() + " " + response['phone']
        : "";
    roleName = response['fonction'];

    projects = [];
    // projects.add(currentProjectData);

    debugPrint("resultat----------: ${response}");
    debugPrint("resultat----------: ${response['projectsList']}");

    response['userProjects'].forEach((project) {
      debugPrint("id: ${project['id']}");
      debugPrint("name: ${project['name']}");

      projects.add(ProjectData(project['id'], project['name']));
    });

    debugPrint("projectList----------: ${response}");
  }

  void fillFromSiteResponse(response) {
    currentProjectData.siteDataList = response;
  }

  void fillFromObservationsTypeResponse(response) {
    currentProjectData.observationsTypeList = response;
  }

  void clear() {
    nameAndSurname = "";
    email = "";
    password = "";
    username = "";
    roleId = -1;
    roleName = "";
    phone = "";
    id = -1;
    currentProjectData = ProjectData(-1, "");
    currentLocation = geoPoint(0, 0);
    isAdmin = false;
    projects = [];
    loginResponse.clear();
  }
}
