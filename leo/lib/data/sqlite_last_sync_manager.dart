import 'package:siren_flutter/data/db_last_sync_manager.dart';
import 'package:siren_flutter/data/last_sync_data.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';

import 'package:flutter/material.dart';

class SqfliteLastsyncManager extends DBLastsyncManager {
  //Global Database Instance (singleton)
  static final SqfliteLastsyncManager instance = SqfliteLastsyncManager._init();

  static Database? _database;

  SqfliteLastsyncManager._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await SqfliteObservationManager.instance.database;
    return _database!;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  @override
  Future<int> create(LastsyncData lastsyncData) async {
    final db = await instance.database;
    final id = await db.insert(tableLastsync, lastsyncData.toMap());
    return id;
  }

  @override
  Future<int> update(LastsyncData lastsyncData) async {
    final db = await instance.database;
    try {
      return await db.update(tableLastsync, lastsyncData.toMap(),
          where: '${LastsyncFields.projet_id} = ?',
          whereArgs: [lastsyncData.projet_id]);
    } catch (e) {
      throw Exception(
          'Error updating answer with ID: ${lastsyncData.projet_id}');
    }
  }

  @override
  Future<int> delete(int projet_id) async {
    final db = await instance.database;
    try {
      return await db.delete(tableLastsync,
          where: '${LastsyncFields.projet_id} = ?', whereArgs: [projet_id]);
    } catch (e) {
      throw Exception('Error deleting answer with ID: $projet_id');
    }
  }

  @override
  Future<String> get(int project_id) async {
    Database db = await instance.database;
    var lastsync = await db.rawQuery(
        "SELECT * FROM $tableLastsync WHERE  $project_id = $tableLastsync.projet_id");
    String lastsyncArr = lastsync.isNotEmpty
        ? lastsync.map((res) => LastsyncData.fromMap(res)).toList()[0].last_sync
        : "empty";

    return lastsyncArr;
  }
}
