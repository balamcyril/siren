import 'package:siren_flutter/data/last_sync_data.dart';

abstract class DBLastsyncManager {
  Future<int> create(LastsyncData lastsyncData);
  Future<int> update(LastsyncData lastsyncData);
  Future<int> delete(int projetId);
  // Future<LastsyncData> get(int projetId);
  Future<String> get(int projetId);
}
