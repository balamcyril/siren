const String tableLastsync = 'lastsync';

class LastsyncFields {
  static const String last_sync = 'last_sync';
  static const dynamic projet_id = 'projet_id';
}

class LastsyncData {
  LastsyncData(this.last_sync, this.projet_id);
  late String last_sync;
  late int projet_id;

  LastsyncData.namedArgumentConstructor(
      {required this.last_sync, required this.projet_id});

  factory LastsyncData.fromMap(Map<String, dynamic> json) {
    return LastsyncData.namedArgumentConstructor(
        last_sync: json['last_sync'], projet_id: json['projet_id']);
  }

  Map<String, dynamic> toMap() {
    return {'last_sync': last_sync, 'projet_id': projet_id};
  }
}
