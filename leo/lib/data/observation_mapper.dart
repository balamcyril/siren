import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/observation_column_mapper.dart';
import 'package:siren_flutter/data/observation_data.dart';

import '../data/observation_type_data.dart';

//  getSpeciesIdFromServerString(observation['especes']['nom_en']);

class ObservationMapper {
  ObservationColumnMapper getColumnMapper(String column) {
    if (column == 'etat') return ObservationColumnMapperStatus();
    if (column == 'id') return ObservationColumnMapperId();
    if (column == 'date') return ObservationColumnMapperDate();
    if (column == 'coord_x') return ObservationColumnMapperGPSLatitude();
    if (column == 'coord_y') return ObservationColumnMapperGPSLongitude();
    if (column == 'patrol_id') return ObservationColumnMapperPatrol();

    if (column == 'type_observations') return ObservationColumnMapperType();
    if (column == 'note') return ObservationColumnMapperComment();
    if (column == 'collecteur') return ObservationColumnMapperObserver();
    if (column == 'especes') return ObservationColumnMapperSpecies();
    if (column == 'groupes') return ObservationColumnMapperGroup();
    if (column == 'sous_groupes') return ObservationColumnMapperSubGroup();
    if (column == 'projet') return ObservationColumnMapperProject();
    if (column == 'resultats') return ObservationColumnMapperAnswer();
    if (column == 'img1' ||
        column == 'img2' ||
        column == 'img3' ||
        column == 'img4') return ObservationColumnMapperImage();
    if (column == 'mort' ||
        column == 'alpha' ||
        column == 'utilisateurs' ||
        column == 'id_inaturalist') return ObservationColumnMapperEmpty();
    throw ("ObservationMapper getColumnMapper unknown column $column");
  }

  // return observations built from the observations rest API
  ObservationData fromServer(Map<String, dynamic> observatioServerMap) {
    Map<String, dynamic> observation_map = Map();
    List<AnswerData> answers = [];
    List<MediaData> medias = [];

    observation_map['nest_id'] = "";
    observation_map['site'] = "";
    observatioServerMap.forEach((key, value) {
      ObservationColumnMapper mapper = getColumnMapper(key);
      debugPrint("${mapper.getColumnName()}: ${key}");
      if (mapper.runtimeType != ObservationColumnMapperEmpty) {
        if (mapper.runtimeType == ObservationColumnMapperAnswer) {
          // Q&A

          // Special case! Observation answer expects string of the species ID
          (mapper as ObservationColumnMapperAnswer).speciesString =
              observatioServerMap['especes']['id'].toString();

          answers = mapper.getData(value);
          if ((observation_map['site'] as String).isEmpty) {
            observation_map['site'] = mapper.getSite();
          }
        } else if (mapper.runtimeType == ObservationColumnMapperImage) {
          // Photos
          if (value != "NULL") medias.add(mapper.getData(value));
        } else {
          // other fields
          observation_map[mapper.getColumnName()] = mapper.getData(value);
        }
      }
    });

    ObservationData observation = ObservationData.fromMap(observation_map);
    observation.answerList = answers;
    observation.mediaList = medias;
    return observation;
  }
}
