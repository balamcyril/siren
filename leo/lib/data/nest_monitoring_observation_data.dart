import 'package:siren_flutter/data/observation_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NestMonitoringObservationData extends ObservationData {
  NestMonitoringObservationData() {
    type = ObservationType.nesting;
  }
  String getTypeString(context) => AppLocalizations.of(context)!.nestMonitoring;
}
