import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/screens/observation_creator.dart';
import 'package:siren_flutter/screens/patrol_creator.dart';

class ObservationSync {
  Future<ObservationSyncResult> sync(
      BuildContext context, int projectId) async {
    final SirenRestServices restServices = SirenRestServices();
    List<int> newObservationIds = [];
    List<int> serverObservationIds = [];

    final observationsFromServer =
        await restServices.getObservations(projectId);
    Set<int> dbIds = await SqfliteObservationManager.instance.getAllServerIDs();
    observationsFromServer.forEach((observation) {
      serverObservationIds.add(observation.server_id);

      // Do not save existing observations and not validated one (status draft)
      if (!dbIds.contains(observation.server_id) &&
          observation.status != Status.draft) {
        localDBObservationManager()
            .create(context, observation, showMessage: false);
        newObservationIds.add(observation.server_id);
      }
    });
    return ObservationSyncResult(
        observationsFromServer.length, newObservationIds, serverObservationIds);
  }

  Future<int> syncValidated(
      BuildContext context, int projectId, bool fullSync) async {
    final SirenRestServices restServices = SirenRestServices();
    int patrolNbr = 0;
    int obsNbr = 0;
    List<int> validatedObservationIds = [];
    List<int> localPatrolIds = [];

    // List<int> localValidatedObservations = [];
    // List<int> validatedObservationIds = [];
    String localValidatedObservations = "";
    String localUploadedObservations = "";
    String localPatrols = "";

    List<PatrolData> localPatrolsId =
        await SqfliteObservationManager.instance.getPatrolByProject(projectId);

    List<ObservationData> LocalUploadedObservations =
        await SqfliteObservationManager.instance.getUploadedObservations();

    //get all local uploaded observation on a list of string separate by a coma
    LocalUploadedObservations.forEach((observation) {
      validatedObservationIds.add(observation.server_id);
      localUploadedObservations = (localUploadedObservations == "")
          ? "${observation.server_id}"
          : "${localUploadedObservations},${observation.server_id}";
    });

    debugPrint("ObservationUploadedDatas ---- ${localUploadedObservations} --");

    //get uploaded observation valideted from a server by passing list of local saved observation uploaded
    final observationsUploadedValidatedFromServer = await restServices
        .getUploadedValidatedObservations(validatedObservationIds);

    //updating on local, all previously uploaded and validated observation send by the server
    if (observationsUploadedValidatedFromServer.length > 0)
      for (var observationId in observationsUploadedValidatedFromServer) {
        ObservationData obs = ObservationData.fromMap(
            await SqfliteObservationManager.instance
                .getByServerId(observationId));
        obs.status = Status.validated;
        await SqfliteObservationManager.instance.update(obs);
      }

    debugPrint("ObservationUploadedDatas ---- cici --");

    validatedObservationIds = [];

    //get all local patrol on a list of string separate by a coma
    localPatrolsId.forEach((patrol) {
      if (patrol.server_id != -1) {
        patrolNbr = patrolNbr + 1;
        validatedObservationIds.add(patrol.server_id);
        localPatrols = (localPatrols == "")
            ? "${patrol.server_id}"
            : "${localPatrols},${patrol.server_id}";
      }
    });

    List<ObservationData> LocalValidatedObservations =
        await SqfliteObservationManager.instance.getValidatedObservations();
    //get all local validated observation on a list of string separate by a coma
    if (LocalValidatedObservations.length > 0)
      LocalValidatedObservations.forEach((observation) {
        obsNbr = obsNbr + 1;
        validatedObservationIds.add(observation.server_id);
        localValidatedObservations = (localValidatedObservations == "")
            ? "${observation.server_id}"
            : "${localValidatedObservations},${observation.server_id}";
      });

    debugPrint("ObservationUploadedDatas ---- cici 2--");

    debugPrint(
        "ObservationValidatedDatas ---- ${localValidatedObservations} --");

    //we add the nomber of the obs and the nomber of the patrol validated in local
    //the server have to know how many time he have to brow the array
    validatedObservationIds.add(patrolNbr);
    validatedObservationIds.add(obsNbr);

    debugPrint("fulll ---- ${fullSync} --");
    // List<ObservationData> observationsValidatedFromServer = [];
    List observationsValidatedFromServer = [];

    //get new validated observation from a server by passing list of local saved observation validated
    // observationsValidatedFromServer =
    //     await restServices.getValidatedObservations(projectId,
    //         validatedObservationIds);
    observationsValidatedFromServer = await restServices
        .getPatrolValidatedObservations(projectId, validatedObservationIds);

    //saving on local, all new validated observation send by the serve
    // observationsValidatedFromServer.forEach((observation) async {
    int getobs = 0;
    observationsValidatedFromServer[1].forEach((observation) async {
      getobs = getobs + 1;
      observation.status = Status.validated;
      localDBObservationManager()
          .create(context, observation, showMessage: false);
      // await SqfliteObservationManager.instance.create(observation);
      debugPrint("creation--observation");
    });

    observationsValidatedFromServer[0].forEach((patrol) async {
      localDBPAtrolManager().create(context, patrol);
      debugPrint("creation--patrol");
    });

    return observationsUploadedValidatedFromServer.length + getobs;
  }
}

class ObservationSyncResult {
  int observationServerCount = 0;
  List<int> newObservationIds = [];
  List<int> serverObservationIds = [];
  ObservationSyncResult(this.observationServerCount, this.newObservationIds,
      this.serverObservationIds);
  @override
  String toString() {
    return "ObservationSyncResult ${observationServerCount} found on server, ${newObservationIds.length} new observations, new Ids: $newObservationIds serverIds: $serverObservationIds";
  }
}
