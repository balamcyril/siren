import 'package:siren_flutter/data/observation_data.dart';

abstract class DBObservationManager {
  Future<int> create(ObservationData data);
  void update(ObservationData data);
  void delete(int observation_id);
  Future<Map> get(int observation_id);
}
