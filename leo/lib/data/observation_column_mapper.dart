import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';

abstract class ObservationColumnMapper {
  String getColumnName();
  dynamic getData(data);
}

//use to skip a filed
class ObservationColumnMapperEmpty extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return "Empty";
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperStatus extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.status;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperId extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.server_id;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperPatrol extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.patrol_id;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperDate extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.timestamp;
  }

  // return a timestamp
  @override
  dynamic getData(data) {
    final formatter = DateFormat(r'''yyyy-MM-dd'T'hh:mm:ss''');
    final dateTimeFromStr = formatter.parse(data);
    // final dateTimeFromStr = formatter.parse((data as String).split('+')[0]);
    return dateTimeFromStr.millisecondsSinceEpoch;
  }
}

class ObservationColumnMapperGPSLatitude extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.latitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class ObservationColumnMapperGPSLongitude extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.longitude;
  }

  @override
  dynamic getData(data) {
    return data.toDouble();
  }
}

class ObservationColumnMapperType extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.type;
  }

  @override
  dynamic getData(data) {
    return data['id'];
  }
}

class ObservationColumnMapperComment extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.comment;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperGroup extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.group_id;
  }

  @override
  dynamic getData(data) {
    return data['id'];
  }
}

class ObservationColumnMapperSubGroup extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.sub_group_id;
  }

  @override
  dynamic getData(data) {
    return data['id'];
  }
}

class ObservationColumnMapperProject extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.project_id;
  }

  @override
  dynamic getData(data) {
    return data['id'];
  }
}

class ObservationColumnMapperImage extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return "img";
  }

  @override
  dynamic getData(data) {
    return MediaData(data, -1);
  }
}

class ObservationColumnMapperAnswer extends ObservationColumnMapper {
  String site = "";
  String speciesString = "";
  @override
  String getColumnName() {
    return "answers";
  }

  @override
  dynamic getData(data) {
    List<AnswerData> answers = [];
    data.forEach((answer) {
      // fill site
      if (answer['questions']['id'] == questionNameToIDMapper['site']) {
        // this.site = answer['contenu'];
        this.site = answer['content'];
      }
      if (answer['questions']['id'] ==
          questionNameToIDMapper['turtle_species']) {
        // Special case! Observation answer expects string of the species ID
        answers.add(AnswerData(answer['questions']['id'], speciesString));
      } else {
        answers.add(AnswerData(answer['questions']['id'], answer['content']));
      }
    });
    return answers;
  }

  String getSite() {
    return this.site;
  }
}

class ObservationColumnMapperObserver extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.observer;
  }

  @override
  dynamic getData(data) {
    return data;
  }
}

class ObservationColumnMapperSpecies extends ObservationColumnMapper {
  @override
  String getColumnName() {
    return ObservationFields.species;
  }

  @override
  dynamic getData(data) {
    return data['id'] - speciesOffset;
    // return data['id'];
  }
}
