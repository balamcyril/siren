import 'package:siren_flutter/data/observation_data.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FemaleEncounterFlowObservationData extends ObservationData {
  FemaleEncounterFlowObservationData() {
    type = ObservationType.femaleEncounter;
  }
  String getTypeString(context) =>
      AppLocalizations.of(context)!.femaleEncounter;
}
