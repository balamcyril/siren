import 'package:flutter/material.dart';
import 'package:siren_flutter/utils/colors.dart';

const TextStyle regular = TextStyle(
    fontSize: 10,
    fontFamily: 'Poppins',
    fontStyle: FontStyle.normal,
    letterSpacing: 0.02);

const TextStyle h1 = TextStyle(
    fontSize: 24,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: ColorsHandler.primary1,
    letterSpacing: 0.0);

const TextStyle h2 = TextStyle(
    fontSize: 24,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    color: ColorsHandler.primary1);

const TextStyle h31 = TextStyle(
    fontSize: 14,
    fontStyle: FontStyle.italic,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    color: ColorsHandler.green01);

const TextStyle h3 =
    TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w500);

const TextStyle h3Semibold = TextStyle(
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);

const TextStyle h3Semibold2 = TextStyle(
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: ColorsHandler.primary2);

const TextStyle body =
    TextStyle(fontSize: 16, fontFamily: 'Inter', fontWeight: FontWeight.normal);

const TextStyle bodySemibold = TextStyle(
    fontSize: 14,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.01);

const TextStyle titleMedium = TextStyle(
    fontSize: 16,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w500,
    letterSpacing: 0.02,
    color: ColorsHandler.primary1);

const TextStyle bodyMedium = TextStyle(
  fontSize: 14,
  fontFamily: 'Poppins',
  fontWeight: FontWeight.w500,
  letterSpacing: 0.01,
);

TextStyle smallTextBold = TextStyle(
  fontSize: 12,
  fontFamily: 'Poppins',
  fontWeight: FontWeight.bold,
  letterSpacing: 0.01,
);

TextStyle smallTextMedium = TextStyle(
  fontSize: 12,
  fontFamily: 'Poppins',
  fontWeight: FontWeight.w500,
  letterSpacing: 0.01,
);

const TextStyle subhead =
    TextStyle(fontSize: 14, fontFamily: 'Inter', fontWeight: FontWeight.normal);

const TextStyle caption =
    TextStyle(fontSize: 12, fontFamily: 'Inter', fontWeight: FontWeight.w500);
