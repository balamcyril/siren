import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:logging/logging.dart';
import 'package:package_info/package_info.dart';

late PackageInfo globalPackageInfo;

Future<String> getMobileStoragePath() async {
  Directory dir;
  if (Platform.isAndroid) {
    debugPrint("------- Platform android -------");
    dir = await getApplicationDocumentsDirectory();
    return dir.path.substring(0, dir.path.lastIndexOf('/')) + '/cache/';
  } else {
    dir = await getLibraryDirectory();
    return dir.path.substring(0, dir.path.lastIndexOf('/')) + '/tmp/';
  }
}

void init_package_info() async {
  globalPackageInfo = await PackageInfo.fromPlatform();
}

void init_logging() async {
  // Get the documents directory
  final directory = await getApplicationDocumentsDirectory();
  var logger = Logger('main');

  // Generate a unique file name with a date and timestamp
  final now = DateTime.now();
  final timestamp =
      '${now.year}${now.month}${now.day}_${now.hour}${now.minute}${now.second}';
  final fileName = 'log_$timestamp.txt';
  final filePath = '${directory.path}/$fileName';

  // Open the log file
  var file = File(filePath);
  var sink =
      file.openWrite(mode: FileMode.append); // Open the file in append mode

  print('Log file path: $filePath'); // Print the file path to the debug console

  // Log messages to the file and console
  logger.onRecord.listen((record) {
    final message = '${record.level.name}: ${record.time}: ${record.message}';
    sink.writeln(message); // Write to file
    print(message); // Print to console
  });

  // Redirect debugPrint to the logger
  debugPrint = (String? message, {int? wrapWidth}) {
    if (message != null) {
      logger.info(message);
    }
  };
}
