import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:siren_flutter/data/observation_data.dart';

class ObservationTypeColor {
  late Color observationFillColor = Colors.grey.shade800;
  late Color observationTextColor = Colors.white;
}

class ColorsHandler {
  static ObservationTypeColor getObservationTypeColor(int observationType) {
    late ObservationTypeColor observationTypeColor = ObservationTypeColor();
    if (observationType == ObservationType.crawl) {
      observationTypeColor.observationFillColor =
          const Color.fromRGBO(200, 240, 255, 1.0);
      observationTypeColor.observationTextColor =
          const Color.fromRGBO(8, 37, 59, 1.0);
    } else if (observationType == ObservationType.nesting) {
      observationTypeColor.observationFillColor =
          const Color.fromRGBO(34, 166, 193, 1.0);
      observationTypeColor.observationTextColor = Colors.white;
    } else if (observationType == ObservationType.nestExhumation) {
      observationTypeColor.observationFillColor =
          Color.fromARGB(255, 11, 122, 145);
      observationTypeColor.observationTextColor = Colors.white;
    } else if (observationType == ObservationType.flash) {
      observationTypeColor.observationFillColor =
          Color.fromARGB(255, 1, 122, 197);
      observationTypeColor.observationTextColor = Colors.white;
    } else {
      observationTypeColor.observationFillColor =
          const Color.fromRGBO(32, 99, 141, 1.0);
      observationTypeColor.observationTextColor = Colors.white;
    }
    return observationTypeColor;
  }

  static const Color primary1 = Color.fromRGBO(8, 37, 59, 1.0);
  static const Color primary2 = Color.fromRGBO(9, 109, 187, 1.0);
  static const Color tertiary = Color.fromRGBO(114, 134, 141, 1.0);
  static const Color tertiaryOp7 = Color.fromRGBO(114, 134, 141, 0.07);
  static const Color tertiaryOp30 = Color.fromRGBO(114, 134, 141, 0.3);
  static const Color tertiaryOp15 = Color.fromRGBO(114, 134, 141, 0.15);
  static const Color tertiaryOp50 = Color.fromRGBO(114, 134, 141, 0.5);
  static const Color green01 = Color.fromRGBO(0, 127, 40, 0.985);
}
