import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/observation_data.dart';

import '../data/sqlite_answer_manager.dart';

Future<String> getObservationTitle(
    BuildContext context, ObservationData obs) async {
  String title =
      "${AppLocalizations.of(context)!.obs}: ${getObservationStringId(obs)}";
  if (obs.nest_id.isNotEmpty) {
    title = "${AppLocalizations.of(context)!.nestID}: ${obs.nest_id}";
  } else {
    String femaleTag =
        await SqfliteAnswerManager.instance.getFemaleTagID(obs.id as int);
    if (femaleTag.isNotEmpty) {
      title = "${AppLocalizations.of(context)!.tag}: $femaleTag";
    }
  }
  return title;
}

String getObservationStringId(ObservationData obs) {
  return (obs.status == Status.uploaded)
      ? obs.server_id.toString()
      : obs.id.toString();
}
