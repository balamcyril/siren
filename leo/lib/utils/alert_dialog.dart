import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/platform.dart';

Future<Widget> showAlertDialog(
    BuildContext context, String title, String content) async {
  return await showDialog(
      context: context,
      builder: (context) {
        return _showAlertDialog(context, title, content);
      });
}

Widget _showAlertDialog(BuildContext context, String title, String content) {
  return AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
    title: Text(title),
    content: Text(content),
    actions: <Widget>[
      TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(AppLocalizations.of(context)!.closeButton)),
    ],
  );
}

Future<Widget> aboutDialog(BuildContext context) async {
  return await showDialog(
      context: context,
      builder: (context) {
        return _showAboutDialog(context);
      });
}

Widget _showAboutDialog(BuildContext context) {
  return AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
    title: Text(AppLocalizations.of(context)!.aboutSiren,
        textAlign: TextAlign.center,
        style: TextStyle(color: ColorsHandler.primary1)),
    content: Column(mainAxisSize: MainAxisSize.min, children: [
      Padding(
        padding: const EdgeInsets.all(0.0),
        child: getImageContainer(),
      ),
      Text(AppLocalizations.of(context)!.sirenWebsite,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, color: ColorsHandler.tertiary)),
      Text(AppLocalizations.of(context)!.ammcoEmail,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, color: ColorsHandler.tertiary)),
      SizedBox(height: 10),
      Text(
          AppLocalizations.of(context)!.version +
              " ${globalPackageInfo.version}.${globalPackageInfo.buildNumber}",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, color: ColorsHandler.tertiary)),
      SizedBox(height: 20),
      Text(AppLocalizations.of(context)!.copyright,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 10, color: ColorsHandler.tertiary)),
    ]),
    actions: <Widget>[
      TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(AppLocalizations.of(context)!.closeButton)),
    ],
  );
}

Widget getImageContainer() {
  return const Image(
    image: AssetImage("assets/turtle.jpg"),
    //width: 140,
    //height: 140,
    fit: BoxFit.cover,
  );
}
