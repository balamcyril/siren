import 'dart:math';

double radians(double degree) {
  return degree * (pi / 180);
}

double calculateDistanceFromGPSLocation(
    double lat1, double lon1, double lat2, double lon2) {
  lat1 = radians(lat1);
  lon1 = radians(lon1);
  lat2 = radians(lat2);
  lon2 = radians(lon2);

  double R = 6371;

  double dlat = lat2 - lat1;
  double dlon = lon2 - lon1;

  double a =
      pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon / 2), 2);
  double c = 2 * atan2(sqrt(a), sqrt(1 - a));
  double distance = R * c;

  return distance;
}
