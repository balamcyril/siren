import 'package:flutter/cupertino.dart';

Widget getDefaultImage(double imageWidth, double imageHeight) {
  return Image(
    image: AssetImage('assets/NoPhoto.png'),
    width: imageWidth,
    height: imageHeight,
    fit: BoxFit.scaleDown,
  );
}
