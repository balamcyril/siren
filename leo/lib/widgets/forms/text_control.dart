import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mockito/mockito.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';

class TextControlWidget extends StatefulWidget {
  final ObservationData observationData;
  final ObservationQuestion question;
  final TextInputType textInputType;

  TextControlWidget({
    Key? key,
    required this.observationData,
    required this.question,
    required this.textInputType,
  }) : super(key: key);

  @override
  _TextControlStateWidget createState() => _TextControlStateWidget();
}

class _TextControlStateWidget extends State<TextControlWidget> {
  static double fontSize = 14;
  late AnswerData bingo = AnswerData(-1, "");
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  String getInitialValue() {
    int questionId = questionNameToIDMapper[widget.question.name];
    List<AnswerData> answers = widget.observationData.answerList;
    String initialString = "";

    if (answers.isNotEmpty) {
      bingo = widget.observationData.answerList.firstWhere(
        (element) => element.questionId == questionId,
        orElse: () => AnswerData(-1, ""),
      );
      debugPrint(
          "_TextControlStateWidget ${widget.question.name} bingo answer: " +
              bingo.answer);
      initialString = bingo.answer;
      // get string if the answer is an ID, excetion will be thrown otherwise
      try {
        if (initialString.isEmpty) {
          return initialString;
        }
        initialString = questionSelectionStringFromAnswerID(
            context, widget.question.name, int.parse(bingo.answer));
        debugPrint(
            "_TextControlStateWidget getInitialValue select id $initialString");
      } catch (e) {
        debugPrint(
            "_TextControlStateWidget getInitialValue valid exception not a select answer $e");
        // do nothing
      }
    }

    return initialString;
  }

  TextInputType _setKeyboardType() {
    TextInputType keyboardTypeChange =
        (widget.textInputType == TextInputType.text
            ? TextInputType.text
            : (widget.textInputType == TextInputType.number
                ? TextInputType.number
                : widget.textInputType == TextInputType.datetime
                    ? TextInputType.datetime
                    : widget.textInputType == TextInputType.multiline
                        ? TextInputType.text
                        : null)!);

    FocusScope.of(context).requestFocus(focusNode);
    return keyboardTypeChange;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(
        children: [
          TextFormField(
            readOnly: widget.question.isReadOnly as bool,
            keyboardType: _setKeyboardType(),
            style: titleMedium,
            minLines:
                widget.textInputType == TextInputType.multiline ? 5 : null,
            maxLines:
                widget.textInputType == TextInputType.multiline ? 8 : null,
            inputFormatters: widget.textInputType == TextInputType.number
                ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
                : null,
            decoration: InputDecoration(
                prefixText: "  ",
                floatingLabelBehavior: FloatingLabelBehavior.always,
                labelStyle:
                    bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
                labelText: widget.question.question,
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: ColorsHandler.tertiary),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: ColorsHandler.primary1, width: 2),
                ),
                suffixText: widget.question.answerSuffix.toString(),
                suffixStyle: regular
                    .merge(TextStyle(color: Color.fromRGBO(0, 0, 0, 0.7)))),
            textAlign: TextAlign.start,
            onChanged: (text) {
              if (bingo.answerId != null && bingo.answerId! > 0) {
                bingo.answer = text;
                widget.observationData.update_answer(bingo);
              } else {
                widget.observationData.update_answer(AnswerData(
                    questionNameToIDMapper[widget.question.name], text));
              }
            },
            controller: TextEditingController(text: getInitialValue()),
            validator: (value) {
              if (widget.question.isRequired &&
                  (value == null || value.isEmpty)) {
                return 'Please enter a ${widget.question.question.toLowerCase()}';
              }
              return null;
            },
          )
        ],
      ),
    );
  }
}
