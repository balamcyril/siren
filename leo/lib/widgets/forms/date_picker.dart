//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';

class DatePicker extends StatefulWidget {
  final ObservationData observationData;
  final ObservationQuestion question;

  const DatePicker({
    Key? key,
    required this.observationData,
    required this.question,
  }) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  late DateTime? _selectedDate;

  void setValue() {
    if (widget.question.isReadOnly == true &&
        widget.question.question ==
            AppLocalizations.of(context)!.estimatedHatchingDate) {
      //_selectedDate = DateTime.now().add(const Duration(days: 52));
      _selectedDate =
          DateTime.fromMillisecondsSinceEpoch(widget.observationData.timestamp)
              .add(const Duration(days: 52));
    } else if (widget.question.isReadOnly == true &&
        widget.question.question ==
            AppLocalizations.of(context)!.estimatedEscavationDate) {
      _selectedDate =
          DateTime.fromMillisecondsSinceEpoch(widget.observationData.timestamp)
              .add(const Duration(days: 52 + 5));
    } else {
      _selectedDate = null;
    }

    if (_selectedDate != null) {
      widget.observationData.update_answer(AnswerData(
          questionNameToIDMapper[widget.question.name],
          DateFormat.yMMMd().format(_selectedDate as DateTime)));
    }
  }

  String getValue() {
    int questionId = questionNameToIDMapper[widget.question.name];
    List<AnswerData> answers = widget.observationData.answerList;

    setValue();
    String initialString = _selectedDate != null
        ? DateFormat.yMMMd().format(_selectedDate as DateTime)
        : "";

    if (answers.isNotEmpty) {
      try {
        AnswerData bingo = widget.observationData.answerList.firstWhere(
          (element) => element.questionId == questionId,
        );

        debugPrint("date_picker bingo answer: " + bingo.answer);
        initialString = bingo.answer;
      } catch (e) {
        debugPrint(
            "date_picker bingo answer error catch - ALL GOOD continue: " +
                e.toString());
      }
    }
    return initialString;
  }

  //Method for showing the date picker
  void _pickDateDialog() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            //which date will display when user open the picker
            firstDate: DateTime(1950),
            //what will be the previous supported year in picker
            lastDate: DateTime
                .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;
        widget.observationData.update_answer(AnswerData(
            questionNameToIDMapper[widget.question.name],
            DateFormat.yMMMd().format(pickedDate)));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelStyle: bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
        labelText: widget.question.question,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorsHandler.tertiary),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorsHandler.primary1, width: 2),
        ),
      ),
      onTap: widget.question.isReadOnly == false ? _pickDateDialog : null,
      readOnly: widget.question.isReadOnly as bool,
      controller: TextEditingController(text: '   ' + getValue()),
    );
  }
}
