import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';

class TextReadOnlyWidget extends StatefulWidget {
  final ObservationData observationData;
  late VoidCallback locationListener;

  void addLocationListener(VoidCallback listener) {
    locationListener = listener;
  }

  void refreshLocatiom() {
    try {
      locationListener();
    } catch (e) {
      debugPrint("TextReadOnlyWidget refreshLocatiom $e");
    }
  }

  TextReadOnlyWidget({
    Key? key,
    required this.observationData,
  }) : super(key: key);

  @override
  _TextReadOnlyStateWidget createState() => _TextReadOnlyStateWidget();
}

class _TextReadOnlyStateWidget extends State<TextReadOnlyWidget> {
  late dynamic dateTimeString;
  late dynamic currentDate;

  void initState() {
    super.initState();

    if (widget.observationData.id == null) {
      // this is a new observation
      widget.observationData.timestamp = DateTime.now().millisecondsSinceEpoch;
    }
    dateTimeString = getDateTimeString();
    widget.addLocationListener(onLocationChange);
  }

  void onLocationChange() {
    debugPrint(
        "_TextReadOnlyStateWidget onLocationChange location is now lat: ${widget.observationData.latitude} long:${widget.observationData.longitude}");
    setState(() {});
  }

  String getDateTimeString() {
    try {
      return DateFormat('MM/dd/yyyy • kk:mm').format(
          DateTime.fromMillisecondsSinceEpoch(
              widget.observationData.timestamp));
    } catch (e) {
      debugPrint("getDateTimeString $e");
      return AppLocalizations.of(context)!.unknown;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      const SizedBox(height: 25),
      Text(
        AppLocalizations.of(context)!.gpsLocation,
        style: smallTextMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
      ),
      const SizedBox(height: 5),
      Row(
        children: [
          (widget.observationData.latitude != 0 &&
                  widget.observationData.longitude != 0)
              ? Text(
                  '  ' +
                      AppLocalizations.of(context)!.lat +
                      '  ' +
                      widget.observationData.latitude.toStringAsFixed(4),
                  style: titleMedium
                      .merge(TextStyle(color: ColorsHandler.primary1)),
                )
              : Container(),
          const SizedBox(
            width: 5,
          ),
          Text(
            "•",
            style: titleMedium.merge(TextStyle(color: ColorsHandler.primary1)),
          ),
          const SizedBox(
            width: 5,
          ),
          (widget.observationData.latitude != 0 &&
                  widget.observationData.longitude != 0)
              ? Text(
                  AppLocalizations.of(context)!.long +
                      '  ' +
                      widget.observationData.longitude.toStringAsFixed(4),
                  style: titleMedium
                      .merge(TextStyle(color: ColorsHandler.primary1)))
              : Container(),
        ],
      ),
      const SizedBox(height: 25),
      Text(AppLocalizations.of(context)!.dateTime,
          style:
              smallTextMedium.merge(TextStyle(color: ColorsHandler.tertiary))),
      const SizedBox(height: 5),
      Row(children: [
        Text('  ' + dateTimeString,
            style: titleMedium.merge(TextStyle(color: ColorsHandler.primary1))),
      ]),
      const SizedBox(height: 25),
      Text(
        AppLocalizations.of(context)!.observerName,
        style: smallTextMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
      ),
      const SizedBox(height: 5),
      Text('  ' + widget.observationData.observer,
          style: titleMedium.merge(TextStyle(color: ColorsHandler.primary1)))
    ]);
  }
}
