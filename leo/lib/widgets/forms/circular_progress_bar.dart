import 'dart:async';

import 'package:auto_reload/auto_reload.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dart:io';
import 'package:siren_flutter/main.dart';

class CircularProgressBarWidget extends StatefulWidget {
  // const CircularProgressBarWidget({Key? key}) : super(key: key);

  // Function runRefreshMain;
  // CircularProgressBarWidget({required this.runRefreshMain});

  late MainScreenStatefulWidget mainScreenStatefulWidget;

  CircularProgressBarWidget(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }

  @override
  _CircularProgressBarWidgetState createState() =>
      _CircularProgressBarWidgetState(mainScreenStatefulWidget);
}

abstract class _AutoReloadState extends State<CircularProgressBarWidget>
    implements AutoReloader {}

class _CircularProgressBarWidgetState extends _AutoReloadState
    with AutoReloadMixin {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  _CircularProgressBarWidgetState(parent) {
    mainScreenStatefulWidget = parent;
  }

  @override
  final Duration autoReloadDuration = const Duration(seconds: 3);

  double upload_progress = 0.0;

  @override
  void initState() {
    super.initState();
    upload_progress = userSessionManager.upload_progress;
    startAutoReload();
  }

  @override
  void dispose() {
    // setState(() {});
    cancelAutoReload();
    super.dispose();
  }

  resetVariable() {
    MainScreenState state = mainScreenStatefulWidget.getState();
    state.setState(() {
      userSessionManager.onUpload = false;
      userSessionManager.upload_progress = 0.0;
    });
    dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (userSessionManager.upload_progress >= 1.0) {
      Timer(Duration(seconds: 7), () {
        resetVariable();
      });
      return Stack(children: [
        Container(
            padding: const EdgeInsets.fromLTRB(15, 6, 0, 0),
            child: IconButton(
                padding: EdgeInsets.only(left: 30, bottom: 3),
                icon: Image.asset('assets/images/succed.png'),
                onPressed: () => {}))
      ]);
    } else
      return CircularPercentIndicator(
        radius: 15.0,
        lineWidth: 3.0,
        animation: true,
        animateFromLastPercent: true,
        backgroundColor: Color.fromARGB(255, 197, 223, 207),
        percent: userSessionManager.upload_progress,
        center: new Text(
          (upload_progress * 100).toStringAsFixed(0),
          style:
              GoogleFonts.poppins(fontSize: 8.0, fontWeight: FontWeight.w500),
        ),
        progressColor: Colors.green,
      );
  }

  @override
  void autoReload() {
    if ((upload_progress < 1.0) && (0.0 <= upload_progress))
      // if (userSessionManager.upload_progress < 1.0)
      setState(() {
        // debugPrint("uploadsize ....: ${userSessionManager.uploadedSize} ");
        // debugPrint("alluploadsize ....: ${userSessionManager.allUploadSize} ");
        // debugPrint("---------------------------");

        if (userSessionManager.uploadedSize != 0.0 &&
            userSessionManager.allUploadSize != 0.0)
          upload_progress = ((userSessionManager.uploadedSize * 100) /
                  userSessionManager.allUploadSize) /
              100;

        userSessionManager.upload_progress =
            double.parse(upload_progress.toStringAsFixed(1));
      });
  }
}
