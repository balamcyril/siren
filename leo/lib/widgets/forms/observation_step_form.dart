import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/observation_step_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/widgets/forms/date_picker.dart';
import 'package:siren_flutter/widgets/forms/dropdown_button_control.dart';
import 'package:siren_flutter/widgets/forms/photo_control.dart';
import 'package:siren_flutter/widgets/forms/text_control.dart';
import 'package:siren_flutter/widgets/forms/text_read_only.dart';
import 'package:siren_flutter/widgets/forms/text_with_add_button.dart';

class ObservationStepFormWidget extends StatefulWidget {
  ObservationData observationData;
  observationStepType stepType;
  late VoidCallback locationListener;

  void addLocationListener(VoidCallback listener) {
    locationListener = listener;
  }

  void onLocationChange() {
    try {
      locationListener();
    } catch (e) {
      debugPrint("ObservationStepFormWidget onLocationChange $e");
    }
  }

  ObservationStepFormWidget({
    Key? key,
    required this.stepType,
    required this.observationData,
  }) : super(key: key);

  @override
  _ObservationStepFormState createState() => _ObservationStepFormState();
}

class _ObservationStepFormState extends State<ObservationStepFormWidget> {
  var readOnlyWidget = null;

  wrapper(int start, int end, [bool readOnly = false]) {
    if (readOnly)
      readOnlyWidget =
          TextReadOnlyWidget(observationData: widget.observationData);
    return Container(
      margin: const EdgeInsets.only(bottom: 15.0),
      padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
      decoration: BoxDecoration(
          border: Border.all(color: ColorsHandler.tertiaryOp50),
          borderRadius: BorderRadius.circular(20)),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        if (readOnly)
          readOnlyWidget
        else
          ...getTextFieldList(context, start, end)
      ]),
    );
  }

  @override
  void didUpdateWidget(ObservationStepFormWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    super.initState();
    widget.addLocationListener(onLocationChange);
  }

  void onLocationChange() {
    if (readOnlyWidget != null) {
      (readOnlyWidget as TextReadOnlyWidget).refreshLocatiom();
    }
  }

  @override
  build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 30.0),
        child: Column(
          children: [
            if (widget.stepType == observationStepType.beach) ...[
              wrapper(0, 0),
              wrapper(0, 0, true)
            ] else if (widget.stepType ==
                observationStepType.beachWithNestID) ...[
              wrapper(0, 1),
              wrapper(0, 0, true)
            ] else if (widget.stepType == observationStepType.newNestInfo) ...[
              wrapper(0, 5),
              wrapper(6, 7)
            ] else if (widget.stepType == observationStepType.images) ...[
              ...getTextFieldList(context, 0, 0)
            ] else if ((widget.stepType == observationStepType.notes) &&
                (widget.observationData.type == ObservationType.flash)) ...[
              wrapper(0, 0),
              wrapper(0, 0, true)
            ] else ...[
              wrapper(
                  0,
                  ObservationStepManager(context)
                          .get(widget.stepType)
                          .questions
                          .length -
                      1),
            ],
          ],
        ));
  }

  List<Widget> getTextFieldList(BuildContext context, int start, int end) {
    List<Widget> fieldWidgets = [];
    List<ObservationQuestion> observationQuestions =
        ObservationStepManager(context).get(widget.stepType).questions;

    do {
      var question = observationQuestions[start];
      switch (question.type) {
        case questionType.textMultipleLines:
          fieldWidgets.add(TextControlWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.multiline,
          ));
          break;
        case questionType.textWithAddButton:
          fieldWidgets.add(TextWithAddButtonWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.number,
          ));
          break;
        case questionType.number:
          fieldWidgets.add(TextControlWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.number,
          ));
          break;
        case questionType.text:
          fieldWidgets.add(TextControlWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.text,
          ));
          break;
        case questionType.date:
          fieldWidgets.add(DatePicker(
            observationData: widget.observationData,
            question: question,
          ));

          break;
        case questionType.select:
          // create a date object, use text for now
          fieldWidgets.add(DropdownButtonControlWidget(
            observationQuestion: question,
            observationData: widget.observationData,
          ));
          break;
        case questionType.image:
          // create a date object, use text for now
          fieldWidgets.add(PhotoControlWidget(
            question: question,
            observationData: widget.observationData,
          ));
          break;
        default:
      }
      start++;
    } while (start <= end);
    return fieldWidgets;
  }
}
