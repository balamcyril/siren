import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/colors.dart';

class Controls extends StatefulWidget {
  final String secondButton;
  final bool visible;
  final void Function()? onBackTap;
  final void Function()? onForwardTap;

  const Controls(
      {Key? key,
      required this.secondButton,
      this.onBackTap,
      this.onForwardTap,
      required this.visible})
      : super(key: key);

  @override
  _Controls createState() => _Controls();
}

class _Controls extends State<Controls> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 5),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Visibility(
            visible: widget.visible,
            child: TextButton(
              child: Text(AppLocalizations.of(context)!.previousButton,
                  style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: ColorsHandler.primary1)),
              onPressed: widget.onBackTap,
            )),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ColorsHandler.primary1),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)))),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 25),
              child: Text(widget.secondButton,
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 16)),
            ),
            onPressed: widget.onForwardTap),
      ]),
    );
  }
}
