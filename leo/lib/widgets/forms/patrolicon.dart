import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:siren_flutter/data/observation_factory.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/patrol_creator.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/patrols_utils.dart';

import '../../data/observation_data.dart';
import '../../data/patrol_data.dart';

class BlinkIcon extends StatefulWidget {
  late MainScreenStatefulWidget parent;
  Function endPatrol;
  BlinkIcon({Key? key, required this.endPatrol, required this.parent})
      : super(key: key);
  @override
  _BlinkIconState createState() => _BlinkIconState(parent);
}

class _BlinkIconState extends State<BlinkIcon>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _opacityAnimation;
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  _BlinkIconState(parent) {
    mainScreenStatefulWidget = parent;
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 700),
    )..repeat(reverse: true);
    _opacityAnimation =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Container viewContent() {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      // margin: EdgeInsets.only(top: 105),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Positioned(
            child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: new Container(
                    height: 28,
                    width: 28,
                    child: Icon(
                      Icons.close,
                      color: Color.fromARGB(255, 153, 153, 153),
                      size: 28,
                    ))),
            right: 15.0,
            top: 10.0,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 23),
                child: Column(children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(8)),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 0),
                            child: Icon(
                              Icons.info_outline_rounded,
                              size: 48.0,
                              color: Color.fromRGBO(500, 32, 9, 9),
                            ),
                          ),
                        ]),
                  ),
                  Text(
                    AppLocalizations.of(context)!.patrolInProcess,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 15, right: 15), //apply padding to all four sides
                    child: Text(
                      AppLocalizations.of(context)!.endPatrolDetail,
                      style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    height: 1,
                    color: Colors.grey[500],
                    margin: EdgeInsets.only(top: 30, bottom: 0),
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.cancel,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                      )),
                      Container(
                        height: 50,
                        width: 1,
                        color: Colors.grey[500],
                      ),
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          MainScreenState state =
                              mainScreenStatefulWidget.getState();
                          state.patrolCancel();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.endPatrol,
                          style: TextStyle(
                            fontSize: 18,
                            color: Color.fromRGBO(500, 32, 9, 9),
                          ),
                        ),
                      )),
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }

  void patrolCancel() async {
    Navigator.of(context).pop();

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(days: 1),
        content: Row(children: [
          SizedBox(width: 5),
          Text(AppLocalizations.of(context)!.stoppingPatrol),
        ])));

    double distance = 0;
    List<ObservationData> observationList = [];

    ObservationData obsdata = ObservationFactory().create(-1);
    userSessionManager.dataPatrol.end_timestamp =
        new DateTime.now().toUtc().millisecondsSinceEpoch;

    userSessionManager.dataPatrol.obs_nbr =
        userSessionManager.NumberObsActualPatrol;
    if (userSessionManager.NumberObsActualPatrol > 1) {
      observationList = await SqfliteObservationManager.instance
          .getAllObservationsForPatrolProject(
              userSessionManager.userData.currentProjectData.id,
              userSessionManager.currentPatrolId,
              -1,
              -1);

      for (var i = 0; i == observationList.length - 1; i++) {
        distance = distance +
            calculateDistanceFromGPSLocation(
                observationList[i].latitude,
                observationList[i].longitude,
                observationList[i + 1].latitude,
                observationList[i + 1].longitude);
      }
    }

    userSessionManager.dataPatrol.status = PatrolStatus.unactif;
    userSessionManager.dataPatrol.distance = distance;

    Duration d = DateTime.fromMillisecondsSinceEpoch(
            userSessionManager.dataPatrol.end_timestamp)
        .difference(DateTime.fromMillisecondsSinceEpoch(
            userSessionManager.dataPatrol.start_timestamp));

    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        1577836800000 + (d.inSeconds * 1000));

    debugPrint("Start date diff: ${dateTime}, ${d},");
    userSessionManager.dataPatrol.timestamp =
        d.toString().split(':')[0] + "h " + d.toString().split(':')[1] + "m";

    Position location =
        await userSessionManager.getGPSPosition(locationCreate, obsdata);
    userSessionManager.dataPatrol.end_latitude = location.latitude;
    userSessionManager.dataPatrol.end_longitude = location.longitude;
    localDBPAtrolManager().update(context, userSessionManager.dataPatrol);
    userSessionManager.patrollist = await SqfliteObservationManager.instance
        .getPatrolByProject(userSessionManager.userData.currentProjectData.id);

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          content: Row(children: [
        ImageIcon(
          AssetImage('assets/images/pstart.png'),
          color: Colors.white,
        ),
        SizedBox(width: 8),
        Text(AppLocalizations.of(context)!.patrolEnd),
      ])));

    userSessionManager.patrol = false;
    userSessionManager.currentPatrolId = -1;
    if (userSessionManager.flashSupportSave) {
      userSessionManager.flashSupport = true;
      userSessionManager.flashSupportSave = false;
    }

    mainScreenStatefulWidget.getState().setState(() {});
  }

  void locationCreate() {
    debugPrint("Start and End getGPSPosition");
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _opacityAnimation,
      builder: (context, child) {
        return Opacity(
          opacity: _opacityAnimation.value,
          child: IconButton(
              icon: Image.asset(
                "assets/images/plaunch.png",
              ),
              onPressed: () {
                // this.widget.endPatrol;
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        elevation: 0,
                        backgroundColor: Colors.transparent,
                        child: viewContent(),
                      );
                    });
              }),
        );
      },
    );
  }
}
