import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';

import '../../utils/platform.dart';

class PhotoControlWidget extends StatefulWidget {
  final ObservationData observationData;
  final ObservationQuestion question;

  const PhotoControlWidget({
    Key? key,
    required this.observationData,
    required this.question,
  }) : super(key: key);

  @override
  _PhotoControlStateWidget createState() => _PhotoControlStateWidget();
}

class _PhotoControlStateWidget extends State<PhotoControlWidget> {
  int get imnageMaxCount => 4;
  int currentImage = 0;
  late List<dynamic> _images = [];
  late ImagePicker imagePicker;
  var type;

  // for Prev / Next
  void loadImages() async {
    String imageDirectoryPath = await getMobileStoragePath();
    for (var element in widget.observationData.mediaList) {
      _images.add(File(imageDirectoryPath + element.path));
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    loadImages();
    imagePicker = ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          getActionWidget(ImageSource.camera),
          getActionWidget(ImageSource.gallery),
        ],
      ),
      getImageTable(),
    ]));
  }

  Widget getActionWidget(source) {
    String buttonText = source == ImageSource.camera
        ? AppLocalizations.of(context)!.takePhoto
        : AppLocalizations.of(context)!.pickFromGallery;
    Image img = source == ImageSource.camera
        ? Image.asset(
            "assets/images/camera.png",
          )
        : Image.asset(
            "assets/images/photo.png",
          );
    return SizedBox(
        child: TextButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(
            EdgeInsets.only(top: 12.0, left: 20, right: 20, bottom: 12)),
        backgroundColor: MaterialStateProperty.resolveWith<Color>((states) {
          if (states.contains(MaterialState.disabled)) {
            return Colors.white;
          }
          return ColorsHandler.primary1;
        }),
        shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: Column(children: [
        img,
        Padding(
            padding:
                EdgeInsets.only(top: 10.0), //apply padding to all four sides
            child: Text(
              buttonText,
              style: const TextStyle(color: Colors.white, fontSize: 14),
            )),
      ]),
      onPressed: () {
        if (_images.length >= imnageMaxCount) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(AppLocalizations.of(context)!.mediaCountMax)));
          // str();
          setState(() {});
        } else {
          pickPhoto(source);
        }
      },
    ));
  }

  void str() async {
    String str = await getMobileStoragePath();
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text('===== ' + str + " ======")));
  }

  void pickPhoto(var source) async {
    try {
      var image = await imagePicker.pickImage(
          source: source,
          imageQuality: 50,
          preferredCameraDevice: CameraDevice.rear);

      if (image != null) {
        // Save to Gallery

        if (source == ImageSource.camera) {
          bool saveToGallery = await GallerySaver.saveImage(image.path) as bool;
          if (saveToGallery) {
            debugPrint("photoControl photo saved to gallery");
          }
        }

        // Update UI and observationData
        setState(() {
          debugPrint("Image " + image.path);
          _images.add(File(image.path));
          String fileName = image.path.split('/').removeLast();
          debugPrint("Pick photo - path saved in media list " + fileName);
          widget.observationData.add_media(MediaData(fileName, -1));
        });
      }
    } on PlatformException catch (e) {
      showAlertDialog(
          context, AppLocalizations.of(context)!.error, "${e.message}");
    } catch (e) {
      showAlertDialog(
          context, AppLocalizations.of(context)!.error, "${e.toString()}");
    }
  }

  Widget getImageTable() {
    return Container(
        margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
        child: Column(children: [
          _images.isNotEmpty
              ? Row(children: [
                  Expanded(child: getImageContainer(0)),
                  Expanded(child: getImageContainer(1))
                ])
              : Container(),
          const SizedBox(height: 10),
          _images.length > 2
              ? Row(children: [
                  Expanded(child: getImageContainer(2)),
                  Expanded(child: getImageContainer(3))
                ])
              : Container()
        ]));
  }

  Widget getImageContainer(int index) {
    return Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        height: 150,
        child: Stack(
          children: [
            getImage(index),
            Positioned(
                left: 0.0,
                top: 0.0,
                child: IconButton(
                    onPressed: () {
                      _images.removeAt(index);
                      widget.observationData.mediaList.removeAt(index);
                      setState(() {});
                    },
                    icon: const Icon(Icons.delete, color: Colors.white)))
          ],
        ));
  }

  Widget getImage(int index) {
    return _images.length > index
        ? Container(
            child: Image.file(
              _images[index],
              height: 150.0,
              fit: BoxFit.cover,
            ),
          )
        : Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            height: 150,
          );
  }
}
