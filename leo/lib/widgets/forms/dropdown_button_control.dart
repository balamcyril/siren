import 'package:flutter/material.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:siren_flutter/widgets/forms/text_control.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DropdownButtonControlWidget extends StatefulWidget {
  bool isInited = false;

  final ObservationQuestion observationQuestion;
  final ObservationData observationData;
  DropdownButtonControlWidget(
      {Key? key,
      required this.observationQuestion,
      required this.observationData})
      : super(key: key);

  @override
  _DropdownButtonControlState createState() => _DropdownButtonControlState();
}

class _DropdownButtonControlState extends State<DropdownButtonControlWidget> {
  late String _selectedDropdownItem; // Option 2
  late List<DropdownMenuItem<String>> _dropdownMenuItems = [];
  late List<DropdownMenuItem<String>> _dropdownMenuItemsTemp;
  bool _isNestIdVisible = false;

  @override
  void initState() {
    super.initState();
  }

  void setInitialState() {
    _dropdownMenuItems = [];
    _dropdownMenuItems.add(DropdownMenuItem<String>(
      value: "(${AppLocalizations.of(context)!.select} " +
          widget.observationQuestion.question +
          ")",
      child: Text(
        "(${AppLocalizations.of(context)!.select} " +
            widget.observationQuestion.question +
            ")",
        style: TextStyle(fontSize: 14),
      ),
    ));

    _dropdownMenuItemsTemp = widget.observationQuestion.options.map(
      (val) {
        return DropdownMenuItem<String>(
          value: val.id.toString(),
          child: Text(val.name),
        );
      },
    ).toList();

    _dropdownMenuItems.addAll(_dropdownMenuItemsTemp);

    int questionId = questionNameToIDMapper[widget.observationQuestion.name];
    List<AnswerData> answers = widget.observationData.answerList;

    _selectedDropdownItem = _dropdownMenuItems[0].value!;

    if (answers.isNotEmpty) {
      for (var a in answers) {
        if (a.questionId == questionId) {
          if (a.answerId == null) {
            //new observation case
            _selectedDropdownItem = a.answer; //it is actually selectionOptionId
          } else {
            debugPrint("Answer is ${a.answer.toString()}");

            for (var element in widget.observationQuestion.options) {
              debugPrint(
                  "Question option ${element.id.toString()} ${element.name.toString()}");
              if (element.id.toString().toLowerCase() ==
                  a.answer.toString().toLowerCase()) {
                _selectedDropdownItem = element.id.toString();
                debugPrint(
                    "selected option found ${element.id.toString()} ${element.name.toString()}");
              }
            }
          }
        }
      }
    }
    if (_selectedDropdownItem.toLowerCase() ==
        trueCrawlID.toString().toLowerCase()) {
      _isNestIdVisible = true;
    }
    if (_selectedDropdownItem.toLowerCase() ==
        falseCrawlID.toString().toLowerCase()) {
      _isNestIdVisible = false;
      widget.observationData.remove_answer(
          AnswerData(questionNameToIDMapper["nest_id"], _selectedDropdownItem));
      answers = widget.observationData.answerList;
    }
  }

  @override
  void didUpdateWidget(covariant DropdownButtonControlWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isInited) {
      setInitialState();
      widget.isInited = true;
      setState(() {});
    }
    return Column(
      children: [
        DropdownButtonFormField(
          style: titleMedium,
          decoration: InputDecoration(
            labelStyle:
                bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
            labelText: "${AppLocalizations.of(context)!.select} " +
                widget.observationQuestion.question,
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            hintText: widget.observationQuestion.question,
            prefixText: "  ",
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ColorsHandler.tertiary),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ColorsHandler.primary1, width: 2),
            ),
          ),
          items: _dropdownMenuItems,
          value: _selectedDropdownItem,
          onChanged: (val) {
            setState(
              () {
                debugPrint(
                    "------------------------ ${val} ------------------------");
                _selectedDropdownItem = val as String;
                widget.observationData.update_answer(AnswerData(
                    questionNameToIDMapper[widget.observationQuestion.name],
                    val));
                if (val.toLowerCase() == trueCrawlID.toString().toLowerCase()) {
                  _isNestIdVisible = true;
                } else {
                  _isNestIdVisible = false;
                }
              },
            );
          },
          validator: (value) {
            if (widget.observationQuestion.isRequired &&
                (value == null || value == _dropdownMenuItems[0].value)) {
              return '${AppLocalizations.of(context)!.pleaseSelectA} ' +
                  widget.observationQuestion.question.toLowerCase();
            }
            return null;
          },
        ),
        Visibility(
            visible: _isNestIdVisible,
            child: TextControlWidget(
              observationData: widget.observationData,
              question: ObservationQuestion("nest_id", questionType.text,
                  AppLocalizations.of(context)!.nestID, [], true),
              textInputType: TextInputType.text,
            )),
      ],
    );
  }
}
