import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';

class TextWithAddButtonWidget extends StatefulWidget {
  final ObservationData observationData;
  final ObservationQuestion question;
  final TextInputType textInputType;
  List<String> tags = [];
  String tagText = "";

  TextWithAddButtonWidget({
    Key? key,
    required this.observationData,
    required this.question,
    required this.textInputType,
  }) : super(key: key);

  @override
  _TextWithAddButtonStateWidget createState() =>
      _TextWithAddButtonStateWidget();
}

class _TextWithAddButtonStateWidget extends State<TextWithAddButtonWidget> {
  static double fontSize = 12;
  final GlobalKey<TagsState> _globalKey = GlobalKey<TagsState>();

  @override
  void initState() {
    super.initState();
  }

  String getInitialValue() {
    int questionId = questionNameToIDMapper[widget.question.name];
    List<AnswerData> answers = widget.observationData.answerList;
    String initialString = "";

    if (answers.isNotEmpty) {
      try {
        AnswerData bingo = widget.observationData.answerList.firstWhere(
          (element) => element.questionId == questionId,
        );

        debugPrint("text_control bingo answer: " + bingo.answer);
        initialString = bingo.answer;
      } catch (e) {
        debugPrint(
            "text_control bingo answer error catch - ALL GOOD continue: " +
                e.toString());
      }
    }
    return initialString;
  }

  getInputField() {
    return Container(
        child: TextFormField(
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      style: titleMedium,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'^[0-9]+[,\.]?[0-9]?'))
      ],
      decoration: InputDecoration(
        hintText:
            "(${AppLocalizations.of(context)!.youCanAdd} ${10 - widget.tags.length} ${AppLocalizations.of(context)!.moreAnswers})",
        hintStyle: bodyMedium
            .merge(TextStyle(color: ColorsHandler.tertiary))
            .apply(fontSizeFactor: 0.8),
        labelStyle: bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        labelText: widget.question.question,
      ),
      onChanged: (text) {
        widget.tagText = text;
      },
      onFieldSubmitted: (text) {
        setState(() {
          if (widget.tags.length >= 10) return;
          widget.tags.add(text);
          widget.observationData.update_answer(AnswerData(
              questionNameToIDMapper[widget.question.name],
              widget.tags.join(" ")));
        });
      },
      controller: TextEditingController(),
      validator: (value) {
        if (widget.question.isRequired && (value == null || value.isEmpty)) {
          return '${AppLocalizations.of(context)!.pleaseEnterA} ${widget.question.question.toLowerCase()}';
        }
        return null;
      },
    ));
  }

  getAddBtn() {
    return Container(
        height: 30,
        child: OutlinedButton(
          child: Text(
            '${AppLocalizations.of(context)!.add} ${AppLocalizations.of(context)!.plus}',
            style:
                TextStyle(color: ColorsHandler.tertiary).merge(smallTextMedium),
          ),
          onPressed: (() {
            setState(() {
              if (widget.tagText != "" && widget.tags.length < 10) {
                widget.tags.add(widget.tagText);
                widget.observationData.update_answer(AnswerData(
                    questionNameToIDMapper[widget.question.name],
                    widget.tags.join(" ")));
                widget.tagText = "";
              }
            });
          }),
          style: OutlinedButton.styleFrom(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            textStyle:
                TextStyle(color: ColorsHandler.tertiary).merge(smallTextMedium),
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
          ),
        ));
  }

  getTags() {
    return Container(
        margin: const EdgeInsets.only(top: 8),
        child: Tags(
            alignment: WrapAlignment.start,
            runAlignment: WrapAlignment.start,
            key: _globalKey,
            itemCount: widget.tags.length,
            columns: 10,
            itemBuilder: (index) {
              final currentItem = widget.tags[index];
              return ItemTags(
                active: false,
                alignment: MainAxisAlignment.start,
                borderRadius: BorderRadius.circular(15),
                key: Key(index.toString()),
                combine: ItemTagsCombine.withTextBefore,
                index: index,
                title: currentItem + " ${AppLocalizations.of(context)!.cm}",
                textStyle: const TextStyle(fontSize: 10),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
                color: Colors.grey.shade300,
                activeColor: ColorsHandler.tertiaryOp30,
                textActiveColor: Colors.black,
                removeButton: ItemTagsRemoveButton(onRemoved: () {
                  setState(() {
                    widget.tags.removeAt(index);
                    widget.observationData.update_answer(AnswerData(
                        questionNameToIDMapper[widget.question.name],
                        widget.tags.isEmpty ? "" : widget.tags.join(" ")));
                  });
                  return true;
                }),
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    if (getInitialValue() != "") {
      widget.tags = getInitialValue().split(" ");
    }

    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: getInputField()),
            getAddBtn(),
          ],
        ),
        getTags(),
      ]),
    );
  }
}
