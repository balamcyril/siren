// to be called from main
import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';

int global_obs_id = 202;
int global_question_id = 10;

Future sqlite_answer_manager_test() async {
  debugPrint("Run sqlite_answer_manager_test");

  // check the number of answer
  var answerList = await SqfliteAnswerManager.instance.get(global_obs_id);
  debugPrint(
      "Get answers for observation $global_obs_id count: ${answerList.length}");
  if (answerList.isNotEmpty) {
    throw Exception(
        "Answer count for observation id $global_obs_id must be 0 when the test starts");
  }

  // create an answer
  AnswerData answerData = AnswerData.namedArgumentConstructor(
      answer: "First answer",
      questionId: global_question_id,
      observationId: global_obs_id);
  debugPrint("Create a first answer");

  int answerId = await SqfliteAnswerManager.instance.create(answerData);
  debugPrint("Answer created - id: $answerId");
  answerData = AnswerData.namedArgumentConstructor(
      answer: "Second answer",
      questionId: global_question_id,
      observationId: global_obs_id);

  debugPrint("Create a second answer");
  answerId = await SqfliteAnswerManager.instance.create(answerData);
  debugPrint("Answer created - id: $answerId");

  answerList = await SqfliteAnswerManager.instance.get(global_obs_id);
  debugPrint(
      "Get answers for observation $global_obs_id count: ${answerList.length}");

  for (var data in answerList) {
    debugPrint(data.toString());
    answerId = data.answerId as int;
  }
  if (answerList.length != 2) {
    throw Exception("Answer count for observation id $global_obs_id must be 2");
  }
  // update
  answerData = AnswerData.namedArgumentConstructor(
      answer: "Third answer",
      questionId: global_question_id,
      observationId: global_obs_id);
  answerData.answerId = answerId;

  answerId = await SqfliteAnswerManager.instance.update(answerData);
  debugPrint("Answer updated - id: $answerId");
  if (answerId != 1) {
    throw Exception("Answer Id updated is wrong");
  }

  // get by observation  id
  answerList = await SqfliteAnswerManager.instance.get(global_obs_id);
  debugPrint(
      "Get answers for observation $global_obs_id count: ${answerList.length}");
  if (answerList.length != 2) {
    throw Exception("Answer count for observation id $global_obs_id must be 2");
  }
  for (var data in answerList) {
    debugPrint(data.toString());
    answerId = data.answerId as int;
  }
  // delete
  var count = await SqfliteAnswerManager.instance.delete(answerId);
  debugPrint("Answer deleted - id: $answerId");
  if (count != 1) {
    throw Exception("Answer Id deleted is wrong");
  }
  answerList = await SqfliteAnswerManager.instance.get(global_obs_id);
  answerId = answerList[0].answerId as int;
  // delete
  count = await SqfliteAnswerManager.instance.delete(answerId);
  debugPrint("Answer deleted - id: $answerId");
  if (count != 1) {
    throw Exception("Answer Id deleted is wrong");
  }
  // get by observation  id
  answerList = await SqfliteAnswerManager.instance.get(global_obs_id);
  debugPrint(
      "Get answers for observation $global_obs_id count: ${answerList.length}");
  if (answerList.isNotEmpty) {
    throw Exception("Answer count for observation id $global_obs_id must be 0");
  }
}
