import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/female_encounter_flow_observation_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

import '../utils/platform.dart';

double longitude = 52.393909;
double latitude = 13.056914;
int userId = 171086;
int projectId = 62155;

Future testRestServices(BuildContext context) async {
  String imageDirectoryPath = await getMobileStoragePath();
  var filePath = (Directory(imageDirectoryPath).listSync()[0]).path;
  var fileName =
      filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length);

  final SirenRestServices restServices = SirenRestServices();

  // debugPrint("SirenRestServices test get all observations for project");
  // final r_obs = await restServices.getObservations(projectId);
  // if (r_obs.length < 1) {
  //   throw Exception(
  //       "SirenRestServices getObservations failed, bad return value ${r_obs.length}");
  // }

  debugPrint("SirenRestServices test login");
  final r = await restServices.login('lab126bonansea@gmail.com', 'michel2024');

  debugPrint("data");
  debugPrint("${r["data"]}");

  if (r['data']['id'] != 170949) {
    throw Exception(
        "SirenRestServices login failed, bad return value ${r['data']['id']}");
  }
  if (r['data']['projectName'] != "Leo test project") {
    throw Exception(
        "SirenRestServices login failed, bad project name ${r['data']['projectName']}");
  }

  debugPrint('SirenRestServices check rest API login failed');
  try {
    final r = await restServices.login('lab126bonansea@gmail.com', 'bla');
  } catch (e) {
    if (e.toString().contains("Exception should have been thrown")) {
      throw Exception("Exception should have been thrown");
    }
  }
  debugPrint("SirenRestServices Test Get sites");
  final sites = await restServices.getSites(projectId);
  if (sites.length > 0 != true) {
    throw Exception("SirenRestServices getSites failed");
  }
  if (sites[0].name != "Beach 88") {
    throw Exception("SirenRestServices getSites return wrong name for index 0");
  }

  debugPrint("SirenRestServices Test Submit simple observation");

  ObservationData observationData = FemaleEncounterFlowObservationData();
  observationData.comment =
      "Submit observation from flutter testing " + DateTime.now().toString();
  observationData.project_id = 62155;
  observationData.timestamp =
      (DateTime.now().millisecondsSinceEpoch / 1000).round();
  observationData.longitude = longitude;
  observationData.latitude = latitude;
  observationData.user_id = userId;
  observationData.species = Species.leatherbackTurtle;
  observationData.group_id = 1;
  observationData.sub_group_id = 1;
  observationData.site_id = 3;
  final id = await restServices.submitObservation(context, observationData);
  if (id < 20900) {
    throw Exception("SirenRestServices submitObservation failed");
  }
  debugPrint('SirenRestServices Submit images');
  observationData = FemaleEncounterFlowObservationData();
  observationData.comment =
      "Submit observation with images from flutter testing " +
          DateTime.now().toString();
  observationData.project_id = 62155;
  observationData.timestamp =
      (DateTime.now().millisecondsSinceEpoch / 1000).round();
  observationData.longitude = longitude;
  observationData.latitude = latitude;
  observationData.user_id = userId;
  observationData.species = Species.leatherbackTurtle;
  observationData.group_id = 1;
  observationData.sub_group_id = 1;
  observationData.site_id = 3;

  observationData.add_media(MediaData(fileName, -1));

  debugPrint("Submit observation with images: ");

  final id1 = await restServices.submitObservation(context, observationData);
  if (id1 < 20900) {
    throw Exception("SirenRestServices submitObservation with images failed");
  }

  debugPrint("id: ${id1}");

  //Get new observation, check that the image is there
  var response =
      await restServices.getMap('observation/readOne?id=' + id1.toString());
  if (!response['data']['img1'].contains("jpg")) {
    throw Exception(
        "SirenRestServices submitObservation with images return wrong data");
  }

  debugPrint('SirenRestServices Submit answers');
  observationData = FemaleEncounterFlowObservationData();
  observationData.comment =
      "Submit observation with answers to questions from flutter testing " +
          DateTime.now().toString();
  observationData.project_id = 62155;
  observationData.timestamp =
      (DateTime.now().millisecondsSinceEpoch / 1000).round();
  observationData.longitude = longitude;
  observationData.latitude = latitude;
  observationData.user_id = userId;
  observationData.species = Species.leatherbackTurtle;
  observationData.group_id = 1;
  observationData.sub_group_id = 1;
  observationData.site_id = 3;

  observationData.add_answer(AnswerData(301, "0")); //select question
  observationData.add_answer(AnswerData(302, "there is 2 adult turtles"));

  observationData.update_answer(AnswerData(303, "there is 3 adult turtles"));

  debugPrint("Submit observation with answers: ");
  debugPrint(
      "Quesion ${observationData.answerList[0].questionId} answer: ${observationData.answerList[0].answer}");
  debugPrint(
      "Quesion ${observationData.answerList[1].questionId} answer: ${observationData.answerList[1].answer}");

  var id2 = await restServices.submitObservation(context, observationData);
  if (id2 < 20900) {
    throw Exception("SirenRestServices submitObservation with answers failed");
  }
}
