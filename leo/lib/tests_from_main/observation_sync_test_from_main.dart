import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/observation_sync.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/data/users_roles_data.dart';

int projectId = 62155;
String localValidatedObservations = "";
List<int> validatedObservationIds = [];

// to be called from main
Future observation_sync_test(BuildContext context) async {
  // QA do not show up
  // Manage images
  debugPrint("Run observation_sync_test");
  ObservationSyncResult results =
      await ObservationSync().sync(context, projectId);
  debugPrint(results.toString());
}

void getListIdObservation(List<ObservationData> LocalValidatedObservations) {
  LocalValidatedObservations.forEach((observation) {
    validatedObservationIds.add(observation.server_id);
    localValidatedObservations = (localValidatedObservations == "")
        ? "${observation.server_id}"
        : "${localValidatedObservations},${observation.server_id}";
  });
}

Future<int> createObservationUser(BuildContext context, miObservation) async {
  ObservationData observation = ObservationData.namedArgumentConstructor(
      status: Status.uploaded,
      server_id: -1,
      patrol_id: -1,
      observer: "Users account",
      timestamp: (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      latitude: 37.78,
      longitude: -122.40,
      site: 'Test Site',
      site_id: 6,
      project: 'Test Project',
      project_id: projectId,
      species: Species.leatherbackTurtle,
      user_id: 170949,
      comment: "delete",
      group_id: 2,
      sub_group_id: 4,
      nest_id: "Nest123Tested",
      type: ObservationType.femaleEncounter);

  if (miObservation) {
    debugPrint("Create an local observation");

    int observationId =
        await SqfliteObservationManager.instance.create(observation);
    debugPrint("creating an observation with id ${observationId}");
    observation.id = observationId;
  } else {
    debugPrint("creation an observation for anodther user on a server");
    observation.user_id = 21;
    observation.status = Status.validated;
  }

  int observationsUploadedId =
      await SirenRestServices().submitObservationValidate(context, observation);
  debugPrint(
      "Uploading an validated observation id ${observationsUploadedId} done");

  if (miObservation) {
    observation.status = Status.uploaded;
    observation.server_id = observationsUploadedId;
    await SqfliteObservationManager.instance.update(observation);
  }

  return observationsUploadedId;
}

Future deleteCreatedObservationUser(
    uploadedObservationId, downloadedObservationId, miObservation) async {
  final SirenRestServices restServices = SirenRestServices();
  if (miObservation) {
    ObservationData obs = ObservationData.fromMap(
        await SqfliteObservationManager.instance
            .getByServerId(uploadedObservationId));
    var count =
        await SqfliteObservationManager.instance.hardDelete(obs.id as int);
    if (count == 1)
      debugPrint(
          "Observation whith id ${obs.id} deleted succefully on local db");
  }

  //delete on the server here
  // if (count == 1)
  //         "Observation whith id ${uploadedObservationId} on the server deleted succefully");
}

Future observation_sync_test_user(
    BuildContext context, admin, accountType) async {
  final SirenRestServices restServices = SirenRestServices();

  List<ObservationData> LocalUploadedObservations =
      await SqfliteObservationManager.instance.getUploadedObservations();
  int observationsFromServerCount = 0;

  debugPrint("Run observation_sync_test_for_user_account_type");

  debugPrint("--- Testing your type account");

  if ((admin) || (accountType == UsersRoles.chercheur)) {
    if (admin) debugPrint("Your are account is an admin account");
    if (accountType == UsersRoles.chercheur)
      debugPrint("Your are account is an Scientists type account");
  } else {
    debugPrint(
        "Your are account is a simple user account, not an admin nor a scientific account");
  }

  debugPrint("--- Creating 2 observations, one in local and one on the server");
  debugPrint("--- Creating local validated none downloaded observation");
  int localServerObservationIdUploaded =
      await createObservationUser(context, true);
  debugPrint("--- Creating other user validated none downloaded observation");
  int serverObservationIdFromOtherUser =
      await createObservationUser(context, false);

  debugPrint("--- Testing simple user or fisher man sync");
  observationsFromServerCount = await ObservationSync()
      .syncValidated(context, projectId, false)
      .then((count) {
    return count;
  });
  if (observationsFromServerCount == 1)
    debugPrint("--- Simple sync successed");
  else
    debugPrint("--- Simple sync failed");

  ObservationData obs = ObservationData.fromMap(await SqfliteObservationManager
      .instance
      .getByServerId(localServerObservationIdUploaded));
  obs.status = Status.uploaded;
  await SqfliteObservationManager.instance.update(obs);

  debugPrint("--- Testing Admin or Scientists sync");
  observationsFromServerCount = await ObservationSync()
      .syncValidated(context, projectId, true)
      .then((count) {
    return count;
  });
  if (2 <= observationsFromServerCount)
    debugPrint("--- Complete sync successed");
  else
    debugPrint("--- Complete sync failed");

  // debugPrint("--- Deleting observations saved for the complete sync");
  // deleteCreatedObservationUser(
  //     localServerObservationIdUploaded, serverObservationIdFromOtherUser, true);

  // debugPrint("--- Deleting observations saved for own simple sync sync");
  // deleteCreatedObservationUser(localServerObservationIdUploaded,
  //     serverObservationIdFromOtherUser, false);
}

Future observation_sync_test_02(BuildContext context) async {
  final SirenRestServices restServices = SirenRestServices();

  debugPrint("Run observation_sync_test_02");

  List<ObservationData> LocalValidatedObservations =
      await SqfliteObservationManager.instance.getValidatedObservations();

  getListIdObservation(LocalValidatedObservations);

  //test @sync_0
  debugPrint("--- Testing failed on getLastSync for a first time @sync_0");
  List<ObservationData> AllObservations =
      await SqfliteObservationManager.instance.getAllObservations();

  debugPrint("delete all obseration");
  AllObservations.forEach((obs) async {
    obs.mediaList.forEach((media) async {
      await SqfliteMediaManager.instance.delete(media.media_id as int);
    });
    await SqfliteObservationManager.instance.hardDelete(obs.id as int);
  });

  try {
    final observationsValidatedFromServer = await restServices
        .getValidatedObservations(projectId, validatedObservationIds);
  } catch (e) {
    debugPrint("Observation sync failed from test ${e.toString()}");
  }

  //test @sync_1
  debugPrint("--- Testing Sync not validated obs @sync_1");
  ObservationData observation = ObservationData.namedArgumentConstructor(
      status: Status.draft,
      server_id: -1,
      patrol_id: -1,
      observer: "Ryan Puzon",
      timestamp: (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      latitude: 37.78,
      longitude: -122.40,
      site: 'Test Site',
      site_id: 6,
      project: 'Test Project',
      project_id: projectId,
      species: Species.leatherbackTurtle,
      user_id: 0,
      comment: "Test Comment",
      group_id: 0,
      sub_group_id: 0,
      nest_id: "Nest123Test",
      type: ObservationType.femaleEncounter);
  debugPrint("Create an observation");
  int observationId =
      await SqfliteObservationManager.instance.create(observation);
  debugPrint("Uploading an observation with id ${observationId}");
  int observationsUploadedId =
      await SirenRestServices().submitObservation(context, observation);
  debugPrint("Uploading an observation id ${observationsUploadedId} done");
  debugPrint("Deleting observation");
  var count =
      await SqfliteObservationManager.instance.hardDelete(observationId);
  if (count != 1) {
    throw Exception(
        "Observation whith id ${observationId} deleted permanently");
  }
  debugPrint("Sync after delete observation");
  final observationsFromServer = await restServices.getValidatedObservations(
      projectId, validatedObservationIds);

  try {
    observationsFromServer.forEach((obs) async {
      if (obs.id == observationId) {
        debugPrint("Error, the observation is synced again");
        throw "";
      }
    });
  } catch (e) {}

  //test @sync_2
  debugPrint("--- Testing Sync validated obs @sync_2");
  ObservationData observation02 = ObservationData.namedArgumentConstructor(
      status: Status.draft,
      server_id: -1,
      patrol_id: -1,
      observer: "Ryan Puzon 02",
      timestamp: (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      latitude: 37.78,
      longitude: -122.40,
      site: 'Test Site',
      site_id: 6,
      project: 'Test Project',
      project_id: projectId,
      species: Species.leatherbackTurtle,
      user_id: 0,
      comment: "Test Comment",
      group_id: 0,
      sub_group_id: 0,
      nest_id: "Nest123Test02",
      type: ObservationType.femaleEncounter);
  debugPrint("Create an observation");
  int observationId02 =
      await SqfliteObservationManager.instance.create(observation02);
  debugPrint("Uploading an observation with id ${observationId02}");
  int observationsUploadedId02 =
      await SirenRestServices().submitObservation(context, observation02);
  debugPrint("Uploading an observation id ${observationsUploadedId02} done");
  //
  //here code to validate observation to a server
  //
  debugPrint("Sync after validation of the observation");
  final observationsFromServer02 = await restServices.getValidatedObservations(
      projectId, validatedObservationIds);
  bool again = false;
  try {
    observationsFromServer02.forEach((obs) async {
      if (obs.id == observationId02) {
        again = true;
        throw "";
      }
    });
  } catch (e) {}

  if (!again) {
    debugPrint("Error, the observation is not send by the server");
  }

  //test @sync_3
  debugPrint("--- Testing Sync twice @sync_3");
  getListIdObservation(LocalValidatedObservations);
  debugPrint("Resync again");
  final observationsFromServer03 = await restServices.getValidatedObservations(
      projectId, validatedObservationIds);

  try {
    observationsFromServer03.forEach((obs) async {
      if (obs.id == observationId02) {
        debugPrint("Error, the observation is syncded again after resync");
        throw "";
      }
    });
  } catch (e) {}

  //test @sync_4
  debugPrint("--- Testing Delete and resync @sync_4");
  debugPrint("Deleting observation");
  var count04 =
      await SqfliteObservationManager.instance.hardDelete(observationId02);
  if (count04 != 1) {
    throw Exception(
        "Observation whith id ${observationId02} deleted permanently");
  }
  getListIdObservation(LocalValidatedObservations);
  debugPrint("Sync after delete validated observation");
  final observationsFromServer04 = await restServices.getValidatedObservations(
      projectId, validatedObservationIds);

  try {
    observationsFromServer04.forEach((obs) async {
      if (obs.id == observationId02) {
        debugPrint("Error, the observation is synced again");
        throw "";
      }
    });
  } catch (e) {}
}
