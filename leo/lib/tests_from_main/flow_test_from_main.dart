//test here as non testable from unit test due to localization
import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/question_flow_manager.dart';

void testFlow(context) {
  try {
    Map typeMap = {
      "femaleEncounter": ObservationType.femaleEncounter,
      "crawls": ObservationType.crawl,
      "nest": ObservationType.nesting,
      "flash": ObservationType.flash,
      "animal": ObservationType.animal,
      "other": ObservationType.other,
      "threat": ObservationType.threat,
      "signOfPresence": ObservationType.signOfPresence,
    };
    final QuestionFlowManager flowManager = QuestionFlowManager(context);

    for (var type in typeMap.keys) {
      List<ObservationStep> steps = flowManager.getSteps(typeMap[type]);
      debugPrint("Flow $type ${steps.length} steps");
      for (var step in steps) {
        debugPrint(step.type.toString());
        for (var question in step.questions) {
          debugPrint(question.toString());
        }
      }
    }
  } catch (e) {
    debugPrint("Exception ${e.toString()}");
  }
}
