import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/observation_type_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';

int projectId = 62155;
String localValidatedObservations = "";

// to be called from main
Future observation_type_test(BuildContext context) async {
  final ObservationsTypeData observationType =
      ObservationsTypeData(0, "", "", "");
  final int ObservationTypeExistId = 1;
  final int ObservationTypeNotExistId = 0;

  debugPrint("Run observation_type_test");

  // check if the observation is supported
  debugPrint("--- check observation not supported by the application");
  if (!observationType.isSupported(ObservationTypeNotExistId))
    debugPrint(
        "Type of observation whit id: ${ObservationTypeNotExistId} is not supported by the application");

  debugPrint("--- check observation supported by the application");
  if (observationType.isSupported(ObservationTypeExistId))
    debugPrint(
        "Type of observation whith id: ${ObservationTypeNotExistId} is supported by the application");

  // get menu text
  debugPrint("--- get the menu text for the type of observation failed");
  try {
    observationType.getMenuText(context, ObservationTypeNotExistId);
  } catch (e) {
    debugPrint("Observation type failed from test ${e.toString()}");
  }

  debugPrint("--- get the menu text for the type of observation");
  try {
    String menuText =
        observationType.getMenuText(context, ObservationTypeExistId);
    debugPrint("Menu text of the observation value : ${menuText}");
  } catch (e) {
    debugPrint("Observation type failed from test ${e.toString()}");
  }

  //get icon
  debugPrint("--- get the menu icon for the type of observation failed");
  try {
    observationType.getIcon(ObservationTypeNotExistId);
  } catch (e) {
    debugPrint("Observation type failed from test ${e.toString()}");
  }

  debugPrint("--- get the menu icon for the type of observation");
  try {
    IconData icon = observationType.getIcon(ObservationTypeExistId);
    debugPrint("Icon of the observation value : ${icon}");
  } catch (e) {
    debugPrint("Observation type failed from test ${e.toString()}");
  }
}
