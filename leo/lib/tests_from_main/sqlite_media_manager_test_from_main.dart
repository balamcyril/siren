// to be called from main
import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';

int global_obs_id = 105;

Future sqlite_media_manager_test() async {
  debugPrint("Run sqlite_media_manager_test");

  // check the number of media
  var mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  debugPrint(
      "Get medias for observation $global_obs_id count: ${mediaList.length}");
  if (mediaList.isNotEmpty) {
    throw Exception(
        "Media count for observation id $global_obs_id must be 0 when the test starts");
  }
  MediaData media = MediaData.namedArgumentConstructor(
      path: "file://image1.jpg", observation_id: global_obs_id);
  debugPrint("Create a first media");

  // create
  int mediaId = await SqfliteMediaManager.instance.create(media);
  debugPrint("Media created - id: $mediaId");

  // check the number of media
  mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  debugPrint(
      "Get medias for observation $global_obs_id count: ${mediaList.length}");
  if (mediaList.length != 1) {
    throw Exception("Media count for observation id $global_obs_id must be 1 ");
  }
  // create a second one
  media = MediaData.namedArgumentConstructor(
      path: "file://image2.jpg", observation_id: global_obs_id);
  debugPrint("Create a second media");
  mediaId = await SqfliteMediaManager.instance.create(media);
  debugPrint("Media created - id: $mediaId");

  // get by observation  id
  mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  debugPrint(
      "Get medias for observation $global_obs_id count: ${mediaList.length}");
  for (var data in mediaList) {
    debugPrint(data.path);
    mediaId = data.media_id as int;
  }
  if (mediaList.length != 2) {
    throw Exception("Media count for observation id $global_obs_id must be 2");
  }
  // update
  media = MediaData.namedArgumentConstructor(
      path: "file://image3.jpg", observation_id: global_obs_id);
  media.media_id = mediaId;
  mediaId = await SqfliteMediaManager.instance.update(media);
  debugPrint("Media updated - id: $mediaId");
  if (mediaId != 1) {
    throw Exception("Media Id updated is wrong");
  }

  // get by observation  id
  mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  debugPrint(
      "Get medias for observation $global_obs_id count: ${mediaList.length}");
  if (mediaList.length != 2) {
    throw Exception("Media count for observation id $global_obs_id must be 2");
  }
  for (var data in mediaList) {
    debugPrint(data.path);
    mediaId = data.media_id as int;
  }
  // delete
  var count = await SqfliteMediaManager.instance.delete(mediaId);
  debugPrint("Media deleted - id: $mediaId");
  if (count != 1) {
    throw Exception("Media Id deleted is wrong");
  }
  mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  mediaId = mediaList[0].media_id as int;
  // delete
  count = await SqfliteMediaManager.instance.delete(mediaId);
  debugPrint("Media deleted - id: $mediaId");
  if (count != 1) {
    throw Exception("Media Id deleted is wrong");
  }
  // get by observation  id
  mediaList =
      await SqfliteMediaManager.instance.getMediaDataList(global_obs_id);
  debugPrint(
      "Get medias for observation $global_obs_id count: ${mediaList.length}");
  if (mediaList.isNotEmpty) {
    throw Exception("Media count for observation id $global_obs_id must be 0");
  }
}

void sqlite_view_DBs() {
  SqfliteObservationManager.instance.viewContent();
}
