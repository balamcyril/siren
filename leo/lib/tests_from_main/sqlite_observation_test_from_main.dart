import 'package:flutter/cupertino.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';

// to be called from main
Future sqlite_observation_test() async {
  debugPrint("Run sqlite_observation_test");

  debugPrint("Test getting all server_id");
  Set<int> ids = await SqfliteObservationManager.instance.getAllServerIDs();
  debugPrint(ids.toString());

  ObservationData observation = ObservationData.namedArgumentConstructor(
      status: Status.draft,
      server_id: -1,
      patrol_id: -1,
      observer: "Ryan Puzon",
      timestamp: (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      latitude: 37.78,
      longitude: -122.40,
      site: 'Test Site',
      site_id: 6,
      project: 'Test Project',
      project_id: 9,
      species: Species.leatherbackTurtle,
      user_id: 0,
      comment: "Test Comment",
      group_id: 0,
      sub_group_id: 0,
      nest_id: "Nest123",
      type: ObservationType.femaleEncounter);
  debugPrint("Create an observation");
  debugPrint(observation.toString());
  int observationId =
      await SqfliteObservationManager.instance.create(observation);
  debugPrint("Observation created - $observationId");
  Map<String, Object?> newObservation =
      await SqfliteObservationManager.instance.get(observationId);
  debugPrint(
      "Get observation ${newObservation['_id']} Species: ${newObservation["species"]} Timestamp: ${newObservation["timestamp"]}");
  debugPrint("Get pending observations");

  if (observation.comment != newObservation["comment"]) {
    throw Exception("Observation creation failed - comment mismatched");
  }

  List<ObservationData> observations =
      await SqfliteObservationManager.instance.getUploadedObservations();
  debugPrint(observations.toString());

  // soft delete
  debugPrint("Delete observation $observationId");
  await SqfliteObservationManager.instance.delete(observationId);

  newObservation = await SqfliteObservationManager.instance.get(observationId);
  if (Status.deleted.index != newObservation["status"]) {
    throw Exception(
        "Observation delete failed - status mismatched ${newObservation["status"]}");
  }

  var deletedObservations =
      await SqfliteObservationManager.instance.getDeletedObservations();
  debugPrint(deletedObservations.toString());

  // Update
  debugPrint("Testing Update");
  Map<String, Object?> observationToBeUpdated =
      await SqfliteObservationManager.instance.get(observationId);
  debugPrint("before update: $observationToBeUpdated");
  Map<String, Object?> newObservationInstance = {
    ...observationToBeUpdated,
    'observer': "New Observer Name"
  };
  SqfliteObservationManager.instance
      .update(ObservationData.fromMap(newObservationInstance));
  debugPrint(
      ObservationData.fromMap(await SqfliteObservationManager.instance.get(2))
          .toString());

  await SqfliteObservationManager.instance.hardDeletes();

  if (observation.observer != newObservation["observer"]) {
    throw Exception("Observation update failed - observer mismatched");
  }
  var count =
      await SqfliteObservationManager.instance.hardDelete(observationId);
  if (count != 1) {
    throw Exception("Observation count must be 1, it is $count");
  }
  try {
    newObservation =
        await SqfliteObservationManager.instance.get(observationId);
  } catch (e) {
    debugPrint("Expected exception from test ${e.toString()}");
  }
}
