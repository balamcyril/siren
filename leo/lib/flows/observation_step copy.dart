import 'package:siren_flutter/flows/observation_question.dart';

enum observationStepType {
  femaleInfo_1,
  newNestInfo,
  nestExhumationInfo_1,
  nestExhumationInfo_2,
  beachWithNestID,
  beach,
  crawlInfo,
  images,
  notes
}

class ObservationStep {
  ObservationStep(this.type, this.questions);
  observationStepType type;
  List<ObservationQuestion> questions;
}
