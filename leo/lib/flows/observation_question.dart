import 'package:siren_flutter/flows/question_select_options.dart';

Map questionNameToIDMapper = {
  "female_tag_id_left": 298,
  "female_tag_id_right": 299,
  "high_tide_line_distance_from_the_nest": 300,
  "nest_status": 301,
  "estimated_hatching_date": 302,
  "estimated_exhumation_date": 303,
  "actual_hatching_date": 304,
  "actual_exhumation_date": 305,
  "live_hatchlings": 306,
  "dead_hatchlings": 307,
  "unhatched_eggs": 308,
  "whole_eggshells": 309,
  "egg_size": 310,
  "live_hatching_size": 311,
  "notes": 312,
  "crawl_width": 313,
  "carapace_width": 314,
  "carapace_length": 315,
  "crawl_type": 316,
  "turtle_species": 317,
  "small_eggshells": 318,
  "pipped_eggs": 319,
  "site": 320,
  "nest_id": 321,
  "top_to_first_egg_depth": 322,
  "predated_eggs": 323,
  "top_to_bottom_depth": 324,
  "unhatched_term_or_pipped": 325
};
enum questionType {
  text,
  textMultipleLines,
  textWithAddButton,
  number,
  select,
  date,
  image
}
/*
For type select, options contains the list of options to display to the user
Otherwise options will be empty
*/

class ObservationQuestion {
  ObservationQuestion(
      this.name, this.type, this.question, this.options, this.isRequired,
      {this.isReadOnly = false, this.answerSuffix = ""});

  String question;
  String name;
  questionType type;
  List<QuestionSelectOption> options;
  bool isRequired;
  bool? isReadOnly;
  String? answerSuffix;

  String toString() {
    return "Name: $name, question: $question, type: $type, options: $options";
  }
}
