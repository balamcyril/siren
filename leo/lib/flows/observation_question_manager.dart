import 'package:flutter/material.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/user_session_manager.dart';

class ObservationQuestionManager {
  late BuildContext context;
  ObservationQuestionManager(this.context);

  ObservationQuestion getByName(String questionName) {
    switch (questionName) {
      case "site":
        List<SiteData> sites =
            userSessionManager.userData.currentProjectData.siteDataList;
        List<QuestionSelectOption> options = [];
        for (var site in sites) {
          options.add(QuestionSelectOption(site.id, site.name));
        }
        return ObservationQuestion(questionName, questionType.select,
            AppLocalizations.of(context)!.beachName, options, true);
      case "nest_id":
        return ObservationQuestion(questionName, questionType.text,
            AppLocalizations.of(context)!.nestID, [], true);
      case "crawl_type":
        return ObservationQuestion(
            questionName,
            questionType.select,
            AppLocalizations.of(context)!.crawlType,
            [
              QuestionSelectOption(
                  trueCrawlID,
                  questionSelectionStringFromAnswerID(
                      context, questionName, trueCrawlID)),
              QuestionSelectOption(
                  falseCrawlID,
                  questionSelectionStringFromAnswerID(
                      context, questionName, falseCrawlID)),
            ],
            false);
      case "turtle_species":
        return ObservationQuestion(
            questionName,
            questionType.select,
            AppLocalizations.of(context)!.turtleSpecies,
            [
              QuestionSelectOption(
                  greenTurtle,
                  questionSelectionStringFromAnswerID(
                      context, questionName, greenTurtle)),
              QuestionSelectOption(
                  hawksbillTurtle,
                  questionSelectionStringFromAnswerID(
                      context, questionName, hawksbillTurtle)),
              QuestionSelectOption(
                  loggerheadSeaTurtle,
                  questionSelectionStringFromAnswerID(
                      context, questionName, loggerheadSeaTurtle)),
              QuestionSelectOption(
                  oliveRidleyTurtle,
                  questionSelectionStringFromAnswerID(
                      context, questionName, oliveRidleyTurtle)),
              QuestionSelectOption(
                  leatherbackTurtle,
                  questionSelectionStringFromAnswerID(
                      context, questionName, leatherbackTurtle))
            ],
            true);
      case "carapace_length":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.carapaceLength, [], true,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case "carapace_width":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.carapaceWidth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case "crawl_width":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.crawlWidth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case "notes":
        return ObservationQuestion(questionName, questionType.textMultipleLines,
            AppLocalizations.of(context)!.notes, [], false);
      case "female_tag_id_left":
        return ObservationQuestion(questionName, questionType.text,
            AppLocalizations.of(context)!.femaleTagIDLeft, [], false);
      case "female_tag_id_right":
        return ObservationQuestion(questionName, questionType.text,
            AppLocalizations.of(context)!.femaleTagIDRight, [], false);
      case "high_tide_line_distance_from_the_nest":
        return ObservationQuestion(
            questionName,
            questionType.number,
            AppLocalizations.of(context)!.highTideLineDistanceFromTheNest,
            [],
            false,
            answerSuffix: "(" + AppLocalizations.of(context)!.meter + ")");
      case "nest_status":
        return ObservationQuestion(
            questionName,
            questionType.select,
            AppLocalizations.of(context)!.nestStatus,
            [
              QuestionSelectOption(
                  nestUndisturbed,
                  questionSelectionStringFromAnswerID(
                      context, questionName, nestUndisturbed)),
              QuestionSelectOption(
                  nestPartiallyLost,
                  questionSelectionStringFromAnswerID(
                      context, questionName, nestPartiallyLost)),
              QuestionSelectOption(
                  nestTotallyLost,
                  questionSelectionStringFromAnswerID(
                      context, questionName, nestTotallyLost)),
            ],
            false);
      case "estimated_hatching_date":
        return ObservationQuestion(questionName, questionType.date,
            AppLocalizations.of(context)!.estimatedHatchingDate, [], false,
            isReadOnly: true);
      case "estimated_exhumation_date":
        return ObservationQuestion(questionName, questionType.date,
            AppLocalizations.of(context)!.estimatedEscavationDate, [], false,
            isReadOnly: true);
      case "actual_hatching_date":
        return ObservationQuestion(questionName, questionType.date,
            AppLocalizations.of(context)!.actualdHatchingDate, [], false);
      case "actual_exhumation_date":
        return ObservationQuestion(questionName, questionType.date,
            AppLocalizations.of(context)!.actualEscavationDate, [], false);
      case "live_hatchlings":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.liveHatchlings, [], true);
      case "dead_hatchlings":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.deadHatchlings, [], true);
      case "unhatched_eggs":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.unhatchedEggs, [], true);
      case "unhatched_term_or_pipped":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.unhatchedTermOrPipped, [], true);
      case "whole_eggshells":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.wholeEggshells, [], true);
      case "small_eggshells":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.smallEggshells, [], true);
      case "pipped_eggs":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.pippedEggs, [], true);
      case "top_to_bottom_depth":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.topToBottomDepth, [], true,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case "top_to_first_egg_depth":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.topToFirstEggDepth, [], true,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case "predated_eggs":
        return ObservationQuestion(questionName, questionType.number,
            AppLocalizations.of(context)!.predatedEggs, [], true);
      case "egg_size":
        return ObservationQuestion(questionName, questionType.textWithAddButton,
            AppLocalizations.of(context)!.eggSize, [], false);
      case "live_hatching_size":
        return ObservationQuestion(questionName, questionType.textWithAddButton,
            AppLocalizations.of(context)!.liveHatchingSize, [], false);
      case "images":
        return ObservationQuestion(questionName, questionType.image,
            AppLocalizations.of(context)!.addImages, [], false);
      default:
    }
    throw Exception("ObservationQuestion unknown question $questionName");
  }

  ObservationQuestion getById(int questionId) {
    switch (questionId) {
      case 298:
        return ObservationQuestion("female_tag_id_left", questionType.text,
            AppLocalizations.of(context)!.femaleTagIDLeft, [], false);
      case 299:
        return ObservationQuestion("female_tag_id_right", questionType.text,
            AppLocalizations.of(context)!.femaleTagIDRight, [], false);
      case 300:
        return ObservationQuestion(
            "high_tide_line_distance_from_the_nest",
            questionType.number,
            AppLocalizations.of(context)!.highTideLineDistanceFromTheNest,
            [],
            false,
            answerSuffix: "(" + AppLocalizations.of(context)!.meter + ")");
      case 301:
        return ObservationQuestion(
            "nest_status",
            questionType.select,
            AppLocalizations.of(context)!.nestStatus,
            [
              QuestionSelectOption(
                  nestUndisturbed,
                  questionSelectionStringFromAnswerID(
                      context, "nest_status", nestUndisturbed)),
              QuestionSelectOption(
                  nestPartiallyLost,
                  questionSelectionStringFromAnswerID(
                      context, "nest_status", nestPartiallyLost)),
              QuestionSelectOption(
                  nestTotallyLost,
                  questionSelectionStringFromAnswerID(
                      context, "nest_status", nestTotallyLost)),
            ],
            false);
      case 302:
        return ObservationQuestion("estimated_hatching_date", questionType.date,
            AppLocalizations.of(context)!.estimatedHatchingDate, [], false);
      case 303:
        return ObservationQuestion(
            "estimated_exhumation_date",
            questionType.date,
            AppLocalizations.of(context)!.estimatedEscavationDate,
            [],
            false);
      case 304:
        return ObservationQuestion("actual_hatching_date", questionType.date,
            AppLocalizations.of(context)!.actualdHatchingDate, [], false);
      case 305:
        return ObservationQuestion("actual_exhumation_date", questionType.date,
            AppLocalizations.of(context)!.actualEscavationDate, [], false);
      case 306:
        return ObservationQuestion("live_hatchlings", questionType.number,
            AppLocalizations.of(context)!.liveHatchlings, [], false);
      case 307:
        return ObservationQuestion("dead_hatchlings", questionType.number,
            AppLocalizations.of(context)!.deadHatchlings, [], false);
      case 308:
        return ObservationQuestion("unhatched_eggs", questionType.number,
            AppLocalizations.of(context)!.unhatchedEggs, [], false);
      case 309:
        return ObservationQuestion("whole_eggshells", questionType.number,
            AppLocalizations.of(context)!.wholeEggshells, [], false);
      case 310:
        return ObservationQuestion("egg_size", questionType.textWithAddButton,
            AppLocalizations.of(context)!.eggSize, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 311:
        return ObservationQuestion(
            "live_hatching_size",
            questionType.textWithAddButton,
            AppLocalizations.of(context)!.liveHatchingSize,
            [],
            false);
      case 312:
        return ObservationQuestion("notes", questionType.textMultipleLines,
            AppLocalizations.of(context)!.notes, [], false);
      case 313:
        return ObservationQuestion("crawl_width", questionType.number,
            AppLocalizations.of(context)!.crawlWidth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 314:
        return ObservationQuestion("carapace_width", questionType.text,
            AppLocalizations.of(context)!.carapaceWidth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 315:
        return ObservationQuestion("carapace_length", questionType.text,
            AppLocalizations.of(context)!.carapaceLength, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 316:
        return ObservationQuestion(
            "crawl_type",
            questionType.select,
            AppLocalizations.of(context)!.crawlType,
            [
              QuestionSelectOption(
                  trueCrawlID,
                  questionSelectionStringFromAnswerID(
                      context, "crawl_type", trueCrawlID)),
              QuestionSelectOption(
                  falseCrawlID,
                  questionSelectionStringFromAnswerID(
                      context, "crawl_type", falseCrawlID)),
            ],
            false);
      case 317:
        return ObservationQuestion(
            "turtle_species",
            questionType.select,
            AppLocalizations.of(context)!.turtleSpecies,
            [
              QuestionSelectOption(
                  greenTurtle,
                  questionSelectionStringFromAnswerID(
                      context, "turtle_species", greenTurtle)),
              QuestionSelectOption(
                  hawksbillTurtle,
                  questionSelectionStringFromAnswerID(
                      context, "turtle_species", hawksbillTurtle)),
              QuestionSelectOption(
                  loggerheadSeaTurtle,
                  questionSelectionStringFromAnswerID(
                      context, "turtle_species", loggerheadSeaTurtle)),
              QuestionSelectOption(
                  oliveRidleyTurtle,
                  questionSelectionStringFromAnswerID(
                      context, "turtle_species", oliveRidleyTurtle)),
              QuestionSelectOption(
                  leatherbackTurtle,
                  questionSelectionStringFromAnswerID(
                      context, "turtle_species", leatherbackTurtle))
            ],
            true);
      case 318:
        return ObservationQuestion("small_eggshells", questionType.number,
            AppLocalizations.of(context)!.smallEggshells, [], false);
      case 319:
        return ObservationQuestion("pipped_eggs", questionType.number,
            AppLocalizations.of(context)!.pippedEggs, [], false);
      case 320:
        return ObservationQuestion("site", questionType.select,
            AppLocalizations.of(context)!.beachName, [], true);
      case 321:
        return ObservationQuestion("nest_id", questionType.text,
            AppLocalizations.of(context)!.nestID, [], false);
      case 322:
        return ObservationQuestion("top_to_first_egg_depth", questionType.text,
            AppLocalizations.of(context)!.topToFirstEggDepth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 323:
        return ObservationQuestion("predated_eggs", questionType.text,
            AppLocalizations.of(context)!.predatedEggs, [], false);
      case 324:
        return ObservationQuestion("top_to_bottom_depth", questionType.text,
            AppLocalizations.of(context)!.topToBottomDepth, [], false,
            answerSuffix: "(" + AppLocalizations.of(context)!.cm + ")");
      case 325:
        return ObservationQuestion(
            "unhatched_term_or_pipped",
            questionType.text,
            AppLocalizations.of(context)!.unhatchedTermOrPipped,
            [],
            false);
      default:
    }
    throw Exception("ObservationQuestion unknown question $questionId");
  }
}

// Siren server expects some fields in addition to answers
// Make sure we copy answers related to a field into the object members
// For select question, put the string back to be stored in the DB
void fillExpectedFieldFromAnswers(
    BuildContext context, ObservationData observationData) {
  for (var answer in observationData.answerList) {
    if (answer.questionId == questionNameToIDMapper['site']) {
      String siteString = "";
      int SiteId = -1;
      List<SiteData> sites =
          userSessionManager.userData.currentProjectData.siteDataList;
      for (var site in sites) {
        if (site.id.toString() == answer.answer) {
          siteString = site.name;
          SiteId = site.id;
        }
      }
      if (observationData.site.isEmpty) {
        observationData.site = siteString;
        observationData.site_id = SiteId;
      }
    }
    if (answer.questionId == questionNameToIDMapper['turtle_species']) {
      int turtleSpeciesId = int.parse(answer.answer);
      if (turtleSpeciesId == greenTurtle) {
        observationData.species = Species.greenTurtle;
      } else if (turtleSpeciesId == leatherbackTurtle) {
        observationData.species = Species.leatherbackTurtle;
      } else if (turtleSpeciesId == oliveRidleyTurtle) {
        observationData.species = Species.oliveRidleyTurtle;
      } else if (turtleSpeciesId == hawksbillTurtle) {
        observationData.species = Species.hawksbillTurtle;
      } else if (turtleSpeciesId == loggerheadSeaTurtle) {
        observationData.species = Species.loggerheadSeaTurtle;
      }
    }
    if (answer.questionId == questionNameToIDMapper['notes']) {
      observationData.comment = answer.answer;
    }
    if (answer.questionId == questionNameToIDMapper['nest_id']) {
      observationData.nest_id = answer.answer;
    }
  }
}
