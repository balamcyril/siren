import 'package:flutter/material.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/observation_step.dart';

class ObservationStepManager {
  late BuildContext context;
  ObservationStepManager(this.context);
  ObservationStep get(observationStepType type) {
    List<ObservationQuestion> questions = [];
    switch (type) {
      case observationStepType.crawlInfo:
        questions.add(
            ObservationQuestionManager(context).getByName("turtle_species"));
        questions
            .add(ObservationQuestionManager(context).getByName("crawl_type"));
        questions
            .add(ObservationQuestionManager(context).getByName("crawl_width"));
        break;
      case observationStepType.femaleInfo_1:
        questions.add(
            ObservationQuestionManager(context).getByName("turtle_species"));
        questions.add(ObservationQuestionManager(context)
            .getByName("female_tag_id_left"));
        questions.add(ObservationQuestionManager(context)
            .getByName("female_tag_id_right"));
        questions.add(
            ObservationQuestionManager(context).getByName("carapace_length"));
        questions.add(
            ObservationQuestionManager(context).getByName("carapace_width"));
        questions
            .add(ObservationQuestionManager(context).getByName("crawl_width"));
        break;
      case observationStepType.beachWithNestID:
        questions.add(ObservationQuestionManager(context).getByName("site"));
        questions.add(ObservationQuestionManager(context).getByName("nest_id"));
        break;
      case observationStepType.beach:
        questions.add(ObservationQuestionManager(context).getByName("site"));
        break;
      case observationStepType.newNestInfo:
        questions.add(
            ObservationQuestionManager(context).getByName("turtle_species"));
        questions.add(ObservationQuestionManager(context)
            .getByName("female_tag_id_left"));
        questions.add(ObservationQuestionManager(context)
            .getByName("female_tag_id_right"));
        questions
            .add(ObservationQuestionManager(context).getByName("crawl_width"));
        questions.add(ObservationQuestionManager(context)
            .getByName("high_tide_line_distance_from_the_nest"));
        questions
            .add(ObservationQuestionManager(context).getByName("nest_status"));
        questions.add(ObservationQuestionManager(context)
            .getByName("estimated_hatching_date"));
        questions.add(ObservationQuestionManager(context)
            .getByName("estimated_exhumation_date"));
        break;
      case observationStepType.nestExhumationInfo_1:
        questions.add(
            ObservationQuestionManager(context).getByName("turtle_species"));
        questions.add(ObservationQuestionManager(context)
            .getByName("actual_hatching_date"));
        questions.add(ObservationQuestionManager(context)
            .getByName("actual_exhumation_date"));
        questions.add(ObservationQuestionManager(context)
            .getByName("top_to_first_egg_depth"));
        questions.add(
            ObservationQuestionManager(context).getByName("live_hatchlings"));
        questions.add(
            ObservationQuestionManager(context).getByName("dead_hatchlings"));
        questions.add(
            ObservationQuestionManager(context).getByName("whole_eggshells"));
        break;
      case observationStepType.nestExhumationInfo_2:
        questions.add(
            ObservationQuestionManager(context).getByName("unhatched_eggs"));
        questions.add(ObservationQuestionManager(context)
            .getByName("unhatched_term_or_pipped"));
        questions.add(
            ObservationQuestionManager(context).getByName("predated_eggs"));
        questions
            .add(ObservationQuestionManager(context).getByName("egg_size"));
        questions.add(ObservationQuestionManager(context)
            .getByName("live_hatching_size"));
        questions.add(ObservationQuestionManager(context)
            .getByName("top_to_bottom_depth"));
        break;
      case observationStepType.images:
        questions.add(ObservationQuestionManager(context).getByName("images"));
        break;

      case observationStepType.notes:
        questions.add(ObservationQuestionManager(context).getByName("notes"));
        break;

      default:
        throw Exception("ObservationStepManager - unknowm step $type");
    }
    return ObservationStep(type, questions);
  }
}
