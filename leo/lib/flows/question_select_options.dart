import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/user_session_manager.dart';

const int trueCrawlID = 0;
const int falseCrawlID = 1;

const int nestUndisturbed = 0;
const int nestPartiallyLost = 1;
const int nestTotallyLost = 2;

class QuestionSelectOption {
  late int id;
  late String name;
  QuestionSelectOption(this.id, this.name);
}

String questionSelectionStringFromAnswerID(
    BuildContext context, String questionName, int id) {
  switch (questionName) {
    case "crawl_type":
      if (id == trueCrawlID) {
        return AppLocalizations.of(context)!.trueCrawl;
      }
      if (id == falseCrawlID) {
        return AppLocalizations.of(context)!.falseCrawl;
      }
      break;
    case "nest_status":
      if (id == nestUndisturbed) {
        return AppLocalizations.of(context)!.nestUndisturbed;
      }
      if (id == nestPartiallyLost) {
        return AppLocalizations.of(context)!.nestPartiallyLost;
      }
      if (id == nestTotallyLost) {
        return AppLocalizations.of(context)!.nestTotallyLost;
      }
      break;
    case "turtle_species":
      if (id == hawksbillTurtle) {
        return AppLocalizations.of(context)!.hawksbill;
      }
      if (id == greenTurtle) {
        return AppLocalizations.of(context)!.greenTurtle;
      }
      if (id == oliveRidleyTurtle) {
        return AppLocalizations.of(context)!.oliveRidley;
      }
      if (id == loggerheadSeaTurtle) {
        return AppLocalizations.of(context)!.loggerhead;
      }
      if (id == leatherbackTurtle) {
        return AppLocalizations.of(context)!.leatherback;
      }
      break;
    case "site":
      List<SiteData> sites =
          userSessionManager.userData.currentProjectData.siteDataList;
      for (var site in sites) {
        if (site.id == id) {
          return site.name;
        }
      }
      return "";
  }
  throw Exception(
      "questionSelectionStringFromAnswerID no answer found for question $questionName and answer id $id");
}

String getStringFromSpeciesEnum(BuildContext context, Species species) {
  if (species == Species.greenTurtle) {
    return AppLocalizations.of(context)!.greenTurtle;
  }
  if (species == Species.oliveRidleyTurtle) {
    return AppLocalizations.of(context)!.oliveRidley;
  }
  if (species == Species.hawksbillTurtle) {
    return AppLocalizations.of(context)!.hawksbill;
  }
  if (species == Species.leatherbackTurtle) {
    return AppLocalizations.of(context)!.leatherback;
  }
  if (species == Species.loggerheadSeaTurtle) {
    return AppLocalizations.of(context)!.loggerhead;
  }
  throw Exception(
      "getStringFromSpeciesEnum no species found for enum $species");
}
