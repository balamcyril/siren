/* 
A flow is a list of steps
A step is a list of question
*/
import 'package:flutter/material.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/observation_step_manager.dart';

class QuestionFlowManager {
  late BuildContext context;
  QuestionFlowManager(this.context);

  List<ObservationStep> getSteps(int type) {
    List<ObservationStep> steps = [];
    if (type == ObservationType.crawl) {
      steps.add(ObservationStepManager(context).get(observationStepType.beach));

      steps.add(
          ObservationStepManager(context).get(observationStepType.crawlInfo));
      steps.add(ObservationStepManager(context).get(observationStepType.notes));
      steps
          .add(ObservationStepManager(context).get(observationStepType.images));
      return steps;
    }
    if (type == ObservationType.femaleEncounter) {
      steps.add(ObservationStepManager(context).get(observationStepType.beach));

      steps.add(ObservationStepManager(context)
          .get(observationStepType.femaleInfo_1));
      steps.add(ObservationStepManager(context).get(observationStepType.notes));
      steps
          .add(ObservationStepManager(context).get(observationStepType.images));
      return steps;
    }
    if (type == ObservationType.nesting) {
      steps.add(ObservationStepManager(context)
          .get(observationStepType.beachWithNestID));
      steps.add(
          ObservationStepManager(context).get(observationStepType.newNestInfo));
      steps.add(ObservationStepManager(context).get(observationStepType.notes));
      steps
          .add(ObservationStepManager(context).get(observationStepType.images));
      return steps;
    }
    if (type == ObservationType.nestExhumation) {
      steps.add(ObservationStepManager(context)
          .get(observationStepType.beachWithNestID));
      steps.add(ObservationStepManager(context)
          .get(observationStepType.nestExhumationInfo_1));
      steps.add(ObservationStepManager(context)
          .get(observationStepType.nestExhumationInfo_2));
      steps.add(ObservationStepManager(context).get(observationStepType.notes));
      steps
          .add(ObservationStepManager(context).get(observationStepType.images));
      return steps;
    }

    if (type == ObservationType.flash) {
      steps
          .add(ObservationStepManager(context).get(observationStepType.images));
      steps.add(ObservationStepManager(context).get(observationStepType.notes));
      return steps;
    }

    throw Exception("QuestionFlowManager -  Unknow observation type $type");
  }
}
