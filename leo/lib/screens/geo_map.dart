import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import "package:latlong2/latlong.dart" as latLng;
import 'package:flutter_map/plugin_api.dart';
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/cached_tile_provider.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/screens/details.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/utils/common_strings.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:siren_flutter/utils/leo_custom_icons.dart';

import '../utils/platform.dart';

class geoPoint {
  geoPoint(this.latitude, this.longitude);
  double latitude = 0;
  double longitude = 0;
  String toString() {
    return "lat. $latitude long $longitude";
  }
}

class GeoMap extends StatefulWidget {
  GeoMap({Key? key}) : super(key: key);
  late SiteData currentSite;
  bool currentSiteAvailable = false;

  void setSite(SiteData site) {
    currentSite = site;
    currentSiteAvailable = true;
  }

  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<GeoMap> {
  late geoPoint currentPosition;
  late double currentZoom;
  late MapController _mapController;
  bool observationLoaded = false;
  String imageDirectoryPath = "";

  _MapState() {
    currentZoom = 14.0;
    listCoordinates = createCoordinates();
    _mapController = MapController();

    initLocation();
  }

  @override
  void didUpdateWidget(GeoMap oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!widget.currentSiteAvailable) {
      return;
    }
    debugPrint("didUpdateWidget currentSite is now ${widget.currentSite.name}");

    initObservations();
  }

  void initObservations() async {
    imageDirectoryPath = await getMobileStoragePath();
    if (!widget.currentSiteAvailable) {
      return;
    }
    observations = widget.currentSite.id == 0
        ? await SqfliteObservationManager.instance.getAllObservations()
        : await SqfliteObservationManager.instance
            .getAllObservationsForSite(widget.currentSite.name);

    setState(() {
      if (userSessionManager.showObservationOnMap) {
        for (int i = 0; i < observations.length; i++) {
          if (observations[i].id == userSessionManager.observationToShow.id) {
            // put the observation on top
            observations.removeAt(i);
            observations.add(userSessionManager.observationToShow);
          }
        }
      }
      observationLoaded = true;
    });
  }

  List<ObservationData> observations = [];

  late List<geoPoint> listCoordinates;

  // for simulation only
  List<geoPoint> createCoordinates() {
    List<geoPoint> listCoords = [];
    double initial_x = 25.239;
    double initial_y = 54.230;
    listCoords.add(geoPoint(25.241, 54.230699));
    for (int i = 0; i < 100; i++) {
      listCoords.add(geoPoint(initial_x + 0.001 * i, initial_y));
    }
    return listCoords;
  }

  Marker getMarker(BuildContext context, ObservationData obs, geoPoint p,
      Color c, bool zoom) {
    double markerWidth = (zoom == false) ? 40.0 : 80.0;
    double markerHeight = (zoom == false) ? 40.0 : 80.0;
    double marketIconSize = (zoom == false) ? 40.0 : 80.0;

    return Marker(
      width: markerWidth,
      height: markerHeight,
      point: latLng.LatLng(p.latitude, p.longitude),
      builder: (ctx) => Container(
        child: IconButton(
          icon: const Icon(Icons.pin_drop_sharp),
          iconSize: marketIconSize,
          color: c,
          onPressed: () {
            if (userSessionManager.showObservationOnMap && !zoom) {
              // Exit zoom mode if a pin is pressed
              userSessionManager.showObservationOnMap = false;
              setState(() {});
            }
            showAlert(context, obs);
            debugPrint(obs.server_id.toString());
          },
        ),
      ),
    );
  }

  bool simulationMode = false;

  List<Marker> getAllMarkers(BuildContext context) {
    List<Marker> markers = [];

    if (simulationMode) {
      for (int i = 0; i < observations.length; i++) {
        print(geoPoint(observations[i].latitude, observations[i].longitude));
        markers.add(getMarker(context, observations[i], listCoordinates[i * 5],
            getColor(observations[i]), false));
      }
    } else {
      bool zoom = false;
      for (int i = 0; i < observations.length; i++) {
        if (i == observations.length - 1 &&
            userSessionManager.showObservationOnMap) {
          // zoom the last observation if coming from go to map
          zoom = true;
          debugPrint("getAllMarkers showObservationOnMap true");
        }
        markers.add(getMarker(
            context,
            observations[i],
            geoPoint(observations[i].latitude, observations[i].longitude),
            ColorsHandler.getObservationTypeColor(observations[i].type)
                .observationFillColor,
            zoom));
      }
    }
    return markers;
  }

  Color getColor(ObservationData obs) {
    Color color = Colors.black;
    switch (obs.status) {
      case Status.draft:
        color = Colors.blue;
        break;
      case Status.uploaded:
        color = Colors.green;
        break;
      case Status.deleted:
        color = Colors.red;
        break;
      default:
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    if (!isCurrentPositionAvailable()) {
      Future.microtask(() => initLocation());
    }
    if (!observationLoaded) {
      Future.microtask(() => initObservations());
    }
    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        center:
            latLng.LatLng(currentPosition.latitude, currentPosition.longitude),
        zoom: currentZoom,
      ),
      children: [
        TileLayer(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
            tileProvider: CachedTileProvider()),
        CurrentLocationLayer(),
        MarkerLayer(
          markers: getAllMarkers(context),
        ),
      ],
    );
  }

  double titleRowSize = 18.0;
  double textRowInfoSize = 14.0;

  Future<String> getDialogTitle(
      BuildContext context, ObservationData obs) async {
    return getObservationTitle(context, obs);
  }

  void showAlert(BuildContext context, ObservationData obs) async {
    String title = await getDialogTitle(context, obs);
    showDialog(
        context: context,
        builder: (context) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: GestureDetector(
                // When the child is tapped, show a snackbar.
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailsPage(
                              observationData: obs,
                              refresh: () {},
                            )),
                  );
                },
                child: Container(
                  margin: const EdgeInsets.all(
                    0.0,
                  ),
                  height: 150,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        getImageContainer(obs),
                        Expanded(child: getInfoContainer(title, obs)),
                      ]),
                ))));
  }

  Widget getInfoContainer(String title, ObservationData obs) {
    String observationType = obs.getObservationTypeString(context);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(obs.type);

    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 2, 0),
            child: Text(
              title,
              style: h3Semibold.merge(TextStyle(color: ColorsHandler.primary1)),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              decoration: BoxDecoration(
                color: observationTypeColor.observationFillColor,
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
              padding: const EdgeInsets.fromLTRB(7, 4, 7, 4),
              child: Text(
                observationType,
                style: caption.merge(TextStyle(
                    color: observationTypeColor.observationTextColor)),
              )),
          SizedBox(
            height: 14,
          ),
          Text(
            "${AppLocalizations.of(context)!.beachName} ${obs.site}",
            style:
                smallTextMedium.merge(TextStyle(color: ColorsHandler.primary1)),
          ),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                getDateString(obs),
                style: smallTextMedium
                    .merge(TextStyle(color: ColorsHandler.tertiary)),
              ),
            ],
          ),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Icon(
                Icons.schedule,
                size: textRowInfoSize,
                color: ColorsHandler.tertiary,
              ),
              Text(
                getTimeString(obs),
                style: smallTextMedium
                    .merge(TextStyle(color: ColorsHandler.tertiary)),
              ),
            ],
          ),
        ]);
  }

  String getDateString(ObservationData obs) {
    try {
      return DateFormat.yMMMd()
          .format(DateTime.fromMillisecondsSinceEpoch(obs.timestamp));
    } catch (e) {
      return "Unknown";
    }
  }

  String getTimeString(ObservationData obs) {
    try {
      return DateFormat(' kk:mm')
          .format(DateTime.fromMillisecondsSinceEpoch(obs.timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.user;
    }
  }

  String getLongitudeLatitude(ObservationData observationData) {
    return "Lat. ${observationData.latitude.toStringAsFixed(3)} • Long. ${observationData.longitude.toStringAsFixed(3)} ";
  }

  Widget getTextBox(String text) {
    return Text(text,
        style: TextStyle(
          fontSize: titleRowSize,
          fontFamily: 'Futura',
          color: const Color.fromRGBO(8, 37, 59, 1.0),
        ));
  }

  Widget getImageContainer(ObservationData obs) {
    late Widget imageContainer = Container();

    String fullPath = "";

    if (obs.mediaList.isNotEmpty) {
      fullPath = imageDirectoryPath + obs.mediaList[0].path;
      debugPrint("GeoMap - image path $fullPath");
    }
    try {
      imageContainer = Container(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
          child: (obs.hasMediaToShow())
              ? Image.file(
                  File(fullPath),
                  height: 120.0,
                  width: 100,
                  fit: BoxFit.fitHeight,
                  errorBuilder: (BuildContext context, Object exception,
                      StackTrace? stackTrace) {
                    debugPrint("$exception");
                    debugPrint("$stackTrace");
                    return getDefaultImage(100, 100);
                  },
                )
              : getDefaultImage(100, 100));
    } catch (e) {
      debugPrint("GeoMap  getImageContainer $e");
      imageContainer = getDefaultImage(100, 100);
    }
    return imageContainer;
  }

  bool isCurrentPositionAvailable() {
    return !(currentPosition.latitude == 0 && currentPosition.longitude == 0);
  }

  void initLocation() async {
    bool currentPositionFound = false;
    currentPosition = geoPoint(0, 0); //island
    try {
      if (userSessionManager.userDataAvailable) {
        geoPoint userPositon =
            await userSessionManager.dbUserManager.getPosition();
        userSessionManager.userData.currentLocation = userPositon;
        currentPosition = userPositon;
        currentPositionFound = true;
        debugPrint("geoMap initLocation userDataAvailable - True");
      } else {
        debugPrint("geoMap initLocation userDataAvailable  - False");
        return;
      }
    } catch (e) {
      debugPrint("geoMap initLocation getPosition $e");
    }

    if (!currentPositionFound) {
      bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        Fluttertoast.showToast(msg: 'Please enable Your Location Service');
      }
      var permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          Fluttertoast.showToast(msg: 'Location permissions are denied');
        }
      }
      if (permission == LocationPermission.deniedForever) {
        Fluttertoast.showToast(
            msg:
                'Location permissions are permanently denied, we cannot request permissions.');
      }
      Position position;
      try {
        position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best);
      } catch (e) {
        debugPrint("geo_map init location - $e");
        throw Exception("Geolocator.getCurrentPosition - $e");
      }
      currentPosition = geoPoint(position.latitude, position.longitude);
      userSessionManager.dbUserManager.setPosition(currentPosition);
    }
    setState(() {
      if (userSessionManager.showObservationOnMap) {
        // user select go to map from observation list or detail
        geoPoint goToPosition = geoPoint(
            userSessionManager.observationToShow.latitude,
            userSessionManager.observationToShow.longitude);
        debugPrint(
            "_MapState initLocation use goToPosition ${goToPosition.toString()}");
        currentPosition = goToPosition;
      }
      _mapController.move(
          latLng.LatLng(currentPosition.latitude, currentPosition.longitude),
          currentZoom);
    });
  }
}
