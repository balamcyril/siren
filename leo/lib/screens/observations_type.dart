import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';

import '../data/observation_type_data.dart';

class ObservationsTypePage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  ObservationsTypePage(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }

  @override
  _ObservationsTypePageState createState() =>
      _ObservationsTypePageState(mainScreenStatefulWidget);
}

class _ObservationsTypePageState extends State<ObservationsTypePage> {
  _ObservationsTypePageState(parent) {
    mainScreenStatefulWidget = parent;
  }
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  @override
  void initState() {
    super.initState();
  }

  List<Widget> getobservationsTypeData() {
    List<Widget> observationType = [];

    debugPrint(
        'current projet:  ${userSessionManager.userData.currentProjectData.id}');
    debugPrint(
        'type lenght :  ${userSessionManager.userData.currentProjectData.observationsTypeList.length}');

    userSessionManager.userData.currentProjectData.observationsTypeList
        .forEach((item) {
      observationType.add(
          getField(context, "${getNameObservation(item)}( id: ${item.id})"));
      observationType.add(SizedBox(height: 10));
    });
    observationType.add(getRefreshButton(context));
    return observationType;
  }

  String getNameObservation(ObservationsTypeData typeObservation) {
    switch ('${AppLocalizations.of(context)!.localeName}') {
      case 'fr':
        return typeObservation.nom_fr;
      // case 'ar':
      //   return "";
      // case 'de':
      //   return "";
      case 'pt':
        return typeObservation.nom_pt;
      default:
        return typeObservation.nom_en;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: ColorsHandler.primary1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          }),
          title: Text(
            "${AppLocalizations.of(context)!.observations}",
            style: const TextStyle(color: ColorsHandler.primary1),
          ),
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 50, bottom: 0, left: 15, right: 15),
              padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: getobservationsTypeData()),
            ),
          ],
        )));
  }

  Widget getField(BuildContext context, observationsType) {
    return TextField(
      readOnly: true,
      controller: TextEditingController(text: observationsType),
      decoration: InputDecoration(
        labelStyle: TextStyle(fontSize: 14),
        labelText: AppLocalizations.of(context)!.observationName,
      ),
      autofocus: false,
    );
  }

  Widget getRefreshButton(BuildContext context) {
    return IconButton(
        onPressed: () {
          userSessionManager
              .refreshObservationsType()
              .then((observationsTypes) {
            userSessionManager.userData.currentProjectData
                .observationsTypeList = observationsTypes;
            debugPrint('refreshing list of observation type');
            debugPrint(observationsTypes.toString());

            setState(() {
              MainScreenState state = mainScreenStatefulWidget.getState();
              state.loginState();
            });
          });
        },
        icon: const Icon(Icons.refresh));
  }
}
