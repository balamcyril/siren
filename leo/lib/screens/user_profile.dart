import 'package:flutter/material.dart';
import 'package:siren_flutter/data/project_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/beaches.dart';
import 'package:siren_flutter/screens/project_list.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';

import 'account_info.dart';
import 'observations_type.dart';

class UserProfilePage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  UserProfilePage(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _UserProfilePageState createState() =>
      _UserProfilePageState(mainScreenStatefulWidget);
}

class _UserProfilePageState extends State<UserProfilePage> {
  _UserProfilePageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  late MainScreenStatefulWidget mainScreenStatefulWidget;
  String getUserData() {
    if (userSessionManager.userDataAvailable) {
      return "${AppLocalizations.of(context)!.user}: " +
          userSessionManager.userData.username +
          "\n${AppLocalizations.of(context)!.project}: " +
          userSessionManager.userData.currentProjectData.name;
    }
    return "No user data available, login required";
  }

  @override
  void initState() {
    super.initState();
  }

  String getSiteData() {
    String sites = AppLocalizations.of(context)!.site_beaches + "\n\n";

    userSessionManager.userData.currentProjectData.siteDataList.forEach((item) {
      sites += item.toString() + "\n";
    });
    return sites.substring(0, sites.length - 1);
  }

  String getLocation() {
    String projectLocation =
        AppLocalizations.of(context)!.project_location + "\n\n";
    return projectLocation + " ${userSessionManager.userData.currentLocation}";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(),
              ListView(
                shrinkWrap: true,
                children: [
                  ListTile(
                    title: Text(AppLocalizations.of(context)!.accountInfo),
                    textColor: ColorsHandler.primary1,
                    leading: Icon(Icons.account_circle_outlined),
                    iconColor: ColorsHandler.primary1,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AccountInfoPage()),
                      );
                    },
                  ),
                  new Divider(
                    height: 2.0,
                    indent: 1.0,
                    color: ColorsHandler.tertiary,
                  ),
                  ListTile(
                    title: Text(AppLocalizations.of(context)!.observations),
                    textColor: ColorsHandler.primary1,
                    leading: Icon(Icons.list_rounded),
                    iconColor: ColorsHandler.primary1,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ObservationsTypePage(mainScreenStatefulWidget)),
                      );
                    },
                  ),
                  new Divider(
                    height: 2.0,
                    indent: 1.0,
                    color: ColorsHandler.tertiary,
                  ),
                  // ListTile(
                  //   title: Text("Project"),
                  //   textColor: ColorsHandler.primary1,
                  //   leading: Icon(Icons.folder_outlined),
                  //   iconColor: ColorsHandler.primary1,
                  //   onTap: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) =>
                  //               ProjectListPage(mainScreenStatefulWidget)),
                  //     );
                  //   },
                  // ),
                  // new Divider(
                  //   height: 2.0,
                  //   indent: 1.0,
                  //   color: ColorsHandler.tertiary,
                  // ),
                  ListTile(
                    title: Text(AppLocalizations.of(context)!.site_beaches),
                    textColor: ColorsHandler.primary1,
                    leading: Icon(Icons.beach_access_outlined),
                    iconColor: ColorsHandler.primary1,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BeachesPage()),
                      );
                    },
                  ),
                  new Divider(
                    height: 2.0,
                    indent: 1.0,
                    color: ColorsHandler.tertiary,
                  ),
                  ListTile(
                    title: Text(AppLocalizations.of(context)!.aboutSiren),
                    textColor: ColorsHandler.primary1,
                    leading: Icon(Icons.info_outline),
                    iconColor: ColorsHandler.primary1,
                    onTap: showAboutDialog,
                  ),
                  new Divider(
                    height: 2.0,
                    indent: 1.0,
                    color: ColorsHandler.tertiary,
                  ),
                  ListTile(
                    title: Text(
                      AppLocalizations.of(context)!.logout,
                      style: TextStyle(fontSize: 16, color: Colors.blue),
                    ),
                    textColor: Colors.blue.shade900,
                    onTap: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: Text(AppLocalizations.of(context)!.logout),
                        content: Text(
                            AppLocalizations.of(context)!.logoutdescription),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context,
                                  AppLocalizations.of(context)!.confirm);
                              userSessionManager.logout();
                              setState(() {
                                MainScreenState state =
                                    mainScreenStatefulWidget.getState();
                                state.logoutState();
                              });
                            },
                            child: Text(AppLocalizations.of(context)!.confirm),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  Future<Widget> showAboutDialog() {
    return aboutDialog(context);
  }

  Widget getLine() {
    return const Divider(
      height: 20,
      thickness: 1,
      indent: 20,
      endIndent: 0,
      color: Colors.black,
    );
  }

  getProjectList() async {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Refreshing..."),
    ));
    try {
      List<ProjectData> projects = await SirenRestServices().refreshData();
      userSessionManager.userData.projects = projects;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Refresh ok."),
      ));
    } catch (e) {
      debugPrint("Error refresh${e} ");
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Error refresh"),
      ));
    }
  }
}
