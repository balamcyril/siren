import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/beaches.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:siren_flutter/variable.dart';
import 'account_info.dart';
import 'observations_type.dart';
import 'package:custom_navigation_bar/custom_navigation_bar.dart';

import '../data/observation_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import '../utils/platform.dart';
import 'dart:io';
import 'dart:ui';
import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:percent_indicator/percent_indicator.dart';

import 'package:auto_reload/auto_reload.dart';
import 'package:flutter/widgets.dart';

class ShowObservationUploadingPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  ShowObservationUploadingPage(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }
  @override
  ShowObservationUploadingPageState createState() =>
      ShowObservationUploadingPageState(mainScreenStatefulWidget);
}

abstract class _AutoReloadState extends State<ShowObservationUploadingPage>
    implements AutoReloader {}

class ShowObservationUploadingPageState extends _AutoReloadState
    with AutoReloadMixin {
  List<ObservationData> observations = [];
  List<ObservationData> obsSelected = [];
  bool checkAll = true;
  List<bool> checkedObs = [];
  String imageDirectoryPath = "";

  @override
  final Duration autoReloadDuration = const Duration(seconds: 2);

  ShowObservationUploadingPageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initObservations();

    startAutoReload();
  }

  late MainScreenStatefulWidget mainScreenStatefulWidget;

  void initObservations() async {
    observations = userSessionManager.observationOnUploaded;
  }

  @override
  void autoReload() {
    setState(() {});
    if (!userSessionManager.onUpload) prevPage();
  }

  @override
  void dispose() {
    cancelAutoReload();
    super.dispose();
  }

  Widget getTitle() {
    return Container(
      alignment: Alignment.topLeft,
      padding: const EdgeInsets.fromLTRB(12, 5, 20, 5),
      child: Text(AppLocalizations.of(context)!.observationsUploadDetail,
          style: h2),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: ColorsHandler.tertiaryOp7,
          padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
          child: Column(children: [
            // getChipsRow(),
            Container(
              child: Row(
                children: <Widget>[
                  getTitle(),
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                icon: Icon(Icons.close,
                                    color: ColorsHandler.primary1),
                                onPressed: () => prevPage(),
                              ),
                            ],
                          ))),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  height: 2,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                  ),
                )),
            Expanded(
                child: ListView(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              children: [
                for (var i = 0; i < getObservationCount(); i++)
                  getObservationRow(context, i),
                SizedBox(
                  height: 100,
                )
              ],
            ))
          ])),
    );
  }

  Widget getObservationRow(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: GestureDetector(
          child: Card(
            color: Colors.white,
            elevation: 0.3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                children: <Widget>[
                  Expanded(child: getInfoContainer(index)),
                ],
              ),
            ),
          ),
        ));
  }

  String getObservationType(int index) {
    return userSessionManager.observationOnUploaded[index]
        .getObservationTypeString(context);
  }

  void cancelUpload(index) {
    if (userSessionManager.observationOnUploaded[index].status !=
        Status.uploaded) {
      MainScreenState state = mainScreenStatefulWidget.getState();
      state.remove_uploadOne(index);
    }
  }

  String getObservationIdString(index) {
    String textToShow = "";

    if (userSessionManager.observationOnUploaded[index].nest_id.isNotEmpty) {
      textToShow = AppLocalizations.of(context)!.nestID +
          " ${userSessionManager.observationOnUploaded[index].nest_id}";
    } else {
      textToShow = userSessionManager.observationOnUploaded[index].server_id ==
              -1
          ? "${AppLocalizations.of(context)!.obs} ${userSessionManager.observationOnUploaded[index].id}"
          : "${AppLocalizations.of(context)!.obs} ${userSessionManager.observationOnUploaded[index].server_id}";
    }

    return textToShow;
  }

  double textRowInfoSize = 12.0;

  Widget getInfoContainer(int index) {
    String observationType = getObservationType(index);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(
            userSessionManager.observationOnUploaded[index].type);

    // setCoordinatesString(index);
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              height: 20,
              margin: const EdgeInsets.only(top: 15),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 2, 0),
                child: Row(children: [
                  Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Text(
                        getObservationIdString(index),
                        style: bodySemibold
                            .merge(TextStyle(color: ColorsHandler.primary1)),
                      )),
                  Container(
                      margin: const EdgeInsets.only(left: 15, top: 0),
                      decoration: BoxDecoration(
                        color: observationTypeColor.observationFillColor,
                        borderRadius: BorderRadius.all(Radius.circular(24.0)),
                      ),
                      padding: const EdgeInsets.fromLTRB(8, 3, 8, 4),
                      child: Text(
                        observationType,
                        style: caption.merge(TextStyle(
                            color: observationTypeColor.observationTextColor)),
                      )),
                  Expanded(
                      child: Container(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: (userSessionManager
                                              .observationOnUploaded[index]
                                              .status !=
                                          Status.uploaded)
                                      ? Icon(Icons.close,
                                          size: 20,
                                          color: ColorsHandler.primary1)
                                      : Image.asset('assets/images/succed.png'),
                                  onPressed: () => {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return dialogCancelUpload(index);
                                            })
                                      }),
                            ],
                          ))),
                ]),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 13, bottom: 15, top: 0),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.end,
              children: [
                Text(
                  " ${AppLocalizations.of(context)!.nestID}  " +
                      getNestString(index),
                  style:
                      caption.merge(TextStyle(color: ColorsHandler.tertiary)),
                ),
              ],
            ),
          ),
          Visibility(
              visible:
                  (userSessionManager.observationOnUploaded[index].status !=
                      Status.uploaded),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, bottom: 15, top: 0),
                child: new LinearPercentIndicator(
                  width: 325.0,
                  barRadius: Radius.circular(10),
                  padding: const EdgeInsets.only(left: 3),
                  lineHeight: 5.0,
                  animation: true,
                  animateFromLastPercent: true,
                  backgroundColor: Color.fromARGB(100, 204, 204, 204),
                  percent: userSessionManager
                      .observationOnUploaded[index].upload_progress,
                  progressColor: Color.fromARGB(200, 0, 128, 74),
                ),
              )),
        ]);
  }

  String getNestString(index) {
    try {
      return userSessionManager.observationOnUploaded[index].nest_id;
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getTimeString(index) {
    try {
      return DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(
          userSessionManager.observationOnUploaded[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  int getObservationCount() {
    return userSessionManager.observationOnUploaded.length;
  }

  void prevPage() {
    Navigator.of(context).pop();
  }

  void checkOnUpload() {
    if (!userSessionManager.onUpload) prevPage();
  }

  void deleteObservation() async {
    for (var i = 0; i < obsSelected.length; i++) {
      debugPrint(
          "OBservation ${obsSelected[i].id} deleted id: ${obsSelected[i].server_id}");

      userSessionManager.observationOnUploaded.remove(obsSelected[i]);
      obsSelected.remove(obsSelected[i]);
      if (obsSelected[i].size > 0)
        userSessionManager.allUploadSize =
            userSessionManager.allUploadSize - obsSelected[i].size;

      if ((obsSelected[i].uploadedSize > 0) &&
          (userSessionManager.uploadedSize >= obsSelected[i].uploadedSize))
        userSessionManager.uploadedSize =
            userSessionManager.uploadedSize - obsSelected[i].uploadedSize;
    }

    setState(() {
      initObservations();
    });
  }

  Widget dialogCancelUpload(int index) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: viewContent(index),
    );
  }

  Container viewContent(int index) {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      // margin: EdgeInsets.only(top: 105),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Positioned(
            child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: new Container(
                    height: 28,
                    width: 28,
                    child: Icon(
                      Icons.close,
                      color: Color.fromARGB(255, 153, 153, 153),
                      size: 28,
                    ))),
            right: 15.0,
            top: 10.0,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 23),
                child: Column(children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(8)),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 0),
                            child: Icon(
                              Icons.info_outline_rounded,
                              size: 48.0,
                              color: Color.fromRGBO(500, 32, 9, 9),
                            ),
                          ),
                        ]),
                  ),
                  Text(
                    AppLocalizations.of(context)!.uploadInProcess,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Text(
                        AppLocalizations.of(context)!.uploadInProcessText,
                        style: TextStyle(
                            fontSize: 14,
                            color: Color.fromARGB(255, 62, 62, 62)),
                        textAlign: TextAlign.center,
                      )),
                  Container(
                    height: 1,
                    color: Colors.grey[500],
                    margin: EdgeInsets.only(top: 30, bottom: 0),
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.cancel,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                      )),
                      Container(
                        height: 50,
                        width: 1,
                        color: Colors.grey[500],
                      ),
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          userSessionManager.allUploadSize =
                              userSessionManager.allUploadSize -
                                  userSessionManager
                                      .observationOnUploaded[index].size;

                          if (userSessionManager
                                  .observationOnUploaded[index].uploadedSize >
                              0)
                            userSessionManager.uploadedSize =
                                userSessionManager.uploadedSize -
                                    userSessionManager
                                        .observationOnUploaded[index]
                                        .uploadedSize;

                          userSessionManager.observationOnUploaded.remove(
                              userSessionManager.observationOnUploaded[index]);

                          if (userSessionManager.observationOnUploaded.length ==
                              0) userSessionManager.onUpload = false;

                          MainScreenState state =
                              mainScreenStatefulWidget.getState();
                          setState(() {});
                          state.setState(() {});
                          Navigator.of(context).pop();
                          checkOnUpload();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.endUpload,
                          style: TextStyle(
                            fontSize: 18,
                            color: Color.fromRGBO(500, 32, 9, 9),
                          ),
                        ),
                      )),
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}
