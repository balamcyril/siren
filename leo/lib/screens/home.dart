import 'package:flutter/material.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'geo_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);
  @override
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  List<SiteData> sites = [];
  int currentSiteIndex = -1;
  int observationCount = 0;
  late GeoMap geoMap;
  bool launchTime = true;

  void refreshCount() {
    if (sites.isEmpty) {
      return;
    }
    if (currentSiteIndex == -1) {
      userSessionManager.lastSelectedSite = "";
      SqfliteObservationManager.instance.getCount().then((value) {
        observationCount = value;
        debugPrint("Home page - refresh observation count for site All");
        setState(() {});
      });
    } else {
      userSessionManager.lastSelectedSite = sites[currentSiteIndex].name;

      SqfliteObservationManager.instance
          .getCountBySite(sites[currentSiteIndex].name)
          .then((value) {
        observationCount = value;
        debugPrint(
            "Home page - refresh observation count for site ${sites[currentSiteIndex].name}");
        setState(() {});
      });
    }
  }

  String getTitle() {
    if (sites.length > currentSiteIndex) {
      return (currentSiteIndex == -1
              ? AppLocalizations.of(context)!.all
              : sites[currentSiteIndex].name) +
          " - $observationCount ${AppLocalizations.of(context)!.observations}";
    }
    return "${AppLocalizations.of(context)!.loading} ...";
  }

  void refreshSites() {
    if (sites.isEmpty) {
      try {
        sites = userSessionManager.userData.currentProjectData.siteDataList;
        setState(() {
          if (launchTime && userSessionManager.lastSelectedSite.isNotEmpty) {
            int index = 0;
            sites.forEach((site) {
              if (site.name == userSessionManager.lastSelectedSite) {
                debugPrint(
                    "Home page enforce site selection ${userSessionManager.lastSelectedSite}");
                currentSiteIndex = index;
              }
              index++;
            });
          }
          launchTime = false;
          refreshCount();
        });
      } catch (e) {
        debugPrint("Home page - $e");
      }
    }
  }

  Widget getMap() {
    geoMap = GeoMap();
    if (sites.isNotEmpty) {
      geoMap.setSite(currentSiteIndex == -1
          ? SiteData(0, AppLocalizations.of(context)!.all)
          : sites[currentSiteIndex]);
    }
    return geoMap;
  }

  @override
  Widget build(BuildContext context) {
    if (sites.isEmpty) {
      launchTime = true;
      Future.microtask(() => refreshSites());
    }
    return Stack(children: [
      Column(
        children: [
          Row(children: [
            IconButton(
                onPressed: () {
                  if (currentSiteIndex > -1) {
                    currentSiteIndex--;
                    refreshCount();
                  }
                  debugPrint("HomePage - sites back index $currentSiteIndex");
                },
                icon: const Icon(Icons.arrow_back)),
            Expanded(child: Text(getTitle(), textAlign: TextAlign.center)),
            IconButton(
                onPressed: () {
                  if (currentSiteIndex < sites.length - 1) {
                    currentSiteIndex++;
                    refreshCount();
                  }
                  debugPrint(
                      "HomePage - sites forward index $currentSiteIndex");
                },
                icon: const Icon(Icons.arrow_forward)),
          ]),
          Flexible(
            child: getMap(),
          ),
        ],
      )
    ]);
  }
}
