import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl_phone_field/countries.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:path/path.dart';
import 'package:siren_flutter/screens/pinCode.dart';
import 'package:siren_flutter/screens/setPassword.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:email_validator/email_validator.dart';
import '../utils/colors.dart';
import '../utils/fonts.dart';
import 'account_flow.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class MailPhonePage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  MailPhonePage(MainScreenStatefulWidget parent, {Key? key}) : super(key: key) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _MailPhonePageState createState() =>
      _MailPhonePageState(mainScreenStatefulWidget);
}

class _MailPhonePageState extends State<MailPhonePage> {
  _MailPhonePageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  late MainScreenStatefulWidget mainScreenStatefulWidget;
  final mailController = TextEditingController();
  final passwordController = TextEditingController();
  String errorMessage = "";
  String ph = "";
  late PhoneNumber phones;
  String codeTel = "";
  String sigle = "";
  String _initialCountryCode = "AE";
  bool send = false;
  bool checkPhone = false;
  bool checkMail = false;

  bool displayText = false;
  FocusNode phoneFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    phoneFocus.addListener(() {
      if (phoneFocus.hasFocus)
        setState(() {
          displayText = true;
        });
      else
        setState(() {
          displayText = false;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    var _country =
        countries.firstWhere((element) => element.code == _initialCountryCode);

    bool isNumeric(String s) {
      if (s == null) {
        return false;
      }
      return int.tryParse(s) != null;
    }

    return Scaffold(
        appBar: AppBar(title: Text("${AppLocalizations.of(context)!.signUp}")),
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  '${AppLocalizations.of(context)!.enterMail}',
                  style: h3Semibold,
                ),
                Text(
                  '${AppLocalizations.of(context)!.weSendCode}',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: ColorsHandler.primary1),
                ),
              ]),

              // Text(errorMessage,
              //     style: const TextStyle(fontSize: 20, color: Colors.red)),
              const SizedBox(
                height: 30,
              ),
              TextField(
                controller: mailController,
                autofocus: true,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: AppLocalizations.of(context)!.email +
                        ' ' +
                        AppLocalizations.of(context)!.address,
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: AppLocalizations.of(context)!.loginEmailHintText,
                    fillColor: Colors.white70),
                onChanged: (mail) {
                  debugPrint('Mail changes:  + ${mail}');
                  checkMail = EmailValidator.validate(mail);
                  checkAll();
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                child: new Stack(
                  //alignment:new Alignment(x, y)
                  // overflow: Overflow.visible,
                  clipBehavior: Clip.none,
                  children: <Widget>[
                    IntlPhoneField(
                      focusNode: phoneFocus,
                      onTap: () {
                        // if (!displayText)
                        //   setState(() {
                        //     displayText = true;
                        //   });
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      initialCountryCode: _initialCountryCode,
                      onChanged: (phone) {
                        phones = phone;
                        ph = phone.number;

                        // print(_country.maxLength);
                        // print(phone.countryISOCode);
                        // print(phone.number);
                        // print(phone.countryCode);

                        if ((phone.number.length == _country.maxLength) &&
                            isNumeric(phone.number))
                          checkPhone = true;
                        else
                          checkPhone = false;

                        checkAll();
                      },
                      onCountryChanged: (country) {
                        debugPrint(
                            'Country changed to:  + ${country.maxLength}');
                        _country = country;
                      },
                    ),
                    displayText
                        ? Positioned(
                            top: -8,
                            left: 12,
                            child: Container(
                              padding: EdgeInsets.only(left: 4.0, right: 4.0),
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 255, 255, 255),
                              ),
                              child: Text(
                                AppLocalizations.of(context)!.phone +
                                    ' ' +
                                    AppLocalizations.of(context)!.number,
                                style:
                                    TextStyle(fontSize: 12, color: Colors.blue),
                              ),
                            ))
                        : Container()
                  ],
                ),
              ),
              // IntlPhoneField(
              //   decoration: InputDecoration(
              //     // border: InputBorder.none,
              //     floatingLabelAlignment: FloatingLabelAlignment.start,

              //     // labelText: AppLocalizations.of(context)!.phone +
              //     //     ' ' +
              //     //     AppLocalizations.of(context)!.number,
              //     border: OutlineInputBorder(
              //       borderRadius: BorderRadius.circular(10.0),
              //     ),
              //   ),
              //   initialCountryCode: _initialCountryCode,
              //   onChanged: (phone) {
              //     phones = phone;
              //     ph = phone.number;

              //     // print(_country.maxLength);
              //     // print(phone.countryISOCode);
              //     // print(phone.number);
              //     // print(phone.countryCode);

              //     if ((phone.number.length == _country.maxLength) &&
              //         isNumeric(phone.number))
              //       checkPhone = true;
              //     else
              //       checkPhone = false;

              //     checkAll();
              //   },
              //   onCountryChanged: (country) {
              //     debugPrint('Country changed to:  + ${country.maxLength}');
              //     _country = country;
              //   },
              // ),
              // const SizedBox(
              //   height: 20,
              // ),
              // TextField(
              //   controller: passwordController,
              //   obscureText: true,
              //   decoration: InputDecoration(
              //       border: OutlineInputBorder(
              //         borderRadius: BorderRadius.circular(10.0),
              //       ),
              //       filled: true,
              //       labelText:
              //           AppLocalizations.of(context)!.loginPasswordLabelText,
              //       hintStyle: TextStyle(color: Colors.grey[800]),
              //       hintText:
              //           AppLocalizations.of(context)!.loginPasswordHintText,
              //       fillColor: Colors.white70),
              // ),
              const SizedBox(
                height: 20,
              ),
              OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(send
                      ? ColorsHandler.primary1
                      : Color.fromARGB(255, 136, 166, 191)),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 50)),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0))),
                ),
                onPressed: () {
                  print('btn next');
                  int taille = ph.length;
                  String email = mailController.text;
                  if (send) if (EmailValidator.validate(email)) {
                    userSessionManager.userInfo.email = email;

                    if ((taille != _country.maxLength) || !isNumeric(ph))
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Phone is not valid."),
                      ));
                    else {
                      userSessionManager.userInfo.codeTel = phones.countryCode;
                      userSessionManager.userInfo.sigle = phones.countryISOCode;
                      userSessionManager.userInfo.phone = phones.number;
                      debugPrint(
                          "Observation saved - code - ${phones.countryCode} - sigle ${phones.countryISOCode}");
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SetPasswordPage()),
                      );
                    }
                  } else
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 10),
                        content: Row(children: [
                          SizedBox(width: 5),
                          Text(
                            'Email is not valid',
                            style: TextStyle(color: Colors.white),
                          ),
                        ])));
                },
                child: Text('Next', style: TextStyle(color: Colors.white)),
              ),
              // OutlinedButton(
              //   onPressed: () {
              //     String email = loginController.text;
              //     String password = passwordController.text;
              //     errorMessage = "";
              //     userSessionManager.login(email, password).then((connected) {
              //       if (connected) {
              //         MainScreenState state =
              //             mainScreenStatefulWidget.getState();
              //         state.loginState();
              //         Navigator.pop(context);
              //       } else {
              //         setState(() {
              //           errorMessage =
              //               AppLocalizations.of(context)!.invalidCredentials;
              //         });
              //       }
              //     });
              //   },
              //   style: ButtonStyle(
              //     shape: MaterialStateProperty.all(RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(30.0))),
              //   ),
              //   child: Text(
              //     AppLocalizations.of(context)!.loginButton,
              //     style: const TextStyle(fontSize: 20, color: Colors.blueGrey),
              //   ),
              // ),
            ],
          ),
        ));
  }

  void checkAll() {
    debugPrint("check- ${checkMail} - phone ${checkPhone}");
    send = checkMail && checkPhone;
    setState(() {});
  }
}
