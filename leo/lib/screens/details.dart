import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/common_strings.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:siren_flutter/widgets/forms/text_control.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:uuid/uuid.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:dio/dio.dart';
import 'package:siren_flutter/screens/sync/spinner.dart';
import 'package:dotted_border/dotted_border.dart';
import 'dart:developer';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';

import '../utils/platform.dart';

class DetailsPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  ObservationData observationData;
  Function refresh;

  DetailsPage({Key? key, required this.observationData, required this.refresh})
      : super(key: key);
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  _DetailsPageState();
  String title = "";
  late List<dynamic> _images = [];
  late List<String> _imagesPath = [];
  String _imageDirectoryPathTemp = "";
  int _currentImageSliderIndex = 0;
  bool downloadingPhotos = true;
  bool error = false;
  bool ontap = false;
  int downloadedPhotos = 0;
  late List<MediaData> media;

  @override
  void initState() {
    super.initState();
    loadImages();
  }

  getMedia() async {
    media = await SqfliteMediaManager.instance
        .getMediaDataList(widget.observationData.id as int);
  }

  void initAnswers() async {
    widget.observationData.answerList = await SqfliteAnswerManager.instance
        .get(widget.observationData.id as int);
    setState(() {});
  }

  void loadImages() async {
    if (widget.observationData.noPicture() &&
        !widget.observationData.downloadNotComplete()) return;
    String imageDirectoryPath = await getMobileStoragePath();
    _imageDirectoryPathTemp = imageDirectoryPath;
    _images = [];
    _imagesPath = [];
    for (var element in widget.observationData.mediaList) {
      _images.add(File(imageDirectoryPath + element.path));
      _imagesPath.add(imageDirectoryPath + element.path);
    }
    setState(() {});
  }

  void downloadMedia() async {
    this.getMedia();
    const uuid = Uuid();
    String imageDirectoryPath = await getMobileStoragePath();
    for (var i = 0; i < widget.observationData.mediaList.length; i++) {
      var element = widget.observationData.mediaList[i];
      var photoName = uuid.v1() + ".jpg";
      var mediaPath = imageDirectoryPath + photoName.toString();

      try {
        await Dio().download(element.path, mediaPath);
        try {
          MediaData updatedMedia =
              MediaData(photoName.toString(), element.observation_id);
          int mid = this.media[i].media_id as int;
          int del = await SqfliteMediaManager.instance.delete(mid);
          debugPrint("media delete : ${del}");
          updatedMedia.media_id =
              await SqfliteMediaManager.instance.create(updatedMedia);
          widget.observationData.mediaList[i] = updatedMedia;
          this.error = false;
        } catch (e) {
          debugPrint("saving image error");
          this.error = true;
        }
      } catch (e) {
        debugPrint("downloading image error");
        this.error = true;
      }
    }
    if (!this.error)
      this.downloadedPhotos = widget.observationData.mediaList.length;
    this.downloadingPhotos = false;
    setState(() {});
    loadImages();
    this.widget.refresh();
    Navigator.pop(context); //display off dialog spinner
  }

  void onTaps() {
    ontap = !ontap;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (widget.observationData.answerList.isEmpty) {
      initAnswers();
    }
    if (title.isEmpty) {
      getTitle();
    }
    String observationType =
        widget.observationData.getObservationTypeString(context);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(widget.observationData.type);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          foregroundColor: ColorsHandler.primary1,
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.observationDetails,
            style: titleMedium.merge(TextStyle(color: ColorsHandler.primary1)),
          ),
        ),
        body: Container(
            color: Colors.white,
            margin: const EdgeInsets.all(0.0),
            padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                    child: getTitleTextBox(title),
                  ),
                  Container(
                      decoration: BoxDecoration(
                        color: observationTypeColor.observationFillColor,
                        borderRadius:
                            const BorderRadius.all(const Radius.circular(24.0)),
                      ),
                      padding: const EdgeInsets.fromLTRB(10, 6, 10, 6),
                      margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Text(
                        observationType,
                        style: TextStyle(
                          color: observationTypeColor.observationTextColor,
                        ).merge(smallTextBold),
                      )),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 6),
                      child: getTextBoxWithIcon(
                          widget.observationData.site,
                          const Icon(
                            Icons.beach_access_outlined,
                            color: ColorsHandler.primary1,
                            size: 16.4,
                          ))),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 6),
                      child: getTextBoxWithIcon(
                          "${AppLocalizations.of(context)!.lat}: ${widget.observationData.latitude.toStringAsFixed(3)}, ${AppLocalizations.of(context)!.long}: ${widget.observationData.longitude.toStringAsFixed(3)}",
                          const Icon(
                            Icons.location_on_outlined,
                            color: ColorsHandler.primary1,
                            size: 16.4,
                          ))),
                  if (widget.observationData.type != ObservationType.flash)
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 6),
                      child: getTextBox(
                          "${AppLocalizations.of(context)!.species}: ${getStringFromSpeciesEnum(context, widget.observationData.species)}"),
                    ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 6),
                    child: getTextBoxWithIcon(
                        widget.observationData.observer,
                        const Icon(
                          Icons.person_outlined,
                          color: ColorsHandler.primary1,
                          size: 16.4,
                        )),
                  ),
                  if (widget.observationData.patrol_id != -1 &&
                      widget.observationData.patrol_id != null)
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 6),
                        child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              ImageIcon(
                                AssetImage('assets/images/plaunch.png'),
                                color: ColorsHandler.primary1,
                                size: 20,
                              ),
                              Text(
                                  " ${AppLocalizations.of(context)!.patrolName} #${widget.observationData.patrol_id}",
                                  style: bodyMedium.merge(
                                      TextStyle(color: ColorsHandler.primary1)))
                            ])),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                    child: getTextBoxWithIcon(
                        getDateTimeString(widget.observationData),
                        const Icon(
                          Icons.access_time,
                          color: ColorsHandler.primary1,
                          size: 16.4,
                        )),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    //padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: !widget.observationData.noPicture()
                        ? ((!widget.observationData.downloadNotComplete())
                            ? getImageSlider()
                            : downloadDisplay())
                        : null,
                  ),
                  SuccessErrorMessage(!downloadingPhotos, downloadedPhotos),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: _images.length > 1
                        ? _imagesPath.map((urlOfItem) {
                            int index = _imagesPath.indexOf(urlOfItem);
                            return Container(
                              width: 10.0,
                              height: 10.0,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _currentImageSliderIndex == index
                                    ? const Color.fromRGBO(0, 0, 0, 0.8)
                                    : const Color.fromRGBO(0, 0, 0, 0.3),
                              ),
                            );
                          }).toList()
                        : [],
                  ),
                  getAnswer(),
                ],
              ),
            )));
  }

  double textRowInfoSize = 14.0;

  void getTitle() async {
    title =
        "${AppLocalizations.of(context)!.obs}: ${getObservationStringId(widget.observationData)}";
    title = await getObservationTitle(context, widget.observationData);
  }

  Widget getImageSlider() {
    return CarouselSlider(
      options: CarouselOptions(
        height: 220.0,
        onPageChanged: (position, reason) {
          debugPrint("reason $reason");
          debugPrint(CarouselPageChangedReason.controller.toString());
          setState(
            () {
              _currentImageSliderIndex = position;
            },
          );
        },
        enableInfiniteScroll: _images.length > 1 ? true : false,
      ),
      items: _images
          .map((item) => Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                alignment: Alignment.centerLeft,
                child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        getImage(item),
                      ],
                    )),
              ))
          .toList(),
    );
  }

  Widget getImageContainer(var image) {
    return Container(
      padding: const EdgeInsets.fromLTRB(1, 1, 0, 0),
      height: 210,
      child: getImage(image),
    );
  }

  Widget getImage(var image) {
    return Image.file(
      image,
      fit: BoxFit.cover,
      width: 220,
      height: 220,
    );
  }

  Image getImage2(var image) {
    return Image.file(
      image,
      fit: BoxFit.fitHeight,
    );
  }

  Widget getAnswer() {
    List<Widget> widgetAnswer = [];
    for (var answer in widget.observationData.answerList) {
      if (answer.questionId == 320 || answer.questionId == 321) {
        continue;
      }

      debugPrint("question id id: ${answer.questionId}");

      var question =
          ObservationQuestionManager(context).getById(answer.questionId);
      if (question.type == questionType.select) {
        question.isReadOnly = true;
        widgetAnswer.add(TextControlWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.text));
      } else {
        question.isReadOnly = true;
        widgetAnswer.add(TextControlWidget(
            observationData: widget.observationData,
            question: question,
            textInputType: TextInputType.text));
      }
    }

    return Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(20)),
        margin: const EdgeInsets.only(bottom: 15.0, top: 25),
        padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
        child: Column(children: widgetAnswer));
  }

  String getDateTimeString(ObservationData obs) {
    try {
      return DateFormat.yMMMd()
              .format(DateTime.fromMillisecondsSinceEpoch(obs.timestamp)) +
          " • " +
          DateFormat('kk:mm')
              .format(DateTime.fromMillisecondsSinceEpoch(obs.timestamp));
    } catch (e) {
      return "${AppLocalizations.of(context)!.unknown}";
    }
  }

  Widget getTextBox(String text) {
    return Row(children: [
      Expanded(
          child: Text("" + text,
              style:
                  bodyMedium.merge(TextStyle(color: ColorsHandler.primary1))))
    ]);
  }

  Widget getTextBoxWithIcon(String text, Icon icon) {
    return Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
      icon,
      Text("  " + text,
          style: bodyMedium.merge(TextStyle(color: ColorsHandler.primary1)))
    ]);
  }

  Widget getTitleTextBox(String text) {
    return Text(text, style: h2);
  }

  Widget downloadDisplay() {
    return InkWell(
      onTapDown: (details) => onTaps(),
      onTap: () {
        onTaps();
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return SpinnerBox();
            });
        downloadMedia();
      },
      borderRadius: BorderRadius.circular(20),
      child: DottedBorder(
        strokeWidth: 2,
        color: !ontap
            ? Color.fromARGB(87, 95, 102, 100)
            : Color.fromRGBO(9, 109, 187, 1.0),
        child: Container(
          height: 232,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color.fromARGB(100, 242, 247, 250),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  !ontap
                      ? Image.asset(
                          "assets/images/downloadin.png",
                        )
                      : Image.asset(
                          "assets/images/downloadout.png",
                        ),
                  Text(
                    AppLocalizations.of(context)!.downloadphoto,
                    style: !ontap ? h3Semibold : h3Semibold2,
                  ),
                ],
              )
            ],
          ),
        ),
        borderType: BorderType.RRect,
        radius: Radius.circular(20),
        dashPattern: [10, 5, 10, 5, 10, 5],
      ),
    );
  }

  Widget SuccessErrorMessage(bool visible, int photos) {
    return Visibility(
      visible: visible,
      child: !error
          ? Container(
              margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(210, 236, 223, 1),
                  border: Border.all(color: Color.fromRGBO(210, 236, 223, 1)),
                  borderRadius: BorderRadius.circular(8)),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 0),
                      child: Icon(
                        Icons.check_circle_outline_rounded,
                        size: 22.0,
                        color: Color.fromRGBO(5, 100, 51, 1),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          "${downloadedPhotos} ${AppLocalizations.of(context)!.photodownloaded}",
                          style: TextStyle(
                              fontSize: 14,
                              color: Color.fromRGBO(5, 100, 51, 1),
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    )
                  ]))
          : Container(
              margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 225, 223, 1),
                  border: Border.all(color: Color.fromRGBO(255, 225, 223, 1)),
                  borderRadius: BorderRadius.circular(8)),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 0),
                      child: Icon(
                        // Icons.check_circle_rounded_amber_outlined,
                        Icons.info_outline_rounded,
                        size: 20.0,
                        color: Color.fromRGBO(192, 25, 16, 1),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, top: 3),
                        child: Text(
                          AppLocalizations.of(context)!.photodownloadedfalled,
                          style: TextStyle(
                              height: 1,
                              fontSize: 14,
                              color: Color.fromRGBO(192, 25, 16, 1),
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    )
                  ]),
            ),
    );
  }
}
