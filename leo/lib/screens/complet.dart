import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:siren_flutter/data/job_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/home.dart';
import 'package:siren_flutter/screens/login.dart';
import 'package:siren_flutter/screens/successSignUp.dart';
import 'package:siren_flutter/user_session_manager.dart';
import '../utils/colors.dart';
import '../utils/fonts.dart';
import 'geo_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:country_picker/country_picker.dart';

class CompletPage extends StatefulWidget {
  CompletPage({Key? key}) : super(key: key);
  @override
  _PinCodePageState createState() {
    return _PinCodePageState();
  }
}

class _PinCodePageState extends State<CompletPage> {
  final fname = TextEditingController();
  final lname = TextEditingController();
  final cname = TextEditingController();
  final location = TextEditingController();
  final oname = TextEditingController();

  String _selectedValue = "1";
  String countryCode = "971";
  List<Job> listOfValue = [];
  bool active = false;
  bool desactive = false;

  @override
  Widget build(BuildContext context) {
    cname.text = "United Arab Emirates";
    return Scaffold(
        appBar: AppBar(title: Text("${AppLocalizations.of(context)!.signUp}")),
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      '${AppLocalizations.of(context)!.completAccount}',
                      style: h3Semibold,
                    ),
                  ]),
              const SizedBox(
                height: 30,
              ),
              // ListView(
              //   shrinkWrap: false,
              //   children: [
              Text(
                '${AppLocalizations.of(context)!.accountName}',
                style: h3Semibold,
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.firstName}',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                readOnly: desactive,
                controller: fname,
                obscureText: false,
                onChanged: (val) => {checkActive()},
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText:
                        '${AppLocalizations.of(context)!.enterFirstName}',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '',
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.lastName}',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                readOnly: desactive,
                controller: lname,
                obscureText: false,
                onChanged: (val) => {checkActive()},
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: '${AppLocalizations.of(context)!.enterLastName}',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '',
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                '${AppLocalizations.of(context)!.location}',
                style: h3Semibold,
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.locationLabel}*',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                readOnly: desactive,
                controller: location,
                obscureText: false,
                onChanged: (val) => {checkActive()},
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: '${AppLocalizations.of(context)!.enterLocation}',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '',
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.country}*',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                readOnly: true,
                onTap: () {
                  showCountryPicker(
                    context: context,
                    // exclude: <String>['KN', 'MF'],
                    // favorite: <String>['SE'],
                    //Optional. Shows phone code before the country name.
                    showPhoneCode: false,
                    onSelect: (Country country) {
                      print('Select country: ${country.displayName}');
                      print('Select country: ${country.phoneCode}');
                      print('Select country: ${country.countryCode}');
                      cname.text = country.name;
                      countryCode = country.phoneCode;
                    },
                    // Optional. Sets the theme for the country list picker.
                    countryListTheme: CountryListThemeData(
                      bottomSheetHeight: 500,
                      // Optional. Sets the border radius for the bottomsheet.
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(0.0),
                        topRight: Radius.circular(0.0),
                      ),
                      // Optional. Styles the search field.
                      inputDecoration: InputDecoration(
                        labelText:
                            '${AppLocalizations.of(context)!.searchTooltip}',
                        hintText:
                            '${AppLocalizations.of(context)!.typingSearch}',
                        prefixIcon: const Icon(Icons.search),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: const Color(0xFF8C98A8).withOpacity(0.2),
                          ),
                        ),
                      ),
                      // Optional. Styles the text in the search field
                      searchTextStyle: TextStyle(
                        color: Colors.blue,
                        fontSize: 18,
                      ),
                    ),
                  );
                },
                controller: cname,
                obscureText: false,
                onChanged: (val) => {checkActive()},
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: '${AppLocalizations.of(context)!.enterCountry}',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '${AppLocalizations.of(context)!.selectCountry}',
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                '${AppLocalizations.of(context)!.professional}',
                style: h3Semibold,
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.organization}*',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                readOnly: desactive,
                controller: oname,
                obscureText: false,
                onChanged: (val) => {checkActive()},
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText:
                        '${AppLocalizations.of(context)!.enterOrganization}',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '',
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${AppLocalizations.of(context)!.jobTitle}*',
                style: TextStyle(
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
              DropdownButtonFormField(
                style: titleMedium,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: '',
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: '',
                    fillColor: Colors.white70),
                // InputDecoration(
                //   labelStyle: bodyMedium
                //       .merge(TextStyle(color: ColorsHandler.tertiary)),
                //   labelText: "ici",
                //   floatingLabelBehavior: FloatingLabelBehavior.auto,
                //   hintText: "",
                //   prefixText: "  ",
                //   enabledBorder: UnderlineInputBorder(
                //     borderSide: BorderSide(color: ColorsHandler.tertiary),
                //   ),
                //   focusedBorder: UnderlineInputBorder(
                //     borderSide:
                //         BorderSide(color: ColorsHandler.primary1, width: 2),
                //   ),
                // ),
                items: userSessionManager.jobList.map((var val) {
                  return DropdownMenuItem(
                    value: val.id.toString(),
                    child: Text(val.name),
                  );
                }).toList(),
                value: _selectedValue,
                onChanged: (val) {
                  print('change val:');
                  print(val);
                  _selectedValue = val.toString();
                  setState(
                    () {},
                  );
                },
                validator: (value) {
                  return null;
                },
              ),
              const SizedBox(
                height: 5,
              ),
              // TextField(
              //   controller: lname,
              //   obscureText: true,
              //   decoration: InputDecoration(
              //       border: OutlineInputBorder(
              //         borderRadius: BorderRadius.circular(10.0),
              //       ),
              //       filled: true,
              //       labelText: 'Job',
              //       hintStyle: TextStyle(color: Colors.grey[800]),
              //       hintText: 'Enter your job title',
              //       fillColor: Colors.white70),
              // ),
              const SizedBox(
                height: 30,
              ),
              OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(active
                      ? ColorsHandler.primary1
                      : Color.fromARGB(255, 136, 166, 191)),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 50)),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0))),
                ),
                onPressed: () async {
                  var payload = {
                    "userId": userSessionManager.userInfo.id,
                    "firstname": fname.text,
                    "lastname": lname.text,
                    "organization": oname.text,
                    "fonctionId": int.parse(_selectedValue),
                    "city": location.text,
                    "countryCode": countryCode
                  };
                  if (active) {
                    setState(() {
                      active = false;
                      desactive = true;
                    });

                    try {
                      bool rt = await SirenRestServices().updateUser(payload);
                      print('success update');
                      print(rt);
                      if (rt) {
                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     duration: Duration(seconds: 15),
                        //     content: Row(children: [
                        //       SizedBox(width: 5),
                        //       Text("Account created successfully."),
                        //     ])));
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SuccessSignUpPage()),
                        );
                      } else {
                        setState(() {
                          active = true;
                          desactive = false;
                        });
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(
                              "Error updating user data, please check your internet connexion"),
                        ));
                      }
                    } catch (e) {
                      debugPrint("Error creation account${e} ");
                      Navigator.pop(context);
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Error creation account"),
                      ));
                    }
                  }
                },
                child: Text('${AppLocalizations.of(context)!.completeSignUp}',
                    style: TextStyle(color: Colors.white)),
              ),
              //   ],
              // ),

              // Expanded(
              //   child: SingleChildScrollView(
              //     child: OutlinedButton(
              //       style: ButtonStyle(
              //         backgroundColor:
              //             MaterialStateProperty.all(ColorsHandler.primary1),
              //         minimumSize:
              //             MaterialStateProperty.all(Size(double.infinity, 50)),
              //         shape: MaterialStateProperty.all(RoundedRectangleBorder(
              //             borderRadius: BorderRadius.circular(10.0))),
              //       ),
              //       onPressed: () {
              //         debugPrint('next code');
              //       },
              //       child: Text('Complete sign up',
              //           style: TextStyle(color: Colors.white)),
              //     ),
              //   ),
              // ),
            ],
          ),
        ));
  }

  checkActive() {
    if (fname.text != '' &&
        lname.text != '' &&
        location.text != '' &&
        oname.text != '')
      setState(() {
        active = true;
      });
    else
      setState(() {
        active = false;
      });
  }
}
