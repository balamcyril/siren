import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/details_patrol.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'dart:math';
import 'package:geolocator/geolocator.dart';
import 'package:siren_flutter/utils/patrols_utils.dart';
import '../data/observation_factory.dart';
import '../data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import '../data/observation_data.dart';
import 'package:siren_flutter/screens/patrol_creator.dart';

class PatrolTabPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  PatrolTabPage(MainScreenStatefulWidget parent, {Key? key}) : super(key: key) {
    mainScreenStatefulWidget = parent;
  }

  @override
  _PatrolTabPageState createState() =>
      _PatrolTabPageState(mainScreenStatefulWidget);
}

class _PatrolTabPageState extends State<PatrolTabPage>
    with AutomaticKeepAliveClientMixin {
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  late BuildContext dialogContext;
  double textRowInfoSize = 12.0;
  double titleRowSize = 16.0;

  @override
  bool get wantKeepAlive => true;

  _PatrolTabPageState(parent) {
    mainScreenStatefulWidget = parent;
    initPatrol();
  }

  @override
  void didUpdateWidget(PatrolTabPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  void initPatrol() async {
    userSessionManager.patrollist = await SqfliteObservationManager.instance
        .getPatrolByProject(userSessionManager.userData.currentProjectData.id);
    debugPrint(
        "Patrol tab init patrol count ${userSessionManager.patrollist.length}");
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // return ListViewWidget();
    return Container(
        color: ColorsHandler.tertiaryOp7,
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Column(children: [
          const SizedBox(
            height: 8,
          ),
          Expanded(
            child: getPatrolList(),
          ),
          const SizedBox(
            height: 60,
          )
        ]));
  }

  Widget getPatrolList() {
    ListView myList = new ListView.builder(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
        itemCount: userSessionManager.patrollist.length,
        itemBuilder: (context, index) {
          return Padding(
              padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
              child: GestureDetector(
                // When the child is tapped, show a snackbar.
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailsPatrolPage(
                              patrolData: userSessionManager.patrollist[index],
                              refresh: () {
                                setState(() {});
                              },
                            )),
                  );
                },
                child: Card(
                  elevation: 0.3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: getInfoPatrolContainer(index)),
                        getPatrolActionsContainer(context, index),
                      ],
                    ),
                  ),
                ),
              ));
        });
    return myList;
  }

  Widget getPatrolRow(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: GestureDetector(
          // When the child is tapped, show a snackbar.
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailsPatrolPage(
                        patrolData: userSessionManager.patrollist[index],
                        refresh: () {
                          setState(() {});
                        },
                      )),
            );
          },
          child: Card(
            elevation: 0.3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(child: getInfoPatrolContainer(index)),
                  getPatrolActionsContainer(context, index),
                ],
              ),
            ),
          ),
        ));
  }

  Widget getInfoPatrolContainer(int index) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(1, 9.41, 2, 7),
            child: Text(
              '${AppLocalizations.of(context)!.patrol} #${userSessionManager.patrollist[index].server_id == -1 ? userSessionManager.patrollist[index].id : userSessionManager.patrollist[index].server_id}',
              // getObservationIdString(index),
              style:
                  bodySemibold.merge(TextStyle(color: ColorsHandler.primary1)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 1, bottom: 6.41),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(
                  Icons.schedule,
                  size: textRowInfoSize,
                  color: ColorsHandler.primary1,
                ),
                Text(
                  " " +
                      getDatePatrolString(index) +
                      " • " +
                      "${userSessionManager.patrollist[index].status == PatrolStatus.actif ? userSessionManager.NumberObsActualPatrol : userSessionManager.patrollist[index].obs_nbr} ${AppLocalizations.of(context)!.observationName}s",
                  style:
                      caption.merge(TextStyle(color: ColorsHandler.tertiary)),
                ),
              ],
            ),
          )
        ]);
  }

  Container viewContent() {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      // margin: EdgeInsets.only(top: 105),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Positioned(
            child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: new Container(
                    height: 28,
                    width: 28,
                    child: Icon(
                      Icons.close,
                      color: Color.fromARGB(255, 153, 153, 153),
                      size: 28,
                    ))),
            right: 15.0,
            top: 10.0,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 23),
                child: Column(children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(8)),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 0),
                            child: Icon(
                              Icons.info_outline_rounded,
                              size: 48.0,
                              color: Color.fromRGBO(500, 32, 9, 9),
                            ),
                          ),
                        ]),
                  ),
                  Text(
                    AppLocalizations.of(context)!.patrolInProcess,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 15, right: 15), //apply padding to all four sides
                    child: Text(
                      AppLocalizations.of(context)!.endPatrolDetail,
                      style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    height: 1,
                    color: Colors.grey[500],
                    margin: EdgeInsets.only(top: 30, bottom: 0),
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.cancel,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                      )),
                      Container(
                        height: 50,
                        width: 1,
                        color: Colors.grey[500],
                      ),
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          // patrolCancel();
                          MainScreenState state =
                              mainScreenStatefulWidget.getState();
                          state.patrolCancel();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.endPatrol,
                          style: TextStyle(
                            fontSize: 18,
                            color: Color.fromRGBO(500, 32, 9, 9),
                          ),
                        ),
                      )),
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }

  void patrolCancel() async {
    double distance = 0;
    List<ObservationData> observationList = [];
    ObservationData obsdata = ObservationFactory().create(-1);
    var patrol = await SqfliteObservationManager.instance
        .getPatrolById(userSessionManager.currentPatrolId);

    if (patrol.length > 0) {
      patrol[0].end_timestamp =
          new DateTime.now().toUtc().millisecondsSinceEpoch;

      patrol[0].obs_nbr = userSessionManager.NumberObsActualPatrol;
      if (userSessionManager.NumberObsActualPatrol > 1) {
        observationList = await SqfliteObservationManager.instance
            .getAllObservationsForPatrolProject(patrolData.project_id,
                patrolData.id as int, patrolData.server_id, 0);

        for (var i = 0; i == observationList.length - 1; i++) {
          distance = distance +
              calculateDistanceFromGPSLocation(
                  observationList[i].latitude,
                  observationList[i].longitude,
                  observationList[i + 1].latitude,
                  observationList[i + 1].longitude);
        }
      }

      patrol[0].status = PatrolStatus.unactif;
      patrol[0].distance = distance;

      Duration d = DateTime.fromMillisecondsSinceEpoch(patrol[0].end_timestamp)
          .difference(
              DateTime.fromMillisecondsSinceEpoch(patrol[0].start_timestamp));

      DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
          1577836800000 + (d.inSeconds * 1000));

      debugPrint("Start date diff: ${dateTime}, ${d},");
      // patrol[0].timestamp = dateTime.toUtc().millisecondsSinceEpoch;
      patrol[0].timestamp =
          d.toString().split(':')[0] + "h " + d.toString().split(':')[1] + "m";

      Position location =
          await userSessionManager.getGPSPosition(locationCreate, obsdata);
      patrol[0].end_latitude = location.latitude;
      patrol[0].end_longitude = location.longitude;
      localDBPAtrolManager().update(context, patrol[0]);
    }

    // await SqfliteObservationManager.instance.erasePatrol();

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Row(children: [
      ImageIcon(
        AssetImage('assets/images/pstart.png'),
        color: Colors.white,
      ),
      SizedBox(width: 8),
      Text(AppLocalizations.of(context)!.patrolEnd),
    ])));
    // MainScreenState state = mainScreenStatefulWidget.getState();

    userSessionManager.patrol = false;
    userSessionManager.currentPatrolId = -1;
    if (userSessionManager.flashSupportSave) {
      userSessionManager.flashSupport = true;
      userSessionManager.flashSupportSave = false;
    }
    setState(() {});

    // state.patrolCancel();

    // state.setState(() {
    //   userSessionManager.patrol = false;
    //   userSessionManager.currentPatrolId = -1;
    //   if (userSessionManager.flashSupportSave) {
    //     userSessionManager.flashSupport = true;
    //     userSessionManager.flashSupportSave = false;
    //   }
    //   initPatrol();
    // });
    initPatrol();
    Navigator.of(context).pop();
  }

  void locationCreate() {
    debugPrint("Start and End getGPSPosition");
  }

  Widget getPatrolActionsContainer(BuildContext context, int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        (userSessionManager.patrollist[index].status == PatrolStatus.actif)
            ? InkWell(
                child: Text(AppLocalizations.of(context)!.current, style: h31),
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          elevation: 0,
                          backgroundColor: Colors.transparent,
                          child: viewContent(),
                        );
                      });
                },
              )
            : IconButton(
                onPressed: () {
                  if (userSessionManager.patrollist[index].status ==
                      PatrolStatus.uploaded)
                    print(userSessionManager.patrollist[index].status);
                  else
                    showActionPatrolBottomSheet(context, index);
                },
                icon: userSessionManager.patrollist[index].status ==
                        PatrolStatus.unactif
                    ? Image.asset(
                        'assets/images/pno.png',
                        width: 18,
                      )
                    : Image.asset(
                        'assets/images/pok.png',
                        width: 18,
                      ))
      ],
    );
  }

  void showActionPatrolBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: getBottomPatrolSheetChildren(context, index));
        });
  }

  List<Widget> getBottomPatrolSheetChildren(BuildContext context, int index) {
    dialogContext = context;
    return <Widget>[
      ListTile(
        leading: const Icon(Icons.upload),
        title: Text(AppLocalizations.of(context)!.upload),
        onTap: () {
          Navigator.pop(dialogContext);
          if (!userSessionManager.onUpload) {
            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            //   content:
            //       Text(AppLocalizations.of(context)!.uploadingPatrolServer),
            // ));
            showActionDialog(
                AppLocalizations.of(context)!.uploadingPatrolServer);
            launch_upload_patrol(index);
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Wait upload in process"),
            ));
          }
        },
      ),
      ListTile(
        leading: const Icon(Icons.delete),
        title: Text(AppLocalizations.of(context)!.delete),
        onTap: () {
          Navigator.pop(dialogContext);
          showActionDialog(AppLocalizations.of(context)!.deletingPatrol);
          deletePatrol(index);
        },
      ),
    ];
  }

  void launch_upload_patrol(int index) async {
    int patrolId = -1;
    userSessionManager.observationOnUploaded = [];
    userSessionManager.observationOnUploaded = await SqfliteObservationManager
        .instance
        .getAllObservationsForPatrolProject(
            userSessionManager.patrollist[index].project_id,
            userSessionManager.patrollist[index].id as int,
            userSessionManager.patrollist[index].server_id,
            -1);

    MainScreenState state = mainScreenStatefulWidget.getState();
    // state.setState(() {});

    debugPrint("Patrol to upload ${userSessionManager.patrollist[index].id} ");
    try {
      patrolId = await SirenRestServices().submitPatrol(context, index);
    } catch (e) {
      debugPrint("Patrol send error ${e} ");
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Upload patrol error"),
        // content: Text(AppLocalizations.of(context)!.uploadFailure),
      ));
    }

    if (patrolId > 0) {
      userSessionManager.patrollist[index].server_id = patrolId;
      userSessionManager.patrollist[index].status = PatrolStatus.uploaded;
      await SqfliteObservationManager.instance
          .updatePatrol(userSessionManager.patrollist[index]);
      state.run_upload(-1, patrolId);
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Row(children: [
        ImageIcon(
          AssetImage('assets/images/pstart.png'),
          color: Colors.white,
        ),
        SizedBox(width: 8),
        Text(AppLocalizations.of(context)!.patrolUploaded),
      ])));
    }
  }

  void deletePatrol(int index) async {
    await SqfliteObservationManager.instance.hardDeletePatrol(
        userSessionManager.patrollist[index].id as int,
        userSessionManager.NumberObsActualPatrol);

    initPatrol();
    setState(() {
      // Navigator.pop(dialogContext);
      Navigator.pop(context);
      initPatrol();
    });
  }

  String getDatePatrolString(index) {
    try {
      return DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(
          userSessionManager.patrollist[index].start_timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  dynamic showActionDialog(String text) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: Container(
            margin: const EdgeInsets.all(20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new CircularProgressIndicator(),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: new Text(text))),
              ],
            ),
          ),
        );
      },
    );
  }
}

class ListViewWidget extends StatefulWidget {
  @override
  _ListViewWidgetState createState() => _ListViewWidgetState();
}

class _ListViewWidgetState extends State<ListViewWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildItem,
      itemCount: 50,
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return ListTile(
      title: Text(index.toString()),
    );
  }
}
