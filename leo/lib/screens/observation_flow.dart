import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/observation_factory.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/flows/observation_step.dart';
import 'package:siren_flutter/flows/question_flow_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/observation_creator.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:siren_flutter/widgets/forms/observation_step_form.dart';
import '../widgets/forms/controls.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/data/observation_data.dart';

enum FlowActionType { editForm, newForm }

class ObservationFlowWidget extends StatefulWidget {
  int observationId;
  late int observationType;
  late ObservationData observationData;
  ObservationFlowWidget(
      {Key? key, required this.observationId, required this.observationType})
      : super(key: key) {
    observationData = ObservationFactory().create(observationType);
    observationData.user_id = userSessionManager.userData.id;
    observationData.project_id =
        userSessionManager.userData.currentProjectData.id;
    observationData.project =
        userSessionManager.userData.currentProjectData.name;
    observationData.observer = userSessionManager.userData.username;
  }

  @override
  _ObservationFlowState createState() => _ObservationFlowState();
}

class _ObservationFlowState extends State<ObservationFlowWidget> {
  int _selectedIndex = 0;
  late Widget widgetForBody;
  bool flowInited = false;
  late FlowActionType flowActionType;

  final _formKey = GlobalKey<FormState>();
  late List<Widget> _widgetOptions = [];

  void getObservation() async {
    if (widget.observationId != -1) {
      await SqfliteObservationManager.instance
          .get(widget.observationId)
          .then((observationDataMap) {
        widget.observationData.updateFromMap(observationDataMap);
        debugPrint(widget.observationData.toString());
      });

      await SqfliteAnswerManager.instance
          .get(widget.observationId)
          .then((answerList) {
        widget.observationData.answerList = answerList;
        widget.observationData.id = widget.observationId;
        setState(() {});
      });

      await SqfliteMediaManager.instance
          .getMediaDataList(widget.observationId)
          .then((mediaList) {
        widget.observationData.mediaList = mediaList;
        widget.observationData.id = widget.observationId;
        setState(() {});
      });
    }
  }

  void onLocationChange() {
    (_widgetOptions.elementAt(_selectedIndex) as ObservationStepFormWidget)
        .onLocationChange();
  }

  void initFlow() {
    var steps = QuestionFlowManager(context).getSteps(widget.observationType);
    _widgetOptions = [];
    for (var step in steps) {
      _widgetOptions.add(ObservationStepFormWidget(
        observationData: widget.observationData,
        stepType: step.type,
      ));
    }
    setState(() {
      flowInited = true;
    });
  }

  @override
  void initState() {
    super.initState();
    flowInited = false;
    Future.delayed(Duration.zero, () {
      initFlow();
    });

    getObservation();
    flowActionType = widget.observationId == -1
        ? FlowActionType.newForm
        : FlowActionType.editForm;
    if (flowActionType == FlowActionType.newForm) {
      WidgetsBinding.instance!.addPostFrameCallback((_) => userSessionManager
          .getGPSPosition(onLocationChange, widget.observationData));
    }
  }

  bool visibility() {
    switch (_selectedIndex) {
      case 0:
        return false;
      default:
        return true;
    }
  }

  void prevPage() {
    if (_selectedIndex > 0) {
      setState(() {
        _selectedIndex -= 1;
      });
    } else {
      Navigator.of(context).pop();
    }
  }

  void nextPage() {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _selectedIndex += 1;
      });
    }
  }

  void submitForm() async {
    if (_formKey.currentState!.validate()) {
      if (flowActionType == FlowActionType.newForm) {
        if (widget.observationData.type == ObservationType.flash)
          widget.observationData.species = Species.unknown;

        if (userSessionManager.patrol &&
            userSessionManager.currentPatrolId != -1) {
          widget.observationData.patrol_id = userSessionManager.currentPatrolId;
          userSessionManager.NumberObsActualPatrol =
              userSessionManager.NumberObsActualPatrol + 1;
        }

        localDBObservationManager().create(context, widget.observationData);
      } else {
        localDBObservationManager().update(context, widget.observationData);
      }
      userSessionManager.lastSelectedSite = widget.observationData.site;

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => MainScreenStatefulWidget()));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            content:
                Text(AppLocalizations.of(context)!.observationFormFailure)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!flowInited) {
      return Container();
    }

    TextStyle observationTypeChip = smallTextBold.merge(TextStyle(
        color: ColorsHandler.getObservationTypeColor(widget.observationType)
            .observationTextColor));

    if (widget.observationData.answerList.isEmpty &&
        flowActionType == FlowActionType.editForm) {
      return Container();
    } else {
      return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.grey.shade50,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back, color: ColorsHandler.primary1),
              tooltip: AppLocalizations.of(context)!.backButton,
              onPressed: () {
                prevPage();
              },
            );
          }),
        ),
        body: Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
            child: Form(
              key: _formKey,
              child: ListView(children: [
                Text(
                  flowActionType == FlowActionType.newForm
                      ? AppLocalizations.of(context)!.newObservation
                      : AppLocalizations.of(context)!.editObservation,
                  style: h2,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Chip(
                    label: Text(
                      widget.observationData.getObservationTypeString(context),
                      style: observationTypeChip,
                    ),
                    backgroundColor: ColorsHandler.getObservationTypeColor(
                            widget.observationType)
                        .observationFillColor,
                  ),
                ),
                _widgetOptions.elementAt(_selectedIndex),
                Controls(
                  visible: visibility(),
                  secondButton: _selectedIndex < (_widgetOptions.length - 1)
                      ? AppLocalizations.of(context)!.nextButton
                      : AppLocalizations.of(context)!.saveButton,
                  onBackTap: (() {
                    prevPage();
                  }),
                  onForwardTap: (() {
                    _selectedIndex < (_widgetOptions.length - 1)
                        ? nextPage()
                        : submitForm();
                  }),
                ),
              ]),
            )),
      );
    }
  }
}
