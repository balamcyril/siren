import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/screens/complet.dart';
import 'package:siren_flutter/screens/setPassword.dart';
import 'package:siren_flutter/user_session_manager.dart';
import '../utils/colors.dart';
import '../utils/fonts.dart';
import 'geo_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCodePage extends StatefulWidget {
  PinCodePage({Key? key}) : super(key: key);
  @override
  _PinCodePageState createState() {
    return _PinCodePageState();
  }
}

class _PinCodePageState extends State<PinCodePage> {
  String code = "";
  bool send = false;
  bool refreshCode = false;
  final spinkit = SpinKitThreeBounce(
    color: ColorsHandler.primary1,
    size: 30.0,
  );

  TextEditingController control = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("${AppLocalizations.of(context)!.signUp}")),
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  '${AppLocalizations.of(context)!.verifCode}',
                  style: h3Semibold,
                ),
                Text(
                  '${AppLocalizations.of(context)!.enterCode}',
                  style: TextStyle(color: ColorsHandler.primary1),
                  textAlign: TextAlign.center,
                ),
              ]),
              const SizedBox(
                height: 30,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: PinCodeTextField(
                  appContext: context,
                  length: 5,
                  controller: control,
                  cursorHeight: 19,
                  enableActiveFill: true,
                  textStyle: h2,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      inactiveColor: Color.fromARGB(255, 232, 239, 244),
                      inactiveFillColor: Color.fromARGB(255, 232, 239, 244),
                      fieldHeight: 60,
                      fieldWidth: 50,
                      selectedColor: ColorsHandler.primary1,
                      selectedFillColor: Color.fromARGB(255, 232, 239, 244),
                      activeFillColor: Color.fromARGB(255, 232, 239, 244),
                      activeColor: ColorsHandler.primary1,
                      borderWidth: 1,
                      borderRadius: BorderRadius.circular(10)),
                  onChanged: ((value) {
                    print(value);
                    code = value;
                    checkCode(value);
                  }),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "${AppLocalizations.of(context)!.didHavCode}",
                style: const TextStyle(
                    fontSize: 14,
                    color: ColorsHandler.primary1,
                    fontWeight: FontWeight.w600),
              ),
              refreshCode
                  ?
                  // Text(
                  //     "...",
                  //     style: const TextStyle(
                  //         fontSize: 30,
                  //         color: ColorsHandler.primary1,
                  //         fontWeight: FontWeight.w600),
                  //   )
                  Column(children: [
                      const SizedBox(
                        height: 18,
                      ),
                      spinkit
                    ])
                  : TextButton(
                      onPressed: () async {
                        refreshCode = true;
                        send = false;
                        setState(() {});

                        try {
                          bool check = await SirenRestServices().refreshCode();
                          if (check) {
                            control.clear();
                            code = "";
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Row(children: [
                              ImageIcon(
                                AssetImage('assets/images/pstart.png'),
                                color: Colors.white,
                              ),
                              SizedBox(width: 5),
                              Text('Check your mail'),
                            ])));
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Error sending code"),
                            ));
                          }

                          refreshCode = false;
                          setState(() {});
                        } catch (e) {
                          refreshCode = false;
                          setState(() {});
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Error sending code"),
                          ));
                        }
                      },
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: refreshCode
                                ? BorderSide.none
                                : BorderSide(color: Colors.black, width: 1.0),
                          ),
                        ),
                        child: Text(
                            '${AppLocalizations.of(context)!.resendCode}',
                            style: const TextStyle(
                                fontSize: 18,
                                color: ColorsHandler.primary1,
                                fontWeight: FontWeight.w600)),
                      )),
              const SizedBox(
                height: 50,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: OutlinedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(send
                          ? ColorsHandler.primary1
                          : Color.fromARGB(255, 136, 166, 191)),
                      minimumSize:
                          MaterialStateProperty.all(Size(double.infinity, 50)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0))),
                    ),
                    onPressed: () async {
                      debugPrint('next code: ${code}');
                      if (send) {
                        setState(() {
                          send = false;
                        });
                        bool check = await SirenRestServices().CheckCode(code);
                        if (check)
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CompletPage()),
                          );
                        else {
                          setState(() {
                            send = true;
                          });
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Wrong code."),
                          ));
                        }
                      }
                    },
                    child: Text('${AppLocalizations.of(context)!.nextButton}',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  void checkCode(String value) {
    if (value.length == 5)
      send = true;
    else
      send = false;

    if (refreshCode) send = false;

    setState(() {});
  }
}

// class PinCodeVerificationScreen extends StatefulWidget {
//   const PinCodeVerificationScreen({
//     Key? key,
//     this.phoneNumber,
//   }) : super(key: key);

//   final String? phoneNumber;

//   @override
//   State<PinCodeVerificationScreen> createState() =>
//       _PinCodeVerificationScreenState();
// }

// class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
//   TextEditingController textEditingController = TextEditingController();
//   // ..text = "123456";

//   // ignore: close_sinks
//   StreamController<ErrorAnimationType>? errorController;

//   bool hasError = false;
//   String currentText = "";
//   final formKey = GlobalKey<FormState>();

//   @override
//   void initState() {
//     errorController = StreamController<ErrorAnimationType>();
//     super.initState();
//   }

//   @override
//   void dispose() {
//     errorController!.close();

//     super.dispose();
//   }

//   // snackBar Widget
//   snackBar(String? message) {
//     return ScaffoldMessenger.of(context).showSnackBar(
//       SnackBar(
//         content: Text(message!),
//         duration: const Duration(seconds: 2),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: ColorsHandler.primary1,
//       body: GestureDetector(
//         onTap: () {},
//         child: SizedBox(
//           height: MediaQuery.of(context).size.height,
//           width: MediaQuery.of(context).size.width,
//           child: ListView(
//             children: <Widget>[
//               const SizedBox(height: 30),
//               SizedBox(
//                 height: MediaQuery.of(context).size.height / 3,
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.circular(30),
//                   child: Image.asset('assets/images/pno.png'),
//                 ),
//               ),
//               const SizedBox(height: 8),
//               const Padding(
//                 padding: EdgeInsets.symmetric(vertical: 8.0),
//                 child: Text(
//                   'Phone Number Verification',
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
//                   textAlign: TextAlign.center,
//                 ),
//               ),
//               Padding(
//                 padding:
//                     const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
//                 child: RichText(
//                   text: TextSpan(
//                     text: "Enter the code sent to ",
//                     children: [
//                       TextSpan(
//                         text: "${widget.phoneNumber}",
//                         style: const TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 15,
//                         ),
//                       ),
//                     ],
//                     style: const TextStyle(
//                       color: Colors.black54,
//                       fontSize: 15,
//                     ),
//                   ),
//                   textAlign: TextAlign.center,
//                 ),
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               Form(
//                 key: formKey,
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(
//                     vertical: 8.0,
//                     horizontal: 30,
//                   ),
//                   child: PinCodeTextField(
//                     appContext: context,
//                     pastedTextStyle: TextStyle(
//                       color: Colors.green.shade600,
//                       fontWeight: FontWeight.bold,
//                     ),
//                     length: 6,
//                     obscureText: true,
//                     obscuringCharacter: '*',
//                     obscuringWidget: const FlutterLogo(
//                       size: 24,
//                     ),
//                     blinkWhenObscuring: true,
//                     animationType: AnimationType.fade,
//                     validator: (v) {
//                       if (v!.length < 3) {
//                         return "I'm from validator";
//                       } else {
//                         return null;
//                       }
//                     },
//                     pinTheme: PinTheme(
//                       shape: PinCodeFieldShape.box,
//                       borderRadius: BorderRadius.circular(5),
//                       fieldHeight: 50,
//                       fieldWidth: 40,
//                       activeFillColor: Colors.white,
//                     ),
//                     cursorColor: Colors.black,
//                     animationDuration: const Duration(milliseconds: 300),
//                     enableActiveFill: true,
//                     errorAnimationController: errorController,
//                     controller: textEditingController,
//                     keyboardType: TextInputType.number,
//                     boxShadows: const [
//                       BoxShadow(
//                         offset: Offset(0, 1),
//                         color: Colors.black12,
//                         blurRadius: 10,
//                       )
//                     ],
//                     onCompleted: (v) {
//                       debugPrint("Completed");
//                     },
//                     // onTap: () {
//                     //   print("Pressed");
//                     // },
//                     onChanged: (value) {
//                       debugPrint(value);
//                       setState(() {
//                         currentText = value;
//                       });
//                     },
//                     beforeTextPaste: (text) {
//                       debugPrint("Allowing to paste $text");
//                       //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
//                       //but you can show anything you want here, like your pop up saying wrong paste format or etc
//                       return true;
//                     },
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 30.0),
//                 child: Text(
//                   hasError ? "*Please fill up all the cells properly" : "",
//                   style: const TextStyle(
//                     color: Colors.red,
//                     fontSize: 12,
//                     fontWeight: FontWeight.w400,
//                   ),
//                 ),
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   const Text(
//                     "Didn't receive the code? ",
//                     style: TextStyle(color: Colors.black54, fontSize: 15),
//                   ),
//                   TextButton(
//                     onPressed: () => snackBar("OTP resend!!"),
//                     child: const Text(
//                       "RESEND",
//                       style: TextStyle(
//                         color: Color(0xFF91D3B3),
//                         fontWeight: FontWeight.bold,
//                         fontSize: 16,
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//               const SizedBox(
//                 height: 14,
//               ),
//               Container(
//                 margin:
//                     const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30),
//                 child: ButtonTheme(
//                   height: 50,
//                   child: TextButton(
//                     onPressed: () {
//                       formKey.currentState!.validate();
//                       // conditions for validating
//                       if (currentText.length != 6 || currentText != "123456") {
//                         errorController!.add(ErrorAnimationType
//                             .shake); // Triggering error shake animation
//                         setState(() => hasError = true);
//                       } else {
//                         setState(
//                           () {
//                             hasError = false;
//                             snackBar("OTP Verified!!");
//                           },
//                         );
//                       }
//                     },
//                     child: Center(
//                       child: Text(
//                         "VERIFY".toUpperCase(),
//                         style: const TextStyle(
//                           color: Colors.white,
//                           fontSize: 18,
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 decoration: BoxDecoration(
//                     color: Colors.green.shade300,
//                     borderRadius: BorderRadius.circular(5),
//                     boxShadow: [
//                       BoxShadow(
//                           color: Colors.green.shade200,
//                           offset: const Offset(1, -2),
//                           blurRadius: 5),
//                       BoxShadow(
//                           color: Colors.green.shade200,
//                           offset: const Offset(-1, 2),
//                           blurRadius: 5)
//                     ]),
//               ),
//               const SizedBox(
//                 height: 16,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   Flexible(
//                     child: TextButton(
//                       child: const Text("Clear"),
//                       onPressed: () {
//                         textEditingController.clear();
//                       },
//                     ),
//                   ),
//                   Flexible(
//                     child: TextButton(
//                       child: const Text("Set Text"),
//                       onPressed: () {
//                         setState(() {
//                           textEditingController.text = "123456";
//                         });
//                       },
//                     ),
//                   ),
//                 ],
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
