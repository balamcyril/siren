import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:siren_flutter/flows/question_select_options.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/flows/observation_question.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/common_strings.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:siren_flutter/widgets/forms/text_control.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:uuid/uuid.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:dio/dio.dart';
import 'package:siren_flutter/screens/sync/spinner.dart';
import 'package:dotted_border/dotted_border.dart';
import 'dart:developer';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';

import '../utils/platform.dart';

import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/screens/details.dart';
import 'package:siren_flutter/screens/geo_map.dart';
import 'package:geocoding/geocoding.dart';
import 'observation_flow.dart';

class DetailsPatrolPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  PatrolData patrolData;
  Function refresh;

  DetailsPatrolPage({Key? key, required this.patrolData, required this.refresh})
      : super(key: key);
  @override
  _DetailsPatrolPageState createState() => _DetailsPatrolPageState();
}

class _DetailsPatrolPageState extends State<DetailsPatrolPage> {
  _DetailsPatrolPageState();

  late List<ObservationData> observationList = [];
  String imageDirectoryPath = "";
  final Map<String, String> _currentAddressList = {};

  @override
  void initState() {
    super.initState();
    loadObs();
  }

  void loadObs() async {
    imageDirectoryPath = await getMobileStoragePath();
    observationList = await SqfliteObservationManager.instance
        .getAllObservationsForPatrolProject(widget.patrolData.project_id,
            widget.patrolData.id as int, widget.patrolData.server_id, 0);

    debugPrint(
        'liste obs patrol: ${observationList.length} project_id:${widget.patrolData.project_id}  patrol_id:${widget.patrolData.id}');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          foregroundColor: ColorsHandler.primary1,
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.patrolDetails,
            style: titleMedium.merge(TextStyle(color: ColorsHandler.primary1)),
          ),
        ),
        body: Container(
            color: Colors.white,
            margin: const EdgeInsets.all(0.0),
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                          child: getTitleTextBox(),
                        ),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                '',
                                widget.patrolData.observer,
                                const Icon(
                                  Icons.person_outline_rounded,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                ))),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                          child: getTextBoxWithIcon(
                              ' Start',
                              getDateTimeString(
                                  widget.patrolData.start_timestamp),
                              const Icon(
                                Icons.access_time,
                                color: ColorsHandler.primary1,
                                size: 16.4,
                              )),
                        ),
                        if (widget.patrolData.end_timestamp != 0)
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                ' End',
                                getDateTimeString(
                                    widget.patrolData.end_timestamp),
                                const Icon(
                                  Icons.access_time,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                )),
                          ),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                " ${AppLocalizations.of(context)!.startGPS}",
                                "${AppLocalizations.of(context)!.lat}: ${widget.patrolData.start_latitude.toStringAsFixed(3)}, ${AppLocalizations.of(context)!.long}: ${widget.patrolData.start_longitude.toStringAsFixed(3)}",
                                const Icon(
                                  Icons.location_on_outlined,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                ))),
                        if (widget.patrolData.end_longitude != 0)
                          Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: getTextBoxWithIcon(
                                  " ${AppLocalizations.of(context)!.endGPS}",
                                  "${AppLocalizations.of(context)!.lat}: ${widget.patrolData.end_latitude.toStringAsFixed(3)}, ${AppLocalizations.of(context)!.long}: ${widget.patrolData.end_longitude.toStringAsFixed(3)}",
                                  const Icon(
                                    Icons.location_on_outlined,
                                    color: ColorsHandler.primary1,
                                    size: 16.4,
                                  ))),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                " ${AppLocalizations.of(context)!.totalPatrolTime}: ",
                                // widget.patrolData.timestamp.toString(),
                                widget.patrolData.timestamp,
                                const Icon(
                                  Icons.access_time,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                ))),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                " ${AppLocalizations.of(context)!.totalDistance}: ",
                                widget.patrolData.distance.toStringAsFixed(2) +
                                    " km",
                                const Icon(
                                  Icons.location_on_outlined,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                ))),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: getTextBoxWithIcon(
                                " ${AppLocalizations.of(context)!.obsRecorded}: ",
                                observationList.length.toString(),
                                const Icon(
                                  Icons.list,
                                  color: ColorsHandler.primary1,
                                  size: 16.4,
                                ))),
                      ],
                    ),
                  ),
                  if (0 < observationList.length)
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 20, 10, 6),
                      child: Text(
                          '${AppLocalizations.of(context)!.patrolName} ${widget.patrolData.id} ${AppLocalizations.of(context)!.observationName} ',
                          style: h2),
                    ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: ListView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          for (var i = 0; i < observationList.length; i++)
                            getObservationRow(context, i),
                          SizedBox(
                            height: 100,
                          )
                        ],
                      ))
                ],
              ),
            )));
  }

  String getDateTimeString(int time) {
    try {
      return DateFormat.yMMMd()
              .format(DateTime.fromMillisecondsSinceEpoch(time)) +
          " • " +
          DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(time));
    } catch (e) {
      return "${AppLocalizations.of(context)!.unknown}";
    }
  }

  Widget getTextBox(String text) {
    return Row(children: [
      Expanded(
          child: Text("" + text,
              style:
                  bodyMedium.merge(TextStyle(color: ColorsHandler.primary1))))
    ]);
  }

  Widget getTextBoxWithIcon(String title, String text, Icon icon) {
    return Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
      icon,
      Text(title + "  " + text,
          style: bodyMedium.merge(TextStyle(color: ColorsHandler.primary1)))
    ]);
  }

  Widget getTitleTextBox() {
    return Row(
      children: [
        Text(
            '${AppLocalizations.of(context)!.patrolName} ${widget.patrolData.server_id == -1 ? widget.patrolData.id : widget.patrolData.server_id} ',
            style: h2),
        widget.patrolData.status == PatrolStatus.unactif
            ? ImageIcon(
                AssetImage('assets/images/pno.png'),
                color: Colors.red,
              )
            : widget.patrolData.status == PatrolStatus.uploaded
                ? ImageIcon(AssetImage('assets/images/pok.png'),
                    color: Colors.green)
                : Container()
      ],
    );
  }

  Widget getObservationRow(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: GestureDetector(
          // When the child is tapped, show a snackbar.
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailsPage(
                        observationData: observationList[index],
                        refresh: () {
                          setState(() {});
                        },
                      )),
            );
          },
          child: Card(
            elevation: 0.3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: getImageContainer(index),
                  ),
                  Expanded(child: getInfoContainer(index)),
                  getActionsContainer(context, index),
                ],
              ),
            ),
          ),
        ));
  }

  void setCoordinatesString(index) async {
    try {
      if (_currentAddressList
          .containsKey(observationList[index].id.toString())) {
        return;
      }
      List<Placemark> placemarks = await placemarkFromCoordinates(
          observationList[index].latitude, observationList[index].longitude);

      Placemark place = placemarks[0];

      setState(() {
        _currentAddressList[observationList[index].id.toString()] =
            "${place.locality}, ${place.postalCode}, ${place.street}";
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  String getObservationType(int index) {
    return observationList[index].getObservationTypeString(context);
  }

  double textRowInfoSize = 12.0;
  double titleRowSize = 16.0;

  Widget getInfoContainer(int index) {
    String observationType = getObservationType(index);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(observationList[index].type);

    setCoordinatesString(index);
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(1, 9.41, 2, 0),
            child: Text(
              getObservationIdString(index),
              style:
                  bodySemibold.merge(TextStyle(color: ColorsHandler.primary1)),
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 1, top: 4),
              decoration: BoxDecoration(
                color: observationTypeColor.observationFillColor,
                borderRadius: BorderRadius.all(Radius.circular(24.0)),
              ),
              padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
              child: Text(
                observationType,
                style: caption.merge(TextStyle(
                    color: observationTypeColor.observationTextColor)),
              )),
          const Text(
            "",
          ),
          Padding(
              padding: const EdgeInsets.only(left: 1, bottom: 2),
              child: Text(
                "${AppLocalizations.of(context)!.beachName}: ${observationList[index].site}",
                style: smallTextMedium
                    .merge(TextStyle(color: ColorsHandler.primary1)),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 1, bottom: 6.41),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(
                  Icons.schedule,
                  size: textRowInfoSize,
                  color: ColorsHandler.primary1,
                ),
                Text(
                  " " + getDateString(index) + " • " + getTimeString(index),
                  style:
                      caption.merge(TextStyle(color: ColorsHandler.tertiary)),
                ),
              ],
            ),
          )
        ]);
  }

  String getDateString(index) {
    try {
      return DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(
          observationList[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getDuration(int time) {
    debugPrint("image time stamp : $time");
    try {
      return DateFormat('kk:mm')
          .format(DateTime.fromMillisecondsSinceEpoch(time));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getTimeString(index) {
    try {
      return DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(
          observationList[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getObservationIdString(index) {
    String textToShow = "";

    if (observationList[index].nest_id.isNotEmpty) {
      textToShow = AppLocalizations.of(context)!.nestID +
          " ${observationList[index].nest_id}";
    } else {
      textToShow = observationList[index].server_id == -1
          ? "${AppLocalizations.of(context)!.obs} ${observationList[index].id}"
          : "${AppLocalizations.of(context)!.obs} ${observationList[index].server_id}";
    }
    if (observationList[index].status == Status.draft) {
      textToShow = textToShow + " (DRAFT)";
    }
    return textToShow;
  }

  Widget getImageContainer(index) {
    late Widget imageContainer;
    String fullPath = "";
    const double imageHeight = 110.0;
    const double imageWidth = 100.0;

    if (observationList[index].mediaList.isNotEmpty) {
      fullPath = imageDirectoryPath + observationList[index].mediaList[0].path;
      debugPrint("image index detail patrol page $index path $fullPath");
    }
    try {
      // if path contains https, this is an observation that was imported without the images

      imageContainer = Container(
          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
          child: (observationList[index].hasMediaToShow())
              ? ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.file(
                    File(fullPath),
                    height: imageHeight,
                    width: imageWidth,
                    fit: BoxFit.cover,
                    errorBuilder: (BuildContext context, Object exception,
                        StackTrace? stackTrace) {
                      debugPrint("$exception");
                      debugPrint("$stackTrace");
                      return getDefaultImage(100, 110);
                    },
                  ))
              : getDefaultImage(100, 110));
    } catch (e) {
      debugPrint(
          "image index error detail patrol page $index obs ${observationList[index].mediaList[0]}");
      imageContainer = getDefaultImage(100, 110);
    }
    return imageContainer;
  }

  Widget getActionsContainer(BuildContext context, int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        IconButton(
          onPressed: () {
            showActionBottomSheet(context, index);
          },
          icon: const Icon(
            Icons.more_horiz,
          ),
          color: ColorsHandler.primary1,
        )
      ],
    );
  }

  void showActionBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: getBottomSheetChildren(context, index));
        });
  }

  late BuildContext dialogContext;
  dynamic showActionDialog(String text) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: Container(
            margin: const EdgeInsets.all(20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new CircularProgressIndicator(),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: new Text(text))),
              ],
            ),
          ),
        );
      },
    );
  }

  void editObservation(int index) async {
    try {
      ObservationData observationData =
          userSessionManager.observationlist[index];

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ObservationFlowWidget(
                    observationType: observationData.type,
                    observationId: observationData.id as int,
                  )));

      debugPrint(
          "OBservation ${userSessionManager.observationlist[index].id} edit server id: ${userSessionManager.observationlist[index].server_id}");
      // toast message
      setState(() {
        loadObs();
      });
    } catch (e) {
      debugPrint("editObservation exception $e");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.editFailure),
      ));
    }
  }

  void hardDeleteObservation(int index) async {
    // TODO delete answers
    userSessionManager.observationlist[index].mediaList.forEach((media) async {
      await SqfliteMediaManager.instance.delete(media.media_id as int);
    });
    await SqfliteObservationManager.instance
        .hardDelete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} permanently deleted");
    // toast message
    setState(() {
      // Navigator.pop(dialogContext);
      Navigator.pop(context);
      loadObs();
    });
  }

  void restoreObservation(int index) async {
    userSessionManager.observationlist[index].status =
        userSessionManager.observationlist[index].server_id == -1
            ? Status.draft
            : Status.uploaded;
    await SqfliteObservationManager.instance
        .update(userSessionManager.observationlist[index]);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} restored id: ${userSessionManager.observationlist[index].server_id}");
    // toast message
    setState(() {
      loadObs();
    });
  }

  void deleteObservation(int index) async {
    await SqfliteObservationManager.instance
        .delete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} deleted idii: ${userSessionManager.observationlist[index].server_id}");
    setState(() {
      Navigator.pop(dialogContext);
      Navigator.pop(context);
      loadObs();
    });
  }

  List<Widget> getBottomSheetChildren(BuildContext context, int index) {
    dialogContext = context;
    switch (userSessionManager.observationlist[index].status) {
      case Status.deleted:
        return <Widget>[];
      case Status.uploaded:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.draft:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.edit),
            title: Text(AppLocalizations.of(context)!.edit),
            onTap: () {
              Navigator.pop(context);
              editObservation(index);
            },
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.validated:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              hardDeleteObservation(index);
            },
          ),
        ];
      case Status.uploading:
        return <Widget>[];
    }
  }

  void goToMap(int index) {
    // go to All sites
    userSessionManager.lastSelectedSite = "";
    userSessionManager
        .showObservation(userSessionManager.observationlist[index]);
    Navigator.pop(context);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => MainScreenStatefulWidget()));
  }
}
