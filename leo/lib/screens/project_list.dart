import 'package:flutter/material.dart';
import 'package:siren_flutter/data/project_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/beaches.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';

import 'account_info.dart';
import 'observations_type.dart';

class ProjectListPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  ProjectListPage(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _ProjectListPageState createState() =>
      _ProjectListPageState(mainScreenStatefulWidget);
}

class _ProjectListPageState extends State<ProjectListPage> {
  _ProjectListPageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  late MainScreenStatefulWidget mainScreenStatefulWidget;
  String getUserData() {
    if (userSessionManager.userDataAvailable) {
      return "${AppLocalizations.of(context)!.user}: " +
          userSessionManager.userData.username +
          "\n${AppLocalizations.of(context)!.project}: " +
          userSessionManager.userData.currentProjectData.name;
    }
    return "No user data available, login required";
  }

  @override
  void initState() {
    super.initState();
  }

  String getSiteData() {
    String sites = AppLocalizations.of(context)!.site_beaches + "\n\n";

    userSessionManager.userData.currentProjectData.siteDataList.forEach((item) {
      sites += item.toString() + "\n";
    });
    return sites.substring(0, sites.length - 1);
  }

  String getLocation() {
    String projectLocation =
        AppLocalizations.of(context)!.project_location + "\n\n";
    return projectLocation + " ${userSessionManager.userData.currentLocation}";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: ColorsHandler.primary1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          }),
          title: Text(
            "${AppLocalizations.of(context)!.projectList}",
            style: const TextStyle(color: ColorsHandler.primary1),
          ),
        ),
        backgroundColor: Colors.white,
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(),
              ListView(shrinkWrap: true, children: getProjectData()
                  // [
                  // ListTile(
                  //   title: Text(AppLocalizations.of(context)!.accountInfo),
                  //   textColor: ColorsHandler.primary1,
                  //   onTap: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) => AccountInfoPage()),
                  //     );
                  //   },
                  // ),
                  // new Divider(
                  //   height: 2.0,
                  //   indent: 1.0,
                  //   color: ColorsHandler.tertiary,
                  // ),
                  // ],
                  ),
            ],
          ),
        ));
  }

  Future<Widget> showAboutDialog() {
    return aboutDialog(context);
  }

  Widget getLine() {
    return const Divider(
      height: 20,
      thickness: 1,
      indent: 20,
      endIndent: 0,
      color: Colors.black,
    );
  }

  List<Widget> getProjectData() {
    List<Widget> projects = [];

    userSessionManager.userData.projects.forEach((item) {
      // projects.add(getField(context, "${item.name} (id: ${item.id})"));

      projects.add(ListTile(
        title: Text(item.name),
        textColor: userSessionManager.userData.currentProjectData.id == item.id
            ? Colors.blue
            : ColorsHandler.primary1,
        onTap: () {
          debugPrint("---- value ${item.id} ");
          userSessionManager.userData.currentProjectData = item;
          setState(() {});
        },
      ));
      projects.add(new Divider(
        height: 2.0,
        indent: 1.0,
        color: ColorsHandler.tertiary,
      ));
      // projects.add(SizedBox(height: 10));
    });
    projects.add(
      ListTile(
        title: Text(
          "Refresh list",
          style: TextStyle(fontSize: 16, color: ColorsHandler.primary1),
        ),
        textColor: ColorsHandler.primary1,
        leading: Icon(Icons.refresh),
        iconColor: ColorsHandler.primary1,
        onTap: () => getProjectList(),
      ),
    );
    // projects.add(getRefreshButton(context));
    return projects;
  }

  Widget getRefreshButton(BuildContext context) {
    return IconButton(
        onPressed: () {
          userSessionManager.refreshSites().then((sites) {
            userSessionManager.userData.currentProjectData.siteDataList = sites;
            debugPrint(sites.toString());
            setState(() {});
          });
        },
        icon: const Icon(Icons.refresh));
  }

  getProjectList() async {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Refreshing..."),
    ));
    try {
      List<ProjectData> projects = await SirenRestServices().refreshData();
      userSessionManager.userData.projects = projects;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Refresh ok."),
      ));
    } catch (e) {
      debugPrint("Error refresh${e} ");
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Error refresh"),
      ));
    }
  }
}
