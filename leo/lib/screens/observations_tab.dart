import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/details.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/utils/fonts.dart';
import '../data/observation_data.dart';
import '../utils/platform.dart';
import 'observation_flow.dart';

class ObservationsTabPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  bool refresh = true;

  ObservationsTabPage(MainScreenStatefulWidget parent, {Key? key})
      : super(key: key) {
    mainScreenStatefulWidget = parent;
  }

  @override
  _ObservationsTabPageState createState() =>
      _ObservationsTabPageState(mainScreenStatefulWidget);
}

class _ObservationsTabPageState extends State<ObservationsTabPage> {
  final searchFieldText = TextEditingController();
  List<ObservationData> observations = [];
  List buttonLabel = [];
  int lastIndexToUpload = 0;
  int obsCount = 0;

  final Map<String, String> _currentAddressList = {};
  String imageDirectoryPath = "";
  final List<bool> isChipSelected = [false, true, false, false, false];
  final List<bool> obsDraftTrashExiste = [false, false, false, false, false];

  late MainScreenStatefulWidget mainScreenStatefulWidget;

  _ObservationsTabPageState(parent) {
    mainScreenStatefulWidget = parent;
    debugPrint("Launch observations tab");
    initObservations();
  }

  @override
  void didUpdateWidget(ObservationsTabPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  void initObservations() async {
    try {
      MainScreenState state = mainScreenStatefulWidget.getState();
      userSessionManager.showObservationOnMap = false;
      imageDirectoryPath = await getMobileStoragePath();

      for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
        if (isChipSelected[indexBtn]) {
          userSessionManager.observationlist = await loadObservations(indexBtn);

          if ((indexBtn == 1) || (indexBtn == 4)) {
            if (0 >= userSessionManager.observationlist.length) {
              state.hideSelect();
              obsExistOnDB(indexBtn, false);
            } else {
              state.showHideSelect(indexBtn);
              obsExistOnDB(indexBtn, true);
            }
          }
          debugPrint(
              "Observation tab init observations tab ${indexBtn} count ${userSessionManager.observationlist.length}");
          setState(() {});
        }
      }
      obsCount = userSessionManager.observationlist.length;
    } catch (e, stackTrace) {
      debugPrint(
          "Observation tab - init observations Exception ${e.toString()}");
      debugPrint("Stack trace: ${stackTrace}");
    }
  }

  void obsExistOnDB(index, value) {
    obsDraftTrashExiste[index] = value;
  }

  int getSelectedIndex() {
    for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
      if (isChipSelected[indexBtn]) {
        return indexBtn;
      }
    }
    throw Exception("Invalid chip selection index in observation list");
  }

  Future<List<ObservationData>> loadObservations(index) async {
    debugPrint("Load observations tab folder ${index} (0-All, 1-Draft...)");
    switch (index) {
      case 0:
        return await SqfliteObservationManager.instance.getAllObservations();
      case 1:
        return await SqfliteObservationManager.instance.getDraftObservations();
      case 2:
        return await SqfliteObservationManager.instance
            .getUploadedObservations();
      case 3:
        return await SqfliteObservationManager.instance
            .getValidatedObservations();
      case 4:
        return await SqfliteObservationManager.instance
            .getDeletedObservations();
    }
    debugPrint(
        "Load observations throw exception - Invalid index while loading observations $index");
    throw Exception("Invalid index while loading observations $index");
  }

  int getObservationCount() {
    return userSessionManager.observationlist.length;
  }

  @override
  Widget build(BuildContext context) {
    if (buttonLabel.isEmpty) {
      buttonLabel.add(AppLocalizations.of(context)!.draft);
      buttonLabel.add(AppLocalizations.of(context)!.uploaded);
      buttonLabel.add(AppLocalizations.of(context)!.validated);
      buttonLabel.add(AppLocalizations.of(context)!.trash);
    }

    return Container(
        color: ColorsHandler.tertiaryOp7,
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Column(children: [
          // getTitle(),
          getSearchField(),
          const SizedBox(
            height: 8,
          ),
          getChipsRow(),
          Expanded(
              child: ListView(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
            children: [
              for (var i = 0; i < getObservationCount(); i++)
                getObservationRow(context, i),
              SizedBox(
                height: 100,
              )
            ],
          ))
        ]));
  }

  Widget getObservationRow(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: GestureDetector(
          // When the child is tapped, show a snackbar.
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailsPage(
                        observationData:
                            userSessionManager.observationlist[index],
                        refresh: () {
                          setState(() {});
                        },
                      )),
            );
          },
          child: Card(
            elevation: 0.3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: getImageContainer(index),
                  ),
                  Expanded(child: getInfoContainer(index)),
                  getActionsContainer(context, index),
                ],
              ),
            ),
          ),
        ));
  }

  Widget test() {
    print('print content =======');
    return Container();
  }

  String getObservationType(int index) {
    return userSessionManager.observationlist[index]
        .getObservationTypeString(context);
  }

  String getDateString(index) {
    try {
      return DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(
          userSessionManager.observationlist[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getTimeString(index) {
    try {
      return DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(
          userSessionManager.observationlist[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  Widget getImageContainer(index) {
    late Widget imageContainer;
    String fullPath = "";
    const double imageHeight = 110.0;
    const double imageWidth = 100.0;

    if (userSessionManager.observationlist[index].mediaList.isNotEmpty) {
      fullPath = imageDirectoryPath +
          userSessionManager.observationlist[index].mediaList[0].path;
      // debugPrint("image index $index path $fullPath");
    }
    try {
      imageContainer = Container(
          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
          child: (userSessionManager.observationlist[index].hasMediaToShow())
              ? ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.file(
                    File(fullPath),
                    height: imageHeight,
                    width: imageWidth,
                    fit: BoxFit.cover,
                    errorBuilder: (BuildContext context, Object exception,
                        StackTrace? stackTrace) {
                      debugPrint("$exception");
                      debugPrint("$stackTrace");
                      return getDefaultImage(100, 110);
                    },
                  ))
              : getDefaultImage(100, 110));
    } catch (e) {
      debugPrint(
          "image index $index obs ${userSessionManager.observationlist[index].mediaList[0]}");
      imageContainer = getDefaultImage(100, 110);
    }
    return imageContainer;
  }

  String getObservationIdString(index) {
    bool showStatus = getSelectedIndex() == 0 ? true : false;
    String textToShow = "";

    if (userSessionManager.observationlist[index].nest_id.isNotEmpty) {
      textToShow = AppLocalizations.of(context)!.nestID +
          " ${userSessionManager.observationlist[index].nest_id}";
    } else {
      textToShow = userSessionManager.observationlist[index].server_id == -1
          ? "${AppLocalizations.of(context)!.obs} ${userSessionManager.observationlist[index].id}"
          : "${AppLocalizations.of(context)!.obs} ${userSessionManager.observationlist[index].server_id}";
    }
    if (showStatus &&
        userSessionManager.observationlist[index].status == Status.draft) {
      textToShow = textToShow + " (DRAFT)";
    }
    return textToShow;
  }

  void setCoordinatesString(index) async {
    try {
      if (_currentAddressList.containsKey(
          userSessionManager.observationlist[index].id.toString())) {
        return;
      }
      List<Placemark> placemarks = await placemarkFromCoordinates(
          userSessionManager.observationlist[index].latitude,
          userSessionManager.observationlist[index].longitude);

      Placemark place = placemarks[0];

      setState(() {
        _currentAddressList[
                userSessionManager.observationlist[index].id.toString()] =
            "${place.locality}, ${place.postalCode}, ${place.street}";
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  String getCurrentAddress(index) {
    if (_currentAddressList
        .containsKey(userSessionManager.observationlist[index].id.toString())) {
      return _currentAddressList[
          userSessionManager.observationlist[index].id.toString()] as String;
    } else {
      return "${AppLocalizations.of(context)!.lat} ${userSessionManager.observationlist[index].latitude.toStringAsFixed(3)} ${AppLocalizations.of(context)!.long} ${userSessionManager.observationlist[index].longitude.toStringAsFixed(3)} ";
    }
  }

  double textRowInfoSize = 12.0;
  double titleRowSize = 16.0;

  Widget getSearchField() {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      alignment: Alignment.center,
      child: Container(
        width: window.physicalSize.width / window.devicePixelRatio - 20,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        child: TextField(
          style: bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
          decoration: InputDecoration(
              suffixIcon: Icon(Icons.search),
              hintText: AppLocalizations.of(context)!.searchObservations,
              contentPadding: EdgeInsets.all(15),
              border: InputBorder.none),
          controller: searchFieldText,
          onChanged: (value) {
            debugPrint(value);
            searchObservations(value);
          },
        ),
      ),
    );
  }

  Widget getTitle() {
    return Container(
      alignment: Alignment.topLeft,
      padding: const EdgeInsets.fromLTRB(12, 5, 20, 5),
      child: Text(AppLocalizations.of(context)!.observationsNavIcon, style: h2),
    );
  }

  Widget getInfoContainer(int index) {
    String observationType = getObservationType(index);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(
            userSessionManager.observationlist[index].type);

    setCoordinatesString(index);
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(1, 9.41, 2, 0),
            child: Text(
              getObservationIdString(index),
              style:
                  bodySemibold.merge(TextStyle(color: ColorsHandler.primary1)),
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 1, top: 4),
              decoration: BoxDecoration(
                color: observationTypeColor.observationFillColor,
                borderRadius: BorderRadius.all(Radius.circular(24.0)),
              ),
              padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
              child: Text(
                observationType,
                style: caption.merge(TextStyle(
                    color: observationTypeColor.observationTextColor)),
              )),
          const Text(
            "",
          ),
          Padding(
              padding: const EdgeInsets.only(left: 1, bottom: 2),
              child: Text(
                "${AppLocalizations.of(context)!.beachName}: ${userSessionManager.observationlist[index].site}",
                style: smallTextMedium
                    .merge(TextStyle(color: ColorsHandler.primary1)),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 1, bottom: 6.41),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(
                  Icons.schedule,
                  size: textRowInfoSize,
                  color: ColorsHandler.primary1,
                ),
                Text(
                  " " + getDateString(index) + " • " + getTimeString(index),
                  style:
                      caption.merge(TextStyle(color: ColorsHandler.tertiary)),
                ),
              ],
            ),
          )
        ]);
  }

  Widget getActionsContainer(BuildContext context, int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        IconButton(
          onPressed: () {
            showActionBottomSheet(context, index);
          },
          icon: const Icon(
            Icons.more_horiz,
          ),
          color: ColorsHandler.primary1,
        )
      ],
    );
  }

  String getButtonLabel(index) {
    return buttonLabel[index];
  }

  Color getObservationTypeBackgoundColor(bool pressed, String label) {
    if (pressed) return Color.fromRGBO(0, 0, 128, 0.5);
    return Color.fromRGBO(12, 38, 38, 0.1);
  }

// does not work
  Color getTextColor(bool pressed, String label) {
    if (pressed) return Colors.white;
    return Colors.black;
  }

  void chipOnSelect(chipIdx, value) {
    searchFieldText.clear();
    setState(() {
      for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
        if (indexBtn != chipIdx) {
          isChipSelected[indexBtn] = !value;
        }
      }
      initObservations();
    });
  }

  Widget getChipsRow() {
    MainScreenState state = mainScreenStatefulWidget.getState();
    TextStyle chipSelectedStyle = TextStyle(
        fontSize: smallTextBold.fontSize,
        fontFamily: smallTextBold.fontFamily,
        fontWeight: smallTextBold.fontWeight,
        letterSpacing: smallTextBold.letterSpacing,
        color: Colors.white);
    TextStyle chipUnselectedStyle = TextStyle(
      fontSize: smallTextBold.fontSize,
      fontFamily: smallTextBold.fontFamily,
      fontWeight: smallTextBold.fontWeight,
      letterSpacing: smallTextBold.letterSpacing,
      color: ColorsHandler.primary1,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        ChoiceChip(
          label: Text(AppLocalizations.of(context)!.all),
          selected: isChipSelected[0],
          onSelected: (value) {
            isChipSelected[0] = value;
            // state.hideSelect();
            // state.onSection(1);
            state.showHideSelect(0);
            chipOnSelect(0, value);
          },
          labelStyle:
              isChipSelected[0] ? chipSelectedStyle : chipUnselectedStyle,
          backgroundColor: ColorsHandler.tertiaryOp15,
          selectedColor: ColorsHandler.primary1,
        ),
        ChoiceChip(
            selected: isChipSelected[1],
            label: Text(
              getButtonLabel(0),
              style:
                  isChipSelected[1] ? chipSelectedStyle : chipUnselectedStyle,
            ),
            onSelected: (value) {
              isChipSelected[1] = value;
              if (obsDraftTrashExiste[1])
                state.showHideSelect(1);
              else
                state.hideSelect();
              chipOnSelect(1, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1),
        ChoiceChip(
            selected: isChipSelected[2],
            onSelected: (value) {
              isChipSelected[2] = value;
              state.showHideSelect(2);
              chipOnSelect(2, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(1),
              style:
                  isChipSelected[2] ? chipSelectedStyle : chipUnselectedStyle,
            )),
        ChoiceChip(
            selected: isChipSelected[3],
            onSelected: (value) {
              isChipSelected[3] = value;
              state.showHideSelect(3);
              chipOnSelect(3, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(2),
              style:
                  isChipSelected[3] ? chipSelectedStyle : chipUnselectedStyle,
            )),
        ChoiceChip(
            selected: isChipSelected[4],
            onSelected: (value) {
              isChipSelected[4] = value;
              // state.onTrashSection();
              // state.onSection(5);
              if (obsDraftTrashExiste[4])
                state.showHideSelect(4);
              else
                state.hideSelect();
              chipOnSelect(4, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(3),
              style:
                  isChipSelected[4] ? chipSelectedStyle : chipUnselectedStyle,
            )),
      ],
    );
  }

  void showActionBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: getBottomSheetChildren(context, index));
        });
  }

  List<Widget> getBottomSheetChildren(BuildContext context, int index) {
    dialogContext = context;
    switch (userSessionManager.observationlist[index].status) {
      case Status.deleted:
        return <Widget>[
          ListTile(
            leading: Image.asset(
              'assets/images/restoreno.png',
              width: 22,
              height: 22,
            ),
            title: Text(AppLocalizations.of(context)!.restore),
            onTap: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                    "${AppLocalizations.of(context)!.restoringObservation}"),
              ));
              restoreObservation(index);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.delete_forever),
            title: Text(AppLocalizations.of(context)!.permanentlyDelete),
            onTap: () {
              showActionDialog(AppLocalizations.of(context)!.permanentlyDelete);
              hardDeleteObservation(index);
            },
          ),
        ];
      case Status.uploaded:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.draft:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.edit),
            title: Text(AppLocalizations.of(context)!.edit),
            onTap: () {
              Navigator.pop(context);
              editObservation(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.upload),
            title: Text(AppLocalizations.of(context)!.upload),
            onTap: () {
              Navigator.pop(dialogContext);
              if (!userSessionManager.onUpload) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(AppLocalizations.of(context)!.uploadingServer),
                ));
                uploadObservation(index);
              } else
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("${AppLocalizations.of(context)!.uploading}"),
                ));
            },
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.validated:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              hardDeleteObservation(index);
            },
          ),
        ];
      case Status.uploading:
        return <Widget>[];
    }
  }

  void editObservation(int index) async {
    try {
      ObservationData observationData =
          userSessionManager.observationlist[index];

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ObservationFlowWidget(
                    observationType: observationData.type,
                    observationId: observationData.id as int,
                  )));

      debugPrint(
          "OBservation ${userSessionManager.observationlist[index].id} edit server id: ${userSessionManager.observationlist[index].server_id}");
      // toast message
      setState(() {
        initObservations();
      });
    } catch (e) {
      debugPrint("editObservation exception $e");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.editFailure),
      ));
    }
  }

  void launch_upload(obsToUpload) async {
    userSessionManager.onUpload = true;
    userSessionManager.observationOnUploaded = [];
    userSessionManager.observationOnUploaded.add(obsToUpload);

    MainScreenState state = mainScreenStatefulWidget.getState();
    state.setState(() {});

    debugPrint(
        "OBservation to upload ${userSessionManager.observationOnUploaded} ");
    state.run_upload(userSessionManager.observationOnUploaded.length - 1, -1);
  }

  void uploadObservation(int index) async {
    try {
      // load QA before uploading
      userSessionManager.observationlist[index].answerList =
          await SqfliteAnswerManager.instance
              .get(userSessionManager.observationlist[index].id as int);
      setState(() {
        initObservations();
      });

      // Submit
      launch_upload(userSessionManager.observationlist[index]);
      debugPrint(
          "OBservation ${userSessionManager.observationlist[index].id} uploaded server id: ${userSessionManager.observationlist[index].server_id}");
    } catch (e) {
      debugPrint("error observation tab upload observations exception $e");
      userSessionManager.observationlist[index].status = Status.draft;
      await SqfliteObservationManager.instance
          .update(userSessionManager.observationlist[index]);

      setState(() {
        initObservations();
      });

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.uploadFailure),
      ));
    }
  }

  void deleteObservation(int index) async {
    await SqfliteObservationManager.instance
        .delete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} deleted id: ${userSessionManager.observationlist[index].server_id}");
    setState(() {
      Navigator.pop(dialogContext);
      Navigator.pop(context);
      initObservations();
    });
  }

  void restoreObservation(int index) async {
    userSessionManager.observationlist[index].status =
        userSessionManager.observationlist[index].server_id == -1
            ? Status.draft
            : Status.uploaded;
    await SqfliteObservationManager.instance
        .update(userSessionManager.observationlist[index]);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} restored id: ${userSessionManager.observationlist[index].server_id}");
    // toast message
    setState(() {
      initObservations();
    });
  }

  void hardDeleteObservation(int index) async {
    // TODO delete answers
    userSessionManager.observationlist[index].mediaList.forEach((media) async {
      await SqfliteMediaManager.instance.delete(media.media_id as int);
    });
    await SqfliteObservationManager.instance
        .hardDelete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} permanently deleted");
    // toast message
    setState(() {
      Navigator.pop(dialogContext);
      Navigator.pop(context);
      initObservations();
    });
  }

  void searchObservations(searchText) async {
    bool isTrash = getSelectedIndex() == 3 ? true : false;
    userSessionManager.observationlist = await SqfliteObservationManager
        .instance
        .searchObservations(searchText, trash: isTrash);
    setState(() {});
  }

  void goToMap(int index) {
    // go to All sites
    userSessionManager.lastSelectedSite = "";
    userSessionManager
        .showObservation(userSessionManager.observationlist[index]);
    Navigator.pop(context);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => MainScreenStatefulWidget()));
  }

  late BuildContext dialogContext;
  dynamic showActionDialog(String text) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: Container(
            margin: const EdgeInsets.all(20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new CircularProgressIndicator(),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: new Text(text))),
              ],
            ),
          ),
        );
      },
    );
  }
}
