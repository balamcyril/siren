import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';

class BeachesPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  BeachesPage({Key? key}) : super(key: key) {}
  @override
  _BeachesPageState createState() => _BeachesPageState();
}

class _BeachesPageState extends State<BeachesPage> {
  @override
  void initState() {
    super.initState();
  }

  List<Widget> getSiteData() {
    List<Widget> sites = [];

    userSessionManager.userData.currentProjectData.siteDataList.forEach((item) {
      sites.add(getField(context, "${item.name} (id: ${item.id})"));
      sites.add(SizedBox(height: 10));
    });
    sites.add(getRefreshButton(context));
    return sites;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: ColorsHandler.primary1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          }),
          title: Text(
            "${AppLocalizations.of(context)!.site_beaches}",
            style: const TextStyle(color: ColorsHandler.primary1),
          ),
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 50, bottom: 0, left: 15, right: 15),
              padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: getSiteData()),
            ),
          ],
        )));
  }

  Future<Widget> showLanguageDialog() {
    return showAlertDialog(
        context,
        AppLocalizations.of(context)!.languageAccountInfoDialogTitle,
        AppLocalizations.of(context)!.languageAccountInfoDialogText);
  }

  Widget getField(BuildContext context, site) {
    return TextField(
      readOnly: true,
      controller: TextEditingController(text: site),
      decoration: InputDecoration(
        labelStyle: TextStyle(fontSize: 14),
        labelText: AppLocalizations.of(context)!.beachName,
      ),
      autofocus: false,
    );
  }

  Widget getRefreshButton(BuildContext context) {
    return IconButton(
        onPressed: () {
          userSessionManager.refreshSites().then((sites) {
            userSessionManager.userData.currentProjectData.siteDataList = sites;
            debugPrint(sites.toString());
            setState(() {});
          });
        },
        icon: const Icon(Icons.refresh));
  }

  Widget getLine() {
    return const Divider(
      height: 20,
      thickness: 1,
      indent: 20,
      endIndent: 0,
      color: Colors.black,
    );
  }
}
