import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';

class AccountInfoPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  AccountInfoPage({Key? key}) : super(key: key) {}
  @override
  _AccountInfoPagePageState createState() => _AccountInfoPagePageState();
}

class _AccountInfoPagePageState extends State<AccountInfoPage> {
  _AccountInfoPagePageState() {}

  HashMap? getInitialUserData() {
    if (userSessionManager.userDataAvailable) {
      HashMap accountInfo = new HashMap();
      accountInfo["name"] = userSessionManager.userData.nameAndSurname;
      accountInfo["projectName"] =
          userSessionManager.userData.currentProjectData.name;
      accountInfo["email"] = userSessionManager.userData.email;
      accountInfo["role"] = userSessionManager.userData.roleName;
      accountInfo["phone"] = userSessionManager.userData.phone;
      accountInfo["loginResponse"] =
          userSessionManager.userData.loginResponse.toString();
      return accountInfo;
    }
    return null;
  }

  String getUserData() {
    if (userSessionManager.userDataAvailable) {
      return "${AppLocalizations.of(context)!.user}: " +
          userSessionManager.userData.username +
          "\n${AppLocalizations.of(context)!.project}: " +
          userSessionManager.userData.currentProjectData.name;
    }
    return "No user data available, login required";
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HashMap? initialData = getInitialUserData();

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: ColorsHandler.primary1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          }),
          title: Text(
            "${AppLocalizations.of(context)!.accountInfo}",
            style: const TextStyle(color: ColorsHandler.primary1),
          ),
        ),
        body: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 50, bottom: 0, left: 15, right: 15),
              padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextField(
                      readOnly: true,
                      controller:
                          TextEditingController(text: initialData!["name"]),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(fontSize: 14),
                        labelText: AppLocalizations.of(context)!.name,
                      ),
                      autofocus: false,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                      readOnly: true,
                      controller:
                          TextEditingController(text: initialData["role"]),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(fontSize: 14),
                        labelText: AppLocalizations.of(context)!.role,
                      ),
                      autofocus: false,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                      readOnly: true,
                      controller: TextEditingController(
                          text: initialData["projectName"]),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(fontSize: 14),
                        labelText: AppLocalizations.of(context)!.projectName,
                      ),
                      autofocus: false,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // DropdownButtonFormField(
                    //   // style: titleMedium,
                    //   decoration: InputDecoration(
                    //       // filled: true,
                    //       labelText: AppLocalizations.of(context)!.projectName,
                    //       hintStyle: TextStyle(color: Colors.grey[800]),
                    //       hintText: '',
                    //       fillColor: Colors.white70),
                    //   // InputDecoration(
                    //   //   labelStyle: bodyMedium
                    //   //       .merge(TextStyle(color: ColorsHandler.tertiary)),
                    //   //   labelText: "ici",
                    //   //   floatingLabelBehavior: FloatingLabelBehavior.auto,
                    //   //   hintText: "",
                    //   //   prefixText: "  ",
                    //   //   enabledBorder: UnderlineInputBorder(
                    //   //     borderSide: BorderSide(color: ColorsHandler.tertiary),
                    //   //   ),
                    //   //   focusedBorder: UnderlineInputBorder(
                    //   //     borderSide:
                    //   //         BorderSide(color: ColorsHandler.primary1, width: 2),
                    //   //   ),
                    //   // ),
                    //   items:
                    //       userSessionManager.userData.projects.map((var val) {
                    //     return DropdownMenuItem(
                    //       value: val.id.toString(),
                    //       child: Text(val.name),
                    //     );
                    //   }).toList(),
                    //   value: userSessionManager.userData.currentProjectData.id
                    //       .toString(),
                    //   onChanged: (val) {
                    //     print('change val:');
                    //     print(val);
                    //     userSessionManager.userData.currentProjectData.id =
                    //         int.parse(val.toString());
                    //     setState(
                    //       () {},
                    //     );
                    //   },
                    //   validator: (value) {
                    //     return null;
                    //   },
                    // ),
                  ]),
            ),
            Container(
                margin: const EdgeInsets.only(
                    top: 25, bottom: 15, left: 15, right: 15),
                padding: const EdgeInsets.only(bottom: 25, left: 15, right: 15),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  children: [
                    TextField(
                      readOnly: true,
                      controller:
                          TextEditingController(text: initialData["email"]),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(fontSize: 14),
                        labelText: AppLocalizations.of(context)!.email,
                      ),
                      autofocus: false,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                      readOnly: true,
                      controller:
                          TextEditingController(text: initialData["phone"]),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(fontSize: 14),
                        labelText: AppLocalizations.of(context)!.phone,
                      ),
                      autofocus: false,
                    ),
                  ],
                )),
            TextButton(
              child: Text(AppLocalizations.of(context)!.language,
                  style: TextStyle(fontSize: 16, color: Colors.blue.shade900)),
              onPressed: () {
                showLanguageDialog();
              },
            ),
          ],
        ));
  }

  Future<Widget> showLanguageDialog() {
    return showAlertDialog(
        context,
        AppLocalizations.of(context)!.languageAccountInfoDialogTitle,
        AppLocalizations.of(context)!.languageAccountInfoDialogText);
  }

  Widget getLine() {
    return const Divider(
      height: 20,
      thickness: 1,
      indent: 20,
      endIndent: 0,
      color: Colors.black,
    );
  }
}
