import 'package:flutter/material.dart';
import 'package:siren_flutter/data/answer_data.dart';
import 'package:siren_flutter/data/media_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/flows/observation_question_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:archive/archive_io.dart';
import 'dart:io';

class localDBObservationManager {
  void create(BuildContext context, ObservationData observationData,
      {bool showMessage = true}) async {
    debugPrint("Observation saved - creerereerererrrere=========");
    try {
      fillExpectedFieldFromAnswers(context, observationData);

      // Add observations to DB observation table
      int observationId =
          await SqfliteObservationManager.instance.create(observationData);
      debugPrint("Observation saved - id - $observationId");

      // Add answers to DB answers table
      for (var answer in observationData.answerList) {
        answer.observationId = observationId;
        int answerId = await SqfliteAnswerManager.instance.create(answer);
        debugPrint("Answer saved ${answer.toString()}");
      }

      debugPrint("Nir: Observation a cree - id - $observationData");

      // Add media to DB media table
      for (var mediaPath in observationData.mediaList) {
        var media = MediaData(mediaPath.path, observationId);
        int mediaId = await SqfliteMediaManager.instance.create(media);
        // compressFileWithCompressionFactor(
        //     mediaPath.path, mediaPath.path.split('.')[0] + '.zip', 5);
        debugPrint("Media saved ${mediaId}");
      }
      if (showMessage) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content:
                  Text(AppLocalizations.of(context)!.observationFormSuccess)),
        );
      }
    } catch (e) {
      debugPrint("observationCreator create exception $e");
    }
  }

  Future<void> compressFileWithCompressionFactor(String sourceFilePath,
      String destinationFilePath, int compressionFactor) async {
    final encoder = ZipFileEncoder();

    // Set the compression level
    // encoder.compressionLevel = compressionFactor;

    try {
      encoder.create(destinationFilePath);
      encoder.addFile(File(sourceFilePath), '');
      encoder.close();
      print('File compressed successfully.');
    } catch (e) {
      print('Failed to compress file: $e');
    }
  }

  void update(BuildContext context, ObservationData observationData) async {
    try {
      fillExpectedFieldFromAnswers(context, observationData);

      //get the existing media db state, find a difference
      await SqfliteMediaManager.instance
          .getMediaDataList(observationData.id as int)
          .then((mediaList) {
        List<MediaData> shouldBeDeletedFromDbList = List.from(mediaList
            .where((value) => !observationData.mediaList.contains(value)));

        for (MediaData item in shouldBeDeletedFromDbList) {
          SqfliteMediaManager.instance.delete(item.media_id as int);
        }
      });

      //get the existing answer data db state, find a difference
      await SqfliteAnswerManager.instance
          .get(observationData.id as int)
          .then((answerList) {
        List<AnswerData> shouldBeDeletedFromDbList = List.from(answerList
            .where((value) => !observationData.answerList.contains(value)));

        for (AnswerData item in shouldBeDeletedFromDbList) {
          SqfliteAnswerManager.instance.delete(item.answerId as int);
        }
      });

      // Update an observation to DB observation table
      await SqfliteObservationManager.instance.update(observationData);

      // Add answers to DB answers table
      for (var answer in observationData.answerList) {
        answer.observationId = observationData.id as int;
        if (answer.answerId == null) {
          int answerId = await SqfliteAnswerManager.instance.create(answer);
        } else {
          await SqfliteAnswerManager.instance.update(answer);
        }
        debugPrint("Answer saved ${answer.toString()}");
      }

      // Add media to DB media table
      for (var mediaItem in observationData.mediaList) {
        mediaItem.observation_id = observationData.id as int;
        if (mediaItem.media_id == null) {
          int mediaId = await SqfliteMediaManager.instance.create(mediaItem);
        } else {
          await SqfliteMediaManager.instance.update(mediaItem);
        }
        await SqfliteMediaManager.instance.update(mediaItem);

        debugPrint("Media saved ${mediaItem.toString()}");
      }

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            content:
                Text(AppLocalizations.of(context)!.observationFormSuccess)),
      );
    } catch (e) {
      debugPrint("observationCreator create exception $e");
    }
  }
}
