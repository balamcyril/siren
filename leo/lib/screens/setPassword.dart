import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:siren_flutter/data/job_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/screens/complet.dart';
import 'package:siren_flutter/screens/pinCode.dart';
import 'package:siren_flutter/user_session_manager.dart';
import '../utils/colors.dart';
import '../utils/fonts.dart';
import 'geo_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:pin_code_fields/pin_code_fields.dart';

class SetPasswordPage extends StatefulWidget {
  String password = "";
  SetPasswordPage({Key? key}) : super(key: key);
  @override
  _PinCodePageState createState() {
    return _PinCodePageState();
  }
}

class _PinCodePageState extends State<SetPasswordPage> {
  final passwordController = TextEditingController();
  final repeatPasswordController = TextEditingController();
  bool send = false;
  bool desactive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("${AppLocalizations.of(context)!.signUp}")),
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  '${AppLocalizations.of(context)!.plsCreatePwd}',
                  style: h3Semibold,
                ),
                Text(
                  '${AppLocalizations.of(context)!.pwdLimit}',
                  style: TextStyle(color: ColorsHandler.primary1),
                  textAlign: TextAlign.center,
                ),
              ]),
              const SizedBox(
                height: 30,
              ),
              TextField(
                readOnly: desactive,
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText:
                        AppLocalizations.of(context)!.loginPasswordLabelText,
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: AppLocalizations.of(context)!.createPwd,
                    fillColor: Colors.white70),
                onChanged: (val) {
                  checkAll();
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                readOnly: desactive,
                controller: repeatPasswordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText: AppLocalizations.of(context)!.repeatPwd,
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: AppLocalizations.of(context)!.confirmPwd,
                    fillColor: Colors.white70),
                onChanged: (val) {
                  checkAll();
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: OutlinedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(send
                          ? ColorsHandler.primary1
                          : Color.fromARGB(255, 136, 166, 191)),
                      minimumSize:
                          MaterialStateProperty.all(Size(double.infinity, 50)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0))),
                    ),
                    onPressed: () async {
                      if (send) if (8 > passwordController.text.length)
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            duration: Duration(seconds: 5),
                            content: Row(children: [
                              SizedBox(width: 5),
                              Text(
                                '${AppLocalizations.of(context)!.pwdLimit}',
                                style: TextStyle(color: Colors.white),
                              ),
                            ])));
                      else {
                        if (passwordController.text ==
                            repeatPasswordController.text) {
                          userSessionManager.userInfo.password =
                              passwordController.text;

                          try {
                            setState(() {
                              send = false;
                              desactive = true;
                            });
                            List<Job> jobs =
                                await SirenRestServices().CreateAccount();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PinCodePage()),
                            );
                          } catch (e) {
                            setState(() {
                              send = true;
                              desactive = false;
                            });
                            debugPrint("Error creation account${e} ");
                            // Navigator.pop(context);
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Error creation account"),
                            ));
                          }
                        } else
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              duration: Duration(seconds: 5),
                              content: Row(children: [
                                SizedBox(width: 5),
                                Text(
                                  'It is not the same password',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ])));
                      }

                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => CompletPage()),
                      // );
                    },
                    child: Text('${AppLocalizations.of(context)!.nextButton}',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  void checkAll() {
    if ((8 <= passwordController.text.length) &&
        (8 <= repeatPasswordController.text.length) &&
        (passwordController.text == repeatPasswordController.text))
      send = true;
    else
      send = false;

    setState(() {});
  }
}
