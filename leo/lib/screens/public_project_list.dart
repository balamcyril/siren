import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:siren_flutter/data/project_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/beaches.dart';
import 'package:siren_flutter/tests_from_main/rest_services_test_from_main.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'account_info.dart';
import 'observations_type.dart';
import 'package:get/get.dart';

class PublicProjectListPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  PublicProjectListPage({Key? key}) {}
  @override
  _PublicProjectListPageState createState() => _PublicProjectListPageState();
}

class _PublicProjectListPageState extends State<PublicProjectListPage> {
  _PublicProjectListPageState() {}

  List<bool> projecSelect = [];
  List<ProjectData> allProject = [];
  List<ProjectData> foundProject = [];

  bool send = false;
  bool sending = false;
  final searchFieldText = TextEditingController();
  final t = 5.obs;

  late Timer _timer;
  int _reste = 5;
  final spinkit = SpinKitThreeBounce(
    color: ColorsHandler.primary1,
    size: 30.0,
  );

  String getUserData() {
    if (userSessionManager.userDataAvailable) {
      return "${AppLocalizations.of(context)!.user}: " +
          userSessionManager.userData.username +
          "\n${AppLocalizations.of(context)!.project}: " +
          userSessionManager.userData.currentProjectData.name;
    }
    return "No user data available, login required";
  }

  @override
  void initState() {
    super.initState();

    allProject = userSessionManager.userData.projects;
    foundProject = userSessionManager.userData.projects;
    userSessionManager.userData.projects.forEach((item) {
      if (item.name == "Siren international")
        projecSelect.add(true);
      else
        projecSelect.add(false);
    });
  }

  String getSiteData() {
    String sites = AppLocalizations.of(context)!.site_beaches + "\n\n";
    userSessionManager.userData.currentProjectData.siteDataList.forEach((item) {
      sites += item.toString() + "\n";
    });
    return sites.substring(0, sites.length - 1);
  }

  String getLocation() {
    String projectLocation =
        AppLocalizations.of(context)!.project_location + "\n\n";
    return projectLocation + " ${userSessionManager.userData.currentLocation}";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(
            "${AppLocalizations.of(context)!.projectList}",
            style: const TextStyle(color: ColorsHandler.primary1),
          ),
        ),
        backgroundColor: Colors.white,
        body: Container(
          margin: const EdgeInsets.all(0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(),
              getSearchField(),
              const SizedBox(
                height: 8,
              ),
              Expanded(
                child: ListView(shrinkWrap: true, children: getProjectData()),
              )
            ],
          ),
        ));
  }

  Future<Widget> showAboutDialog() {
    return aboutDialog(context);
  }

  Widget getLine() {
    return const Divider(
      height: 20,
      thickness: 1,
      indent: 20,
      endIndent: 0,
      color: Colors.black,
    );
  }

  Widget getSearchField() {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      alignment: Alignment.center,
      child: Container(
        width: window.physicalSize.width / window.devicePixelRatio - 20,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        child: TextField(
          style: bodyMedium.merge(TextStyle(color: ColorsHandler.tertiary)),
          decoration: InputDecoration(
            // fillColor: Colors.white70,
            suffixIcon: Icon(Icons.search),
            hintText: 'Search project',
            contentPadding: EdgeInsets.all(15),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          controller: searchFieldText,
          onChanged: (value) {
            debugPrint(value);
            searchObservations(value);
          },
        ),
      ),
    );
  }

  List<Widget> getProjectData() {
    List<Widget> projects = [];

    foundProject.asMap().forEach((index, item) {
      // projects.add(getField(context, "${item.name} (id: ${item.id})"));

      projects.add(ListTile(
        contentPadding: EdgeInsets.all(0),
        horizontalTitleGap: 0,
        leading: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 44,
              minHeight: 44,
              maxWidth: 64,
              maxHeight: 64,
            ), // fixed width and height
            child: verIfCheck(item.id)
                ? IconButton(
                    icon: Image.asset('assets/images/checkall.png'),
                    onPressed: () {
                      check(item.id);
                    },
                  )
                : IconButton(
                    icon: Image.asset('assets/images/emptycheck.png'),
                    onPressed: () {
                      check(item.id);
                    })),
        title: Text(item.name),
        textColor: verIfCheck(item.id) ? Colors.blue : ColorsHandler.primary1,
        onTap: () {
          check(item.id);
        },
      ));
      projects.add(Padding(
          padding: const EdgeInsets.fromLTRB(47, 0, 17, 0),
          child: new Divider(
            height: 2.0,
            indent: 1.0,
            color: ColorsHandler.tertiary,
          )));
    });

    projects.add(Container(
      margin:
          const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30, top: 20),
      child: SingleChildScrollView(
        child: sending
            ? spinkit
            : OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all((send && !sending)
                      ? ColorsHandler.primary1
                      : Color.fromARGB(255, 136, 166, 191)),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 50)),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0))),
                ),
                onPressed: () async {
                  List<int> projectId = [];

                  if (send && !sending) {
                    setState(() {
                      sending = true;
                    });
                    for (var i = 0; i < projecSelect.length; i++) {
                      if (projecSelect[i]) {
                        projectId
                            .add(userSessionManager.userData.projects[i].id);
                        debugPrint(
                            '- ${userSessionManager.userData.projects[i]}');
                      }
                    }
                    try {
                      bool check =
                          await SirenRestServices().AssignProject(projectId);
                      setState(() {
                        sending = false;
                      });
                      if (check) {
                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //   content: Text("Project added."),
                        // ));

                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     content: Row(children: [
                        //   ImageIcon(
                        //     AssetImage('assets/images/pstart.png'),
                        //     color: Colors.white,
                        //   ),
                        //   SizedBox(width: 5),
                        //   Text('Project added.'),
                        // ])));

                        timers();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return dialogGoToHome();
                            });

                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     content: Row(children: [
                        //   ImageIcon(
                        //     AssetImage('assets/images/pstart.png'),
                        //     color: Colors.white,
                        //   ),
                        //   SizedBox(width: 5),
                        //   Text('Configuration saved.'),
                        // ])));

                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => MainScreen()),
                        // );
                      }
                    } catch (e) {
                      debugPrint("Error set default project${e} ");
                      // Navigator.pop(context);
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Error setting project"),
                      ));
                    }
                  }
                },
                child: Text('Validate', style: TextStyle(color: Colors.white)),
              ),
      ),
    ));
    return projects;
  }

  setProject() {}

  void timers() {
    t.value = 5;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (t.value <= 0) {
        timer.cancel();
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MainScreen()),
        );
      } else {
        t.value--;
      }
    });
  }

  void searchObservations(String value) {
    List<ProjectData> results = [];

    if (value.isEmpty)
      results = allProject;
    else
      results = allProject
          .where(
              (user) => user.name.toLowerCase().contains(value.toLowerCase()))
          .toList();

    setState(() {
      foundProject = results;
    });
  }

  void check(int id) {
    int index = -1;
    bool fact = false;

    index = allProject.indexWhere((user) => (user.id == id));

    projecSelect.asMap().forEach((ind, it) {
      if (ind != index) fact = it || fact;
    });

    setState(() {
      send = !projecSelect[index] || fact;
      projecSelect[index] = !projecSelect[index];
    });
  }

  bool verIfCheck(int id) {
    int index = -1;
    index = allProject.indexWhere((user) => (user.id == id));
    return projecSelect[index];
  }

  Widget dialogGoToHome() {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: viewContent(),
    );
  }

  Container viewContent() {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      // margin: EdgeInsets.only(top: 105),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15),
                child: Column(children: [
                  Text(
                    'Project added',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  // SizedBox(
                  //   height: 5,
                  // ),
                  // Padding(
                  //     padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  //     child: Text(
                  //       'Please login with the account you created to access your Projects on SIREN',
                  //       // '${reste}',
                  //       style: TextStyle(
                  //           fontSize: 14,
                  //           color: Color.fromARGB(255, 62, 62, 62)),
                  //       textAlign: TextAlign.center,
                  //     )),
                  Container(
                    height: 1,
                    color: Colors.grey[500],
                    margin: EdgeInsets.only(top: 15, bottom: 0),
                  ),
                  Row(
                    // mainAxisSize: MainAxisSize.max,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          _timer.cancel();
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainScreen()),
                          );
                        },
                        child: Obx(() => Text(
                              'You are being redirected in ${t.value}',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                            )),
                      )),
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}
