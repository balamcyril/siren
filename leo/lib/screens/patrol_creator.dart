import 'package:flutter/material.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/user_session_manager.dart';

class localDBPAtrolManager {
  void create(BuildContext context, PatrolData patrolData,
      {bool showMessage = true}) async {
    try {
      int patrolId =
          await SqfliteObservationManager.instance.createPatrol(patrolData);
      debugPrint("patrol saved - id - $patrolId");

      userSessionManager.currentPatrolId = patrolId;
      userSessionManager.dataPatrol.id = patrolId;

      userSessionManager.dbUserManager.setPatrolId(patrolId);
      print(patrolData);
      print(userSessionManager.currentPatrolId);
    } catch (e) {
      debugPrint("patrolCreator create exception $e");
    }
  }

  void endPatrol() {
    try {
      userSessionManager.dbUserManager.setPatrolId(-1);
    } catch (e) {
      debugPrint("patrol ended exception $e");
    }
  }

  void update(BuildContext context, PatrolData patrolData) async {
    try {
      int patrolId =
          await SqfliteObservationManager.instance.updatePatrol(patrolData);
      debugPrint("patrol end - id - $patrolId");
    } catch (e) {
      debugPrint("patrolCreator end exception $e");
    }
  }

  void erase(BuildContext context) async {
    try {
      await SqfliteObservationManager.instance.erasePatrol();
      debugPrint("patrol end - erase");
    } catch (e) {
      debugPrint("patrolCreator end exception $e");
    }
  }
}
