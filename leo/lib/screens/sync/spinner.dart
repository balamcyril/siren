import 'package:flutter/material.dart';

class SpinnerBox extends StatefulWidget {
  const SpinnerBox({Key? key}) : super(key: key);

  @override
  _SpinnerBoxState createState() => _SpinnerBoxState();
}

class _SpinnerBoxState extends State<SpinnerBox> {
  _SpinnerBoxState();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Content(context),
    );
  }

  Container Content(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 45),
      margin: EdgeInsets.only(top: 45),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            "assets/images/rolling.gif",
            height: 100,
            width: 100,
          )
        ],
      ),
    );
  }
}
