import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/observation_sync.dart';
import 'package:siren_flutter/data/sqlite_last_sync_manager.dart';
import 'package:siren_flutter/data/last_sync_data.dart';
import 'package:siren_flutter/screens/home.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';

class SyncDialogBox extends StatefulWidget {
  final String title;
  final int projetId;
  final bool firstSync;
  final bool fullSync;
  Function setdate;

  SyncDialogBox(
      {Key? key,
      this.title = "",
      this.projetId = 0,
      required this.setdate,
      this.firstSync = false,
      this.fullSync = false})
      : super(key: key);

  @override
  _SyncDialogBoxState createState() =>
      _SyncDialogBoxState(this.title, this.firstSync, this.fullSync);
}

class _SyncDialogBoxState extends State<SyncDialogBox> {
  String title;
  bool syncro;
  bool fullSync;
  String visible = "sync";
  int observationCount = 0;
  int view = 0;

  _SyncDialogBoxState(this.title, this.syncro, this.fullSync);

  void _changedView(int value) {
    setState(() {
      this.view = value;
    });
  }

  void _changedVisibility(String value) {
    setState(() {
      visible = value;
    });
  }

  void _chargedObservation(int value) {
    setState(() {
      this.observationCount = value;
    });
  }

  void _setLastSyncDate(int projectId) async {
    int time = new DateTime.now().toUtc().millisecondsSinceEpoch;
    userSessionManager.dbUserManager.setLastSync(time.toString());
    this.widget.setdate();
  }

  void _synchro(int projectId) async {
    int observationsFromServerCount = await ObservationSync()
        .syncValidated(context, projectId, fullSync)
        .then((count) {
      _setLastSyncDate(projectId);
      _chargedObservation(count);
      _changedVisibility('success');
      _changedView(0);
      return count;
    }).catchError((error) {
      _changedVisibility('error');
      _changedView(0);
    });
    debugPrint(observationsFromServerCount.toString());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: viewContent(context, view),
    );
  }

  Container viewContent(BuildContext context, int v) {
    if (v == 0) {
      return Container(
        padding: EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 45),
        margin: EdgeInsets.only(top: 45),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Align(
              alignment: Alignment(1.11, 0),
              child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  iconSize: 24,
                  icon: Icon(
                    Icons.close,
                    color: Color.fromARGB(255, 61, 61, 61),
                  )),
            ),
            SizedBox(
              height: 30,
            ),
            Visibility(
              visible: this.visible == "sync" ? true : false,
              child: Column(children: [
                Text(
                  widget.title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  syncro
                      ? AppLocalizations.of(context)!.syncFirstSyncInfo
                      : AppLocalizations.of(context)!.syncWait,
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 1,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 243, 229, 1),
                      border:
                          Border.all(color: Color.fromRGBO(255, 243, 229, 1)),
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: Icon(
                            Icons.warning_amber_outlined,
                            size: 24.0,
                            color: Color.fromRGBO(210, 119, 10, 1),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              syncro
                                  ? AppLocalizations.of(context)!
                                      .syncFirstSyncDetail
                                  : AppLocalizations.of(context)!.syncInfo,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Color.fromRGBO(210, 119, 10, 1),
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        )
                      ]),
                ),
                Container(
                  height: 1,
                  width: 200,
                  color: Colors.grey[500],
                  margin: EdgeInsets.only(top: 30, bottom: 20),
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton.icon(
                    onPressed: () {
                      _changedView(1);
                      _synchro(widget.projetId);
                    },
                    icon: Icon(
                      Icons.sync,
                      size: 24.0,
                      color: ColorsHandler.primary2,
                    ),
                    label: Text(AppLocalizations.of(context)!.synchronize,
                        style: TextStyle(color: ColorsHandler.primary2)),
                  ),
                ),
              ]),
            ),
            Visibility(
              visible: this.visible == "success" ? true : false,
              child: Column(children: [
                Text(
                  widget.title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(210, 236, 223, 1),
                      border:
                          Border.all(color: Color.fromRGBO(210, 236, 223, 1)),
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 0),
                          child: Icon(
                            // Icons.check_circle_rounded_amber_outlined,
                            Icons.check_circle_outline_rounded,
                            size: 24.0,
                            color: Color.fromRGBO(5, 100, 51, 1),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              AppLocalizations.of(context)!.syncSuccessful,
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color.fromRGBO(5, 100, 51, 1),
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        )
                      ]),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "${observationCount} " +
                      AppLocalizations.of(context)!.syncInfoSuccess,
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: 1,
                  width: 200,
                  color: Colors.grey[500],
                  margin: EdgeInsets.only(top: 30, bottom: 20),
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(AppLocalizations.of(context)!.close),
                  ),
                ),
              ]),
            ),
            Visibility(
              visible: this.visible == "error" ? true : false,
              child: Column(children: [
                Text(
                  AppLocalizations.of(context)!.syncError,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 225, 223, 1),
                      border:
                          Border.all(color: Color.fromRGBO(255, 225, 223, 1)),
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 0),
                          child: Icon(
                            Icons.info_outline_rounded,
                            size: 24.0,
                            color: Color.fromRGBO(192, 25, 16, 1),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 10, top: 3),
                            child: Text(
                              AppLocalizations.of(context)!.syncInfoError,
                              style: TextStyle(
                                  height: 1,
                                  fontSize: 18,
                                  color: Color.fromRGBO(192, 25, 16, 1),
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        )
                      ]),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  AppLocalizations.of(context)!.syncInfoAgain,
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: 1,
                  width: 200,
                  color: Colors.grey[500],
                  margin: EdgeInsets.only(top: 30, bottom: 20),
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton.icon(
                    onPressed: () {
                      _changedView(1);
                      _synchro(widget.projetId);
                    },
                    icon: Icon(
                      Icons.sync,
                      size: 24.0,
                    ),
                    label: Text(AppLocalizations.of(context)!.synchronize),
                  ),
                ),
              ]),
            ),
          ],
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 45),
        margin: EdgeInsets.only(top: 45),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              "assets/images/rolling.gif",
              height: 100,
              width: 100,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              AppLocalizations.of(context)!.syncInProcess,
              style: TextStyle(
                  fontSize: 24, color: Color.fromRGBO(177, 234, 241, 1)),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
    }
  }
}
