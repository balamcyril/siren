import 'package:flutter/material.dart';
import 'package:siren_flutter/screens/complet.dart';
import 'package:siren_flutter/screens/mailphone_account.dart';
import 'package:siren_flutter/screens/public_project_list.dart';
import 'package:siren_flutter/screens/successSignUp.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../utils/colors.dart';
import 'account_flow.dart';

class LoginPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  LoginPage(MainScreenStatefulWidget parent, {Key? key}) : super(key: key) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _LoginPageState createState() => _LoginPageState(mainScreenStatefulWidget);
}

class _LoginPageState extends State<LoginPage> {
  _LoginPageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  late MainScreenStatefulWidget mainScreenStatefulWidget;
  final loginController = TextEditingController();
  final passwordController = TextEditingController();
  String errorMessage = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(AppLocalizations.of(context)!.siren)),
        body: Container(
          margin: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text(AppLocalizations.of(context)!.userLogin,
                  style: TextStyle(fontSize: 30, color: Colors.blue)),
              Text(errorMessage,
                  style: const TextStyle(fontSize: 20, color: Colors.red)),
              const SizedBox(
                height: 20,
              ),
              TextField(
                controller: loginController,
                autofocus: true,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText:
                        AppLocalizations.of(context)!.loginEmailLabelText,
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText: AppLocalizations.of(context)!.loginEmailHintText,
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    filled: true,
                    labelText:
                        AppLocalizations.of(context)!.loginPasswordLabelText,
                    hintStyle: TextStyle(color: Colors.grey[800]),
                    hintText:
                        AppLocalizations.of(context)!.loginPasswordHintText,
                    fillColor: Colors.white70),
              ),
              const SizedBox(
                height: 20,
              ),
              OutlinedButton(
                onPressed: () {
                  String email = loginController.text;
                  String password = passwordController.text;
                  errorMessage = "";
                  userSessionManager.login(email, password).then((connected) {
                    if (connected) {
                      MainScreenState state =
                          mainScreenStatefulWidget.getState();
                      state.loginState();
                      Navigator.pop(context);
                    } else {
                      setState(() {
                        errorMessage =
                            AppLocalizations.of(context)!.invalidCredentials;
                      });
                    }
                  });
                },
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0))),
                ),
                child: Text(
                  AppLocalizations.of(context)!.loginButton,
                  style: const TextStyle(fontSize: 20, color: Colors.blueGrey),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              //get bottom
              // Expanded(
              //   child: SingleChildScrollView(
              //     child: Column(
              //       children: <Widget>[
              //         Text(
              //           "${AppLocalizations.of(context)!.noAccount}",
              //           style: const TextStyle(
              //               fontSize: 15, color: Colors.blueGrey),
              //         ),
              //         const SizedBox(
              //           height: 10,
              //         ),
              //         OutlinedButton(
              //           style: ButtonStyle(
              //             minimumSize: MaterialStateProperty.all(
              //                 Size(double.infinity, 50)),
              //             shape: MaterialStateProperty.all(
              //                 RoundedRectangleBorder(
              //                     borderRadius: BorderRadius.circular(10.0))),
              //           ),
              //           onPressed: () {
              //             // Navigator.push(
              //             //   context,
              //             //   MaterialPageRoute(
              //             //       builder: (context) => CompletPage()),
              //             // );

              //             // Navigator.push(
              //             //   context,
              //             //   MaterialPageRoute(
              //             //       builder: (context) => PublicProjectListPage()),
              //             // );
              //             Navigator.push(
              //               context,
              //               MaterialPageRoute(
              //                   builder: (context) =>
              //                       MailPhonePage(mainScreenStatefulWidget)),
              //             );
              //           },
              //           child: Text('${AppLocalizations.of(context)!.signUp}'),
              //         ),
              //         // const SizedBox(
              //         //   height: 10,
              //         // ),
              //         // OutlinedButton.icon(
              //         //   style: ButtonStyle(
              //         //     minimumSize: MaterialStateProperty.all(
              //         //         Size(double.infinity, 50)),
              //         //     shape: MaterialStateProperty.all(
              //         //         RoundedRectangleBorder(
              //         //             borderRadius: BorderRadius.circular(10.0))),
              //         //   ),
              //         //   onPressed: () {
              //         //     print('btn click');
              //         //     signInWithFacebook();
              //         //     // final accessToken = FacebookAuth.instance.accessToken;
              //         //   },
              //         //   icon: Icon(Icons.facebook),
              //         //   label: Text('Sign up with Facebook'),
              //         // ),
              //       ],
              //     ),
              //   ),
              // ),
              //keep top one

              // Text(
              //   "Don't have an account ?",
              //   style: const TextStyle(fontSize: 15, color: Colors.blueGrey),
              // ),
              // const SizedBox(
              //   height: 10,
              // ),
              // OutlinedButton(
              //   style: ButtonStyle(
              //     minimumSize:
              //         MaterialStateProperty.all(Size(double.infinity, 50)),
              //     shape: MaterialStateProperty.all(RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(10.0))),
              //   ),
              //   onPressed: () {
              //     print('btn click');
              //     // FacebookLog();
              //     // final accessToken = FacebookAuth.instance.accessToken;
              //   },
              //   child: Text('Sign up'),
              // ),
              // const SizedBox(
              //   height: 10,
              // ),
              // OutlinedButton.icon(
              //   style: ButtonStyle(
              //     minimumSize:
              //         MaterialStateProperty.all(Size(double.infinity, 50)),
              //     shape: MaterialStateProperty.all(RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(10.0))),
              //   ),
              //   onPressed: () {
              //     print('btn click');
              //     FacebookLog();
              //     // final accessToken = FacebookAuth.instance.accessToken;
              //   },
              //   icon: Icon(Icons.facebook),
              //   label: Text('Sign up with Facebook'),
              // ),
            ],
          ),
        ));
  }

  FacebookLog() async {
    final LoginResult result = await FacebookAuth.instance.login();
    if (result.status == LoginStatus.success)
      print('success connect to facebok');
  }

  Future<UserCredential> signInWithFacebook() async {
    // Trigger the sign-in flow

    final LoginResult loginResult = await FacebookAuth.i.login();

    // Create a credential from the access token
    final OAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(loginResult.accessToken!.token);

    final userInfo = FacebookAuth.instance.getUserData();
    print(userInfo);

    // Once signed in, return the UserCredential
    return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
  }
}
