import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/beaches.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/utils/alert_dialog.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'account_info.dart';
import 'observations_type.dart';
import 'package:custom_navigation_bar/custom_navigation_bar.dart';

import '../data/observation_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import '../utils/platform.dart';
import 'dart:io';
import 'dart:ui';
import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/utils/fonts.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';

class ObservationsSelectAllPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  ObservationsSelectAllPage(MainScreenStatefulWidget parent, {Key? key}) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _ObservationsSelectAllPageState createState() =>
      _ObservationsSelectAllPageState(mainScreenStatefulWidget);
}

class _ObservationsSelectAllPageState extends State<ObservationsSelectAllPage> {
  List<ObservationData> observations = [];
  List<ObservationData> obsSelected = [];
  bool checkAll = true;
  List<bool> checkedObs = [];
  String imageDirectoryPath = "";
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  late MainScreenState state;

  _ObservationsSelectAllPageState(parent) {
    mainScreenStatefulWidget = parent;
    state = mainScreenStatefulWidget.getState();
    initObservations();
  }

  void initObservations() async {
    observations = await loadObservations();

    copyObservationList();
    imageDirectoryPath = await getMobileStoragePath();
    setState(() {});
  }

  Future<List<ObservationData>> loadObservations() async {
    if (userSessionManager.ObservationSection == 4)
      return await SqfliteObservationManager.instance.getDeletedObservations();
    else
      return await SqfliteObservationManager.instance.getDraftObservations();
  }

  void copyObservationList() {
    if (0 < getObservationCount()) {
      for (var i = 0; i < getObservationCount(); i++) {
        obsSelected.add(observations[i]);
        checkedObs.add(true);
      }
    }
  }

  void simpleCopyObservationList() {
    for (var i = 0; i < getObservationCount(); i++) {
      obsSelected.add(observations[i]);
    }
  }

  void Clean() async {
    observations = await loadObservations();
    if (0 < observations.length) {
      simpleCopyObservationList();
      checkAll = true;
      checkedAll();
    } else {
      setState(() {
        state.setState(() {});
      });

      Navigator.of(context).pop();
    }
  }

  void ifChecked(obs, index) {
    if (obsSelected.contains(obs)) {
      obsSelected.remove(obs);
      checkedObs[index] = false;
    } else {
      obsSelected.add(obs);
      checkedObs[index] = true;
    }

    if ((observations.length > 0) &&
        (obsSelected.length == observations.length)) {
      checkAll = true;
    } else {
      checkAll = false;
    }

    setState(() {});
  }

  void checkedAll() {
    // copyObservationList();
    obsSelected = [];
    if (checkAll) {
      for (var i = 0; i < getObservationCount(); i++) {
        checkedObs[i] = false;
      }
      checkAll = false;
    } else {
      for (var i = 0; i < getObservationCount(); i++) {
        obsSelected.add(observations[i]);
        checkedObs[i] = true;
      }
      checkAll = true;
    }

    setState(() {});
  }

  Widget getTitle() {
    return Container(
      alignment: Alignment.topLeft,
      padding: const EdgeInsets.fromLTRB(12, 5, 20, 5),
      child: Text(AppLocalizations.of(context)!.observationsNavIcon, style: h2),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.grey.shade50,
          leadingWidth: 100,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: Text(
                AppLocalizations.of(context)!.cancel,
                style: GoogleFonts.poppins(
                    color: Colors.blue,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500),
              ),
              tooltip: AppLocalizations.of(context)!.backButton,
              onPressed: () {
                prevPage();
              },
            );
          }),
          actions: <Widget>[
            ((obsSelected.length != 0) &&
                    (obsSelected.length != observations.length))
                ? IconButton(
                    icon: Image.asset('assets/images/partial.png'),
                    onPressed: () => checkedAll(),
                  )
                : IconButton(
                    icon: (obsSelected.length == 0)
                        ? Image.asset('assets/images/emptycheck.png')
                        : Image.asset('assets/images/checkall.png'),
                    tooltip: AppLocalizations.of(context)!.notificationsTooltip,
                    onPressed: () {
                      checkedAll();
                    })
          ]),
      body: Container(
          color: ColorsHandler.tertiaryOp7,
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Column(children: [
            // getChipsRow(),
            Expanded(
                child: ListView(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              children: [
                for (var i = 0; i < getObservationCount(); i++)
                  getObservationRow(context, i),
                SizedBox(
                  height: 100,
                )
              ],
            ))
          ])),
      bottomNavigationBar: CustomNavigationBar(
        onTap: (int val) {
          if (0 < obsSelected.length) {
            if (val == 0) {
              // deleteObservation();

              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return dialogDelete();
                  });
            }
            if (val == 2) {
              if (state.selectOptionTrash)
                launch_restaure();
              else
                launch_upload();

              // uploadObservation();

              Navigator.of(context).pop();
            }
          }
        },
        scaleFactor: 0.01,
        strokeColor: Colors.white,
        isFloating: true,
        borderRadius: Radius.circular(10),
        elevation: 10,
        backgroundColor: Colors.white,
        items: [
          CustomNavigationBarItem(
            icon: Icon(Icons.delete,
                color: (obsSelected.length > 0) ? Colors.red : Colors.grey),
          ),
          CustomNavigationBarItem(
            title: SizedBox(
                height: 32,
                child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        ),
                    child: Text(
                      '${obsSelected.length} ${AppLocalizations.of(context)!.itemsSelected}',
                      // style: TextStyle(color: Colors.red),
                    ))),
            icon: SizedBox(child: null),
          ),
          CustomNavigationBarItem(
            icon: state.selectOptionTrash
                ? Image.asset(
                    ((obsSelected.length > 0)
                        ? 'assets/images/restore.png'
                        : 'assets/images/restoreno.png'),
                    scale: 0.2)
                : Icon(Icons.upload,
                    color: (obsSelected.length > 0)
                        ? ColorsHandler.primary1
                        : Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget getObservationRow(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: GestureDetector(
          // When the child is tapped, show a snackbar.
          onTap: () {
            ifChecked(observations[index], index);
          },
          child: Card(
            color: checkedObs[index]
                ? Color.fromARGB(255, 225, 222, 222)
                : Colors.white,
            elevation: 0.3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: getImageContainer(index),
                  ),
                  Expanded(child: getInfoContainer(index)),
                  getActionsContainer(context, index),
                ],
              ),
            ),
          ),
        ));
  }

  Widget getActionsContainer(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Icon(
              Icons.check,
              color: checkedObs[index] ? ColorsHandler.primary1 : Colors.white,
            ),
          ],
        ));
  }

  String getObservationType(int index) {
    return observations[index].getObservationTypeString(context);
  }

  String getObservationIdString(index) {
    // bool showStatus = getSelectedIndex() == 0 ? true : false;
    String textToShow = "";

    if (observations[index].nest_id.isNotEmpty) {
      textToShow = AppLocalizations.of(context)!.nestID +
          " ${observations[index].nest_id}";
    } else {
      textToShow = observations[index].server_id == -1
          ? "${AppLocalizations.of(context)!.obs} ${observations[index].id}"
          : "${AppLocalizations.of(context)!.obs} ${observations[index].server_id}";
    }
    // if (showStatus && observations[index].status == Status.draft) {
    //   textToShow = textToShow + " (DRAFT)";
    // }
    return textToShow;
  }

  double textRowInfoSize = 12.0;

  Widget getInfoContainer(int index) {
    String observationType = getObservationType(index);
    ObservationTypeColor observationTypeColor =
        ColorsHandler.getObservationTypeColor(observations[index].type);

    // setCoordinatesString(index);
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(1, 9.41, 2, 0),
            child: Text(
              getObservationIdString(index),
              style:
                  bodySemibold.merge(TextStyle(color: ColorsHandler.primary1)),
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 1, top: 4),
              decoration: BoxDecoration(
                color: observationTypeColor.observationFillColor,
                borderRadius: BorderRadius.all(Radius.circular(24.0)),
              ),
              padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
              child: Text(
                observationType,
                style: caption.merge(TextStyle(
                    color: observationTypeColor.observationTextColor)),
              )),
          const Text(
            "",
          ),
          Padding(
              padding: const EdgeInsets.only(left: 1, bottom: 2),
              child: Text(
                "${AppLocalizations.of(context)!.beachName}: ${observations[index].site}",
                style: smallTextMedium
                    .merge(TextStyle(color: ColorsHandler.primary1)),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 1, bottom: 6.41),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(
                  Icons.schedule,
                  size: textRowInfoSize,
                  color: ColorsHandler.primary1,
                ),
                Text(
                  " " + getDateString(index) + " • " + getTimeString(index),
                  style:
                      caption.merge(TextStyle(color: ColorsHandler.tertiary)),
                ),
              ],
            ),
          )
        ]);
  }

  String getDateString(index) {
    try {
      return DateFormat.yMMMd().format(
          DateTime.fromMillisecondsSinceEpoch(observations[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  String getTimeString(index) {
    try {
      return DateFormat('kk:mm').format(
          DateTime.fromMillisecondsSinceEpoch(observations[index].timestamp));
    } catch (e) {
      return AppLocalizations.of(context)!.unknown;
    }
  }

  Widget getImageContainer(index) {
    late Widget imageContainer;
    String fullPath = "";
    const double imageHeight = 110.0;
    const double imageWidth = 100.0;

    if (observations[index].mediaList.isNotEmpty) {
      fullPath = imageDirectoryPath + observations[index].mediaList[0].path;
      debugPrint("image index select all page $index path $fullPath");
    }
    try {
      // if path contains https, this is an observation that was imported without the images

      imageContainer = Container(
          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
          child: (observations[index].hasMediaToShow())
              ? ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.file(
                    File(fullPath),
                    height: imageHeight,
                    width: imageWidth,
                    fit: BoxFit.cover,
                    errorBuilder: (BuildContext context, Object exception,
                        StackTrace? stackTrace) {
                      debugPrint("$exception");
                      debugPrint("$stackTrace");
                      return getDefaultImage(100, 110);
                    },
                  ))
              : getDefaultImage(100, 110));
    } catch (e) {
      debugPrint(
          "image index erro select all page $index obs ${observations[index].mediaList[0]}");
      imageContainer = getDefaultImage(100, 110);
    }
    return imageContainer;
  }

  int getObservationCount() {
    return observations.length;
  }

  void prevPage() {
    Navigator.of(context).pop();
  }

  void global_pourcentage(d) {
    String inString = d.toStringAsFixed(2);
    userSessionManager.upload_progress = double.parse(inString);
    setState(() {});
  }

  void launch_upload() async {
    userSessionManager.onUpload = true;
    userSessionManager.allUploadSize = 0;
    userSessionManager.uploadedSize = 0;
    userSessionManager.observationOnUploaded = [...obsSelected];
    MainScreenState state = mainScreenStatefulWidget.getState();
    state.setState(() {});

    state.run_upload(-1, -1);
  }

  void launch_restaure() async {
    int count = obsSelected.length;
    userSessionManager.observationlist = [];
    for (var i = 0; i < count; i++) {
      restoreObservation(i);

      observations.remove(obsSelected[i]);

      debugPrint(
          "OBservation ${obsSelected[i].id} restored id: ${obsSelected[i].id}");
    }

    userSessionManager.observationlist =
        await SqfliteObservationManager.instance.getDeletedObservations();

    if (userSessionManager.observationlist.length == 0) {
      state.hideSelect();
    } else {
      state.setState(() {});
    }
  }

  void restoreObservation(int index) async {
    obsSelected[index].status =
        obsSelected[index].server_id == -1 ? Status.draft : Status.uploaded;
    await SqfliteObservationManager.instance.update(obsSelected[index]);
  }

  void end_upload() {
    userSessionManager.onUpload = false;
    userSessionManager.observationOnUploaded = [];

    setState(() {});
  }

  void deleteObservation() async {
    Navigator.of(context).pop();
    int count = obsSelected.length;
    userSessionManager.observationlist = [];
    for (var i = 0; i < count; i++) {
      if (userSessionManager.ObservationSection == 4) {
        await SqfliteObservationManager.instance
            .hardDelete(obsSelected[i].id as int);
      } else {
        await SqfliteObservationManager.instance
            .delete(obsSelected[i].id as int);
      }

      debugPrint(
          "OBservation ${obsSelected[i].id} deleted id: ${obsSelected[i].id}");

      observations.remove(obsSelected[i]);
    }

    if (userSessionManager.ObservationSection == 4) {
      userSessionManager.observationlist =
          await SqfliteObservationManager.instance.getDeletedObservations();
    } else {
      userSessionManager.observationlist =
          await SqfliteObservationManager.instance.getDraftObservations();
    }

    if (userSessionManager.observationlist.length == 0) {
      state.hideSelect();
    } else {
      state.setState(() {});
    }

    Clean();
  }

  Widget dialogDelete() {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: viewContent(),
    );
  }

  Container viewContent() {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      // margin: EdgeInsets.only(top: 105),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Positioned(
            child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: new Container(
                    height: 28,
                    width: 28,
                    child: Icon(
                      Icons.close,
                      color: Color.fromARGB(255, 153, 153, 153),
                      size: 28,
                    ))),
            right: 15.0,
            top: 10.0,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 23),
                child: Column(children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20.0),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(8)),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 0),
                            child: Icon(
                              Icons.info_outline_rounded,
                              size: 48.0,
                              color: Color.fromRGBO(500, 32, 9, 9),
                            ),
                          ),
                        ]),
                  ),
                  Text(
                    state.selectOptionTrash
                        ? '${AppLocalizations.of(context)!.deleteItems} ?'
                        : '${AppLocalizations.of(context)!.deleteInProgress}...',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Text(
                        state.selectOptionTrash
                            ? AppLocalizations.of(context)!.deleteItemsText
                            : AppLocalizations.of(context)!
                                .temporarlyDeleteText,
                        style: TextStyle(
                            fontSize: 14,
                            color: Color.fromARGB(255, 62, 62, 62)),
                        textAlign: TextAlign.center,
                      )),
                  Container(
                    height: 1,
                    color: Colors.grey[500],
                    margin: EdgeInsets.only(top: 30, bottom: 0),
                  ),
                  Row(
                    // mainAxisSize: MainAxisSize.max,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.cancel,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                      )),
                      Container(
                        height: 50,
                        width: 1,
                        color: Colors.grey[500],
                      ),
                      Expanded(
                          child: TextButton(
                        onPressed: () {
                          deleteObservation();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.delete,
                          style: TextStyle(
                            fontSize: 18,
                            color: Color.fromRGBO(500, 32, 9, 9),
                          ),
                        ),
                      )),
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}
