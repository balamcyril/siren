import 'package:flutter/material.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/utils/colors.dart';

class ObservationPatrolPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  ObservationPatrolPage(MainScreenStatefulWidget parent, {Key? key})
      : super(key: key) {
    mainScreenStatefulWidget = parent;
  }
  @override
  _ObservationPatrolPageState createState() {
    return _ObservationPatrolPageState(mainScreenStatefulWidget);
  }
}

class _ObservationPatrolPageState extends State<ObservationPatrolPage>
    with SingleTickerProviderStateMixin {
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  _ObservationPatrolPageState(parent) {
    mainScreenStatefulWidget = parent;
  }

  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 2, vsync: this);
    // return Scaffold(
    //     backgroundColor: ColorsHandler.tertiaryOp7,
    //     body:

    return Column(
      children: [
        Container(
          child: TabBar(
            controller: _tabController,
            indicatorColor: Colors.blue[800],
            labelColor: Colors.blue[800],
            unselectedLabelColor: Colors.grey,
            labelStyle: TextStyle(fontSize: 24),
            tabs: const <Widget>[
              Tab(
                text: 'Observations',
              ),
              Tab(
                text: 'Patrols',
              ),
            ],
          ),
        ),
        Container(
            width: double.maxFinite,
            height: 300,
            child: TabBarView(controller: _tabController, children: [
              Text('observation'),
              Text('patrol'),
            ]))
      ],
    );
  }
}
