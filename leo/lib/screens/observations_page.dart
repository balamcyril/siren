import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/data/sqlite_media_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:siren_flutter/screens/details.dart';
import 'package:siren_flutter/screens/patrol_tab.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/common_images.dart';
import 'package:siren_flutter/utils/fonts.dart';
import '../data/observation_data.dart';
import '../utils/platform.dart';
import 'details_patrol.dart';
import 'observation_flow.dart';
import 'observations_tab.dart';

class ObservationsPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;
  bool refresh = true;

  ObservationsPage(MainScreenStatefulWidget parent, {Key? key})
      : super(key: key) {
    mainScreenStatefulWidget = parent;
  }

  @override
  _ObservationsPageState createState() =>
      _ObservationsPageState(mainScreenStatefulWidget);
}

class _ObservationsPageState extends State<ObservationsPage>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  final searchFieldText = TextEditingController();
  List<ObservationData> observations = [];
  List buttonLabel = [];
  int lastIndexToUpload = 0;
  int showImg = 0;

  final Map<String, String> _currentAddressList = {};
  String imageDirectoryPath = "";
  final List<bool> isChipSelected = [false, true, false, false, false];
  final List<bool> obsDraftTrashExiste = [false, false, false, false, false];

  late MainScreenStatefulWidget mainScreenStatefulWidget;

  _ObservationsPageState(parent) {
    debugPrint("Launch observations page");
    mainScreenStatefulWidget = parent;
    initObservations();
  }

  @override
  void didUpdateWidget(ObservationsPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  void initPatrol() async {
    userSessionManager.patrollist = await SqfliteObservationManager.instance
        .getPatrolByProject(userSessionManager.userData.currentProjectData.id);
  }

  void initObservations() async {
    MainScreenState state = mainScreenStatefulWidget.getState();
    userSessionManager.showObservationOnMap = false;
    imageDirectoryPath = await getMobileStoragePath();

    initPatrol();

    for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
      if (isChipSelected[indexBtn]) {
        userSessionManager.observationlist = await loadObservations(indexBtn);

        if ((indexBtn == 1) || (indexBtn == 4)) {
          if (0 >= userSessionManager.observationlist.length) {
            state.hideSelect();
            obsExistOnDB(indexBtn, false);
          } else {
            state.showHideSelect(indexBtn);
            obsExistOnDB(indexBtn, true);
          }
        }
        debugPrint(
            "Observation page init observations tab ${indexBtn} count ${userSessionManager.observationlist.length}");
        setState(() {});
      }
    }
  }

  void obsExistOnDB(index, value) {
    obsDraftTrashExiste[index] = value;
  }

  int getSelectedIndex() {
    for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
      if (isChipSelected[indexBtn]) {
        return indexBtn;
      }
    }
    throw Exception("Invalid chip selection index in observation list");
  }

  Future<List<ObservationData>> loadObservations(index) async {
    debugPrint(
        "Load observations from page tab folder ${index} (0-All, 1-Draft...)");
    switch (index) {
      case 0:
        return await SqfliteObservationManager.instance.getAllObservations();
      case 1:
        return await SqfliteObservationManager.instance.getDraftObservations();
      case 2:
        return await SqfliteObservationManager.instance
            .getUploadedObservations();
      case 3:
        return await SqfliteObservationManager.instance
            .getValidatedObservations();
      case 4:
        return await SqfliteObservationManager.instance
            .getDeletedObservations();
    }
    throw Exception("Invalid index while loading observations $index");
  }

  @override
  Widget build(BuildContext context) {
    int index = 0;
    TabController _tabController =
        TabController(length: 2, vsync: this, initialIndex: index);
    String titleone = AppLocalizations.of(context)!.observationsNavIcon;
    if (buttonLabel.isEmpty) {
      buttonLabel.add(AppLocalizations.of(context)!.draft);
      buttonLabel.add(AppLocalizations.of(context)!.uploaded);
      buttonLabel.add(AppLocalizations.of(context)!.validated);
      buttonLabel.add(AppLocalizations.of(context)!.trash);
    }

    // if (userSessionManager.patrol)
    return Column(
      children: [
        Container(
          child: TabBar(
            onTap: (index) {
              print(index);
              _tabController.index = index;
            },
            controller: _tabController,
            indicatorColor: Colors.blue[800],
            labelColor: Colors.blue[800],
            unselectedLabelColor: Colors.grey,
            labelStyle: h2,
            tabs: const <Widget>[
              Tab(
                text: 'Observations',
              ),
              Tab(
                text: 'Patrols',
              ),
            ],
          ),
        ),
        Expanded(
            child: TabBarView(
          controller: _tabController,
          children: [
            ObservationsTabPage(mainScreenStatefulWidget),
            PatrolTabPage(mainScreenStatefulWidget)
          ],
        ))
      ],
    );
  }

  String getButtonLabel(index) {
    return buttonLabel[index];
  }

  void chipOnSelect(chipIdx, value) {
    searchFieldText.clear();
    setState(() {
      for (int indexBtn = 0; indexBtn < isChipSelected.length; indexBtn++) {
        if (indexBtn != chipIdx) {
          isChipSelected[indexBtn] = !value;
        }
      }
      initObservations();
    });
  }

  Widget getChipsRow() {
    MainScreenState state = mainScreenStatefulWidget.getState();
    TextStyle chipSelectedStyle = TextStyle(
        fontSize: smallTextBold.fontSize,
        fontFamily: smallTextBold.fontFamily,
        fontWeight: smallTextBold.fontWeight,
        letterSpacing: smallTextBold.letterSpacing,
        color: Colors.white);
    TextStyle chipUnselectedStyle = TextStyle(
      fontSize: smallTextBold.fontSize,
      fontFamily: smallTextBold.fontFamily,
      fontWeight: smallTextBold.fontWeight,
      letterSpacing: smallTextBold.letterSpacing,
      color: ColorsHandler.primary1,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        ChoiceChip(
          label: Text(AppLocalizations.of(context)!.all),
          selected: isChipSelected[0],
          onSelected: (value) {
            debugPrint("OBservation ${value}");
            isChipSelected[0] = value;
            state.showHideSelect(0);
            chipOnSelect(0, value);
          },
          labelStyle:
              isChipSelected[0] ? chipSelectedStyle : chipUnselectedStyle,
          backgroundColor: ColorsHandler.tertiaryOp15,
          selectedColor: ColorsHandler.primary1,
        ),
        ChoiceChip(
            selected: isChipSelected[1],
            label: Text(
              getButtonLabel(0),
              style:
                  isChipSelected[1] ? chipSelectedStyle : chipUnselectedStyle,
            ),
            onSelected: (value) {
              debugPrint("OBservation ${value}");
              isChipSelected[1] = value;
              // state.onDrafSection();
              // state.onSection(2);

              if (obsDraftTrashExiste[1])
                state.showHideSelect(1);
              else
                state.hideSelect();
              chipOnSelect(1, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1),
        ChoiceChip(
            selected: isChipSelected[2],
            onSelected: (value) {
              isChipSelected[2] = value;
              // state.hideSelect();
              // state.onSection(3);
              state.showHideSelect(2);
              chipOnSelect(2, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(1),
              style:
                  isChipSelected[2] ? chipSelectedStyle : chipUnselectedStyle,
            )),
        ChoiceChip(
            selected: isChipSelected[3],
            onSelected: (value) {
              isChipSelected[3] = value;
              // state.hideSelect();
              // state.onSection(4);
              state.showHideSelect(3);
              chipOnSelect(3, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(2),
              style:
                  isChipSelected[3] ? chipSelectedStyle : chipUnselectedStyle,
            )),
        ChoiceChip(
            selected: isChipSelected[4],
            onSelected: (value) {
              isChipSelected[4] = value;
              // state.onTrashSection();
              // state.onSection(5);
              if (obsDraftTrashExiste[4])
                state.showHideSelect(4);
              else
                state.hideSelect();
              chipOnSelect(4, value);
            },
            backgroundColor: ColorsHandler.tertiaryOp15,
            selectedColor: ColorsHandler.primary1,
            label: Text(
              getButtonLabel(3),
              style:
                  isChipSelected[4] ? chipSelectedStyle : chipUnselectedStyle,
            )),
      ],
    );
  }

  void showActionBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: getBottomSheetChildren(context, index));
        });
  }

  void showActionPatrolBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: getBottomPatrolSheetChildren(context, index));
        });
  }

  List<Widget> getBottomPatrolSheetChildren(BuildContext context, int index) {
    dialogContext = context;
    return <Widget>[
      ListTile(
        leading: const Icon(Icons.upload),
        title: Text(AppLocalizations.of(context)!.upload),
        onTap: () {
          Navigator.pop(dialogContext);
          if (!userSessionManager.onUpload) {
            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            //   content:
            //       Text(AppLocalizations.of(context)!.uploadingPatrolServer),
            // ));
            showActionDialog(
                AppLocalizations.of(context)!.uploadingPatrolServer);
            launch_upload_patrol(index);
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Wait upload in process"),
            ));
          }
        },
      ),
      ListTile(
        leading: const Icon(Icons.delete),
        title: Text(AppLocalizations.of(context)!.delete),
        onTap: () {
          Navigator.pop(dialogContext);
          showActionDialog(AppLocalizations.of(context)!.deletingPatrol);
          deletePatrol(index);
        },
      ),
    ];
  }

  List<Widget> getBottomSheetChildren(BuildContext context, int index) {
    dialogContext = context;
    switch (userSessionManager.observationlist[index].status) {
      case Status.deleted:
        return <Widget>[
          ListTile(
            leading: Image.asset(
              'assets/images/restoreno.png',
              width: 22,
              height: 22,
            ),
            title: Text(AppLocalizations.of(context)!.restore),
            onTap: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                    "${AppLocalizations.of(context)!.restoringObservation}"),
              ));
              restoreObservation(index);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.delete_forever),
            title: Text(AppLocalizations.of(context)!.permanentlyDelete),
            onTap: () {
              showActionDialog(AppLocalizations.of(context)!.permanentlyDelete);
              hardDeleteObservation(index);
            },
          ),
        ];
      case Status.uploaded:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.draft:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.edit),
            title: Text(AppLocalizations.of(context)!.edit),
            onTap: () {
              Navigator.pop(context);
              editObservation(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.upload),
            title: Text(AppLocalizations.of(context)!.upload),
            onTap: () {
              Navigator.pop(dialogContext);
              if (!userSessionManager.onUpload) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(AppLocalizations.of(context)!.uploadingServer),
                ));
                uploadObservation(index);
              } else
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Wait upload in process"),
                ));
            },
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              deleteObservation(index);
            },
          ),
        ];
      case Status.validated:
        return <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text(AppLocalizations.of(context)!.goToMap),
            onTap: () {
              goToMap(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete),
            title: Text(AppLocalizations.of(context)!.delete),
            onTap: () {
              showActionDialog(
                  AppLocalizations.of(context)!.deletingObservation);
              hardDeleteObservation(index);
            },
          ),
        ];
      case Status.uploading:
        return <Widget>[];
    }
  }

  void editObservation(int index) async {
    try {
      ObservationData observationData =
          userSessionManager.observationlist[index];

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ObservationFlowWidget(
                    observationType: observationData.type,
                    observationId: observationData.id as int,
                  )));

      debugPrint(
          "OBservation ${userSessionManager.observationlist[index].id} edit server id: ${userSessionManager.observationlist[index].server_id}");
      // toast message
      setState(() {
        initObservations();
      });
    } catch (e) {
      debugPrint("editObservation exception $e");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.editFailure),
      ));
    }
  }

  void launch_upload(obsToUpload) async {
    userSessionManager.onUpload = true;

    userSessionManager.observationOnUploaded = [];
    userSessionManager.observationOnUploaded.add(obsToUpload);

    MainScreenState state = mainScreenStatefulWidget.getState();
    state.setState(() {});

    debugPrint(
        "Observation page - upload ${userSessionManager.observationOnUploaded} ");
    state.run_upload(userSessionManager.observationOnUploaded.length - 1, -1);
  }

  void launch_upload_patrol(int index) async {
    int patrolId = -1;
    userSessionManager.observationOnUploaded = [];
    userSessionManager.observationOnUploaded = await SqfliteObservationManager
        .instance
        .getAllObservationsForPatrolProject(
            userSessionManager.patrollist[index].project_id,
            userSessionManager.patrollist[index].id as int,
            userSessionManager.patrollist[index].server_id,
            -1);

    MainScreenState state = mainScreenStatefulWidget.getState();
    state.setState(() {});

    debugPrint("Patrol to upload ${userSessionManager.patrollist[index].id} ");
    try {
      patrolId = await SirenRestServices().submitPatrol(context, index);
    } catch (e) {
      debugPrint("Patrol send error ${e} ");
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Upload patrol error"),
        // content: Text(AppLocalizations.of(context)!.uploadFailure),
      ));
    }

    if (patrolId > 0) {
      userSessionManager.patrollist[index].server_id = patrolId;
      userSessionManager.patrollist[index].status = PatrolStatus.uploaded;
      await SqfliteObservationManager.instance
          .updatePatrol(userSessionManager.patrollist[index]);
      state.run_upload(-1, patrolId);
      Navigator.pop(context);
    }
  }

  void uploadObservation(int index) async {
    try {
      // load QA before uploading
      userSessionManager.observationlist[index].answerList =
          await SqfliteAnswerManager.instance
              .get(userSessionManager.observationlist[index].id as int);

      setState(() {
        initObservations();
      });

      launch_upload(userSessionManager.observationlist[index]);

      debugPrint(
          "OBservation ${userSessionManager.observationlist[index].id} uploaded server id: ${userSessionManager.observationlist[index].server_id}");
    } catch (e) {
      debugPrint("error observation page upload observation exception $e");
      userSessionManager.observationlist[index].status = Status.draft;
      await SqfliteObservationManager.instance
          .update(userSessionManager.observationlist[index]);

      setState(() {
        initObservations();
      });

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.uploadFailure),
      ));
    }
  }

  void deleteObservation(int index) async {
    await SqfliteObservationManager.instance
        .delete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} deleted id: ${userSessionManager.observationlist[index].server_id}");
    setState(() {
      Navigator.pop(dialogContext);
      Navigator.pop(context);
      initObservations();
    });
  }

  void deletePatrol(int index) async {
    await SqfliteObservationManager.instance.hardDeletePatrol(
        userSessionManager.patrollist[index].id as int,
        userSessionManager.NumberObsActualPatrol);
    setState(() {
      // Navigator.pop(dialogContext);
      Navigator.pop(context);
      initPatrol();
    });
  }

  void restoreObservation(int index) async {
    userSessionManager.observationlist[index].status =
        userSessionManager.observationlist[index].server_id == -1
            ? Status.draft
            : Status.uploaded;
    await SqfliteObservationManager.instance
        .update(userSessionManager.observationlist[index]);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} restored id: ${userSessionManager.observationlist[index].server_id}");
    // toast message
    setState(() {
      initObservations();
    });
  }

  void hardDeleteObservation(int index) async {
    // TODO delete answers
    userSessionManager.observationlist[index].mediaList.forEach((media) async {
      await SqfliteMediaManager.instance.delete(media.media_id as int);
    });
    await SqfliteObservationManager.instance
        .hardDelete(userSessionManager.observationlist[index].id as int);

    debugPrint(
        "OBservation ${userSessionManager.observationlist[index].id} permanently deleted");
    // toast message
    setState(() {
      // Navigator.pop(dialogContext);
      Navigator.pop(context);
      initObservations();
    });
  }

  void searchObservations(searchText) async {
    bool isTrash = getSelectedIndex() == 3 ? true : false;
    userSessionManager.observationlist = await SqfliteObservationManager
        .instance
        .searchObservations(searchText, trash: isTrash);
    setState(() {});
  }

  void goToMap(int index) {
    // go to All sites
    userSessionManager.lastSelectedSite = "";
    userSessionManager
        .showObservation(userSessionManager.observationlist[index]);
    Navigator.pop(context);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => MainScreenStatefulWidget()));
  }

  late BuildContext dialogContext;
  dynamic showActionDialog(String text) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: Container(
            margin: const EdgeInsets.all(20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new CircularProgressIndicator(),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: new Text(text))),
              ],
            ),
          ),
        );
      },
    );
  }
}

class SubPage extends StatefulWidget {
  final int tab;
  Widget getElement;
  SubPage({
    Key? key,
    this.tab = 0,
    required this.getElement,
  }) : super(key: key);
  @override
  _SubPageState createState() => _SubPageState(this.tab, this.getElement);
}

class _SubPageState extends State<SubPage> with AutomaticKeepAliveClientMixin {
  int tab = 0;
  Widget getElement;
  _SubPageState(this.tab, this.getElement);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return getElement;
    // return ListViewWidget(
    //   tab: this.tab,
    //   getElement: this.getElement,
    // );
  }

  @override
  bool get wantKeepAlive => true;
}

class ListViewWidget extends StatefulWidget {
  final int tab;
  Widget getElement;
  ListViewWidget({Key? key, this.tab = 0, required this.getElement})
      : super(key: key);
  @override
  _ListViewWidgetState createState() => _ListViewWidgetState(tab, getElement);
}

class _ListViewWidgetState extends State<ListViewWidget> {
  int tab;
  Widget getElement;
  _ListViewWidgetState(this.tab, this.getElement);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildItem,
      itemCount: 50,
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return ListTile(
      title: Text(index.toString()),
    );
  }
}
