import 'package:flutter/material.dart';
import 'package:siren_flutter/screens/complet.dart';
import 'package:siren_flutter/screens/login.dart';
import 'package:siren_flutter/screens/mailphone_account.dart';
import 'package:siren_flutter/screens/public_project_list.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/main.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../utils/colors.dart';
import 'account_flow.dart';

class SuccessSignUpPage extends StatefulWidget {
  late MainScreenStatefulWidget mainScreenStatefulWidget;

  SuccessSignUpPage({Key? key}) : super(key: key) {}
  @override
  _SuccessSignUpPageState createState() => _SuccessSignUpPageState();
}

class _SuccessSignUpPageState extends State<SuccessSignUpPage> {
  _SuccessSignUpPageState() {}

  final loginController = TextEditingController();
  final passwordController = TextEditingController();
  String errorMessage = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/backgroundConfimation.png"),
          fit: BoxFit.cover,
        ),
      ),
      // margin: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/accountOk.png",
                    height: 150,
                  ),
                  Text(
                    "${AppLocalizations.of(context)!.sighSuccess}",
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "${AppLocalizations.of(context)!.joinProject}",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ]),
          )),
          Container(
            margin: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30),
            child: SingleChildScrollView(
              child: OutlinedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(ColorsHandler.primary1),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 50)),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0))),
                ),
                onPressed: () async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PublicProjectListPage()),
                  );
                },
                child: Text('${AppLocalizations.of(context)!.nextButton}',
                    style: TextStyle(color: Colors.white)),
              ),
            ),
          )
        ],
      ),
    ));
  }
}
