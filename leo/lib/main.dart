import 'dart:async';
import 'dart:ui';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:camera/camera.dart';
import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:siren_flutter/screens/observation_flow.dart';
import 'package:siren_flutter/screens/patrol_creator.dart';
import 'package:siren_flutter/screens/show_observation_uploading_page.dart';
import 'package:siren_flutter/screens/sync/sync.dart';
import 'package:siren_flutter/tests_from_main/flow_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/observation_sync_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/observation_type_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/rest_services_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/sqlite_answer_manager_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/sqlite_media_manager_test_from_main.dart';
import 'package:siren_flutter/tests_from_main/sqlite_observation_test_from_main.dart';
import 'package:siren_flutter/user_session_manager.dart';
import 'package:siren_flutter/utils/colors.dart';
import 'package:siren_flutter/utils/patrols_utils.dart';
import 'package:siren_flutter/utils/platform.dart';
import 'package:siren_flutter/widgets/forms/circular_progress_bar.dart';
import 'data/observation_data.dart';
import 'data/observation_factory.dart';
import 'screens/home.dart';
import 'screens/login.dart';
import 'screens/observations_page.dart';
import 'screens/observation_select_all_page.dart';
import 'screens/user_profile.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';
import 'data/users_roles_data.dart';
import 'package:siren_flutter/widgets/forms/patrolicon.dart';

bool test_from_main = false;
bool test_flow_from_main = false;
bool test_rest_services_from_main = false;
bool test_observation_sync = false;
bool test_observation_sync_user = false;
bool test_observation_type = false;

const String make_test = String.fromEnvironment('TEST', defaultValue: 'none');
late PatrolData patrolData;

void main() {
  switch (make_test) {
    case 'test_all':
      // test_from_main = true;
      // test_flow_from_main = true;
      // test_rest_services_from_main = true;
      test_observation_sync = true;
      // test_observation_sync_user = true;
      // test_observation_type = true;
      break;
    case 'test_sync':
      debugPrint("--- sync test... ----");
      test_observation_sync_user = true;
      break;
    case 'test_observation_type':
      test_observation_type = true;
      break;

    default:
      debugPrint("--- No tests to launch... ----");
  }

  if (test_from_main) {
    sqlite_observation_test();
    sqlite_answer_manager_test();
    sqlite_media_manager_test();
    sqlite_view_DBs();
  } else {
    WidgetsFlutterBinding.ensureInitialized();
    init_logging();
    init_package_info();
    runApp(const MainScreen());
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint(
        "Width of the screen - ${window.physicalSize.width}, pixel ratio: ${window.devicePixelRatio}");
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateTitle: (context) {
          var t = AppLocalizations.of(context)!;
          return t.appTitle;
        },
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: ThemeData(primarySwatch: Colors.blue, useMaterial3: false),
        home: MainScreenStatefulWidget());
  }
}

class MainScreenStatefulWidget extends StatefulWidget {
  MainScreenStatefulWidget({Key? key}) : super(key: key);

  late MainScreenState mainScreenState;

  @override
  State<StatefulWidget> createState() {
    mainScreenState = MainScreenState();
    return mainScreenState;
  }

  MainScreenState getState() {
    return mainScreenState;
  }
}

class MainScreenState extends State<MainScreenStatefulWidget>
    with WidgetsBindingObserver {
  // final showKey = GlobalKey<ShowObservationUploadingPageState>();
  GlobalKey<ShowObservationUploadingPageState> showKey = GlobalKey();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  checkPatrol() async {
    try {
      int idPat = await userSessionManager.dbUserManager.getPatrolId();
      if (idPat > -1) {
        userSessionManager.currentPatrolId = idPat;
        userSessionManager.patrol = true;

        List<PatrolData> pats = await SqfliteObservationManager.instance
            .getPatrolById(userSessionManager.currentPatrolId);

        List<ObservationData> lstObs = await SqfliteObservationManager.instance
            .getAllObservationsForPatrolProject(
                pats[0].project_id, userSessionManager.currentPatrolId, -1, -1);

        userSessionManager.NumberObsActualPatrol = lstObs.length;

        userSessionManager.dataPatrol = pats[0];
      } else {
        userSessionManager.patrol = false;
      }
    } catch (e) {
      debugPrint(e.toString());
      userSessionManager.currentPatrolId = -1;
      userSessionManager.patrol = false;
    }
    setState(() {});
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.inactive) print("app is inactif");
    if (state == AppLifecycleState.paused) print("app is paused");
    if (state == AppLifecycleState.detached) print("app is detache");

    if (state == AppLifecycleState.inactive ||
        state == AppLifecycleState.paused) return;

    final isBack = state == AppLifecycleState.detached;

    if (isBack) {
      print('end end patrol here, ondispose');

      if (userSessionManager.patrol) {
        setState(() {
          cancelByTimer = true;
        });
        patrolCancel();
      }
    }
  }

  MainScreenState() {
    userSessionManager = UserSessionManager();

    userSessionManager.start().then((connected) async {
      checkPatrol();
      loginPageRequired = !connected;
      if (!loginPageRequired) {
        userSessionManager.fetchSites().then((sites) {
          userSessionManager.userData.currentProjectData.siteDataList = sites;
          _getSyncDate();
          debugPrint(sites.toString());
        });
        userSessionManager.fetchObservationType().then((observationsTypes) {
          observationsTypes.forEach((data) {
            // if (userSessionManager.patrol)
            //   userSessionManager.flashSupport = false;
            // else {
            if (data.id == ObservationType.flash) {
              userSessionManager.flashSupport = true;
            }
            // }
            setState(() {});
          });
          userSessionManager.userData.currentProjectData.observationsTypeList =
              observationsTypes;
          debugPrint(observationsTypes.toString());
          makeMenuObservationsType();
        });
      }
      setState(() {
        _selectedIndex = 0;
      });
    });
  }
  late Timer _timer;
  int _selectedIndex = 0;
  late bool loginPageRequired = false;
  late BuildContext savedContext;
  late List<Widget> _widgetOptions;
  String _lastSyncDate = "";
  bool _doSync = false;
  bool _firstSync = false;
  late List<Widget> menuObservation = [];
  late List<Widget> topMenu = [];
  late bool showSelect = false;
  bool selectOptionTrash = false;
  bool selectOptionDraft = false;

  late StateSetter _stf;
  late Dialog menu;
  late Dialog ma;
  late Dialog mb;
  bool cancelByTimer = false;

  final List<bool> showselectlist = [false, true, false, false, true];

  Future<CameraDescription> initCamera() async {
    final cameras = await availableCameras();

    // Get a specific camera from the list of available cameras.
    final camera = cameras.first;
    debugPrint(camera.toString());
    return camera;
  }

  void launchLoginPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage(widget)),
    );
  }

  Widget getWidgetToShow() {
    return _widgetOptions.elementAt(_selectedIndex);
  }

  void loginState() {
    loginPageRequired = false;

    userSessionManager.refreshSites().then((sites) {
      userSessionManager.userData.currentProjectData.siteDataList = sites;
      _getSyncDate();
      debugPrint(sites.toString());
      setState(() {});
    });
    userSessionManager.refreshObservationsType().then((observationsTypes) {
      userSessionManager.userData.currentProjectData.observationsTypeList =
          observationsTypes;

      observationsTypes.forEach((data) {
        if (data.id == ObservationType.flash) {
          userSessionManager.flashSupport = true;
          setState(() {});
        }
      });

      print("observation type list:");
      debugPrint(observationsTypes.toString());
      makeMenuObservationsType();
    });
  }

  //run obs upload

  void run_upload(int indexUploadObs, int patrolId) {
    userSessionManager
        .calculateSizeOfUpload(context, widget, indexUploadObs, patrolId)
        .then((value) => userSessionManager.upload(
            context, widget, indexUploadObs, patrolId));
  }

  void remove_uploadOne(index) {
    userSessionManager.observationOnUploaded
        .remove(userSessionManager.observationOnUploaded[index]);

    userSessionManager.allUploadSize = userSessionManager.allUploadSize -
        userSessionManager.observationOnUploaded[index].size;

    userSessionManager.uploadedSize = userSessionManager.uploadedSize -
        userSessionManager.observationOnUploaded[index].uploadedSize;
    setState(() {});
  }

  void logoutState() {
    menuObservation = [];
    loginPageRequired = true;
    setState(() {
      _selectedIndex = 0;
    });
  }

  void refeshState() {
    setState(() {});
  }

  void hideSelect() {
    showSelect = false;
    setState(() {});
  }

  void showHideSelect(index) {
    showSelect = showselectlist[index];
    userSessionManager.ObservationSection = index;
    if (index == 4)
      selectOptionTrash = true;
    else
      selectOptionTrash = false;

    setState(() {});
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void eraseObservation() async {
    List<ObservationData> observations =
        await SqfliteObservationManager.instance.getAllObservations();

    observations.forEach((observation) async {
      await SqfliteObservationManager.instance
          .hardDelete(observation.id as int);
    });

    List<ObservationData> observationsD =
        await SqfliteObservationManager.instance.getDeletedObservations();

    observationsD.forEach((observation) async {
      await SqfliteObservationManager.instance
          .hardDelete(observation.id as int);
    });
  }

  void _getSyncDate() async {
    String rs = await userSessionManager.dbUserManager.getLastSync();
    setState(() {
      if (rs == "") {
        _doSync = true;
        _lastSyncDate = AppLocalizations.of(context)!.never;
        _firstSync = true;
      } else {
        _firstSync = false;
        int timestamp = int.parse(rs);
        int today = new DateTime.now().toUtc().millisecondsSinceEpoch;
        DateTime lastDate = DateTime.fromMillisecondsSinceEpoch(timestamp);
        _lastSyncDate = DateFormat('dd, yyyy • kk:mm').format(lastDate);
        _lastSyncDate =
            "${AppLocalizations.of(context)!.lastSync}: ${DateFormat.MMM().format(lastDate)} ${_lastSyncDate}";

        //if today time is more than last sync date + 24hour change sync button
        if (today > (timestamp + 86400000))
          _doSync = true;
        else {
          _doSync = false;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (test_flow_from_main) {
      testFlow(context);
    }
    if (test_rest_services_from_main) {
      testRestServices(context);
    }
    if (test_observation_sync) {
      observation_sync_test_02(context);
    }

    if (test_observation_type) {
      observation_type_test(context);
    }

    if (test_observation_sync_user) {
      observation_sync_test_user(context, userSessionManager.userData.isAdmin,
          userSessionManager.userData.roleId);
    }

    savedContext = context;
    _widgetOptions = <Widget>[
      HomePage(),
      ObservationsPage(widget),
      UserProfilePage(widget),
      ShowObservationUploadingPage(widget),
    ];
    mb = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Container(
            margin: const EdgeInsets.only(top: 20.0, left: 30.0),
            padding: const EdgeInsets.only(bottom: 20.0),
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Text(AppLocalizations.of(context)!.addObservationTooltip,
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: ColorsHandler.primary2)),
                  ],
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: Column(
                      children: menuObservation,
                    ))
              ],
            )));

    ma = menu = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Container(
            margin: const EdgeInsets.only(top: 20.0, left: 30.0),
            padding: const EdgeInsets.only(bottom: 20.0),
            width: 300,
            height: 225,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Icon(Icons.add, color: ColorsHandler.primary2),
                    Text(AppLocalizations.of(context)!.textNew,
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: ColorsHandler.primary2)),
                  ],
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: Column(
                      children: topMenu,
                    ))
              ],
            )));
    if (loginPageRequired) {
      Future.microtask(() => launchLoginPage(context));
    }

    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          // _selectedIndex == 1 ? ColorsHandler.tertiaryOp7 : Colors.white,
          elevation: 0,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
                icon: (!userSessionManager.onUpload)
                    ? Icon(
                        Icons.sync,
                        size: 24.0,
                        color: _doSync
                            ? Color.fromARGB(255, 210, 57, 10)
                            : Color.fromARGB(255, 7, 184, 48),
                      )
                    : Icon(
                        Icons.sync,
                        size: 24.0,
                        color: Color.fromARGB(255, 132, 132, 132),
                      ),
                onPressed: () {
                  (!userSessionManager.onUpload)
                      ? showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return SyncDialogBox(
                                title: _lastSyncDate,
                                projetId: userSessionManager
                                    .userData.currentProjectData.id,
                                firstSync: _firstSync,
                                fullSync: makeFullSync(),
                                setdate: () {
                                  this._getSyncDate();
                                  setState(() {});
                                });
                          })
                      : Null;
                });
          }),
          actions: <Widget>[
            Container(
                child: userSessionManager.patrol
                    ? BlinkIcon(endPatrol: patrolCancel, parent: widget)
                    : null),
            Container(
                child: (_selectedIndex == 1)
                    ? (userSessionManager.onUpload)
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ShowObservationUploadingPage(
                                          widget,
                                          key: showKey,
                                        )),
                              );
                              // _onItemTapped(3);
                            },
                            child: CircularProgressBarWidget(widget),
                          )
                        : (showSelect && !userSessionManager.patrol)
                            ? TextButton(
                                child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        AppLocalizations.of(context)!.select,
                                        style: GoogleFonts.poppins(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.w500,
                                            color: ColorsHandler.primary2),
                                      ),
                                    ]),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ObservationsSelectAllPage(widget)),
                                  );
                                },
                              )
                            : null
                    : null),
            IconButton(
                icon: const Icon(Icons.notifications,
                    color: ColorsHandler.primary1),
                tooltip: AppLocalizations.of(context)!.notificationsTooltip,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return showNotificationDialog();
                      });
                }),
          ]),
      body: getWidgetToShow(),
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: _selectedIndex == 0
          ? Stack(
              children: <Widget>[
                Visibility(
                  visible: userSessionManager.flashSupport,
                  child: Padding(
                    padding: (!(userSessionManager.flashSupport &&
                                (userSessionManager.userData.currentProjectData
                                        .observationsTypeList.length ==
                                    1)) &&
                            (menuObservation.length != 0))
                        ? EdgeInsets.only(bottom: 70)
                        : EdgeInsets.only(bottom: 0),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: FloatingActionButton(
                        heroTag: "btnFlash",
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ObservationFlowWidget(
                                      observationType: ObservationType.flash,
                                      observationId: -1,
                                    )),
                          );
                        },
                        tooltip:
                            AppLocalizations.of(context)!.addObservationTooltip,
                        shape: CircleBorder(
                            side: BorderSide(
                                color: ColorsHandler.primary1, width: 2)),
                        child: Image.asset(
                          "assets/images/flash.png",
                        ),
                        elevation: 2.0,
                        backgroundColor: Color.fromARGB(255, 237, 241, 244),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: !(userSessionManager.flashSupport &&
                          (userSessionManager.userData.currentProjectData
                                  .observationsTypeList.length ==
                              1)) &&
                      (menuObservation.length != 0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: FloatingActionButton(
                      heroTag: "btnObs",
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => StatefulBuilder(
                                    builder: (stfcontext, stfSetState) {
                                  _stf = stfSetState;
                                  if (userSessionManager.patrol) menu = mb;

                                  return menu;
                                })).then((value) {
                          if (!userSessionManager.patrol) menu = ma;
                        });
                      },
                      tooltip:
                          AppLocalizations.of(context)!.addObservationTooltip,
                      child: const Icon(Icons.add, color: Colors.white),
                      elevation: 2.0,
                      backgroundColor: ColorsHandler.primary1,
                    ),
                  ),
                ),
              ],
            )
          : null,
      extendBody: true,
      bottomNavigationBar: CustomNavigationBar(
        onTap: (int val) {
          _onItemTapped(val);
        },
        scaleFactor: 0.01,
        strokeColor: Colors.white,
        isFloating: true,
        borderRadius: Radius.circular(10),
        elevation: 10,
        unSelectedColor: ColorsHandler.tertiary,
        selectedColor: ColorsHandler.primary1,
        backgroundColor: Colors.white,
        currentIndex: (_selectedIndex == 3) ? 1 : _selectedIndex,
        items: [
          _selectedIndex == 0
              ? CustomNavigationBarItem(
                  icon: Icon(Icons.home_filled),
                )
              : CustomNavigationBarItem(
                  icon: Icon(Icons.home_outlined),
                ),
          (_selectedIndex == 1) || (_selectedIndex == 3)
              ? CustomNavigationBarItem(
                  icon: Icon(Icons.assignment),
                )
              : CustomNavigationBarItem(
                  icon: Icon(Icons.assignment_outlined),
                ),
          _selectedIndex == 2
              ? CustomNavigationBarItem(
                  icon: Icon(Icons.account_circle),
                )
              : CustomNavigationBarItem(
                  icon: Icon(Icons.account_circle_outlined),
                ),
        ],
      ),
    );
  }

  String progress() {
    double all = userSessionManager.allUploadSize;
    double partial = userSessionManager.uploadedSize;
    double fr = (partial) / all;

    double r = ((userSessionManager.uploadedSize * 100) /
        userSessionManager.allUploadSize);
    print(r);
    if (userSessionManager.uploadedSize != 0) {
      double result = ((userSessionManager.uploadedSize * 100) /
          userSessionManager.allUploadSize);
      print(result);

      return result.toStringAsFixed(1);
    }

    return "0.0";
  }

  bool ShowSelectToUpload() {
    if (!userSessionManager.onUpload && (userSessionManager.canUpload))
      return true;
    else
      return false;
  }

  Widget showNotificationDialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      title: Text(AppLocalizations.of(context)!.notificationsTooltip),
      content: Text(AppLocalizations.of(context)!.noNewNotifications),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.closeButton)),
      ],
    );
  }

  void makeMenuObservationsType() {
    menuObservation = [];
    topMenu = [];
    userSessionManager.userData.currentProjectData.observationsTypeList
        .forEach((observationsType) {
      if ((observationsType.id != ObservationType.flash) &&
          observationsType.isSupported(observationsType.id))
        menuObservation.add(getTextWithIconButton(
            observationsType.getMenuText(context, observationsType.id),
            observationsType.getIcon(observationsType.id),
            observationsType.id));
    });
    topMenu = getTopMenu();

    setState(() {});
  }

  bool makeFullSync() {
    if ((userSessionManager.userData.isAdmin) ||
        (userSessionManager.userData.roleId == UsersRoles.chercheur))
      return true;
    return false;
  }

  List<Widget> getTopMenu() {
    return [
      TextButton(
        child: Row(children: [
          ImageIcon(
            AssetImage('assets/images/obs.png'),
            color: ColorsHandler.primary2,
          ),
          SizedBox(width: 8),
          Text(
            AppLocalizations.of(context)!.observationName,
            style: GoogleFonts.poppins(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
                color: ColorsHandler.primary2),
          ),
        ]),
        onPressed: () {
          _stf(() {
            menu = mb;
          });
        },
      ),
      TextButton(
        child: Row(children: [
          ImageIcon(
            AssetImage('assets/images/plaunch.png'),
            color: ColorsHandler.primary2,
          ),
          SizedBox(width: 8),
          Text(
            AppLocalizations.of(context)!.patrolName,
            style: GoogleFonts.poppins(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
                color: ColorsHandler.primary2),
          ),
        ]),
        onPressed: () {
          _stf(() {
            Navigator.pop(context);
            patrolStarted();
          });
        },
      )
    ];
  }

  void locationCreate() {
    debugPrint("Start and End getGPSPosition");
  }

  void patrolStarted() async {
    _timer = Timer.periodic(Duration(hours: 2), (timer) {
      setState(() {
        if (userSessionManager.dataPatrol.obs_nbr >
            userSessionManager.dataPatrol.old_obs_nbr) {
          userSessionManager.dataPatrol.old_obs_nbr =
              userSessionManager.dataPatrol.obs_nbr;
          setState(() {});
        } else {
          _timer.cancel();
          cancelByTimer = true;
          setState(() {});

          patrolCancel();
        }
      });
    });
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(days: 1),
        content: Row(children: [
          SizedBox(width: 5),
          Text(AppLocalizations.of(context)!.startingPatrol),
        ])));
    // await SqfliteObservationManager.instance.erasePatrol();

    ObservationData obsdata = ObservationFactory().create(-1);
    // patrolData =
    //     new PatrolData(userSessionManager.userData.currentProjectData.id);
    userSessionManager.dataPatrol =
        new PatrolData(userSessionManager.userData.currentProjectData.id);
    userSessionManager.dataPatrol.observer =
        userSessionManager.userData.username;
    userSessionManager.dataPatrol.start_timestamp =
        new DateTime.now().toUtc().millisecondsSinceEpoch;

    userSessionManager.dataPatrol.timestamp = "...";

    userSessionManager.dataPatrol.status = PatrolStatus.actif;
    userSessionManager.NumberObsActualPatrol = 0;

    Position location =
        await userSessionManager.getGPSPosition(locationCreate, obsdata);

    userSessionManager.dataPatrol.start_latitude = location.latitude;
    userSessionManager.dataPatrol.start_longitude = location.longitude;
    userSessionManager.dataPatrol.user_id = userSessionManager.userData.id;

    localDBPAtrolManager().create(context, userSessionManager.dataPatrol);

    userSessionManager.patrol = true;

    // if (userSessionManager.flashSupport) {
    //   userSessionManager.flashSupport = false;
    //   userSessionManager.flashSupportSave = true;
    // }

    setState(() {});

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          content: Row(children: [
        ImageIcon(
          AssetImage('assets/images/pstart.png'),
          color: Colors.white,
        ),
        SizedBox(width: 8),
        Text(AppLocalizations.of(context)!.patrolStart),
      ])));
  }

  void patrolCancel() async {
    int actualPatrolId = await userSessionManager.dbUserManager.getPatrolId();
    if (!cancelByTimer) Navigator.of(context).pop();

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(days: 1),
        content: Row(children: [
          SizedBox(width: 5),
          Text(AppLocalizations.of(context)!.stoppingPatrol),
        ])));

    double distance = 0;
    List<ObservationData> observationList = [];
    // List<PatrolData> patrol = [];
    ObservationData obsdata = ObservationFactory().create(-1);
    // patrol = await SqfliteObservationManager.instance
    //     .getPatrolById(userSessionManager.currentPatrolId);

    // if (patrol.length > 0) {
    userSessionManager.dataPatrol.end_timestamp =
        new DateTime.now().toUtc().millisecondsSinceEpoch;

    userSessionManager.dataPatrol.obs_nbr =
        userSessionManager.NumberObsActualPatrol;

    if (userSessionManager.NumberObsActualPatrol > 1) {
      observationList = await SqfliteObservationManager.instance
          .getAllObservationsForPatrolProject(
              userSessionManager.userData.currentProjectData.id,
              // userSessionManager.currentPatrolId,
              actualPatrolId,
              -1,
              -1);

      for (var i = 0; i < observationList.length - 1; i++) {
        /* for testing only on simulator
        observationList[i].latitude = 51.524;
        observationList[i].longitude = -0.159;
        observationList[i + 1].latitude = 51.526;
        observationList[i + 1].longitude = -0.157;
        */
        distance = distance +
            calculateDistanceFromGPSLocation(
                observationList[i].latitude,
                observationList[i].longitude,
                observationList[i + 1].latitude,
                observationList[i + 1].longitude);
      }
    }

    userSessionManager.dataPatrol.status = PatrolStatus.unactif;
    userSessionManager.dataPatrol.distance = distance;

    Duration d = DateTime.fromMillisecondsSinceEpoch(
            userSessionManager.dataPatrol.end_timestamp)
        .difference(DateTime.fromMillisecondsSinceEpoch(
            userSessionManager.dataPatrol.start_timestamp));

    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        1577836800000 + (d.inSeconds * 1000));

    debugPrint("Start date diff: ${dateTime}, ${d},");
    // userSessionManager.dataPatrol.timestamp = dateTime.toUtc().millisecondsSinceEpoch;
    userSessionManager.dataPatrol.timestamp =
        d.toString().split(':')[0] + "h " + d.toString().split(':')[1] + "m";

    Position location =
        await userSessionManager.getGPSPosition(locationCreate, obsdata);
    userSessionManager.dataPatrol.id = actualPatrolId;
    // await userSessionManager.dbUserManager.getPatrolId();
    userSessionManager.dataPatrol.end_latitude = location.latitude;
    userSessionManager.dataPatrol.end_longitude = location.longitude;
    localDBPAtrolManager().update(context, userSessionManager.dataPatrol);
    userSessionManager.patrollist = await SqfliteObservationManager.instance
        .getPatrolByProject(userSessionManager.userData.currentProjectData.id);
    // }

    userSessionManager.patrol = false;
    cancelByTimer = false;
    userSessionManager.currentPatrolId = -1;
    localDBPAtrolManager().endPatrol();
    // if (userSessionManager.flashSupportSave) {
    //   userSessionManager.flashSupport = true;
    //   userSessionManager.flashSupportSave = false;
    // }
    setState(() {});

    // ScaffoldMessenger.of(context).removeCurrentSnackBar();

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          content: Row(children: [
        ImageIcon(
          AssetImage('assets/images/pstart.png'),
          color: Colors.white,
        ),
        SizedBox(width: 8),
        Text(AppLocalizations.of(context)!.patrolEnd),
      ])));

    // _timer.cancel();
  }

  Widget getTextWithIconButton(
      String title, IconData iconData, int observationType) {
    return TextButton(
      child: Row(children: [
        Icon(
          iconData,
          color: ColorsHandler.primary2,
        ),
        SizedBox(width: 8),
        Text(
          title,
          style: GoogleFonts.poppins(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
              color: ColorsHandler.primary2),
        ),
      ]),
      onPressed: () {
        userSessionManager.showObservationOnMap = false;
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ObservationFlowWidget(
                    observationType: observationType,
                    observationId: -1,
                  )),
        );
      },
    );
  }
}
