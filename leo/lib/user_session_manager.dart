import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:siren_flutter/data/db_user_manager.dart';
import 'package:siren_flutter/data/hive_user_manager.dart';
import 'package:siren_flutter/data/job_data.dart';
import 'package:siren_flutter/data/observation_data.dart';
import 'package:siren_flutter/data/patrol_data.dart';
import 'package:siren_flutter/data/project_data.dart';
import 'package:siren_flutter/data/siren_rest_services.dart';
import 'package:siren_flutter/data/site_data.dart';
import 'package:siren_flutter/data/user_data.dart';
import 'package:siren_flutter/screens/geo_map.dart';
import 'package:siren_flutter/data/sqflite_observation_manager.dart';

import 'data/observation_type_data.dart';
import 'package:siren_flutter/data/sqlite_answer_manager.dart';
import 'package:siren_flutter/main.dart';

late UserSessionManager userSessionManager;

class UserSessionManager {
  late DBUserManager dbUserManager;
  late SirenRestServices restServices;
  late UserData userData;
  late bool dbInitialized = false;
  bool unitTesting = false;
  bool userDataAvailable = false;
  String lastSelectedSite = "";
  late ObservationData observationToShow;
  late List<ObservationData> observationOnUploaded;
  late List<ObservationData> observationlist = [];
  late List<PatrolData> patrollist = [];
  bool showObservationOnMap = false;
  bool flashSupport = false;
  bool flashSupportSave = false;
  bool patrol = false;
  late double upload_progress = 0;
  bool onUpload = false;
  bool canUpload = false;
  late double allUploadSize = 0;
  late double uploadedSize = 0;
  bool refreshPage = true;
  int ObservationSection = 2;
  bool loginPageRequired = true;
  late int currentPatrolId = -1;
  late int NumberObsActualPatrol = 0;
  late PatrolData dataPatrol;
  late UserData userInfo;
  late List<Job> jobList;
  // late List<ProjectData> projecList;

  static final UserSessionManager _instance = UserSessionManager._internal();
  factory UserSessionManager() => _instance;

  UserSessionManager._internal() {
    dbUserManager = HiveUserManager();
    restServices = SirenRestServices();
    userData = UserData();
    userInfo = UserData();
    // projecList = [];
  }

  Future<bool> start() async {
    try {
      if (dbInitialized == false) {
        // await necessary for the init to complete
        var db = await dbUserManager.init(test: unitTesting);
        dbInitialized = true;
      }

      debugPrint("Check local DB user data");
      Map data = await dbUserManager.get() as Map;
      userData.fillFromLoginResponse(data);
      userDataAvailable = true;
      debugPrint("Local DB user data found: ${userData.email}");
    } catch (e) {
      // no user data yet, first time, ask user
      debugPrint(e.toString());
      return false;
    }

    return true;
  }

  Future<bool> login(String email, String password) async {
    try {
      debugPrint("Attempting to login email $email");
      final response = await restServices.login(email, password);
      debugPrint("User $email logged in");
      debugPrint("----- ${response} -----");

      if (userDataAvailable == false) {
        debugPrint("Save response received from server to DB");
        userData.fillFromLoginResponse(response["data"]);
        dbUserManager.set(userData);
        userDataAvailable = true;
      }
      debugPrint('Current project ${userData.currentProjectData.name}');
    } catch (e) {
      debugPrint("connection denied, exception: $e");
      return false;
    }
    return true;
  }

  void logout() {
    debugPrint("User session - logging out");
    userDataAvailable = false;
    dbUserManager.delete();
    reset();
  }

  void eraseObservation() async {
    List<ObservationData> observations =
        await SqfliteObservationManager.instance.getAllObservations();

    await SqfliteObservationManager.instance.erasePatrol();

    observations.forEach((observation) async {
      await SqfliteObservationManager.instance
          .hardDelete(observation.id as int);
    });

    List<ObservationData> observationsD =
        await SqfliteObservationManager.instance.getDeletedObservations();

    observationsD.forEach((observation) async {
      await SqfliteObservationManager.instance
          .hardDelete(observation.id as int);
    });
  }

  Future<List<SiteData>> fetchSites() async {
    try {
      return await dbUserManager.getSites();
    } catch (e) {
      // no user data yet, first time,
      debugPrint(e.toString());
    }
    final response =
        await restServices.getSites(userData.currentProjectData.id);
    userData.fillFromSiteResponse(response);
    dbUserManager.setSites(userData.currentProjectData.siteDataList);
    return userData.currentProjectData.siteDataList;
  }

  Future<List<SiteData>> refreshSites() async {
    final response =
        await restServices.getSites(userData.currentProjectData.id);

    userData.fillFromSiteResponse(response);
    dbUserManager.setSites(userData.currentProjectData.siteDataList);
    return userData.currentProjectData.siteDataList;
  }

  Future<List<ObservationsTypeData>> fetchObservationType() async {
    try {
      return await dbUserManager.getObservationsType();
    } catch (e) {
      // no user data yet, first time,
      debugPrint(e.toString());
    }
    final response = await restServices
        .getObservationSupported(userData.currentProjectData.id);
    userData.fillFromObservationsTypeResponse(response);
    dbUserManager
        .setObservationsType(userData.currentProjectData.observationsTypeList);
    return userData.currentProjectData.observationsTypeList;
  }

  Future<List<ObservationsTypeData>> refreshObservationsType() async {
    final response = await restServices
        .getObservationSupported(userData.currentProjectData.id);

    for (int i = 0; i < response.length; i++) {
      debugPrint("flash id ${ObservationType.flash}");
      if (response[i].id == ObservationType.flash) {
        this.flashSupport = true;
      }
      if (response[i].id == ObservationType.crawl) break;
      if (response[i].id == ObservationType.femaleEncounter) break;
      if (response[i].id == ObservationType.flash) break;
      if (response[i].id == ObservationType.nestExhumation) break;
      if (response[i].id == ObservationType.nesting) break;

      debugPrint(
          'Observation type with id: ${response[i].id} named: ${response[i].nom_en} dont supported by the application');
    }

    userData.fillFromObservationsTypeResponse(response);
    dbUserManager
        .setObservationsType(userData.currentProjectData.observationsTypeList);

    return userData.currentProjectData.observationsTypeList;
  }

  Future<Position> getGPSPosition(
      VoidCallback onLocationFound, ObservationData observationData) async {
    bool serviceEnabled;
    LocationPermission permission;
    late Position currentPosition;

    debugPrint("Start getGPSPosition");

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      Fluttertoast.showToast(msg: 'Please enable Your Location Service');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        Fluttertoast.showToast(msg: 'Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      Fluttertoast.showToast(
          msg:
              'Location permissions are permanently denied, we cannot request permissions.');
    }
    try {
      currentPosition = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);

      debugPrint("getGPSPosition Position found $currentPosition");
      observationData.latitude = currentPosition.latitude;
      observationData.longitude = currentPosition.longitude;
      onLocationFound();
    } catch (e) {
      debugPrint("getGPSPosition $e");
    }
    return currentPosition;
  }

  void showObservation(ObservationData observation) {
    showObservationOnMap = true;
    observationToShow = observation;
  }

  void stopShowingObservation() {
    showObservationOnMap = false;
  }

  void reset() {
    dbInitialized = false;
    unitTesting = false;
    userDataAvailable = false;
    lastSelectedSite = "";
    showObservationOnMap = false;
    flashSupport = false;
    userData.clear();
  }

  Future<bool> calculateSizeOfUpload(BuildContext context,
      MainScreenStatefulWidget parent, int indexUploadObs, int patrolId) async {
    MainScreenState state = parent.getState();
    allUploadSize = 0;
    // try {
    if (indexUploadObs == -1)
      for (var i = 0; i < observationOnUploaded.length; i++) {
        await SirenRestServices()
            .getFullSize(context, observationOnUploaded[i], i, patrolId)
            .then((value) {
          allUploadSize = allUploadSize + observationOnUploaded[i].size;
          state.refeshState();
        }).catchError((e) {
          debugPrint("calculate size exception $e");
        });
      }
    else {
      await SirenRestServices()
          .getFullSize(context, observationOnUploaded[indexUploadObs],
              indexUploadObs, patrolId)
          .then((value) {
        allUploadSize =
            allUploadSize + observationOnUploaded[indexUploadObs].size;
        state.refeshState();
      }).catchError((e) {
        debugPrint("calculate size exception $e");
      });
    }

    return false;
  }

  Future<void> internalrequest(BuildContext context,
      MainScreenStatefulWidget parent, int indexUploadObs, int patrolId) async {
    MainScreenState state = parent.getState();
    bool success = false;
    try {
      observationOnUploaded[indexUploadObs].server_id =
          await SirenRestServices().submitObservationAll(
              context,
              observationOnUploaded[indexUploadObs],
              indexUploadObs,
              patrolId,
              parent);
      success = true;
    } catch (e) {
      debugPrint(" internalrequest error: $e");
      // userSessionManager.observationlist
      //     .remove(observationOnUploaded[indexUploadObs])
      // userSessionManager.observationOnUploaded
      //     .remove(userSessionManager.observationOnUploaded[indexUploadObs]);
    }

    if (success) {
      // debugPrint("--------------------------");
      observationOnUploaded[indexUploadObs].status = Status.uploaded;
      await SqfliteObservationManager.instance
          .update(observationOnUploaded[indexUploadObs]);

      debugPrint(
          "OBservation ${observationOnUploaded[indexUploadObs].id} uploaded server ids: ${observationOnUploaded[indexUploadObs].server_id}");

      debugPrint("index $indexUploadObs");
    } else {
      if (observationOnUploaded.length > 0) {
        userSessionManager.onUpload = false;
      }

      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            'Observation with id :${userSessionManager.observationOnUploaded[indexUploadObs].id} failed to upload'),
      ));
    }

    userSessionManager.observationlist =
        await SqfliteObservationManager.instance.getDraftObservations();
    if (0 >= patrolId) {
      // userSessionManager.observationlist =
      //     await SqfliteObservationManager.instance.getDraftObservations();

      if (userSessionManager.observationlist.length == 0) {
        state.hideSelect();
      } else {
        state.refeshState();
      }
    }
  }

  Future<void> upload(BuildContext context, MainScreenStatefulWidget parent,
      int indexUploadObs, int patrolId) async {
    MainScreenState state = parent.getState();

    uploadedSize = 0;

    if (indexUploadObs == -1)
      for (var i = 0; i < observationOnUploaded.length; i++) {
        try {
          userSessionManager.observationOnUploaded[i].answerList =
              await SqfliteAnswerManager.instance
                  .get(userSessionManager.observationOnUploaded[i].id as int);
          await internalrequest(context, parent, i, patrolId);
        } catch (e) {
          debugPrint("multiple upload Observation exception $e");
          continue;
        }
      }
    else {
      try {
        userSessionManager.observationOnUploaded[indexUploadObs].answerList =
            await SqfliteAnswerManager.instance.get(userSessionManager
                .observationOnUploaded[indexUploadObs].id as int);
        await internalrequest(context, parent, indexUploadObs, patrolId);
      } catch (e) {
        debugPrint("simple upload Observation exception $e");
      }
    }

    end_upload();
    await SqfliteObservationManager.instance.getDraftCount().then((value) {
      if (value <= 0) {
        // state.selectOptionDraft = false;
        state.showselectlist[1] = false;
        state.showSelect = false;
        // state.refeshState();
      }
    }).catchError((e) {
      debugPrint("get draft count exception $e");
    });
    state.refeshState();
  }

  void end_upload() async {
    // userSessionManager.upload_progress = 0.0;
    // userSessionManager.onUpload = false;
    userSessionManager.observationOnUploaded = [];
  }
}
